import React from 'react';
const ReactNative = require('react-native');
const {
  StyleSheet,
  ScrollView
} = ReactNative;
import LunarCalendar from '../../widgets/lunar_calendar';

var ChildView = require('../../views/base/main_child_view');
// var options={['Nhu cầu 1', 'Nhu cầu 2', 'Nhu cầu 3', 'Nhu cầu 4', 'Nhu cầu 5', 'Nhu cầu 6', 'Nhu cầu 7', 'Nhu cầu 8', 'Nhu cầu 31', 'Nhu cầu 32', 'Nhu cầu 33']}

class KeyboardAvoidingViewExample extends ChildView {
  static title = '<KeyboardAvoidingView>';
  static description = 'Base component for views that automatically adjust their height or position to move out of the way of the keyboard.';

  state = {
    behavior: 'padding',
    modalOpen: false,
  };

  onSegmentChange = (segment) => {
    this.setState({ behavior: segment.toLowerCase() });
  };

  renderContainer() {
    // let aaa = moment().year(1995).month(2).date(9).lunar().format('YYYY-MM-DD');
    return (
      <ScrollView scrollEnabled={false}>
        <LunarCalendar
          renderModal={this.renderModal.bind(this)}
          setVisibleModal={this.setVisibleModal.bind(this)}
          onDateSelect={(date) => console.log(date)}
          onMonthSelect={(mon) => console.log(mon)} />
      </ScrollView>

    );
  };
  onDateChange(date) {

  }
}


const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
  textInput: {
    borderRadius: 5,
    borderWidth: 1,
    height: 44,
    paddingHorizontal: 10,
  },
  segment: {
    marginBottom: 10,
  },
  closeButton: {
    position: 'absolute',
    top: 30,
    left: 10,
  }
});

module.exports = KeyboardAvoidingViewExample;