// import React, { Component } from 'react';
// import {
//   Navigator,
//   Platform,
//   BackAndroid
// } from 'react-native';

// import routes from './routes'
// import Orientation from 'react-native-orientation';

// let listener = null;
// const NoBackSwipe ={
//   ...Navigator.SceneConfigs.FloatFromRight,
//     gestures: {
//       pop: {},
//     },
// };
// export default class DemoReact extends Component {
//   render() {
//     return (
//       <Navigator
//         ref={(mRef) => this.navigator = mRef}
//         initialRoute={routes.splash}
//         renderScene={this.renderScene}
//         configureScene={() => NoBackSwipe} />
//     )
//   }
//   renderScene(route, navigator) {
//     const { ViewComponent, props } = route
//     return <ViewComponent
//       {...props}
//       {...{ route, navigator }}
//       onBack={() => navigator.pop()}
//     />
//   }

//   componentDidMount() {
//     Orientation.lockToPortrait();
//     if (Platform.OS == "android" && listener == null) {
//       listener = BackAndroid.addEventListener("hardwareBackPress", () => {
//         if (this.navigator.getCurrentRoutes().length > 1) {
//           this.navigator.pop();
//         }
//         return true;
//       });
//     }
//   }
//   componentWillUnmount() {
//     if (Platform.OS == "android" && listener != null) {
//       BackAndroid.removeEventListener('hardwareBackPress', this.handleBack);
//       listener = null;
//     }
//   }
// }
