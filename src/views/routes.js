import {
  Splash,
  Main,
  Login,
  Task,
  SaleCar,
  AddEditCustomer,
  CustomerInfo,
  CarInfo,
  ContactHistory,
  ServiceHistory,
  CustomerFilter,
  AddNewCar,
  EditCar,
  PointHistory,
  TransferCustomer,
  Notify,
  DuplicateFolow,
  RegisterDevice,
  AddEditEvent,
  EventDetail,
  ForgotPassword,
  StaffKpi,
  ContractInfo,
  CreateContract,
  FindCustomer,
  EditDeliveryPlan,
  Test
} from './views'
import Const from '../global/const'

export default {
  splash: {
    screen: Splash,
    props: {}
  },
  register_device: {
    screen: RegisterDevice,
    props: {}
  },
  main: {
    screen: Main,
    props: {
      ga_page: Const.GA_PAGE_NAMES.home
    }
  },
  login: {
    screen: Login,
    props: {}
  },
  task: {
    screen: Task,
    props: {}
  },
  sale_car: {
    screen: SaleCar,
    props: {}
  },
  add_customer: {
    screen: AddEditCustomer,
    props: {
      headerText: 'Thông tin khách hàng',
      type: 'add_customer'
    }
  },
  edit_customer: {
    screen: AddEditCustomer,
    props: {
      headerText: 'Thông tin khách hàng',
      type: 'edit_customer',
      ga_page: Const.GA_PAGE_NAMES.customer_info
    }
  },
  contract_customer: {
    screen: AddEditCustomer,
    props: {
      headerText: 'Thông tin khách hàng',
      type: 'contract',
      ga_page: Const.GA_PAGE_NAMES.customer_info
    }
  },
  customer_info: {
    screen: CustomerInfo,
    props: {
      headerText: 'Thông tin khách hàng',
      ga_page: Const.GA_PAGE_NAMES.customer_history
    }
  },
  car_info: {
    screen: CarInfo,
    props: {
      type: 'car_info',
      ga_page: Const.GA_PAGE_NAMES.customer_car_having_maitenance
    }
  },
  service_car_info: {
    screen: CarInfo,
    props: {
      type: 'service_car_info',
      ga_page: null
    }
  },
  folow_car_info: {
    screen: CarInfo,
    props: {
      type: 'folow_car_info',
      ga_page: Const.GA_PAGE_NAMES.customer_car_follow_sale_support
    }
  },
  lost_car_info: {
    screen: CarInfo,
    props: {
      type: 'lost_car_info',
      ga_page: Const.GA_PAGE_NAMES.customer_sale_support_car_losing
    }
  },
  contact_history: {
    screen: ContactHistory,
    props: {
      headerText: 'Lịch sử liên hệ'
    }
  },
  service_history: {
    screen: ServiceHistory,
    props: {
      headerText: 'Lịch sử làm dịch vụ'
    }
  },
  customer_filter: {
    screen: CustomerFilter,
    props: {
      headerText: 'Bộ lọc khách hàng'
    }
  },
  add_new_car: {
    screen: AddNewCar,
    props: {
      headerText: 'Thông tin xe'
    }
  },
  edit_car: {
    screen: EditCar,
    props: {
      headerText: 'Thông tin xe'
    }
  },
  point_history: {
    screen: PointHistory,
    props: {
      headerText: 'Lịch sử tích điểm'
    }
  },
  transfer_customer: {
    screen: TransferCustomer,
    props: {
      headerText: 'Chuyển giao khách hàng'
    }
  },
  notify: {
    screen: Notify,
    props: {
      headerText: 'Thông báo'
    }
  },
  duplicate_folow: {
    screen: DuplicateFolow,
    props: {
      headerText: 'Theo dõi KH tiềm năng'
    }
  },
  add_edit_event: {
    screen: AddEditEvent,
    props: {
      headerText: 'Tạo lịch mới'
    }
  },
  event_detail: {
    screen: EventDetail,
    props: {}
  },
  forgot_password: {
    screen: ForgotPassword,
    props: {}
  },
  staff_kpi: {
    screen: StaffKpi,
    props: {}
  },
  contract_info: {
    screen: ContractInfo,
    props: {
      headerText: 'Thông tin hợp đồng'
    }
  },
  create_contract: {
    screen: CreateContract,
    props: {
      headerText: 'Tạo hợp đồng'
    }
  },
  find_customer: {
    screen: FindCustomer,
    props: {
      headerText: 'Danh sách khách hàng',
      listType: 'all'
    }
  },
  edit_delivery_plan: {
    screen: EditDeliveryPlan,
    props: {
    }
  },
  test: {
    screen: Test,
    props: {}
  }
}