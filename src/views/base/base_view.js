import React, { Component } from 'react';
import {
    View,
    StatusBar,
    TouchableWithoutFeedback,
    StyleSheet,
    ActivityIndicator,
    TouchableOpacity,
    Text
} from 'react-native';
import Utils from '../../global/utils'
import Colors from '../../global/colors'
import { NavigationActions, StackActions } from 'react-navigation';

class BaseView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModal: false,
            modalView: null,
            isLoading: false,
            modalTransparent: true,
            containerMarginTop: 0,
            isPopupOpen: false,
            popupView: null
        }
        if (this.props.gaTracker && this.props.ga_page) {
            this.props.gaTracker.trackScreenView(this.props.ga_page);
        }
    }

    render() {
        return (
            <View style={[styles.container, { marginTop: this.state.containerMarginTop }]}>
                <StatusBar
                    hidden={true}
                />
                {this.renderBaseContent()}
                {this.state.isModal &&
                    <TouchableWithoutFeedback onPress={() => this.setVisibleModal(false, false)}>
                        <View ref="modalContent" style={{ backgroundColor: this.state.modalTransparent ? 'transparent' : 'rgba(0, 0, 0, 0.6)', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableWithoutFeedback>{this.state.modalView}</TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                }
                {this.state.isLoading &&
                    <View ref="loadingView" style={{ zIndex: 99999, backgroundColor: 'rgba(0, 0, 0, 0.1)', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large"
                            color={Colors.dark_blue} />
                    </View>}
                {this.state.isPopupOpen &&
                    <View ref="popupView" style={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                        {this.state.popupView}
                    </View>}
            </View>

        );
    }

    renderPopup(view, title, message) {
        var mView = view || (
            <View style={styles.popup_box}>
                {title && <Text style={styles.popup_title}>{title}</Text>}
                <Text style={styles.popup_message}>{message}</Text>
                <TouchableOpacity underlayColor='transparent' style={styles.popup_btn} onPress={() => { this.setState({ isPopupOpen: false }) }}>
                    <Text style={styles.popup_btn_text}>Đồng ý</Text>
                </TouchableOpacity>
            </View>
        );
        this.setState({
            popupView: mView,
            isPopupOpen: true
        });
    }

    pushPage(pageView, params) {
        const { navigate } = this.props.navigation;
        navigate(pageView, params);
    }

    goBack() {
        const { goBack } = this.props.navigation;
        goBack();
    }

    resetPage(index, pageView, params) {
        const { dispatch } = this.props.navigation;
        dispatch(StackActions.reset({
            index: index,
            key: null,
            actions: [NavigationActions.navigate({ routeName: pageView, params: params })]
        }))
    }

    onFocus(view) {
        Utils.updateUIWhenShowKeyboard(view, this);
    }

    onFocusWithContext(view) {
        Utils.updateUIWhenShowKeyboardWithContext(view, this);
    }

    onEndEditingWithContext(context) {
        Utils.updateUIWhenHideKeyboard(context);
    }
    onEndEditing(view) {
        Utils.updateUIWhenHideKeyboard(this);
    }

    setVisibleModal(value, transparent) {
        this.setState({
            isModal: value,
            modalTransparent: transparent
        });
    }

    renderModal(gen_view) {
        this.setState({
            modalView: gen_view
        });
    }

    renderBaseContent() {
    }

    setLoading(value) {
        this.setState({
            isLoading: value
        });
    }

}

module.exports = BaseView;
const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%"
    },
    popup_box: {
        position: 'absolute',
        right: 30,
        left: 30,
        backgroundColor: 'white',
        borderRadius: 5,
        alignItems: 'center',
        paddingLeft: 8,
        paddingRight: 8
    },
    popup_title: {
        fontSize: 18,
        color: Colors.dark_blue,
        fontFamily: "Roboto-Bold",
        marginTop: 10
    },
    popup_message: {
        fontSize: 16,
        color: Colors.dark,
        marginTop: 20,
        fontFamily: "Roboto-Regular",
    },
    popup_btn: {
        height: 35,
        width: 120,
        backgroundColor: Colors.dark_blue,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20
    },
    popup_btn_text: {
        fontSize: 14,
        color: Colors.smoke,
        fontFamily: "Roboto-Regular"
    }
});
