
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';
var BaseView = require('./base_view');
import ModalDropdown from '../../widgets/modal_dropdown';
import Routes from '../routes';
import Colors from '../../global/colors';

class CrmChildView extends BaseView {
  constructor(props) {
    super(props);
    this.state = {
      isDropdown: true,
      topRightMenuItems: ['Gọi điện thoại', 'Nhắn tin'],
      headerBackgroundColor: Colors.dark_blue
    }
  }

  renderBaseContent() {
    const { navigation } = this.props;
    const headerText = navigation.getParam('headerText', '');
    return (
      <View ref='mainView' style={{ height: '100%', width: '100%' }}>
        <View style={[styles.header, { backgroundColor: this.state.headerBackgroundColor ? this.state.headerBackgroundColor : Colors.dark_blue }]}>
          <TouchableOpacity style={styles.button} onPress={this.onBack.bind(this)}>
            <Image source={require('../../assets/images/back.png')} resizeMode={'contain'} style={styles.icon_qure} />
          </TouchableOpacity>

          <View style={styles.content_center}>
            <Text style={{ fontSize: 16, color: 'white', fontFamily: 'Roboto-Bold' }} numberOfLines={1}>{headerText}</Text>
          </View>
          <View style={styles.right_button}>
            {this.renderRightButton()}
          </View>
        </View>

        <View style={[styles.container, { marginTop: this.state.childTop }]}>
          {this.renderContainer()}
        </View>

      </View>
    );
  }

  renderRightButton() {
    var views = [];
    views.push(
      <TouchableOpacity key={'right-btn-1'} underlayColor={'transparent'} style={styles.button} onPress={() => { this.pushPage('add_customer') }}>
        <Image source={require('../../assets/images/plus_white.png')} resizeMode={'contain'} style={styles.icon_qure} />
      </TouchableOpacity>
    );
    views.push(
      <TouchableOpacity key={'right-btn-2'} underlayColor={'transparent'} style={styles.button} onPress={() => { this.pushPage('notify') }}>
        <View style={styles.notify_box}>
          <Image source={require('../../assets/images/notify.png')} resizeMode={'contain'} style={styles.icon_qure} />
          <View style={styles.notify_number_box}>
            <Text style={styles.notify_number} numberOfLines={1}>10</Text>
          </View>
        </View>
      </TouchableOpacity>
    );

    views.push(
      <TouchableOpacity key={'right-btn-3'} underlayColor={'transparent'} style={styles.button} >
        <View style={styles.icon_box}>
          {this.renderMenuDropDown()}
        </View>
      </TouchableOpacity>
    );

    return views;
  }

  renderMenuDropDown() {
    return <ModalDropdown
      options={this.state.topRightMenuItems ? this.state.topRightMenuItems : ['Gọi điện thoại', 'Nhắn tin']}
      renderButton={this.renderMenuDropDownBtn}
      isNormalArray={true}
      mStyle={2}
      renderModal={this.renderModal.bind(this)}
      setVisibleModal={this.setVisibleModal.bind(this)}
      positionStyle={{ width: 200, right: 8 }} />
  }

  renderMenuDropDownBtn(showDropdown) {
    return (
      <View style={[{ backgroundColor: showDropdown ? '#00554E' : 'transparent' }, styles.icon_box]}>
        <Image source={require('../../assets/images/three_dots_white.png')} resizeMode={'contain'} style={styles.icon} />
      </View>
    );
  }
  renderContainer() {

  }

  onBack() {
    this.goBack();
  }
}
module.exports = CrmChildView;
const styles = StyleSheet.create({
  header: {
    height: 50,
    width: '100%',
    flexDirection: 'row'
  },
  right_button: {
    position: 'absolute',
    flexDirection: 'row',
    height: 50,
    right: 0,
    justifyContent: 'flex-end'
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50
  },
  content_center: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    flex: 1
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50
  },
  icon: {
    height: 20,
    width: 20
  },
  icon_qure: {
    height: 30,
    width: 30
  },
  content_center: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
  },
  notify_number_box: {
    position: 'absolute',
    backgroundColor: '#E35B5A',
    width: 18, height: 18,
    top: 0,
    right: 0,
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center'
  },
  notify_number: {
    color: 'white',
    fontSize: 10,
    backgroundColor: 'transparent',
    fontFamily: "Roboto-Regular"
  },
  icon_box: {
    borderRadius: 3,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  }
})