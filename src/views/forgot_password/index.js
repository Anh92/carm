import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Image} from 'react-native';
import { connect } from 'react-redux';
import TextField from '../../widgets/text_input';
import BaseView from '../../views/base/base_view'
import styles from './styles'
import StringSource from '../../global/strings'
import Color from '../../global/colors'

class ForgotPass extends BaseView {
    constructor(props) {
        super(props);
        this.state = {

        };

    }


    renderBaseContent() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.logo_box}>
                    <Image style={styles.logo} resizeMode={'contain'} source={require('../../assets/images/logo.png')} ></Image>
                </View>
                <View style={styles.form}>
                    <TextField
                        ref="tf_pass"
                        label={StringSource.new_pass}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        height={40}
                    />

                    <TextField
                        ref="tf_re_pass"
                        label={StringSource.type_pass_again}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        secureTextEntry={true}
                        height={40}
                    />
                    <TextField
                        ref="tf_otp"
                        label={StringSource.type_otp}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        secureTextEntry={true}
                        height={40}
                    />
                    <View style={styles.select_date_btn_box}>
                        <TouchableOpacity underlayColor='transparent' style={[styles.select_date_btn, { backgroundColor: Color.smoke }]} onPress={() => {
                            this.goBack();
                        }}>
                            <Text style={[styles.select_date_btn_text, { color: Color.dark_blue }]}>Hủy</Text>
                        </TouchableOpacity>
                        <TouchableOpacity underlayColor='transparent' style={[styles.select_date_btn, { backgroundColor: Color.dark_blue, marginLeft: 20 }]} onPress={() => { }}>
                            <Text style={[styles.select_date_btn_text, { color: Color.smoke }]}>Xác nhận</Text>
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity>
                        <Text style={styles.forgot_pass}>{StringSource.resend_otp}</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}


export default connect(null, null)(ForgotPass);
