import {
    StyleSheet
} from 'react-native';
import Color from '../../global/colors'

module.exports = StyleSheet.create({
    container: {
        backgroundColor: Color.white
    },
    logo_box: {
        width: '100%',
        alignItems: 'center',
        marginTop: 90
    },
    logo: {
        width: 159,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center'
    },
    form: {
        marginTop: 40,
        paddingLeft: 32,
        paddingRight: 32,
        justifyContent: 'center',
        alignItems: 'center'
    },
    submit: {
        marginTop: 50,
        height: 50,
        width: 200,
        backgroundColor: Color.dark_blue,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn_text: {
        color: 'white',
        fontSize: 18,
        fontFamily: "Roboto-Regular",
        // fontWeight: 'bold'
    },
    forgot_pass: {
        fontSize: 14,
        color: '#676766',
        marginTop: 25,
        fontFamily: "Roboto-Italic",
        marginBottom: 20
    },
    select_date_btn: {
        height: 35,
        width: 120,
        backgroundColor: Color.dark_blue,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    select_date_btn_text: {
        fontSize: 14,
        color: Color.gray,
        fontFamily: "Roboto-Regular"
    },
    select_date_btn_box: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 25
    },
});