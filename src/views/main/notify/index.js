import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  TouchableOpacity,
  Text,
  FlatList,
  Image
} from 'react-native';
var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');

class Notify extends ChildView {
  renderContainer() {
    return (
      <FlatList
        style={styles.container}
        data={[{ key: 'TET E T ET E T E TE T E Td', time: '04:45PM', when: '2 phut truoc', read: false }, { key: 'fdfsd fdsfsb', time: null, when: '5 phut truoc', read: true }]}
        renderItem={({ item }) => this.renderNotifyItem(item)}
      />
    );
  }
  renderNotifyItem(item) {
    return (
      <View style={[styles.row, { backgroundColor: item.read ? 'transparent' : '#F2F2F2' }]}>
        <Text style={styles.text_title}>{item.key + ((item.time) ? (" - " + item.time) : '')}</Text>
        <View style={styles.when_box}>
          <Image source={require('../../../assets//images/clock.png')} resizeMode={'contain'} style={styles.icon} />
          <Text style={styles.text_time}>{item.when}</Text>
        </View>
      </View>
    );
  }
  renderRightButton() {
    var views = [];
    views.push(
      <TouchableOpacity key={"btn-add-customer"} underlayColor={'transparent'} style={styles.button} >
        <Image source={require('../../../assets//images/plus_white.png')} resizeMode={'contain'} style={styles.icon_qure} />
      </TouchableOpacity>
    );

    views.push(
      <TouchableOpacity key={"btn-action"} underlayColor={'transparent'} style={styles.button} >
        <View style={styles.icon_box}>
          {this.renderMenuDropDown()}
        </View>
      </TouchableOpacity>
    );

    return views;
  }
}
module.exports = Notify;