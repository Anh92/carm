import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors'

module.exports = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white'
  },
  icon: {
    tintColor: Colors.dark_blue,
    width: 15,
    height: 15
  },
  text_time: {
    color: Colors.black,
    fontFamily: "Roboto-Regular",
    fontSize: 12,
    marginLeft: 8
  },
  text_title: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 15
  },
  row: {
    height: 70,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
    paddingLeft: 20,
    paddingRight: 20
  },
  when_box: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50
  },
  icon_qure: {
    height: 30,
    width: 30
  }
});
