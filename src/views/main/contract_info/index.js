import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import Routes from '../../../views/routes';
import SickyScrollView from '../../../widgets/sticky_scrollview';
import { connect } from 'react-redux';
import Tabbar from '../../../widgets/tabbar';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');

var setBackId = Object.freeze({

});
const tabbarData = [
  {
    key: 'no_deposit',
    text: 'CHƯA ĐẶT CỌC'
  },
  {
    key: 'deposited',
    text: 'ĐÃ ĐẶT CỌC'
  }];
class ContractInfo extends ChildView {
  constructor(props) {
    super(props);
    // this.props.navigator;
    this.state = {
      tabSelected: (this.props.type == 'delivery_plan') ? 'deposited' : 'no_deposit',
      headerMarginTop: 0,
      // customerData: this.props.customerData,
      customerData: {
        fullname: 'Nguyen Van A',
        nickname: 'A Van',
        phones: [{ active: true, number: '0167829839' }],
        email: 'nguyenvana@gmail.com'
      },
      service: [{ title: 'dfds', car: 'fdsf' }],
      topRightMenuItems: ['Gọi điện thoại', 'Nhắn tin', 'Đặt hẹn', 'Chuyển giao'],
      reasonLostCar: ''
    }
    console.log(this.props.type);
  }

  renderContainer() {
    let { customerData } = this.state;
    return (
      <SickyScrollView
        maxHeight={130}
        minHeight={40}
        isLoading={this.showLoading.bind(this)}
        onRefresh={this.onRefresh.bind(this)}
        renderHeader={this.renderScrollViewHeader.bind(this)}
        renderContent={this.renderTabView.bind(this)}
      />
    );
  }
  showLoading() {
    return false;
    // switch (this.state.tabSelected) {

    // }
  }
  renderScrollViewHeader() {
    let { customerData } = this.state;
    return (
      <View style={{ width: '100%', height: '100%' }}>
        <TouchableOpacity style={[styles.avatar_box, { marginTop: this.state.headerMarginTop }]} activeOpacity={0.8} onPress={() => { this.openEditProfilePage() }}>
          {/* <Image source={customerData.iconProfile ? { uri: customerData.iconProfile } : require('../../../assets//images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} /> */}
          <Image source={require('../../../assets//images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} />
          <View style={styles.info_text_box}>
            <Text style={styles.user_name_text}>{customerData.fullname || customerData.nickname}</Text>
            <Text style={styles.user_phone_text}>{((customerData.phones && customerData.phones.length > 0) && this.showPhone(customerData.phones))}</Text>
            <Text style={styles.user_email_text}>{customerData.email}</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.tabbar}>
          <Tabbar
            data={tabbarData}
            fixFlex={true}
            defaultKey={this.state.tabSelected}
            onChange={this.onTabSelect.bind(this)} />
        </View>
      </View>);
  }
  onRefresh() {
    return false;
    // switch (this.state.tabSelected) {

    // }
  }

  sendTabGa() {
    // if (this.props.gaTracker) {
    //   switch (this.state.tabSelected) {
    //     case 'customerHis':
    //       this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_history);
    //       break;
    //   }
    // }
  }

  showPhone(phones) {
    var activePhone = phones.filter((e) => {
      return e.active;
    });
    if (activePhone.length > 0) {
      return activePhone[0].number;
    }
    return phones[0].number;
  }

  openEditProfilePage() {
    var page = Routes.edit_customer
    var props = {
      ...page.props,
      customerData: this.state.customerData,
      setBackId: setBackId.EDIT_PROFILE,
      setBack: this.setBack.bind(this)
    };
    this.pushPage('edit_customer', props);
  }

  onTabSelect(item) {
    this.setState({
      tabSelected: item.key
    }, () => {
      this.onRefresh();
      this.sendTabGa();
    });
  }

  renderTabView() {
    switch (this.state.tabSelected) {
      case 'no_deposit':
        return (
          <View style={styles.tabview_box}>
            <TouchableOpacity style={styles.add_more_car_box} onPress={() => { this.openCreateContractPage() }}>
              <Image source={require('../../../assets//images/add_more.png')} resizeMode={'contain'} style={{ width: 25, height: 25 }} />
              <Text style={styles.add_more_car_text}>Thêm hợp đồng</Text>
            </TouchableOpacity>
            {this.renderContract()}
          </View>
        );
      case 'deposited':
        return (
          <View style={styles.tabview_box}>
            {this.renderDepositedContract()}
          </View>
        );
    }
  }

  //////////////////////////START RENDER no_deposit TAB////////////////////
  renderContract() {
    var views = [];
    for (var i = 0; i < listCarTemp.length; i++) {
      views.push(this.renderContractRows(listCarTemp[i], i));
    }
    return views;
  }
  renderContractRows(item, index) {
    return (
      <View key={"contract" + index} style={styles.car_box}>
        <Text style={styles.car_name}>{item.carGradeName + " - " + item.carColorName}</Text>
        <Text style={styles.car_status}>{item.carColorName}</Text>
        <Text style={styles.staff_name}>{item.transactionStatusDisplayname}</Text>
      </View>
    );
  }

  openCreateContractPage() {
    var page = Routes.create_contract;
    var props = {
      ...page.props,
      // customerSubProfileId: this.props.customerData.customerSubProfileId,
      setBackId: setBackId.CUSTOMER_HIS,
      setBack: this.setBack.bind(this)
    };
    this.pushPage('create_contract', props);
  }
  //////////////////////////END RENDER no_deposit TAB////////////////////

  //////////////////////////START RENDER deposited TAB////////////////////
  renderDepositedContract() {
    var views = [];
    for (var i = 0; i < listCarTemp.length; i++) {
      views.push(this.renderdepositedContractRows(listCarTemp[i], i));
    }
    return views;
  }
  renderdepositedContractRows(item, index) {
    return (
      <TouchableOpacity key={"deposited-contract" + index} style={styles.car_box} onPress={() => { this.openEditDeliveryPlanPage(item) }}>
        <Text style={styles.car_name}>{item.carGradeName + " - " + item.carColorName}</Text>
        <Text style={styles.car_status}>{item.carColorName}</Text>
        <Text style={styles.staff_name}>{item.transactionStatusDisplayname}</Text>
      </TouchableOpacity>
    );
  }

  openEditDeliveryPlanPage(item) {
    var page = Routes.edit_delivery_plan;
    var props = {
      ...page.props,
      carDetail: item,
      headerText: item.carGradeName,
    }
    this.pushPage('edit_delivery_plan', props);
  }
  //////////////////////////END RENDER deposited TAB////////////////////
  setBack(id, data) {
    switch (id) {

    }
  }
}

// function mapStateToProps(state) {
//   return { customerInfoPage: state.customerInfoPage };
// }

export default connect(null, null)(ContractInfo);
const listCarTemp = [
  {
    "staffId": 481,
    "staffName": "481",
    "subProfileCarInfoId": 112,
    "isClosed": false,
    "causalClosed": null,
    "isContact": false,
    "relationships": "RELATED",
    "deliveredDate": null,
    "id": 112,
    "carGradeId": 32,
    "carColorId": 15,
    "carBrandName": "Toyota",
    "carModelName": "Fortuner",
    "carGradeName": "Fortuner 2.7V 4x4",
    "carColorName": "Trắng ngọc trai",
    "vin": null,
    "engineNumber": null,
    "plateNumber": null,
    "km": null,
    "nextKmMaintenance": null,
    "nextDateMaintenance": null,
    "isNew": true,
    "transactionId": null,
    "transactionContractId": null,
    "transactionStatusId": 1,
    "transactionStatusName": "contact",
    "transactionStatusDisplayname": "Tiếp cận",
    "transactionStatusDescription": null,
    "transactionStatuses": [
      {
        "name": "contact",
        "displayName": "Tiếp cận",
        "order": 1
      },
      {
        "name": "contract",
        "displayName": "Hợp đồng",
        "order": 2
      },
      {
        "name": "deposit",
        "displayName": "Đặt cọc",
        "order": 3
      },
      {
        "name": "paymented",
        "displayName": "Thanh toán",
        "order": 4
      },
      {
        "name": "delivered",
        "displayName": "Giao xe",
        "order": 5
      },
      {
        "name": "onek",
        "displayName": "1k",
        "order": 6
      },
      {
        "name": "fivek",
        "displayName": "5k",
        "order": 7
      },
      {
        "name": "service",
        "displayName": "Bảo dưỡng định kỳ",
        "order": 8
      }
    ]
  },
  {
    "staffId": null,
    "staffName": "null",
    "subProfileCarInfoId": 113,
    "isClosed": false,
    "causalClosed": null,
    "isContact": false,
    "relationships": "RELATED",
    "deliveredDate": null,
    "id": 113,
    "carGradeId": 32,
    "carColorId": 15,
    "carBrandName": "Toyota",
    "carModelName": "Fortuner",
    "carGradeName": "Fortuner 2.7V 4x4",
    "carColorName": "Trắng ngọc trai",
    "vin": null,
    "engineNumber": null,
    "plateNumber": null,
    "km": null,
    "nextKmMaintenance": null,
    "nextDateMaintenance": null,
    "isNew": true,
    "transactionId": null,
    "transactionContractId": null,
    "transactionStatusId": 1,
    "transactionStatusName": "contact",
    "transactionStatusDisplayname": "Tiếp cận",
    "transactionStatusDescription": null,
    "transactionStatuses": [
      {
        "name": "contact",
        "displayName": "Tiếp cận",
        "order": 1
      },
      {
        "name": "contract",
        "displayName": "Hợp đồng",
        "order": 2
      },
      {
        "name": "deposit",
        "displayName": "Đặt cọc",
        "order": 3
      },
      {
        "name": "paymented",
        "displayName": "Thanh toán",
        "order": 4
      },
      {
        "name": "delivered",
        "displayName": "Giao xe",
        "order": 5
      },
      {
        "name": "onek",
        "displayName": "1k",
        "order": 6
      },
      {
        "name": "fivek",
        "displayName": "5k",
        "order": 7
      },
      {
        "name": "service",
        "displayName": "Bảo dưỡng định kỳ",
        "order": 8
      }
    ]
  }
]