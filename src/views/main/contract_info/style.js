import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white'
  },
  avatar_box: {
    position: 'absolute',
    bottom: 40,
    height: 90,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.light_dark
  },
  avatar: {
    marginLeft: 15,
    width: 60,
    height: 60,
    borderRadius: 30
  },
  user_name_text: {
    fontSize: 14,
    color: Colors.smoke,
    fontFamily: "Roboto-Bold"
  },
  user_phone_text: {
    fontSize: 14,
    color: Colors.smoke,
    marginTop: 3,
    fontFamily: "Roboto-Regular"
  },
  user_email_text: {
    fontSize: 12,
    color: Colors.smoke,
    marginTop: 3,
    fontFamily: "Roboto-Regular"
  },
  info_text_box: {
    marginLeft: 30
  },
  tabbar: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 40,
    backgroundColor: Colors.ink,
    flexDirection: 'row'
  },
  add_more_car_box: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  view_point_his_box: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  tabview_box: {
    padding: 20
  },
  add_more_car_text: {
    color: Colors.dark,
    fontFamily: "Roboto-Bold",
    fontSize: 12,
    marginLeft: 10
  },
  title_box: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 30,
    borderBottomColor: '#DFE0E1',
    borderBottomWidth: 0.5
  },
  title_text: {
    color: Colors.dark,
    fontFamily: "Roboto-Light",
    fontSize: 20
  },
  car_box: {
    height: 80,
    borderBottomColor: '#DFE0E1',
    borderBottomWidth: 0.5,
    justifyContent: 'center'
  },
  car_name: {
    fontSize: 14,
    color: Colors.dark,
    fontFamily: "Roboto-Bold"
  },
  view_point_his_text: {
    fontSize: 14,
    color: Colors.dark_pink,
    fontFamily: "Roboto-Bold"
  },
  car_status: {
    fontSize: 12,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  staff_name: {
    fontSize: 12,
    color: Colors.dark,
    fontFamily: "Roboto-Italic",
    marginTop: 5
  },
  service_box: {
    height: 60,
    borderBottomColor: '#DFE0E1',
    borderBottomWidth: 0.5,
    justifyContent: 'center'
  },
  cus_info_text: {
    fontSize: 12,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  modal_box: {
    position: 'absolute',
    left: 30,
    right: 30,
    padding: 20,
    backgroundColor: "white"
  },
  input_row: {
    flex: 1,
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  },
  text_input: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    height: 40,
    paddingVertical: 0,
    paddingHorizontal: 0,
    marginTop: -5
  },
  btn_box: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 10
  },
  submit: {
    height: 35,
    width: 120,
    backgroundColor: Colors.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  btn_text: {
    fontSize: 15,
    color: 'white',
    fontFamily: "Roboto-Regular"
  }
});
