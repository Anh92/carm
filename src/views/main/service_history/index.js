import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Colors from '../../../global/colors';
import InputRow from '../../../widgets/input_row';

var Global = require('../../../global/const');
var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');
const hisData = [{
  time_in: '14:00 01/02/2017',
  time_out: '18:00 01/02/2017',
  km: 1000,
  staff: 'Huy Hoang',
  service_type: 'Bao duong'
},
{
  time_in: '09:00 05/02/2017',
  time_out: '18:00 01/02/2017',
  km: 1001,
  staff: 'Huy Han',
  service_type: 'Sửa chữa'
}]

class ServiceHistory extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      viewDetailIndex: 0,
      viewDetail: false,
    }
  }

  renderContainer() {
    if (this.state.viewDetail) {
      let selectItem = hisData[this.state.viewDetailIndex];
      return (
        <ScrollView style={{ backgroundColor: Colors.smoke, padding: 20 }}>
          <InputRow
            title={'Thời gian xe vào'}
            mStyle={0}
            defaultValue={selectItem.time_in} />
          <InputRow
            title={'Thời gian xe ra'}
            mStyle={0}
            defaultValue={selectItem.time_out} />
          <InputRow
            title={'Số KM xe vào'}
            mStyle={0}
            defaultValue={selectItem.km} />
          <InputRow
            title={'Cố vấn dịch vụ'}
            mStyle={0}
            defaultValue={selectItem.staff} />
          <InputRow
            title={'Loại dịch vụ'}
            mStyle={0}
            defaultValue={selectItem.service_type} />
        </ScrollView>
      );
    }
    return (
      <FlatList
        data={hisData}
        renderItem={({ item, index }) => this.renderRows(item, index)}
      />
    );
  }

  renderRows(item, index) {
    return (
      <TouchableOpacity style={styles.row} onPress={() => { this.showDetail(index) }}>
        <Text style={styles.title_text}>{item.time_in}</Text>
        <Text style={styles.service_type_text}>{item.service_type}</Text>
        <View style={styles.btn_right} >
          <Image source={require('../../../assets//images/double_arrows_right.png')} resizeMode={'contain'} style={[styles.double_arrow, { tintColor: Colors.gray }]} />
        </View>
      </TouchableOpacity>
    );
  }

  showDetail(index) {
    this.setState({
      viewDetailIndex: index,
      viewDetail: true
    });
  }
  onBack() {
    if (this.state.viewDetail) {
      this.setState({
        viewDetail: false
      });
    } else {
      this.goBack();
    }

  }

}
module.exports = ServiceHistory;