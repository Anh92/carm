import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  row: {
    height: 60,
    marginLeft: 20,
    marginRight: 20,
    borderBottomColor: Colors.underline,
    justifyContent: 'center',
    borderBottomWidth: 0.5
  },
  title_text: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Bold"
  },
  title_detail_text:{
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular"
  },
  service_type_text: {
    color: Colors.gray,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  double_arrow: {
    position: 'absolute',
    right: 10,
    width: 15,
    height: 15,
    tintColor: Colors.dark_pink,
  },
  btn_right: {
    position: 'absolute',
    right: 0,
    height: 60,
    width: 60,
    justifyContent: 'center'
  }
});
