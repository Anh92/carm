import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import ModalDropdown from '../../../widgets/modal_dropdown';
import ModalDatePicker from '../../../widgets/modal_date_picker';
import ModelAction from '../../../widgets/modal_actions';
import ModalFilterDropdown from '../../../widgets/modal_filter_dropdown';
import Routes from '../../../views/routes';
import { connect } from 'react-redux';
import { doAdd, doEdit, checkPhoneExist, doUpdateForContract } from '../../../actions/mainActions/addEditCustomerAction';
import { doGetProvinceData } from '../../../actions/apiActions/provinceDistrictApiAction';
import { doGetEnumTypeData } from '../../../actions/apiActions/enumTypeApiAction';
import InputRow from '../../../widgets/input_row';

import ImagePicker from 'react-native-image-picker';
var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');
var options = {
  title: 'Chọn hình đại diện',
  maxWidth: 150,
  maxHeight: 150,
  cancelButtonTitle: 'Thoát',
  takePhotoButtonTitle: 'Từ camera',
  chooseFromLibraryButtonTitle: 'Từ thư viện ảnh',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};
const phoneAction = [{
  key: 'make_call',
  text: 'Gọi điện'
}, {
  key: 'send_mes',
  text: 'Nhắn tin'
}, {
  key: 'add_more_phone',
  text: 'Thêm số điện thoại'
}, {
  key: 'choose_as_main_num',
  text: 'Chọn làm số liên lạc chính'
}, {
  key: 'delete_phone',
  text: 'Xoá số điện thoại'
}];
const mainPhoneAction = [{
  key: 'make_call',
  text: 'Gọi điện'
}, {
  key: 'send_mes',
  text: 'Nhắn tin'
}, {
  key: 'add_more_phone',
  text: 'Thêm số điện thoại'
}];
var setBackId = Object.freeze({
  ADD_CAR: 'ADD_CAR'
});

class AddCustomer extends ChildView {
  constructor(props) {
    super(props);
    this.phoneRefs = [];
    this.state = {
      userInfo: {
        ...this.props.customerData,
        // phones: !(this.props.type == 'add_customer') ? this.props.customerData.phones : [{
        //   number: '',
        //   type: "MOBILE",
        //   active: true
        // }]
        phones: [{
          number: '',
          type: "MOBILE",
          active: true
        }]
      },
      car: null,
      phonesView: <View></View>,
      // avatarSource: (this.props.type == 'edit_customer') ? (this.props.customerData.iconProfile ? { uri: this.props.customerData.iconProfile } : require('../../../assets//images/avatar_default.jpg')) : require('../../../assets//images/avatar_default.jpg'),
      avatarSource: require('../../../assets//images/avatar_default.jpg'),
      listDistrict: []
    }
    console.log(this.props.customerData);
  }

  renderContainer() {
    let { listProvinceDistrict } = this.props.provinceDistrictApi;
    let { genders, maritals } = this.props.enumTypeApi.enumData ? this.props.enumTypeApi.enumData : {};
    let isAddNew = this.props.type == 'add_customer';
    let isContract = this.props.type == 'contract';
    return (
      <ScrollView>
        <View style={styles.avatar_box}>
          <TouchableOpacity onPress={() => { this.changeAvatar() }}>
            <Image source={this.state.avatarSource} resizeMode={'cover'} style={styles.avatar} />
          </TouchableOpacity>
        </View>
        <View style={styles.user_info_box}>
          {!isAddNew && < View style={styles.input_row}>
            <Text style={styles.text_title}>Mã khách hàng</Text>
            <Text underlineColorAndroid={'transparent'} style={styles.text_info}>{this.state.userInfo.code}</Text>
          </View>}
          {isAddNew && this.state.phonesView}
          <InputRow
            onFocus={this.onFocusWithContext.bind(this)}
            onEndEditing={this.onEndEditing.bind(this)}
            title={'Họ và tên'}
            requireChar={isContract}
            defaultValue={this.state.userInfo.fullname}
            onChangeText={(text) => { this.setState({ userInfo: { ...this.state.userInfo, fullname: text } }) }} />
          <InputRow
            onFocus={this.onFocusWithContext.bind(this)}
            onEndEditing={this.onEndEditing.bind(this)}
            title={'Tên thường gọi'}
            requireChar={true}
            defaultValue={this.state.userInfo.nickname}
            onChangeText={(text) => { this.setState({ userInfo: { ...this.state.userInfo, nickname: text } }) }} />
          {!isAddNew && this.state.phonesView}
          <InputRow
            onFocus={this.onFocusWithContext.bind(this)}
            onEndEditing={this.onEndEditing.bind(this)}
            title={'Email'}
            defaultValue={this.state.userInfo.email}
            onChangeText={(text) => { this.setState({ userInfo: { ...this.state.userInfo, email: text } }) }} />
          <ModalDropdown
            // options={genders}
            mStyle={1}
            ref={'ref_md_gender'}
            isNormalArray={true}
            options={['Nam', 'Nữ', 'Les', 'Gay', 'Nam', 'Nữ', 'Les', 'Gay', 'Nam', 'Nữ', 'Les', 'Gay', 'Nam', 'Nữ', 'Les', 'Gay']}
            // keyItems={'displayName'}
            requireChar={isContract}
            renderModal={this.renderModal.bind(this)}
            reloadData={this.loadDefaultData.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            defaultValue={this.state.userInfo.genderDisplayname}
            onSelect={(item) => this.setState({ userInfo: { ...this.state.userInfo, gender: item.name } })}
            title={'Giới tính'} />
          <ModalDatePicker
            ref={'ref_md_datepicker'}
            title={"Ngày sinh"}
            onPickerChange={(date) => this.setState({
              userInfo: { ...this.state.userInfo, birthday: date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2) }
            })}
            requireChar={true}
            defaultDate={this.state.userInfo.birthday ? new Date(this.state.userInfo.birthday) : undefined} />
          <InputRow
            onFocus={this.onFocusWithContext.bind(this)}
            onEndEditing={this.onEndEditing.bind(this)}
            title={'CMND'}
            requireChar={isContract}
            defaultValue={this.state.userInfo.email}
            onChangeText={(text) => { this.setState({ userInfo: { ...this.state.userInfo, passportId: text } }) }} />
          <InputRow
            onFocus={this.onFocusWithContext.bind(this)}
            onEndEditing={this.onEndEditing.bind(this)}
            title={'Địa chỉ'}
            requireChar={isContract}
            defaultValue={this.state.userInfo.address}
            onChangeText={(text) => { this.setState({ userInfo: { ...this.state.userInfo, address: text } }) }} />
          <ModalFilterDropdown
            ref='ref_mf_district'
            options={['Tinh 1', 'Tinh 2', 'Tinh 3', 'Tinh 4', 'Tinh 5', 'Tinh 6', 'Tinh 7', 'Tinh 8', 'Tinh 31', 'Tinh 32', 'Tinh 33']}
            // options={listProvinceDistrict}
            // keysFilter={['name']}
            isNormalArray={true}
            requireChar={isContract}
            reloadData={this.loadDefaultData.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            renderModal={this.renderModal.bind(this)}
            onSelect={this.onCityChange.bind(this)}
            defaultValue={this.state.userInfo.province}
            title={'Tỉnh thành'} />
          <ModalFilterDropdown
            ref='ref_md_list_district'
            // options={this.state.listDistrict}
            // keysFilter={['name']}
            options={['Quan 1', 'Quan 2', 'Quan 3', 'Quan 4', 'Quan 5', 'Quan 6', 'Quan 7', 'Quan 8', 'Quan 31', 'Quan 32', 'Quan 33']}
            // options={this.state.listDistrict}
            // keysFilter={['name']}
            isNormalArray={true}
            requireChar={isContract}
            reloadData={this.loadDistrict.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            renderModal={this.renderModal.bind(this)}
            onSelect={(type, item) => this.setState({ userInfo: { ...this.state.userInfo, district: (type == 1) ? item.name : null } })}
            defaultValue={this.state.userInfo.district}
            title={'Quận huyện'} />
          <InputRow
            onFocus={this.onFocusWithContext.bind(this)}
            onEndEditing={this.onEndEditing.bind(this)}
            title={'Nghề nghiệp'}
            defaultValue={this.state.userInfo.job}
            onChangeText={(text) => { this.setState({ userInfo: { ...this.state.userInfo, job: text } }) }} />
          <ModalDropdown
            ref='ref_md_marital'
            // options={maritals}
            mStyle={1}
            isNormalArray={true}
            options={['Đã kết hôn', 'Ly Dị', 'Độc Thân']}
            // keyItems={'displayName'}
            reloadData={this.loadDefaultData.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            renderModal={this.renderModal.bind(this)}
            onSelect={(item) => this.setState({ userInfo: { ...this.state.userInfo, marital: item.name } })}
            defaultValue={this.state.userInfo.maritalDisplayname}
            title={'Tình trạng hôn nhân'} />
          <TouchableOpacity
            onPress={() => {
              // this.onPressSubmit()
            }}
            style={styles.submit}
            underlayColor='transparent'>
            <Text style={styles.btn_text}>Lưu</Text>
          </TouchableOpacity>
        </View>
      </ScrollView >
    );
  }

  onCityChange(type, item) {
    this.setState({ userInfo: { ...this.state.userInfo, province: (type == 1) ? item.name : null, district: null }, listDistrict: (type == 1) ? item.districts : [] }, () => {
      this.refs.ref_md_list_district.resetDefault();
    });
  }

  loadDistrict() {
    var listProvinceDistrict = this.props.provinceDistrictApi.listProvinceDistrict;
    if (listProvinceDistrict == null || listProvinceDistrict.length <= 0) {
      this.props.doGetProvinceData(this.setLoading.bind(this), this.props.navigator);
    } else {
      var provinceIndex = listProvinceDistrict.findIndex((element) => {
        if (this.state.userInfo.province == element.name) {
          return true;
        }
        return false;
      })
      if (provinceIndex != -1) {
        this.setState({
          listDistrict: listProvinceDistrict[provinceIndex].districts
        });
      }
    }
  }
  onPressSubmit() {
    var activePhoneIndex = this.state.userInfo.phones.findIndex((element) => {
      return element.active;
    })
    if (activePhoneIndex == -1 || this.state.userInfo.phones[activePhoneIndex].number == null || this.state.userInfo.phones[activePhoneIndex].number == '') {
      this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập số điện thoại chính');
      return;
    }
    if (this.state.userInfo.nickname == null || this.state.userInfo.nickname == '') {
      this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập tên thường gọi');
      return;
    }
    if (this.props.type == 'contract') {
      if (this.state.userInfo.fullname == null || this.state.userInfo.fullname == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập họ và tên');
        return;
      }
      if (this.state.userInfo.gender == null || this.state.userInfo.gender == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập giới tính');
        return;
      }
      if (this.state.userInfo.gender == null || this.state.userInfo.gender == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập giới tính');
        return;
      }
      if (this.state.userInfo.passportId == null || this.state.userInfo.passportId == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập CMND');
        return;
      }
      if (this.state.userInfo.address == null || this.state.userInfo.address == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập địa chỉ');
        return;
      }
      if (this.state.userInfo.province == null || this.state.userInfo.province == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập tỉnh thành');
        return;
      }
      if (this.state.userInfo.district == null || this.state.userInfo.district == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập quận huyện');
        return;
      }
      this.props.doUpdateForContract(this.state.userInfo, this);
      return;
    }
    if (this.props.type == 'add_customer') {
      if (this.state.car != null && this.state.car != undefined) {
        this.props.doAdd({ customer: this.state.userInfo, car: this.state.car }, this);
      } else {
        var page = Routes.add_new_car;
        // page.props = {
        //   ...page.props,
        //   type: 'add_customer',
        //   setBackId: setBackId.ADD_CAR,
        //   setBack: this.setBack.bind(this)
        // };
        this.pushPage('add_new_car', {
          ...page.props,
          type: 'add_customer',
          setBackId: setBackId.ADD_CAR,
          setBack: this.setBack.bind(this)
        });
      }
    } else {
      this.props.doEdit(this.state.userInfo, this);
    }

  }

  changeAvatar() {
    // console.log(ImagePicker.showImagePicker)
    // return
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // console.log(response.data);
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  async cropImage(path) {
    ImageEditor.cropImage(
      path,
      cropData,
      successURI => this.setState({ path: successURI }),
      error => console.log(error.message)
    )
  }

  ////////////////RENDER PHONE/////////////////
  renPhoneInputsView() {
    if (this.state.userInfo.phones && this.state.userInfo.phones.length > 0) {
      var views = [];
      for (var i = 0; i < this.state.userInfo.phones.length; i++) {
        views.push(this.renPhoneInput(i));
      }
      this.setState({
        phonesView: views
      });
    } else {
      this.setState({
        userInfo: {
          ...this.state.userInfo,
          phones: [{
            number: '',
            type: "MOBILE",
            active: true
          }]
        }
      }, () => {
        this.setState({
          phonesView: this.renPhoneInput(0)
        });
      });
    }
  }

  onChangePhoneText(text, index) {
    var phonesTemp = this.state.userInfo.phones;
    phonesTemp[index] = {
      ...phonesTemp[index],
      number: text
    };
    this.renPhoneInputsView();
  }
  renPhoneInput(index) {
    return (
      <View key={"phone" + index}>
        <InputRow
          onFocus={this.onFocusWithContext.bind(this)}
          onEndEditing={this.phoneEndEdit.bind(this)}
          title={'Số điện thoại'}
          isNumber={true}
          defaultValue={this.state.userInfo.phones[index].number}
          onChangeText={(text) => { this.onChangePhoneText(text, index) }}
          keyboardType={"phone-pad"}
          requireChar={this.state.userInfo.phones[index].active} />
        <View style={{ right: 0, position: 'absolute' }}>
          <ModelAction
            setVisibleModal={this.setVisibleModal.bind(this)}
            renderModal={this.renderModal.bind(this)}
            onSelect={this.doPhoneAction.bind(this)}
            actionList={this.state.userInfo.phones[index].active ? mainPhoneAction : phoneAction}
            index={index} />
        </View>
      </View>
    );
  }

  autoFillExistData(data) {
    if (data) {
      this.setState({
        userInfo: {
          ...data,
          phones: this.state.userInfo.phones
        }
      }, () => {
        this.loadDistrict();
      });
      this.refs.ref_md_gender.resetDefault(data.genderDisplayname);
      this.refs.ref_md_datepicker.resetDefault(data.birthday ? new Date(data.birthday) : undefined);
      this.refs.ref_mf_district.resetDefault(data.province);
      this.refs.ref_md_list_district.resetDefault(data.district);
      this.refs.ref_md_marital.resetDefault(data.maritalDisplayname);
    }

  }

  phoneEndEdit(index) {
    if (this.props.type == 'add_customer') {
      this.props.checkPhoneExist({
        code: null,
        passportId: null,
        phoneNumbers: this.state.userInfo.phones
      }, this);
    }
    // console.log(this.phoneRefs[index]._getText());
    this.onEndEditing(this);
  }
  phoneFocus(index) {
    this.onFocusWithContext(this.phoneRefs[index], this);
  }

  doPhoneAction(item, index) {
    switch (item.key) {
      case 'make_call':
        break;
      case 'send_mes':
        break;
      case 'add_more_phone':
        this.addPhone();
        break;
      case 'choose_as_main_num':
        var phonesTemp = this.state.userInfo.phones;
        var activePhoneIndex = phonesTemp.findIndex((element) => {
          return element.active;
        })
        if (activePhoneIndex != -1) {
          phonesTemp[activePhoneIndex].active = false;
        }
        phonesTemp[index].active = true;
        this.renPhoneInputsView();
        break;
      case 'delete_phone':
        this.removePhone(index);
        break;
      default:
        break;
    }
  }

  addPhone() {
    var phonesTemp = this.state.userInfo.phones;
    if (phonesTemp.length != 0) {
      if (phonesTemp[phonesTemp.length - 1].number != null && phonesTemp[phonesTemp.length - 1].number != '') {
        phonesTemp.push({
          number: '',
          type: "MOBILE",
          active: false
        });
        this.renPhoneInputsView();
      }
    } else {
      phonesTemp.push({
        number: '',
        type: "MOBILE",
        active: true
      });
      this.renPhoneInputsView();
    }
  }

  removePhone(index) {
    if (this.state.userInfo.phones.length == 1) {
      return;
    }
    var phonesTemp = this.state.userInfo.phones;
    phonesTemp.splice(index, 1);
    this.renPhoneInputsView();
  }

  ////////////////END RENDER PHONE/////////////////

  renderRightButton() {

  }

  loadDefaultData() {
    if (this.props.provinceDistrictApi.listProvinceDistrict == null || this.props.provinceDistrictApi.listProvinceDistrict.length <= 0) {
      this.props.doGetProvinceData(this.setLoading.bind(this), this.props.navigator);
    } else {
      if (this.state.userInfo.province != null) {
        this.loadDistrict();
      }
    }
    this.props.doGetEnumTypeData(this.setLoading.bind(this), this.props.navigator);
    // if (this.props.enumTypeApi.enumData == null || Object.keys(this.props.enumTypeApi.enumData).length <= 0) {
    //   this.props.doGetEnumTypeData(this.setLoading.bind(this), this.props.navigator);
    // }
  }
  setBack(id, data) {
    switch (id) {
      case setBackId.ADD_CAR:
        this.setState({
          car: data
        }, () => {
          this.props.doAdd({ customer: this.state.userInfo, car: data }, this);
          // this.props.doAdd(this.state.userInfo, this);
        });
        break;
    }
  }
  componentDidMount() {
    this.renPhoneInputsView();
    // this.loadDefaultData();
  }
}
function mapStateToProps(state) {
  return {
    provinceDistrictApi: state.provinceDistrictApi,
    enumTypeApi: state.enumTypeApi
  };
}
export default connect(mapStateToProps, { doAdd, doEdit, doGetProvinceData, doGetEnumTypeData, checkPhoneExist, doUpdateForContract })(AddCustomer);
