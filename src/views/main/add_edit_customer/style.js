import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  avatar_box: {
    alignItems: 'center',
    height: 120,
    width: '100%',
    backgroundColor: Colors.light_dark
  },
  user_info_box: {
    marginTop: -10,
    backgroundColor: 'white',
    marginBottom: 10,
    padding: 10,
    marginLeft: 10,
    marginRight: 10
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  },
  text_input: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    height: 40,
    paddingVertical: 0,
    paddingHorizontal: 0,
    marginTop: -5
  },
  require_character: {
    color: 'red',
    marginTop: 10
  },
  text_input_add_more: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    height: 40,
    marginTop: -5,
    paddingVertical: 0,
    paddingHorizontal: 0
  },
  avatar: {
    marginTop: 10,
    width: 80,
    height: 80,
    borderRadius: 40
  },
  input_row: {
    flex: 1,
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  submit: {
    marginTop: 30,
    marginBottom: 20,
    height: 35,
    width: 120,
    backgroundColor: Colors.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  btn_text: {
    color: 'white',
    fontSize: 15,
    fontFamily: "Roboto-Regular",
    // fontWeight: 'bold'
  },
  text_info: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  }
});
