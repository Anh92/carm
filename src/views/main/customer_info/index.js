import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  FlatList,
  TextInput
} from 'react-native';
import Colors from '../../../global/colors';
import ModelAction from '../../../widgets/modal_actions';
import Routes from '../../../views/routes';
import SickyScrollView from '../../../widgets/sticky_scrollview';
import { connect } from 'react-redux';
import { doGetCarList, doCloseCar } from '../../../actions/mainActions/customerInfoAction';
import Tabbar from '../../../widgets/tabbar';
import Const from '../../../global/const';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');
const flowCarActions = [{ key: 'lostCar', text: 'Xe đã mất' }]
var setBackId = Object.freeze({
  EDIT_PROFILE: 'EDIT_PROFILE',
  CUSTOMER_HIS: 'CUSTOMER_HIS'
});
const tabbarData = [{
  key: 'customerHis',
  text: 'LỊCH SỬ KH'
},
{
  key: 'service',
  text: 'DỊCH VỤ'
},
{
  key: 'cusClose',
  text: 'THÂN THIẾT'
}];

const carList = {
  lsCarContact: [{
    carGradeName: "Fortuner 2.7V 4x4",
    carColorName: "Trắng ngọc trai",
    transactionStatusDisplayname: "Tiếp cận",
    plateNumber: '60A22121',
    vin: 'VINABC009',
    engineNumber: 'ENGN9087',
    transactionStatusId: 3,
    staffName:'Nguyen Van A',
    transactionStatuses: [{ order: 1, displayName: 'Đang tiếp cận' }, { order: 2, displayName: 'Có thông tin' }, { order: 3, displayName: 'Đã đặt cọc' }, { order: 4, displayName: 'Làm giấy tờ' }, { order: 5, displayName: 'Hoàn thành' }]
  }, {
    "carGradeName": "Fortuner 2.7V 4x4",
    "carColorName": "Trắng ngọc trai",
    transactionStatusDisplayname: "Tiếp cận",
    plateNumber: '60A22121',
    vin: 'VINABC009',
    engineNumber: 'ENGN9087',
    staffName:'Nguyen Van A',
    transactionStatusId: 4,
    transactionStatuses: [{ order: 1, displayName: 'Đang tiếp cận' }, { order: 2, displayName: 'Có thông tin' }, { order: 3, displayName: 'Đã đặt cọc' }, { order: 4, displayName: 'Làm giấy tờ' }, { order: 5, displayName: 'Hoàn thành' }]
  }
  ],
  lsCarDelivered: [{
    carGradeName: "Fortuner 2.7V 4x4",
    carColorName: "Trắng ngọc trai",
    transactionStatusDisplayname: "Đang trả góp",
    plateNumber: '60A22121',
    vin: 'VINABC009',
    staffName:'Nguyen Van A',
    engineNumber: 'ENGN9087',
    transactionStatusId: 5,
    transactionStatuses: [{ order: 1, displayName: 'Đang tiếp cận' }, { order: 2, displayName: 'Có thông tin' }, { order: 3, displayName: 'Đã đặt cọc' }, { order: 4, displayName: 'Làm giấy tờ' }, { order: 5, displayName: 'Hoàn thành' }]
  }, {
    "carGradeName": "Fortuner 2.7V 4x4",
    "carColorName": "Trắng ngọc trai",
    transactionStatusDisplayname: "Đã giao xe",
    plateNumber: '60A22121',
    vin: 'VINABC009',
    staffName:'Nguyen Van A',
    engineNumber: 'ENGN9087',
    transactionStatusId: 4,
    transactionStatuses: [{ order: 1, displayName: 'Đang tiếp cận' }, { order: 2, displayName: 'Có thông tin' }, { order: 3, displayName: 'Đã đặt cọc' }, { order: 4, displayName: 'Làm giấy tờ' }, { order: 5, displayName: 'Hoàn thành' }]
  }
  ]
}
class CustomerInfo extends ChildView {
  constructor(props) {
    super(props);
    // this.props.navigator;
    this.state = {
      tabSelected: 'customerHis',
      headerMarginTop: 0,
      // customerData: this.props.customerData,
      customerData: {
        fullname: 'Nguyen Van A',
        nickname: 'A Van',
        phones: [{ active: true, number: '0167829839' }],
        email: 'nguyenvana@gmail.com'
      },
      service: [{
        title: 'Thay Dầu',
        car: 'Fortuner 2.7V 4x4 - 60A22121',
        carGradeName: "Fortuner 2.7V 4x4",
        carColorName: "Trắng ngọc trai",
        transactionStatusDisplayname: "Đang trả góp",
        plateNumber: '60A22121',
        vin: 'VINABC009',
        engineNumber: 'ENGN9087',
        transactionStatusId: 5,
        transactionStatuses: [{ order: 1, displayName: 'Đang tiếp cận' }, { order: 2, displayName: 'Có thông tin' }, { order: 3, displayName: 'Đã đặt cọc' }, { order: 4, displayName: 'Làm giấy tờ' }, { order: 5, displayName: 'Hoàn thành' }]
      }],
      topRightMenuItems: ['Gọi điện thoại', 'Nhắn tin', 'Đặt hẹn', 'Chuyển giao'],
      reasonLostCar: ''
    }
  }

  renderContainer() {
    let { customerData } = this.state;
    return (
      <SickyScrollView
        maxHeight={130}
        minHeight={40}
        isLoading={this.showLoading.bind(this)}
        onRefresh={this.onRefresh.bind(this)}
        renderHeader={this.renderScrollViewHeader.bind(this)}
        renderContent={this.renderTabView.bind(this)}
      />
    );
  }
  showLoading() {
    switch (this.state.tabSelected) {
      case 'customerHis':
        return this.props.customerInfoPage.isCarListLoading;
        break;
      case 'service':
        return false;
        break;
      case 'cusClose':
        return false;
        break;
    }
  }
  renderScrollViewHeader() {
    let { customerData } = this.state;
    return (
      <View style={{ width: '100%', height: '100%' }}>
        <TouchableOpacity style={[styles.avatar_box, { marginTop: this.state.headerMarginTop }]} activeOpacity={0.8} onPress={() => { this.openEditProfilePage() }}>
          {/* <Image source={customerData.iconProfile ? { uri: customerData.iconProfile } : require('../../../assets//images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} /> */}
          <Image source={require('../../../assets//images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} />
          <View style={styles.info_text_box}>
            <Text style={styles.user_name_text}>{customerData.fullname || customerData.nickname}</Text>
            <Text style={styles.user_phone_text}>{((customerData.phones && customerData.phones.length > 0) && this.showPhone(customerData.phones))}</Text>
            <Text style={styles.user_email_text}>{customerData.email}</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.tabbar}>
          <Tabbar
            data={tabbarData}
            fixFlex={true}
            onChange={this.onTabSelect.bind(this)} />
        </View>
      </View>);
  }
  onRefresh() {
    switch (this.state.tabSelected) {
      case 'customerHis':
        // this.props.doGetCarList(this.props.customerData.code, this);
        break;
      case 'service':

        break;
      case 'cusClose':

        break;
    }
  }

  sendTabGa() {
    if (this.props.gaTracker) {
      switch (this.state.tabSelected) {
        case 'customerHis':
          this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_history);
          break;
        case 'service':
          this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_service);
          break;
        case 'cusClose':
          this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_member);
          break;
      }
    }
  }

  showPhone(phones) {
    var activePhone = phones.filter((e) => {
      return e.active;
    });
    if (activePhone.length > 0) {
      return activePhone[0].number;
    }
    return phones[0].number;
  }

  openEditProfilePage() {
    var page = Routes.edit_customer;
    var props = {
      ...page.props,
      customerData: this.state.customerData,
      setBackId: setBackId.EDIT_PROFILE,
      setBack: this.setBack.bind(this)
    };
    this.pushPage('edit_customer', props);
  }

  onTabSelect(item) {
    this.setState({
      tabSelected: item.key
    }, () => {
      this.onRefresh();
      this.sendTabGa();
    });
  }

  renderTabView() {
    let { loadCarListError } = this.props.customerInfoPage;
    switch (this.state.tabSelected) {
      case 'customerHis':
        return (
          <View style={styles.tabview_box}>
            <TouchableOpacity style={styles.add_more_car_box} onPress={() => { this.openAddNewCarPage() }}>
              <Image source={require('../../../assets//images/add_more.png')} resizeMode={'contain'} style={{ width: 25, height: 25 }} />
              <Text style={styles.add_more_car_text}>Thêm xe</Text>
            </TouchableOpacity>
            {loadCarListError &&
              <Text style={[styles.car_status, { marginTop: 10 }]}>Tải dữ liệu không thành công, vui lòng kéo xuống để tải lại.</Text>}
            {(carList.lsCarContact != null && carList.lsCarContact.length > 0) && <View style={[styles.title_box, { marginTop: 10 }]}>
              <Text style={styles.title_text}>Xe đang theo</Text>
            </View>}
            {(carList.lsCarContact != null && carList.lsCarContact.length > 0) &&
              this.renderFolowingCar()}
            {(carList.lsCarDelivered != null && carList.lsCarDelivered.length > 0) && <View style={[styles.title_box, { marginTop: 35 }]}>
              <Text style={styles.title_text}>Xe đã có</Text>
            </View>}
            {(carList.lsCarDelivered != null && carList.lsCarDelivered.length > 0) && this.renderSoldCar()}
            {(carList.lsCarClosed != null && carList.lsCarClosed.length > 0) && <View style={[styles.title_box, { marginTop: 35 }]}>
              <Text style={styles.title_text}>Xe đã mất</Text>
            </View>}
            {(carList.lsCarClosed != null && carList.lsCarClosed.length > 0) && this.renderLostCar()}
          </View>
        );
      case 'service':
        return (
          <View style={styles.tabview_box}>
            {this.renderService()}
          </View>
        );
      case 'cusClose':
        return (
          <View style={styles.tabview_box}>
            <View style={styles.service_box}>
              <Text style={styles.car_name}>Mã thẻ</Text>
              <Text style={styles.cus_info_text}>TMD_SDFD_122392017</Text>
            </View>
            <View style={styles.service_box}>
              <Text style={styles.car_name}>Tổng điểm tích lũy</Text>
              <Text style={styles.cus_info_text}>1000</Text>
            </View>
            <View style={styles.service_box}>
              <Text style={styles.car_name}>Hạng</Text>
              <Text style={styles.cus_info_text}>Vàng</Text>
            </View>
            <TouchableOpacity style={styles.view_point_his_box} onPress={() => { this.openPointHistoryPage() }}>
              <Text style={styles.view_point_his_text}>Lịch sử tích điểm</Text>
              <Image source={require('../../../assets//images/double_arrows_right.png')} resizeMode={'contain'} style={{ width: 10, height: 10, tintColor: Colors.dark_pink, marginLeft: 5 }} />
            </TouchableOpacity>
          </View>
        );
    }
  }


  openPointHistoryPage() {
    this.pushPage('point_history');
  }

  /////////////////////////RENDER customerHis TAB//////////////////////////
  openAddNewCarPage() {
    var page = Routes.add_new_car;
    var props = {
      ...page.props,
      // customerSubProfileId: this.props.customerData.customerSubProfileId,
      // setBackId: setBackId.CUSTOMER_HIS,
      // setBack: this.setBack.bind(this)
    };
    this.pushPage('add_new_car', props);
  }
  renderFolowingCar() {
    let { lsCarContact } = carList;
    var views = [];
    for (var i = 0; i < lsCarContact.length; i++) {
      views.push(
        this.renderFolowCarRows(lsCarContact[i], i)
      );
    }
    return views;
  }
  renderFolowCarRows(item, index) {
    return (
      <TouchableOpacity key={"flow-car" + index} style={styles.car_box} onPress={() => { this.openFolowCarInfoPage(item) }}>
        <Text style={styles.car_name}>{item.carGradeName + " - " + item.carColorName}</Text>
        <Text style={styles.car_status}>{item.carColorName}</Text>
        <Text style={styles.staff_name}>{item.transactionStatusDisplayname}</Text>
        <View style={{ right: 0, top: 5, position: 'absolute' }}>
          <ModelAction
            setVisibleModal={this.setVisibleModal.bind(this)}
            renderModal={this.renderModal.bind(this)}
            onSelect={this.doFolowCarActions.bind(this)}
            actionList={flowCarActions}
            dataBack={item.subProfileCarInfoId} />
        </View>
      </TouchableOpacity>
    );
  }

  openFolowCarInfoPage(item) {
    var page = Routes.folow_car_info;
    var props = {
      ...page.props,
      carDetail: item,
      headerText: item.carGradeName,
      setBackId: setBackId.CUSTOMER_HIS,
      setBack: this.setBack.bind(this)
    }
    this.pushPage('folow_car_info', props);
  }

  renderLostCar() {
    let { lsCarClosed } = this.props.customerInfoPage.carList;
    var views = [];
    for (var i = 0; i < lsCarClosed.length; i++) {
      views.push(
        this.renderLostCarRows(lsCarClosed[i], i)
      );
    }
    return views;
  }
  renderLostCarRows(item, index) {
    return (
      <TouchableOpacity key={"lost-car" + index} style={styles.car_box} onPress={() => { this.openLostCarInfoPage(item) }}>
        <Text style={styles.car_name}>{item.carGradeName + " - " + item.carColorName}</Text>
        <Text style={styles.staff_name}>{item.carColorName}</Text>
      </TouchableOpacity>
    );
  }
  openLostCarInfoPage(item) {
    var page = Routes.lost_car_info;
    var props = {
      ...page.props,
      carDetail: item,
      headerText: item.carGradeName,
      setBackId: setBackId.CUSTOMER_HIS,
      setBack: this.setBack.bind(this)
    }
    this.pushPage('lost_car_info', props);
  }

  renderSoldCar() {
    let { lsCarDelivered } = carList;
    var views = [];
    for (var i = 0; i < lsCarDelivered.length; i++) {
      views.push(
        this.renderSoldCarRows(lsCarDelivered[i], i)
      );
    }
    return views;
  }
  renderSoldCarRows(item, index) {
    return (
      <TouchableOpacity key={"sold-car" + index} style={styles.car_box} onPress={() => { this.openCarInfoPage(item) }}>
        <Text style={styles.car_name}>{item.carGradeName + " - " + item.carColorName}</Text>
        <Text style={styles.car_status}>{item.carColorName}</Text>
        <Text style={styles.staff_name}>{item.transactionStatusDisplayname}</Text>
      </TouchableOpacity>
    );
  }
  openCarInfoPage(item) {
    var page = Routes.car_info;
    var props = {
      ...page.props,
      carDetail: item,
      headerText: item.carGradeName,
      setBackId: setBackId.CUSTOMER_HIS,
      setBack: this.setBack.bind(this)
    }
    this.pushPage('car_info', props);
  }
  doFolowCarActions(item, carInfoId) {
    switch (item.key) {
      case 'lostCar':
        this.setState({ reasonLostCar: '' });
        this.renderModal(
          <View style={styles.modal_box}>
            <View style={styles.input_row}>
              <Text style={styles.text_title}>Lý do mất khách hàng</Text>
              <TextInput
                onChangeText={(text) => this.setState({ reasonLostCar: text })}
                underlineColorAndroid={'transparent'}
                style={styles.text_input}
                placeholderTextColor={'#989899'} />
            </View>
            <View style={styles.btn_box}>
              <TouchableOpacity
                onPress={() => { this.setVisibleModal(false) }}
                style={[styles.submit, { backgroundColor: "#E4E4E4", marginRight: 30 }]}
                underlayColor='transparent'>
                <Text style={[styles.btn_text, { color: Colors.dark }]}>Hủy</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => { this.doCloseCar(carInfoId) }}
                style={styles.submit}
                underlayColor='transparent'>
                <Text style={styles.btn_text}>Lưu</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
        this.setVisibleModal(true);
        break;
      case 'deleCar':
        console.log('delete car id: ' + item);
        // var flowingCar = this.state.flowingCar;
        // flowingCar.splice(index, 1);
        break;
      default:
        break;
    }
  }

  doCloseCar(carInfoId) {
    if (this.state.reasonLostCar != null && this.state.reasonLostCar != '') {
      this.props.doCloseCar({
        customerSubProfileCarInfoId: carInfoId,
        causalClosed: this.state.reasonLostCar
      }, this);
      this.setVisibleModal(false);
    } else {
      this.renderPopup(null, null, 'Vui lòng nhập lý do');
    }
  }

  /////////////////////////END RENDER customerHis TAB//////////////////////////

  /////////////////////////RENDER service TAB//////////////////////////
  renderService() {
    var views = [];
    for (var i = 0; i < this.state.service.length; i++) {
      views.push(
        this.renderServiceRow(i)
      );
    }
    return views;
  }
  renderServiceRow(i) {
    return (
      <TouchableOpacity key={'service-row' + i} style={styles.service_box} onPress={() => { this.openCarServiceInfoPage(this.state.service[i]) }}>
        <Text style={styles.car_name}>{this.state.service[i].title}</Text>
        <Text style={styles.car_status}>{this.state.service[i].car}</Text>
      </TouchableOpacity>
    );
  }
  openCarServiceInfoPage(item) {
    var page = Routes.service_car_info;
    var props = {
      ...page.props,
      carDetail: item,
      headerText: item.carGradeName,
      setBackId: setBackId.CUSTOMER_HIS,
      setBack: this.setBack.bind(this)
    }
    this.pushPage('service_car_info', props);
  }
  /////////////////////////END RENDER service TAB//////////////////////////

  setBack(id, data) {
    switch (id) {
      case setBackId.EDIT_PROFILE:
        this.setState({
          customerData: data
        });
        break;
      case setBackId.CUSTOMER_HIS:
        // this.props.doGetCarList(this.props.customerData.code, this);
        break;
    }
  }
  // onBack() {
  //   this.props.navigator.pop();
  //   if (this.props.setBack)
  //   this.props.setBack(this.props.setBackId, null);
  // }
  componentDidMount() {
    // this.props.doGetCarList(this.props.customerData.code, this);
  }
  componentWillUnmount() {
    if (this.props.setBack)
      this.props.setBack(this.props.setBackId, null);
  }
}

function mapStateToProps(state) {
  return { customerInfoPage: state.customerInfoPage };
}

export default connect(mapStateToProps, { doGetCarList, doCloseCar })(CustomerInfo);