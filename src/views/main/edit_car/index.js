import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  FlatList
} from 'react-native';
import Colors from '../../../global/colors';
import Routes from '../../../views/routes';
import ModalDropdown from '../../../widgets/modal_dropdown';
import { connect } from 'react-redux';
import { doEditCar } from '../../../actions/mainActions/editCarAction';
import { doGetCarBrandsData } from '../../../actions/apiActions/carBrandsApiAction';
import InputRow from '../../../widgets/input_row';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');

class EditCar extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      carInfo: {
        id: this.props.carDetail.id,
        isNew: this.props.carDetail.isNew,
        carBrandId: this.props.carDetail.carBrandId,
        carModelId: this.props.carDetail.carModelId,
        carGradeId: this.props.carDetail.carGradeId,
        carColorId: this.props.carDetail.carColorId,
        carBrandName: this.props.carDetail.carBrandName,
        carModelName: this.props.carDetail.carModelName,
        carGradeName: this.props.carDetail.carGradeName,
        carColorName: this.props.carDetail.carColorName,
        vin: this.props.carDetail.vin,
        engineNumber: this.props.carDetail.engineNumber,
        plateNumber: this.props.carDetail.plateNumber,
      },
      carModels: [],
      carGrades: [],
      carColors: [],
      firstOpen: true
    }
  }

  renderContainer() {
    let carBrandsData = this.props.carBrandsApi.carBrandsData;
    return (
      <ScrollView>
        <View style={styles.tabview_box}>
          {this.state.carInfo.isNew && (
            <View>
              <ModalDropdown
                options={carBrandsData}
                mStyle={1}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadCarBrandsData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                onSelect={this.onCarBrandsChange.bind(this)}
                defaultValue={this.state.carInfo.carBrandName}
                requireChar={true}
                title={'Thương hiệu'} />
              <ModalDropdown
                ref='ref_md_car_models'
                options={this.state.carModels}
                mStyle={1}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadCarBrandsData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                onSelect={this.onCarModelChange.bind(this)}
                defaultValue={this.state.carInfo.carModelName}
                title={'Mẫu xe'}
                requireChar={true} />
              <ModalDropdown
                ref='ref_md_car_grades'
                options={this.state.carGrades}
                mStyle={1}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadCarBrandsData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                onSelect={this.onCarGradesChange.bind(this)}
                defaultValue={this.state.carInfo.carGradeName}
                title={'Dòng xe'}
                requireChar={true} />
              <ModalDropdown
                ref='ref_md_car_colors'
                options={this.state.carColors}
                mStyle={1}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadCarBrandsData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                onSelect={(item) => this.setState({ carInfo: { ...this.state.carInfo, carColorId: item.id, carColorName: item.name } })}
                defaultValue={this.state.carInfo.carColorName}
                title={'Màu sắc'}
                requireChar={true} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Biển số'}
                onChangeText={(text) => this.setState({ carInfo: { ...this.state.carInfo, plateNumber: text } })}
                defaultValue={this.state.carInfo.plateNumber} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Số khung'}
                onChangeText={(text) => this.setState({ carInfo: { ...this.state.carInfo, vin: text } })}
                defaultValue={this.state.carInfo.vin} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Số máy'}
                onChangeText={(text) => this.setState({ carInfo: { ...this.state.carInfo, engineNumber: text } })}
                defaultValue={this.state.carInfo.engineNumber} />
            </View>)}
          {!this.state.carInfo.isNew && (
            <View>
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Dòng xe'}
                requireChar={true}
                onChangeText={(text) => this.setState({ carInfo: { ...this.state.carInfo, carGradeName: text } })}
                defaultValue={this.state.carInfo.carGradeName} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Biển số'}
                onChangeText={(text) => this.setState({ carInfo: { ...this.state.carInfo, plateNumber: text } })}
                defaultValue={this.state.carInfo.plateNumber} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Số khung'}
                onChangeText={(text) => this.setState({ carInfo: { ...this.state.carInfo, vin: text } })}
                defaultValue={this.state.carInfo.vin} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Số máy'}
                onChangeText={(text) => this.setState({ carInfo: { ...this.state.carInfo, engineNumber: text } })}
                defaultValue={this.state.carInfo.engineNumber} />
            </View>)}
          <TouchableOpacity
            style={styles.submit}
            onPress={() => { this.onSubmit() }}
            underlayColor='transparent'>
            <Text style={styles.btn_text}>Lưu</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
  onCarBrandsChange(item) {
    this.setState({
      carInfo: {
        ...this.state.carInfo,
        carBrandId: item.id,
        carBrandName: item.name,
        carModelId: null,
        carModelName: null,
        carGradeId: null,
        carGradeName: null,
        carColorId: null,
        carColorName: null
      }, carModels: item.carModels
    }, () => {
      this.refs.ref_md_car_models.resetDefault();
      this.refs.ref_md_car_grades.resetDefault();
      this.refs.ref_md_car_colors.resetDefault();
    });

  }
  onCarModelChange(item) {
    this.setState({
      carInfo: {
        ...this.state.carInfo,
        carModelId: item.id,
        carModelName: item.name,
        carGradeId: null,
        carGradeName: null,
        carColorId: null,
        carColorName: null
      }, carGrades: item.carGrades
    }, () => {
      this.refs.ref_md_car_grades.resetDefault();
      this.refs.ref_md_car_colors.resetDefault();
    });
  }
  onCarGradesChange(item) {
    this.setState({
      carInfo: {
        ...this.state.carInfo,
        carGradeId: item.id,
        carGradeName: item.name,
        carColorId: null,
        carColorName: null
      }, carColors: item.colors
    }, () => {
      this.refs.ref_md_car_colors.resetDefault();
    });
  }
  onSubmit() {
    if (this.state.carInfo.isNew) {
      if (this.state.carInfo.carBrandName == null || this.state.carInfo.carBrandName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn thương hiệu');
        return;
      }
      if (this.state.carInfo.carModelName == null || this.state.carInfo.carModelName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn mẫu xe');
        return;
      }
      if (this.state.carInfo.carGradeName == null || this.state.carInfo.carGradeName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn dòng xe');
        return;
      }
      if (this.state.carInfo.carColorName == null || this.state.carInfo.carColorName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn màu xe');
        return;
      }
    }
    else {
      if (this.state.carInfo.carGradeName == null || this.state.carInfo.carGradeName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập dòng xe');
        return;
      }
      if (this.state.carInfo.plateNumber == null || this.state.carInfo.plateNumber == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập biển số');
        return;
      }
    }
    // console.log(this.state.carInfo)
    this.props.doEditCar(this.state.carInfo, this);
  }

  renderRightButton() {
  }
  componentDidUpdate() {
    let carBrandsData = this.props.carBrandsApi.carBrandsData;
    if (carBrandsData != null && carBrandsData.length > 0 && this.state.firstOpen) {
      this.loadDropdownDefault();
    }
  }
  loadDropdownDefault() {
    let carBrandsData = this.props.carBrandsApi.carBrandsData;
    ////FIND Car Model
    var carModelIndex = carBrandsData.findIndex((element) => {
      if (this.state.carInfo.carBrandName == element.name) {
        return true;
      }
      return false;
    });
    var carModelsTemp = [];
    if (carModelIndex != -1) {
      carModelsTemp = carBrandsData[carModelIndex].carModels;
    }
    ///FIND Car Grade
    var carGradeIndex = carModelsTemp.findIndex((element) => {
      if (this.state.carInfo.carModelName == element.name) {
        return true;
      }
      return false;
    });
    var carGradesTemp = [];
    if (carGradeIndex != -1) {
      carGradesTemp = carModelsTemp[carGradeIndex].carGrades;
    }
    ///FIND car Color
    var carColorIndex = carGradesTemp.findIndex((element) => {
      if (this.state.carInfo.carGradeName == element.name) {
        return true;
      }
      return false;
    });
    var carColorsTemp = [];
    if (carColorIndex != -1) {
      carColorsTemp = carGradesTemp[carColorIndex].colors;
    }
    ///Save sate
    this.setState({
      carModels: carModelsTemp,
      carGrades: carGradesTemp,
      carColors: carColorsTemp,
      firstOpen: false
    });
  }
  loadCarBrandsData() {
    let carBrandsData = this.props.carBrandsApi.carBrandsData;
    if (carBrandsData == null || carBrandsData.length <= 0) {
      this.props.doGetCarBrandsData(this.setLoading.bind(this), this.props.navigator);
    } else {
      this.loadDropdownDefault()
    }
  }
  componentDidMount() {
    this.loadCarBrandsData();
  }
}
function mapStateToProps(state) {
  return {
    carBrandsApi: state.carBrandsApi
  };
}
export default connect(mapStateToProps, { doEditCar, doGetCarBrandsData })(EditCar);