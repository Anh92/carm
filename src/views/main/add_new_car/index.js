import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  FlatList
} from 'react-native';
import HeaderTick from '../../../widgets/header_tick';
import Colors from '../../../global/colors';
import Routes from '../../../views/routes';
import ModalDropdown from '../../../widgets/modal_dropdown';
import { connect } from 'react-redux';
import { doAddNewCar } from '../../../actions/mainActions/addNewCarAction';
import { doGetEnumTypeData } from '../../../actions/apiActions/enumTypeApiAction';
import { doGetCarBrandsData } from '../../../actions/apiActions/carBrandsApiAction';
import InputRow from '../../../widgets/input_row';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');

class AddNewCar extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      carInfo: {
        customerSubProfileId: this.props.customerSubProfileId,
        isNew: true,
        carBrandId: null,
        carModelId: null,
        carBrandName: null,
        carModelName: null,
        carGradeId: null,
        carColorId: null,
        carGradeName: null,
        carColorName: null,
        plateNumber: null,
        demands: null
      },
      carModels: [],
      carGrades: [],
      carColors: []
    }
    console.log("this.state.customerSubProfileId " + this.props.customerSubProfileId);
  }

  renderContainer() {
    let { demands } = this.props.enumTypeApi.enumData;
    let carBrandsData = this.props.carBrandsApi.carBrandsData;
    let departmentSlugIndex = this.props.userContext.departmentSlug ? this.props.userContext.departmentSlug.indexOf("kinh-doanh") : -1;
    return (
      <ScrollView>
        <View style={styles.tabview_box}>
          <HeaderTick
            ref='ref_ht_new_car'
            title={"Xe mới"}
            defaultValue={this.state.carInfo.isNew}
            onChange={(value) => { this.resetData(true) }} />
          {this.state.carInfo.isNew && (
            <View>
              <ModalDropdown
                // options={carBrandsData}
                options={['Thương hiệu 1', 'Thương hiệu 2', 'Thương hiệu 3', 'Thương hiệu 4', 'Thương hiệu 5', 'Thương hiệu 6', 'Thương hiệu 7', 'Thương hiệu 8', 'Thương hiệu 31', 'Thương hiệu 32', 'Thương hiệu 33']}
                isNormalArray={true}
                mStyle={1}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadCarBrandsData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                onSelect={this.onCarBrandsChange.bind(this)}
                title={'Thương hiệu'}
                requireChar={true} />
              <ModalDropdown
                ref='ref_md_car_models'
                // options={this.state.carModels}
                options={['Mẫu xe 1', 'Mẫu xe 2', 'Mẫu xe 3', 'Mẫu xe 4', 'Mẫu xe 5', 'Mẫu xe 6', 'Mẫu xe 7', 'Mẫu xe 8', 'Mẫu xe 31', 'Mẫu xe 32', 'Mẫu xe 33']}
                mStyle={1}
                isNormalArray={true}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadCarBrandsData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                onSelect={this.onCarModelChange.bind(this)}
                title={'Mẫu xe'}
                requireChar={true} />
              <ModalDropdown
                ref='ref_md_car_grades'
                // options={this.state.carGrades}
                options={['Dòng xe 1', 'Dòng xe 2', 'Dòng xe 3', 'Dòng xe 4', 'Dòng xe 5', 'Dòng xe 6', 'Dòng xe 7', 'Dòng xe 8', 'Dòng xe 31', 'Dòng xe 32', 'Dòng xe 33']}
                mStyle={1}
                isNormalArray={true}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadCarBrandsData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                onSelect={this.onCarGradesChange.bind(this)}
                title={'Dòng xe'}
                requireChar={true} />
              <ModalDropdown
                ref='ref_md_car_colors'
                // options={this.state.carColors}
                options={['Xanh', 'Đỏ', 'Tím', 'Vàng']}
                isNormalArray={true}
                mStyle={1}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadCarBrandsData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                onSelect={(item) => this.setState({ carInfo: { ...this.state.carInfo, carColorId: item.id, carColorName: item.name } })}
                title={'Màu sắc'}
                requireChar={true} />
              {/* {(departmentSlugIndex != -1) &&  */}
              <ModalDropdown
                // options={demands}
                mStyle={1}
                options={['Nhu cầu 1', 'Nhu cầu 2', 'Nhu cầu 3', 'Nhu cầu 4', 'Nhu cầu 5', 'Nhu cầu 6', 'Nhu cầu 7', 'Nhu cầu 8', 'Nhu cầu 31', 'Nhu cầu 32', 'Nhu cầu 33']}
                isNormalArray={true}
                // keyItems={'displayName'}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadEnumData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                requireChar={true}
                onSelect={(item) => this.setState({ carInfo: { ...this.state.carInfo, demands: item.name } })}
                title={'Nhu cầu khách hàng'} />
              {/* } */}
            </View>)}

          <HeaderTick
            ref='ref_ht_old_car'
            title={"Xe cũ"}
            marginTop={30}
            defaultValue={!this.state.carInfo.isNew}
            onChange={(value) => { this.resetData(false) }} />
          {!this.state.carInfo.isNew && (
            <View>
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Dòng xe'}
                onChangeText={(text) => { this.setState({ carInfo: { ...this.state.carInfo, carGradeName: text } }) }}
                requireChar={true} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Biển số'}
                onChangeText={(text) => { this.setState({ carInfo: { ...this.state.carInfo, plateNumber: text } }) }}
                requireChar={true} />
              {(departmentSlugIndex != -1) && <ModalDropdown
                options={demands}
                mStyle={1}
                keyItems={'displayName'}
                renderModal={this.renderModal.bind(this)}
                reloadData={this.loadEnumData.bind(this)}
                setVisibleModal={this.setVisibleModal.bind(this)}
                requireChar={true}
                onSelect={(item) => this.setState({ carInfo: { ...this.state.carInfo, demands: item.name } })}
                title={'Nhu cầu khách hàng'} />}
            </View>)}

          <TouchableOpacity
            style={styles.submit}
            onPress={() => { this.onSubmit() }}
            underlayColor='transparent'>
            <Text style={styles.btn_text}>Lưu</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }

  resetData(isNewCar) {
    this.setState({
      carInfo: {
        customerSubProfileId: this.props.customerSubProfileId,
        isNew: isNewCar,
        carBrandId: null,
        carModelId: null,
        carBrandName: null,
        carModelName: null,
        carGradeId: null,
        carColorId: null,
        carGradeName: null,
        carColorName: null,
        plateNumber: null,
        demands: null
      },
      carModels: [],
      carGrades: [],
      carColors: []
    });
    this.refs.ref_ht_new_car.setValue(isNewCar);
    this.refs.ref_ht_old_car.setValue(!isNewCar);
  }
  onCarBrandsChange(item) {
    this.setState({
      carInfo: {
        ...this.state.carInfo,
        carBrandId: item.id,
        carBrandName: item.name,
        carModelId: null,
        carModelName: null,
        carGradeId: null,
        carGradeName: null,
        carColorId: null,
        carColorName: null
      },
      carModels: item.carModels,
      carGrades: [],
      carColors: []
    }, () => {
      this.refs.ref_md_car_models.resetDefault();
      this.refs.ref_md_car_grades.resetDefault();
      this.refs.ref_md_car_colors.resetDefault();
    });
  }
  onCarModelChange(item) {
    this.setState({
      carInfo: {
        ...this.state.carInfo,
        carModelId: item.id,
        carModelName: item.name,
        carGradeId: null,
        carGradeName: null,
        carColorId: null,
        carColorName: null
      },
      carGrades: item.carGrades,
      carColors: []
    }, () => {
      this.refs.ref_md_car_grades.resetDefault();
      this.refs.ref_md_car_colors.resetDefault();
    });
  }
  onCarGradesChange(item) {
    this.setState({
      carInfo: {
        ...this.state.carInfo,
        carGradeId: item.id,
        carGradeName: item.name,
        carColorId: null,
        carColorName: null
      },
      carColors: item.colors
    }, () => {
      this.refs.ref_md_car_colors.resetDefault();
    });
  }

  onSubmit() {
    let departmentSlugIndex = this.props.userContext.departmentSlug ? this.props.userContext.departmentSlug.indexOf("kinh-doanh") : -1;
    if (this.state.carInfo.isNew) {
      if (this.state.carInfo.carBrandName == null || this.state.carInfo.carBrandName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn thương hiệu');
        return;
      }
      if (this.state.carInfo.carModelName == null || this.state.carInfo.carModelName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn mẫu xe');
        return;
      }
      if (this.state.carInfo.carGradeName == null || this.state.carInfo.carGradeName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn dòng xe');
        return;
      }
      if (this.state.carInfo.carColorName == null || this.state.carInfo.carColorName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn màu xe');
        return;
      }
      if ((this.state.carInfo.demands == null || this.state.carInfo.demands == '') && departmentSlugIndex != -1) {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn nhu cầu khách hàng');
        return;
      }
    } else {
      if (this.state.carInfo.carGradeName == null || this.state.carInfo.carGradeName == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập dòng xe');
        return;
      }
      if (this.state.carInfo.plateNumber == null || this.state.carInfo.plateNumber == '') {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng nhập biển số');
        return;
      }

      if ((this.state.carInfo.demands == null || this.state.carInfo.demands == '') && departmentSlugIndex != -1) {
        this.renderPopup(null, 'Thiếu trường bắt buộc', 'Vui lòng chọn nhu cầu khách hàng');
        return;
      }
    }
    this.goBack();

    // console.log(this.state.carInfo);
    // if (this.props.type == 'add_customer') {
    //   this.props.setBack(this.props.setBackId, this.state.carInfo);
    //   this.goBack();
    // } else {
    //   this.props.doAddNewCar(this.state.carInfo, this);
    // }
  }

  renderRightButton() {
  }

  loadEnumData() {
    this.props.doGetEnumTypeData(this.setLoading.bind(this), this.props.navigator);
  }
  loadCarBrandsData() {
    if (this.props.carBrandsApi.carBrandsData.length <= 0) {
      this.props.doGetCarBrandsData(this.setLoading.bind(this), this.props.navigator);
    }
  }
  componentDidMount() {
    // this.loadEnumData();
    // this.loadCarBrandsData();
  }
}
function mapStateToProps(state) {
  return {
    enumTypeApi: state.enumTypeApi,
    carBrandsApi: state.carBrandsApi,
    userContext: state.userContext
  };
}
export default connect(mapStateToProps, { doAddNewCar, doGetEnumTypeData, doGetCarBrandsData })(AddNewCar);