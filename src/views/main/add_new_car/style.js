import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  tabview_box: {
    padding: 20,
    paddingTop: 10
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  },
  text_input: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    height: 40,
    paddingVertical: 0,
    paddingHorizontal: 0,
    marginTop: -5
  },
  submit: {
    marginTop: 30,
    marginBottom: 20,
    height: 35,
    width: 120,
    backgroundColor: Colors.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  btn_text: {
    color: 'white',
    fontSize: 15,
    fontFamily: "Roboto-Regular"
  },
  row_image_right: {
    position: 'absolute',
    right: 8,
    width: 20,
    height: 20,
  },
  title_box: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
    marginTop: 10
  },
  require_character: {
    color: 'red',
    marginTop: 10
  },
  input_row: {
    flex: 1,
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  title_text:{
    color: Colors.dark,
    fontSize: 16,
    fontFamily: "Roboto-Regular"
  }
});
