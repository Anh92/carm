import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Switch from '../../../widgets/switch';
import HeaderTick from '../../../widgets/header_tick';
import Colors from '../../../global/colors';
import ModalDropdown from '../../../widgets/modal_dropdown';
import ModalFilterDropdown from '../../../widgets/modal_filter_dropdown';
import { connect } from 'react-redux';
import { doGetEnumTypeData } from '../../../actions/apiActions/enumTypeApiAction';
import { doGetProvinceData } from '../../../actions/apiActions/provinceDistrictApiAction';
import { doGetCarBrandsData } from '../../../actions/apiActions/carBrandsApiAction';

var Global = require('../../../global/const');
var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');

class CustomerFilter extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      customerNeedId: -1,
      filterData: {
        province: null,
        demandSelected: null
      },
      listDistrict: [],
      carGrades: [],
      carModels: [],
      customerNeedTick: false
    }
  }

  renderContainer() {
    let { listProvinceDistrict } = this.props.provinceDistrictApi;
    let carBrandsData = this.props.carBrandsApi.carBrandsData;
    return (
      <ScrollView>
        <View style={styles.container}>
          <HeaderTick
            mStyle={0}
            title={"Nhu cầu khách hàng"}
            defaultValue={this.state.customerNeedTick}
            onChange={(value) => { this.setState({ customerNeedTick: value }); }} />
          <View style={styles.cus_select_box}>
            {this.renderSelectItems()}
          </View>
          <HeaderTick
            mStyle={0}
            marginTop={30}
            title={"Dòng xe"}
            onChange={(value) => { alert(value) }} />
          <ModalDropdown
            ref='ref_md_car_brand'
            // options={carBrandsData}
            options={['Thương hiệu 1', 'Thương hiệu 2', 'Thương hiệu 3', 'Thương hiệu 4', 'Thương hiệu 5', 'Thương hiệu 6', 'Thương hiệu 7', 'Thương hiệu 8', 'Thương hiệu 31', 'Thương hiệu 32', 'Thương hiệu 33']}
            isNormalArray={true}
            mStyle={1}
            renderModal={this.renderModal.bind(this)}
            reloadData={this.loadCarBrandsData.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            onSelect={this.onCarBrandsChange.bind(this)}
            title={'Thương hiệu'} />
          <ModalDropdown
            ref='ref_md_car_model'
            // options={this.state.carModels}
            options={['Mẫu xe 1', 'Mẫu xe 2', 'Mẫu xe 3', 'Mẫu xe 4', 'Mẫu xe 5', 'Mẫu xe 6', 'Mẫu xe 7', 'Mẫu xe 8', 'Mẫu xe 31', 'Mẫu xe 32', 'Mẫu xe 33']}
            isNormalArray={true}
            mStyle={1}
            renderModal={this.renderModal.bind(this)}
            reloadData={this.loadCarBrandsData.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            onSelect={this.onCarModelChange.bind(this)}
            title={'Mẫu xe'} />
          <ModalDropdown
            ref='ref_md_car_grade'
            options={['Dòng xe 1', 'Dòng xe 2', 'Dòng xe 3', 'Dòng xe 4', 'Dòng xe 5', 'Dòng xe 6', 'Dòng xe 7', 'Dòng xe 8', 'Dòng xe 31', 'Dòng xe 32', 'Dòng xe 33']}
            // options={this.state.carGrades}
            mStyle={1}
            isNormalArray={true}
            renderModal={this.renderModal.bind(this)}
            reloadData={this.loadCarBrandsData.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            title={'Dòng xe'} />
          <HeaderTick
            mStyle={0}
            marginTop={30}
            title={"Địa điểm"}
            onChange={(value) => { }} />
          <ModalFilterDropdown
            ref='ref_md_city'
            options={['Tinh 1', 'Tinh 2', 'Tinh 3', 'Tinh 4', 'Tinh 5', 'Tinh 6', 'Tinh 7', 'Tinh 8', 'Tinh 31', 'Tinh 32', 'Tinh 33']}
            // options={listProvinceDistrict}
            // keysFilter={['name']}
            isNormalArray={true}
            reloadData={this.loadDefaultData.bind(this)}
            renderModal={this.renderModal.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            onSelect={this.onCityChange.bind(this)}
            defaultValue={this.state.filterData.province}
            title={'Tỉnh thành'} />
          <ModalFilterDropdown
            ref='ref_md_district'
            options={['Quan 1', 'Quan 2', 'Quan 3', 'Quan 4', 'Quan 5', 'Quan 6', 'Quan 7', 'Quan 8', 'Quan 31', 'Quan 32', 'Quan 33']}
            // options={this.state.listDistrict}
            // keysFilter={['name']}
            isNormalArray={true}
            reloadData={this.loadDistrict.bind(this)}
            renderModal={this.renderModal.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            onSelect={(type, item) => this.setState({ filterData: { ...this.state.filterData, district: (type == 1) ? item.name : null } })}
            defaultValue={this.state.filterData.district}
            title={'Quận huyện'} />
          <HeaderTick
            mStyle={0}
            marginTop={30}
            title={"Nhân viên"}
            onChange={(value) => { }} />
          <ModalFilterDropdown
            ref='ref_md_staff'
            options={['Tu van vien 1', 'Tu van vien 2', 'Tu van vien 3', 'Tu van vien 4', 'Tu van vien 5', 'Tu van vien 6', 'Tu van vien 7', 'Tu van vien 8', 'Tu van vien 31', 'Tu van vien 32', 'Tu van vien 33']}
            isNormalArray={true}
            renderModal={this.renderModal.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            title={'Tư vấn bán hàng'} />
          <View style={styles.btn_box}>
            <TouchableOpacity
              onPress={() => { this.onCancel() }}
              style={[styles.submit, { backgroundColor: "#E4E4E4", marginRight: 40 }]}
              underlayColor='transparent'>
              <Text style={[styles.btn_text, { color: Colors.dark }]}>Hủy</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => { this.onSetBack() }}
              style={styles.submit}
              underlayColor='transparent'>
              <Text style={styles.btn_text}>Lọc</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }

  onCityChange(type, item) {
    this.setState({
      listDistrict: (type == 1) ? item.districts : null
    }, () => {
      this.refs.ref_md_district.resetDefault();
    });
  }

  onCarBrandsChange(item) {
    this.setState({
      carModels: item.carModels,
      carGrades: [],
    }, () => {
      this.refs.ref_md_car_model.resetDefault();
      this.refs.ref_md_car_grade.resetDefault();
    });
  }
  onCarModelChange(item) {
    this.setState({
      carGrades: item.carGrades,
    }, () => {
      this.refs.ref_md_car_grade.resetDefault();
    });
  }

  renderSelectItems() {
    // let { demands } = this.props.enumTypeApi.enumData;
    let demands = [{
      displayName: 'Nhu Cau 1',
      name: 'nhucau1'
    }, {
      displayName: 'Nhu Cau 2',
      name: 'nhucau2'
    }, {
      displayName: 'Nhu Cau 3',
      name: 'nhucau3'
    }, {
      displayName: 'Nhu Cau 4',
      name: 'nhucau4'
    }, {
      displayName: 'Nhu Cau 5 dai dai dai dai',
      name: 'nhucau5'
    }]
    if (demands != null && demands != undefined) {
      var views = [];
      for (var i = 0; i < demands.length; i++) {
        views.push(this.renderSelectRow(demands[i], i));
      }
      return views;
    }
  }
  renderSelectRow(item, index) {
    return (
      <TouchableOpacity key={'customer-demand' + index} style={styles.select_item} onPress={() => { this.setState({ filterData: { ...this.state.filterData, demandSelected: item.name } }) }}>
        <Image source={this.state.filterData.demandSelected == item.name ? require('../../../assets//images/single_choice_ac.png') : require('../../../assets//images/single_choice_unac.png')} resizeMode={'contain'} style={styles.select_icon} />
        <Text style={styles.select_text}>{item.displayName}</Text>
      </TouchableOpacity>);
  }

  onCancel() {
    this.setState({
      filterData: {
        province: null,
        demandSelected: null
      },
      listDistrict: [],
      carGrades: [],
      carModels: []
    }, () => {
      this.refs.ref_md_car_brand.resetDefault();
      this.refs.ref_md_car_model.resetDefault();
      this.refs.ref_md_car_grade.resetDefault();
      this.refs.ref_md_city.resetDefault();
      this.refs.ref_md_district.resetDefault();
      this.refs.ref_md_staff.resetDefault();
    });

  }

  renderRightButton() {

  }
  onSetBack() {
    this.goBack();
    if (this.props.setBack) {
      this.props.setBack("df");
    }
  }
  loadDistrict() {
    var listProvinceDistrict = this.props.provinceDistrictApi.listProvinceDistrict;
    if (listProvinceDistrict == null || listProvinceDistrict.length <= 0) {
      this.props.doGetProvinceData(this.setLoading.bind(this), this.props.navigator);
    }
    //  else {
    //   var provinceIndex = listProvinceDistrict.findIndex((element) => {
    //     if (this.state.filterData.province == element.name) {
    //       return true;
    //     }
    //     return false;
    //   })
    //   if (provinceIndex != -1) {
    //     this.setState({
    //       listDistrict: listProvinceDistrict[provinceIndex].districts
    //     });
    //   }
    // }
  }
  loadDefaultData() {
    if (this.props.provinceDistrictApi.listProvinceDistrict == null || this.props.provinceDistrictApi.listProvinceDistrict <= 0) {
      this.props.doGetProvinceData(this.setLoading.bind(this), this.props.navigator);
    } else {
      if (this.state.filterData.province != null) {
        this.loadDistrict();
      }
    }
    this.props.doGetEnumTypeData(this.setLoading.bind(this), this.props.navigator);
  }
  loadCarBrandsData() {
    if (this.props.carBrandsApi.carBrandsData.length <= 0) {
      this.props.doGetCarBrandsData(this.setLoading.bind(this), this.props.navigator);
    }
  }
  componentDidMount() {
    // this.loadDefaultData();
    // this.loadCarBrandsData();
  }
}
function mapStateToProps(state) {
  return {
    provinceDistrictApi: state.provinceDistrictApi,
    enumTypeApi: state.enumTypeApi,
    carBrandsApi: state.carBrandsApi
  };
}
export default connect(mapStateToProps, { doGetProvinceData, doGetEnumTypeData, doGetCarBrandsData })(CustomerFilter);