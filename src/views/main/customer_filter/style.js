import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  container: {
    // padding: 20,
    backgroundColor: 'white',
    marginBottom: 10,
    padding: 10,
    marginLeft: 10,
    marginRight: 10
  },
  btn_text: {
    fontSize: 15,
    color: 'white',
    fontFamily: "Roboto-Regular"
  },
  cus_select_box: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  select_icon: {
    width: 20,
    height: 20
  },
  select_text: {
    fontSize: 14,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    marginLeft: 5
  },
  select_item: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 10,
    height: 35
  },
  title_box: {
    height: 30,
    justifyContent: 'center',
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
    flex: 1,
    marginTop: 30
  },
  title_text: {
    fontSize: 16,
    color: Colors.dark,
    fontFamily: "Roboto-Bold"
  },
  btn_box: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 50
  },
  submit: {
    height: 35,
    width: 120,
    backgroundColor: Colors.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
});
