import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  FlatList,
  ActivityIndicator
} from 'react-native';
import Colors from '../../../global/colors';
import Routes from '../../../views/routes';
import Tabbar from '../../../widgets/tabbar';
import HeaderTick from '../../../widgets/header_tick';
import ModalDatePicker from '../../../widgets/modal_date_picker';
import SickyScrollView from '../../../widgets/sticky_scrollview'
import { connect } from 'react-redux';
import Utils from '../../../global/utils';
import Const from '../../../global/const';
import ModalDropdown from '../../../widgets/modal_dropdown';
import InputRow from '../../../widgets/input_row';
import ModalTimePicker from '../../../widgets/modal_time_picker';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');
const tabbarData = [{
  key: 'car_delivery',
  text: 'KẾ HOẠCH DỰ KIẾN GIAO XE'
},
{
  key: 'car_furniture',
  text: 'KẾ HOẠCH LẮP NỘI THẤT'
},
{
  key: 'car_insurance',
  text: 'KẾ HOẠCH KHAI THÁC BẢO HIỂM'
}];

class EditDeliveryPlan extends ChildView {
  constructor(props) {
    super(props);
    this.allRefs = [];
    this.state = {
      tabbarSelected: 'car_delivery',
      contactPerSelectedId: null,
      salesSupportData: {},
      updateCompanyData: {},
      updateBrokerData: {},
      carDetail: props.navigation.getParam('carDetail', {}),
      carMaintenance: {
        id: props.navigation.getParam('carDetail', {}).id,
        km: props.navigation.getParam('carDetail', {}).km,
        nextKmMaintenance: props.navigation.getParam('carDetail', {}).nextKmMaintenance,
        nextDateMaintenance: props.navigation.getParam('carDetail', {}).nextDateMaintenance
      }
    }
    // console.log(this.props.carDetail)
  }

  renderContainer() {
    return (
      <SickyScrollView
        maxHeight={210}
        minHeight={40}
        isLoading={this.showLoading.bind(this)}
        onRefresh={this.onRefresh.bind(this)}
        renderHeader={this.renderScrollViewHeader.bind(this)}
        renderContent={this.renderTabView.bind(this)}
      />
    );
  }

  onRefresh() {
    switch (this.state.tabbarSelected) {

    }
  }

  showLoading() {
    return false;
    switch (this.state.tabbarSelected) {

    }
  }


  renderScrollViewHeader() {
    let { carDetail } = this.state;
    return (
      <View style={{ width: '100%', height: '100%' }}>
        <View style={styles.header_box}>
          {(this.props.type == 'folow_car_info' || this.props.type == 'lost_car_info') && <Text style={styles.staff_info}>{"TVBH: " + carDetail.staffName}</Text>}
          <TouchableOpacity style={styles.car_tittle_box} onPress={() => { this.onpenEditCarPage() }}>
            <Text style={styles.car_name}>{carDetail.carGradeName}</Text>
            <Text style={styles.car_color}>{carDetail.carColorName}</Text>
            <Text style={styles.brand_text}>{carDetail.carBrandName}</Text>

          </TouchableOpacity>
          {this.renderProcessPoints(carDetail.transactionStatusId, carDetail.transactionStatuses)}
        </View>
        <View style={styles.tabbar}>
          <Tabbar
            data={tabbarData}
            onChange={this.onTabbarChange.bind(this)} />
        </View>
      </View >);
  }

  onpenEditCarPage() {
    var page = Routes.edit_car;
    var props = { ...page.props, carDetail: this.state.carDetail, setBack: this.setBack.bind(this) }
    this.pushPage('edit_car', props);
  }
  onTabbarChange(item) {
    this.setState({
      tabbarSelected: item.key
    }, () => {
      this.onRefresh();
    });
  }
  renderProcessPoints(id, array) {
    var acticeIndex = array.findIndex((element) => {
      return id == element.order;
    })
    if (acticeIndex == -1) acticeIndex = 0;
    var views = [];
    for (var i = 0; i < array.length; i++) {
      views.push(
        <View key={"process-point" + i} style={styles.process_point}>
          <View style={[styles.yellow_point, { backgroundColor: acticeIndex >= i ? Colors.yellow : Colors.gray }]}></View>
          <Text style={[styles.point_text, { color: acticeIndex >= i ? Colors.yellow : 'white' }]}>{array[i].displayName}</Text>
        </View>
      );
    }

    return (<View style={styles.car_header_detail_box}>
      <View style={styles.process_line_box}>
        <View style={{ flex: 1 }}></View>
        <View style={{ flex: array.length * 2 - 2, height: 1, backgroundColor: Colors.dark, flexDirection: 'row', marginTop: 5 }}>
          <View style={{ flex: acticeIndex, backgroundColor: Colors.yellow }}></View>
          <View style={{ flex: array.length - acticeIndex - 1 }}></View>
        </View>
        <View style={{ flex: 1 }}></View>
      </View>
      <View style={styles.process_line_box}>
        {views}
      </View>
    </View>);
  }

  renderTabView() {
    switch (this.state.tabbarSelected) {
      case 'car_delivery':
        return (
          <View style={styles.tabview_box}>
            <HeaderTick
              mStyle={0}
              title={"Kế hoạch xuất hoá đơn"}
              onChange={(value) => { }} />
            <ModalTimePicker
              title={"Thời gian"}
              setVisibleModal={this.setVisibleModal.bind(this)}
              renderModal={this.renderModal.bind(this)} />
            <HeaderTick
              mStyle={0}
              marginTop={30}
              title={"Kế hoạch giao xe"}
              onChange={(value) => { }} />
            <ModalTimePicker
              title={"Thời gian"}
              setVisibleModal={this.setVisibleModal.bind(this)}
              renderModal={this.renderModal.bind(this)} />
            <HeaderTick
              mStyle={0}
              marginTop={30}
              title={"Kế hoạch PDS"}
              onChange={(value) => { }} />
            <ModalTimePicker
              title={"Thời gian"}
              setVisibleModal={this.setVisibleModal.bind(this)}
              renderModal={this.renderModal.bind(this)} />
            <ModalDropdown
              mStyle={1}
              options={[{ name: '1 dfds' }, { name: '4 dfsfsd' }]}
              renderModal={this.renderModal.bind(this)}
              setVisibleModal={this.setVisibleModal.bind(this)}
              title={'Thanh toán'} />
            <TouchableOpacity
              onPress={() => { }}
              style={styles.submit}
              underlayColor='transparent'>
              <Text style={styles.btn_text}>Lưu</Text>
            </TouchableOpacity>
          </View>
        );
      case 'car_furniture':
        return (
          <View style={styles.tabview_box}>
            <HeaderTick
              mStyle={0}
              title={"Kế hoạch lắp nội thất"}
              onChange={(value) => { }} />
            <ModalTimePicker
              title={"Thời gian vào"}
              setVisibleModal={this.setVisibleModal.bind(this)}
              renderModal={this.renderModal.bind(this)} />
            <ModalTimePicker
              title={"Thời gian ra"}
              setVisibleModal={this.setVisibleModal.bind(this)}
              renderModal={this.renderModal.bind(this)} />
            <InputRow
              onFocus={this.onFocusWithContext.bind(this)}
              onEndEditing={this.onEndEditing.bind(this)}
              title={'Khuyến mãi'}
              onChangeText={(text) => { }} />
            <InputRow
              onFocus={this.onFocusWithContext.bind(this)}
              onEndEditing={this.onEndEditing.bind(this)}
              title={'Lắp thêm'}
              onChangeText={(text) => { }} />
            <TouchableOpacity
              onPress={() => { }}
              style={styles.submit}
              underlayColor='transparent'>
              <Text style={styles.btn_text}>Lưu</Text>
            </TouchableOpacity>
          </View>
        );
      case 'car_insurance':
        return (
          <View style={styles.tabview_box}>
            <HeaderTick
              mStyle={0}
              title={"Kế hoạch khai thác bảo hiểm"}
              onChange={(value) => { }} />
            <InputRow
              onFocus={this.onFocusWithContext.bind(this)}
              onEndEditing={this.onEndEditing.bind(this)}
              title={'Khuyến mãi'}
              onChangeText={(text) => { }} />
            <InputRow
              onFocus={this.onFocusWithContext.bind(this)}
              onEndEditing={this.onEndEditing.bind(this)}
              title={'Mua thêm'}
              onChangeText={(text) => { }} />
            <TouchableOpacity
              onPress={() => { }}
              style={styles.submit}
              underlayColor='transparent'>
              <Text style={styles.btn_text}>Lưu</Text>
            </TouchableOpacity>
          </View>
        );
    }
  }


  loadDefaultData() {
    // this.props.doGetEnumTypeData(this.setLoading.bind(this), this.props.navigator);
  }

  componentDidMount() {
    // this.loadDefaultData();
  }

}

// function mapStateToProps(state) {
//   return {
//     carInfoPage: state.carInfoPage,
//     enumTypeApi: state.enumTypeApi
//   };
// }
export default connect(null, null)(EditDeliveryPlan);