import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white'
  },
  header_box: {
    position: 'absolute',
    bottom: 40,
    height: 170,
    width: '100%',
    alignItems: 'center',
    backgroundColor: Colors.light_dark
  },
  tabview_box: {
    padding: 20,
    paddingTop: 10
  },
  view_contact_his_box: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center'
  },
  view_contact_his_text: {
    fontSize: 14,
    color: Colors.dark_pink,
    fontFamily: "Roboto-Regular"
  },
  double_arrow: {
    width: 15,
    height: 15,
    tintColor: Colors.dark_pink,
    marginLeft: 10
  },
  input_row: {
    flex: 1,
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 13
  },
  text_input: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    height: 40,
    paddingVertical: 0,
    paddingHorizontal: 0,
    marginTop: -5
  },
  text_name: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
  },
  text_info: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  submit: {
    marginTop: 30,
    marginBottom: 20,
    height: 35,
    width: 120,
    backgroundColor: Colors.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  btn_text: {
    color: 'white',
    fontSize: 15,
    fontFamily: "Roboto-Regular"
  },
  car_tittle_box: {
    alignItems: 'center',
    marginTop: 30
  },
  staff_info: {
    position: 'absolute',
    color: 'white',
    fontSize: 10,
    fontFamily: "Roboto-Regular",
    left: 8,
    top: 8
  },
  process_line_box: {
    position: 'absolute',
    top: 0,
    left: 8,
    right: 8,
    height: 40,
    flexDirection: 'row'
  },
  brand_text: {
    color: 'white',
    fontSize: 14,
    fontFamily: "Roboto-Regular"
  },
  car_name: {
    color: 'white',
    fontSize: 25,
    fontFamily: "Roboto-Bold"
  },
  car_color: {
    color: 'white',
    fontSize: 16,
    fontFamily: "Roboto-Regular"
  },
  car_header_detail_box: {
    marginTop: 20,
    width: '100%',
    height: 40,
    flexDirection: 'row'
  },
  detail_item_box: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightColor: Colors.gray,
    borderRightWidth: 0.5
  },
  detail_title_text: {
    color: '#CACCCE',
    fontSize: 12,
    fontFamily: "Roboto-Bold"
  },
  detail_text: {
    color: Colors.yellow,
    fontSize: 12,
    fontFamily: "Roboto-Bold",
    marginTop: 5
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
    marginLeft: 20,
    marginRight: 20
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  row_image_right: {
    position: 'absolute',
    right: 0,
    width: 20,
    height: 20,
  },
  tabbar: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 40
  },
  process_point: {
    flex: 1,
    alignItems: 'center'
  },
  point_text: {
    color: 'white',
    fontSize: 10,
    fontFamily: "Roboto-Regular",
    marginTop: 8,
  },
  yellow_point: {
    width: 10,
    height: 10,
    borderRadius: 5
  },
  text_select: {
    fontSize: 12,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  error_loading_text: {
    fontSize: 12,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  }
});
