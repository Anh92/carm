import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView
} from 'react-native';
import Colors from '../../../global/colors'

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');
var contractList = [{
  month: '10/2016',
  number: 4
},
{
  month: '11/2016',
  number: 5
},
{
  month: '12/2016',
  number: 3
},
{
  month: '1/2017',
  number: 10
}];
var listChartColors = [Colors.dark_blue, Colors.yellow, Colors.orange, Colors.blue, Colors.purple, Colors.gray, Colors.dark, Colors.ink, Colors.smoke, Colors.dark_pink];


class SaleCar extends ChildView {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  renderContainer() {
    return (
      <View>
        <ScrollView style={{ height: '100%', width: '100%' }}>
          <View style={{ paddingLeft: 16, paddingRight: 16, }}>
            <View style={styles.expend_title_box}>
              <Text style={styles.expend_title}>dfsh fdsfds</Text>
            </View>
            {this.renderContractChart()}
          </View>
        </ScrollView>
      </View>
    );
  }

  renderContractChart() {
    var maxNumber = 0;
    var views = [];
    for (var i = 0; i < contractList.length; i++) {
      if (contractList[i].number > maxNumber) {
        maxNumber = contractList[i].number;
      }
    }
    for (var i = 0; i < contractList.length; i++) {
      views.push(
        <View key={'contract-chart-row' + i} style={styles.chart_row}>
          <Text style={styles.chart_text}>{contractList[i].month}</Text>
          <View style={styles.chart_col_box}>
            <View style={[styles.chart_col_box, { backgroundColor: listChartColors[i % 10], flex: contractList[i].number }]}>
              <Text style={styles.chart_col_text}>{contractList[i].number}</Text>
            </View>
            <View style={{ flex: maxNumber - contractList[i].number }}></View>
          </View>
        </View>
      );
    }
    return views;
  }

}
module.exports = SaleCar;