import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors'

module.exports = StyleSheet.create({
  expend_title_box: {
    paddingTop: 25,
    height: 50,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  expend_title: {
    fontSize: 16,
    color: Colors.dark_blue,
    fontFamily: "Roboto-Bold"
  },
  chart_row: {
    width: '100%',
    height: 20,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 11
  },
  chart_text: {
    fontSize: 12,
    color: Colors.dark,
    width: 60,
    fontFamily: "Roboto-Regular"
  },
  chart_col_box: {
    height: 20,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  chart_col_text: {
    fontSize: 12,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    paddingLeft: 8
  }
});
