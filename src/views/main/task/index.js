import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView
} from 'react-native';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');


class Task extends ChildView {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  renderContainer() {
    return (
      <View>
        <ScrollView style={{ height: '100%', width: '100%' }}>
          <View style={{ paddingLeft: 16, paddingRight: 16, }}>
            <View style={styles.expend_title_box}>
              <Text style={styles.expend_title}>dfsh fdsfds</Text>
            </View>
            {this.renderDaylyProcess('#009B97', '#9BD8DB', 3, 1, "Strings.meet_customer")}
            {this.renderDaylyProcess('#009B97', '#9BD8DB', 5, 3, "Strings.meet_customer")}
            {this.renderDaylyProcess('#009B97', '#9BD8DB', 3, 2, "Strings.meet_customer")}
          </View>
        </ScrollView>
      </View>
    );
  }

  renderDaylyProcess(doneColor, color, totalTask, taskDone, title) {
    return (
      <TouchableOpacity>
        <View>
          <View style={styles.process_row}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View style={styles.point}></View>
              <Text style={styles.task_title}>{title}</Text>
            </View>
            <Text style={styles.process_right_text}>{taskDone + '/' + totalTask}</Text>
          </View>
          <View style={[styles.process_bar, { backgroundColor: color }]}>
            <View style={{ width: taskDone / totalTask * 100 + '%', height: '100%', backgroundColor: doneColor }}></View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

}
module.exports = Task;