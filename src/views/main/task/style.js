import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors'

module.exports = StyleSheet.create({
  expend_title_box: {
    paddingTop: 25,
    height: 50,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
  },
  expend_title: {
    fontSize: 16,
    color: Colors.dark_blue,
    fontFamily: "Roboto-Bold"
  },
  process_row: {
    height: 52,
    paddingTop: 25,
    width: '100%'
  },
  point: {
    height: 8,
    width: 8,
    borderRadius: 4,
    backgroundColor: Colors.dark_blue
  },
  task_title: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    marginLeft: 10,
  },
  process_right_text: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    position: 'absolute',
    right: 0,
    bottom: 8
  },
  process_bar: {
    height: 4,
    width: '100%'
  }
});
