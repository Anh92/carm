import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  RefreshControl,
  ActivityIndicator
} from 'react-native';
import Strings from '../../../global/strings';
import Routes from '../../../views/routes';
import Colors from '../../../global/colors'
import { connect } from 'react-redux';
import Utils from '../../../global/utils';
import { doGetData } from '../../../actions/mainActions/customerListAction';
var ChildView = require('../../../views/base/main_child_view');

var styles = require('./style.js');

class FindCustomer extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      customerModalVisible: true,
      refreshing: false,
      filterKey: '',
      customerList: [{
        fullname: 'Nguyen Van A',
        nickname: 'A Van',
        phones: [{ active: true, number: '0164354332' }],
        email: 'nguyenvana@gmail.com'
      }, {
        fullname: 'Nguyen Van B',
        nickname: 'A Van',
        phones: [{ active: true, number: '01645654654' }],
        email: 'nguyenvana@gmail.com'
      }, {
        fullname: 'Nguyen Van C',
        nickname: 'A Van',
        phones: [{ active: true, number: '09043543534' }],
        email: 'nguyenvana@gmail.com',
        isDuplicateSubProfile: true
      }, {
        fullname: 'Nguyen Van D',
        nickname: 'A Van',
        phones: [{ active: true, number: '0840853495' }],
        email: 'nguyenvana@gmail.com',
        isDuplicateSubProfile: true
      }, {
        fullname: 'Nguyen Van E',
        nickname: 'A Van',
        phones: [{ active: true, number: '07839832993' }],
        email: 'nguyenvana@gmail.com'
      }, {
        fullname: 'Nguyen Van F',
        nickname: 'A Van',
        phones: [{ active: true, number: '01893923293' }],
        email: 'nguyenvana@gmail.com'
      }]
    }
  }

  renderContainer() {
    const { isLoading } = this.props.customerPage;
    return (
      <View style={styles.container}>
        <View style={styles.filter_box}>
          <TouchableOpacity style={styles.button}>
            <Image source={require('../../../assets//images/search.png')} resizeMode={'contain'} style={{ width: 30, height: 30, backgroundColor: this.props.isDropdown ? '#00554E' : 'transparent', borderRadius: 3 }} />
          </TouchableOpacity>
          <TextInput
            onChangeText={(text) => this.setState({ filterKey: text })}
            value={this.state.filterKey}
            underlineColorAndroid={'transparent'}
            style={styles.input_filter}
            placeholder={Strings.filter_name_phone_hint}
            placeholderTextColor={'#989899'}
            placeholderStyle={styles.input_holder} />
        </View>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={false}
              tintColor="transparent"
              onRefresh={this.onRefresh.bind(this)}
            />
          }
          data={this.getCustomerList()}
          renderItem={({ item, index }) => this.renderItems(item, index)}
        />
        {isLoading &&
          <View style={styles.loading_box}>
            <ActivityIndicator size="large"
              color={Colors.gray} />
          </View>}
      </View>
    );
  }
  onRefresh() {
    this.props.doGetData(this.props.listType, this);
  }

  getCustomerList() {
    // var { customerList } = this.props.customerPage;
    var { customerList } = this.state;
    if (this.state.filterKey == '' || this.state.filterKey == null)
      return customerList;
    return customerList.filter((e) => {
      var mFilter = Utils.convertVietNameLetter(this.state.filterKey);
      var mFullname = Utils.convertVietNameLetter(e.fullname || "");
      var mNickname = Utils.convertVietNameLetter(e.nickname || "");
      if (mFullname.indexOf(mFilter) != -1) {
        e.showName = e.fullname;
        return true;
      }
      if (mNickname.indexOf(mFilter) != -1) {
        e.showName = e.nickname;
        return true;
      }
      mFilter = "" + parseInt("1" + mFilter);
      mFilter = mFilter.substr(1);
      if (mFilter.length == this.state.filterKey.length && e.phones && e.phones.length > 0) {
        for (var i = 0; i < e.phones.length; i++) {
          if (e.phones[i].number.indexOf(mFilter) != -1) {
            customerList.showPhone = e.phones[i].number;
            return true;
          }
        }
      }
      return false;
    });
  }

  openEditProfilePage(item, index) {
    // var page = Routes.contract_customer;
    // var props = {
    //   ...page.props,
    //   customerData: item
    // };
    // this.pushPage('contract_customer', props);
    var page = Routes.create_contract;
    var props = {
      ...page.props,
      customerData: item
    };
    this.pushPage('create_contract', props);
  }

  openModalApprove(item, index) {
    this.props.renderModal(
      <View style={styles.modal_box}>
        <Text style={styles.text_title}>{item.name + " sẽ được chọn để chăm sóc khách hàng"}</Text>
        <View style={styles.btn_box}>
          <TouchableOpacity
            onPress={() => { this.props.setVisibleModal(false) }}
            style={[styles.submit, { backgroundColor: "#E4E4E4", marginRight: 30 }]}
            underlayColor='transparent'>
            <Text style={[styles.btn_text, { color: Colors.dark }]}>Hủy</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => { this.doApprove(item) }}
            style={styles.submit}
            underlayColor='transparent'>
            <Text style={styles.btn_text}>Lưu</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
    this.props.setVisibleModal(true);
  }

  showPhone(phones) {
    var activePhoneIndex = phones.findIndex((element) => {
      return element.active;
    })
    if (activePhoneIndex != -1) {
      return phones[activePhoneIndex].number;
    }
    return phones[0].number;
  }

  //     {this.props.listType != 'customers_list' && <Text style={styles.text_status}>{item.status}</Text>}
  renderItems(item, index) {
    return (
      <TouchableOpacity onPress={() => { this.openEditProfilePage(item, index) }}>
        <View style={styles.row}>
          <Image source={item.iconProfile ? { uri: item.iconProfile } : require('../../../assets//images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} />
          <View style={{ marginLeft: 10, backgroundColor: 'transparent' }}>
            <Text style={styles.text_name}>{item.showName || item.fullname || item.nickname}</Text>
            <Text style={styles.text_phone}>{item.showPhone || ((item.phones && item.phones.length > 0) && this.showPhone(item.phones))}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  componentDidMount() {
    // this.props.doGetData(this.props.listType, this);
  }
}

function mapStateToProps(state) {
  return { customerPage: state.customerPage };
}

export default connect(mapStateToProps, { doGetData })(FindCustomer);
