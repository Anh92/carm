import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  FlatList
} from 'react-native';
import Colors from '../../../global/colors';
import Tabbar from '../../../widgets/tabbar';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');
const tabbarData = [{
  key: 'call',
  text: 'ĐIỆN THOẠI'
},
{
  key: 'mess',
  text: 'TIN NHẮN'
},
{
  key: 'other',
  text: 'LIÊN HỆ KHÁC'
}];
const callHisData = [{
  title: 'Tiếp xúc khách hàng',
  staff: 'Lê Tuấn Anh',
  note: 'Khách hàng muốn giảm giá',
  date: '14:00 01/02/2017'
},
{
  title: 'Tiếp xúc khách hàng',
  staff: 'Lê Tuấn Anh',
  note: 'KH không đồng ý mức giá',
  date: '09:00 05/02/2017'
}];
const messHisData = [{
  title: 'Tiếp xúc khách hàng',
  staff: 'Lê Tuấn Anh',
  note: 'Khách hàng muốn giảm giá',
  date: '14:00 01/02/2017'
},
{
  title: 'Tiếp xúc khách hàng',
  staff: 'Lê Tuấn Anh',
  note: 'KH không đồng ý mức giá',
  date: '09:00 05/02/2017'
}];

class ContactHistory extends ChildView {
  constructor(props) {
    super(props);
    this.listRefs = [];
    this.state = {
      refreshing: false,
      tabbarSelected: 'call',
      otherHisData: [{
        title: 'Tiếp xúc khách hàng',
        location: 'Cafe',
        note: 'Khách hàng muốn giảm giá',
        date: '14:00 01/02/2017'
      },
      {
        title: 'Dam phan lai gia phong',
        location: 'Tai nha rieng',
        note: 'KH không đồng ý mức giá',
        date: '09:00 05/02/2017'
      }],
      otherDataTemp: {
        title: '',
        location: '',
        note: '',
        date: ''
      }
    }
  }

  renderContainer() {
    return (
      <View style={styles.container}>
        <Tabbar
          data={tabbarData}
          fixFlex={true}
          onChange={this.onTabbarChange.bind(this)} />
        {this.renderTabView()}
      </View>
    );
  }

  onTabbarChange(item) {
    this.setState({
      tabbarSelected: item.key
    });
  }

  renderTabView() {
    switch (this.state.tabbarSelected) {
      case 'call':
        return (
          <FlatList
            data={callHisData}
            renderItem={({ item }) => this.renderCallHistoryRows(item)}
          />
        );
      case 'mess':
        return (
          <FlatList
            data={messHisData}
            renderItem={({ item }) => this.renderMessHistoryRows(item)}
          />
        );
        return;
      case 'other':
        return (
          <View style={{ flex: 1 }}>
            <TouchableOpacity style={styles.add_more_contact} onPress={() => { this.renderAddMoreContactModal() }}>
              <Image source={require('../../../assets//images/add_more.png')} resizeMode={'contain'} style={{ width: 25, height: 25 }} />
              <Text style={styles.add_more_contact_text}>Thêm liên hệ</Text>
            </TouchableOpacity>
            <FlatList
              data={this.state.otherHisData}
              renderItem={({ item, index }) => this.renderOtherHistoryRows(item, index)}
            />
          </View>
        );
        return;
    }
  }
  onRefresh() {
    // this.setState({ refreshing: true });
    // fetchData().then(() => {
    //   this.setState({ refreshing: false });
    // });
  }

  /////////////////////////RENDER CALL TAB///////////////////
  renderCallHistoryRows(item) {
    return (
      <View style={styles.call_row}>
        <Image source={require('../../../assets//images/phone.png')} resizeMode={'contain'} style={[styles.phone_icon, { tintColor: Colors.dark_blue }]} />
        <View style={styles.call_info}>
          <Text style={styles.call_title}>{item.title}</Text>
          <Text style={styles.call_text}>{item.staff}</Text>
          <Text style={styles.call_text}>{item.note}</Text>
        </View>
        <View style={{ height: 80 }}>
          <Text style={styles.call_date}>{item.date}</Text>
        </View>

      </View>
    );
  }

  /////////////////////////END RENDER CALL TAB///////////////////

  /////////////////////////RENDER mess TAB///////////////////
  renderMessHistoryRows(item) {
    return (
      <View style={styles.mess_row}>
        <Image source={require('../../../assets//images/message.png')} resizeMode={'contain'} style={styles.phone_icon} />
        <View style={styles.call_info}>
          <Text style={styles.call_title}>{item.date}</Text>
          <Text style={styles.call_text}>{item.staff}</Text>
        </View>
      </View>
    );
  }

  /////////////////////////END RENDER mess TAB///////////////////

  /////////////////////////RENDER other TAB///////////////////
  renderOtherHistoryRows(item, index) {
    return (
      <TouchableOpacity style={styles.call_row} onPress={() => { this.renderAddMoreContactModal(index) }}>
        <View style={styles.other_info}>
          <Text style={styles.call_title}>{item.title}</Text>
          <Text style={styles.call_text}>{item.location}</Text>
          <Text style={styles.call_text}>{item.note}</Text>
        </View>
        <View style={{ height: 80 }}>
          <Text style={styles.call_date}>{item.date}</Text>
        </View>
      </TouchableOpacity>
    );
  }
  onCustomFocus(ref) {
    this.onFocusWithContext(this.listRefs[ref], this);
  }
  renderAddMoreContactModal(index) {
    let mData = { title: '', location: '', note: '', date: '' };
    if (index != null) {
      mData = this.state.otherHisData[index];
      this.setState({
        otherDataTemp: this.state.otherHisData[index]
      });
    }
    this.renderModal(
      <View style={styles.modal_box}>
        <View style={styles.input_row}>
          <Text style={styles.text_title}>Nội dung liên hệ</Text>
          <TextInput
            ref={(key) => { this.listRefs['ref_input_contact_content'] = key }}
            onFocus={this.onCustomFocus.bind(this, "ref_input_contact_content")}
            onEndEditing={this.onEndEditing.bind(this)}
            defaultValue={mData.title}
            onChangeText={(text) => this.setState({ otherDataTemp: { ...this.state.otherDataTemp, title: text } })}
            underlineColorAndroid={'transparent'}
            style={styles.text_input}
            placeholderTextColor={'#989899'} />
        </View>
        <View style={styles.input_row}>
          <Text style={styles.text_title}>Địa điểm</Text>
          <TextInput
            ref={(key) => { this.listRefs['ref_input_address'] = key }}
            onFocus={this.onCustomFocus.bind(this, "ref_input_address")}
            onEndEditing={this.onEndEditing.bind(this)}
            defaultValue={mData.location}
            onChangeText={(text) => this.setState({ otherDataTemp: { ...this.state.otherDataTemp, location: text } })}
            underlineColorAndroid={'transparent'}
            style={styles.text_input}
            placeholderTextColor={'#989899'} />
        </View>
        <View style={styles.input_row}>
          <Text style={styles.text_title}>Ghi chú</Text>
          <TextInput
            ref={(key) => { this.listRefs['ref_input_note'] = key }}
            onFocus={this.onCustomFocus.bind(this, "ref_input_note")}
            onEndEditing={this.onEndEditing.bind(this)}
            defaultValue={mData.note} onChangeText={(text) => this.setState({ otherDataTemp: { ...this.state.otherDataTemp, note: text } })}
            underlineColorAndroid={'transparent'}
            style={styles.text_input}
            placeholderTextColor={'#989899'} />
        </View>
        <View style={styles.modal_btn_box}>
          <TouchableOpacity style={[styles.select_btn, { backgroundColor: '#E4E4E4' }]} onPress={() => { this.setVisibleModal(false); }}>
            <Text style={[styles.select_btn_text, { color: Colors.dark }]}>Hủy</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.select_btn, { backgroundColor: Colors.dark_blue, marginLeft: 20 }]} onPress={() => { this.addNewContact(index) }}>
            <Text style={[styles.select_btn_text, { color: Colors.smoke }]}>Lưu</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
    this.setVisibleModal(true);
  }

  addNewContact(index) {
    this.setVisibleModal(false);
    if (index == null) {
      var currentDate = new Date();
      currentDate = ('0' + currentDate.getHours()).slice(-2) + ":" + ('0' + currentDate.getMinutes()).slice(-2) + " " + ('0' + currentDate.getDate()).slice(-2) + "/" + ('0' + (currentDate.getMonth() + 1)).slice(-2) + "/" + currentDate.getFullYear();
      this.state.otherDataTemp.date = currentDate;
      let dataTemp = [].concat(this.state.otherHisData)
      dataTemp.push(this.state.otherDataTemp)
      this.setState({
        otherHisData: dataTemp
      });
    }
    else {
      this.state.otherHisData[index] = this.state.otherDataTemp;
      let dataTemp = this.state.otherHisData.slice();
      this.setState({
        otherHisData: dataTemp
      });
    }
  }

  /////////////////////////END RENDER other TAB///////////////////
}
module.exports = ContactHistory;