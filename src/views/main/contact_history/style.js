import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white'
  },
  call_row: {
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  mess_row: {
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  phone_icon: {
    width: 35,
    height: 35
  },
  call_info: {
    flex: 1,
    height: 80,
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  other_info: {
    flex: 1,
    height: 80,
    justifyContent: 'center',
    marginRight: 10
  },
  call_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Bold",
    backgroundColor: 'transparent'
  },
  call_text: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    backgroundColor: 'transparent',
    marginTop: 5
  },
  call_date: {
    color: Colors.dark,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    backgroundColor: 'transparent',
    marginTop: 15
  },
  add_more_contact: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    marginLeft: 20,
    marginTop: 10
  },
  add_more_contact_text: {
    color: Colors.dark,
    fontFamily: "Roboto-Bold",
    fontSize: 12,
    marginLeft: 10
  },
  modal_box: {
    position: 'absolute',
    right: 30,
    left: 30,
    backgroundColor: 'white',
    padding: 10
  },
  input_row: {
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 13
  },
  text_input: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    height: 40,
    marginTop: -5,
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  modal_btn_box: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 30,
    marginBottom: 20
  },
  select_btn: {
    height: 35,
    flex: 1,
    borderRadius: 17.5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  select_btn_text: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular"
  }
});
