import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  row: {
    height: 60,
    marginLeft: 20,
    marginRight: 20,
    borderBottomColor: Colors.underline,
    justifyContent: 'center',
    borderBottomWidth: 0.5
  },
  title_text: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Bold"
  },
  service_type_text: {
    color: Colors.gray,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  point_text: {
    position: 'absolute',
    right: 0,
    bottom: 10,
    color: Colors.dark,
    fontSize: 12,
    fontFamily: "Roboto-Regular"
  }
});
