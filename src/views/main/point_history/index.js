import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Colors from '../../../global/colors';

var Global = require('../../../global/const');
var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');
const hisData = [{
  time_in: '14:00 01/02/2017',
  point: 1000,
  service_type: 'Bao duong'
},
{
  time_in: '09:00 05/02/2017',
  point: 1001,
  service_type: 'Sửa chữa'
}]

class ServiceHistory extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  renderContainer() {
    return (
      <FlatList
        data={hisData}
        renderItem={({ item, index }) => this.renderRows(item, index)}
      />
    );
  }

  renderRows(item, index) {
    return (
      <View style={styles.row}>
        <Text style={styles.title_text}>{item.time_in}</Text>
        <Text style={styles.service_type_text}>{item.service_type}</Text>
        <Text style={styles.point_text}>{item.point+ " điểm"}</Text>
      </View>
    );
  }
}
module.exports = ServiceHistory;