import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView
} from 'react-native';
import Strings from '../../../global/strings';
import ModelAction from '../../../widgets/modal_actions';
import Routes from '../../../views/routes';
import Colors from '../../../global/colors'

var ChildView = require('../../../views/base/main_child_view');
var styles = require('./style.js');


class DuplicateFolow extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      dupData: [
        { name: 'Staff A', avatar: 'https://pbs.twimg.com/profile_images/831993825635745796/HnVmB0-k.jpg', key: 1 },
        { name: 'Staff B', avatar: 'https://pbs.twimg.com/profile_images/831993825635745796/HnVmB0-k.jpg', key: 2 }
      ]
    }
  }
  renderContainer() {
    return (
      <View style={styles.container}>
        <View style={styles.fix_dup_staff_box}>
          <FlatList
            data={this.state.dupData}
            renderItem={({ item, index }) => this.renderDupItems(item, index)}
          />
        </View>
      </View>
    );
  }

  doApprove(item) {
    this.goBack();
    this.setVisibleModal(false);
  }
  onpenCustomerInfoPage() {
    // var page = Routes.customer_info;
    this.pushPage('customer_info');
  }

  openModalApprove(item, index) {
    this.renderModal(
      <View style={styles.modal_box}>
        <Text style={styles.text_title}>{item.name + " sẽ được chọn để chăm sóc khách hàng"}</Text>
        <View style={styles.btn_box}>
          <TouchableOpacity
            onPress={() => { this.setVisibleModal(false) }}
            style={[styles.submit, { backgroundColor: "#E4E4E4", marginRight: 30 }]}
            underlayColor='transparent'>
            <Text style={[styles.btn_text, { color: Colors.dark }]}>Hủy</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => { this.doApprove(item) }}
            style={styles.submit}
            underlayColor='transparent'>
            <Text style={styles.btn_text}>Lưu</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
    this.setVisibleModal(true);
  }

  renderDupItems(item, index) {
    return (
      <TouchableOpacity style={styles.row} onPress={() => { this.onpenCustomerInfoPage() }}>
        <Image source={{ uri: item.avatar }} resizeMode={'cover'} style={styles.avatar} />
        <View style={{ marginLeft: 10, backgroundColor: 'transparent' }}>
          <Text style={styles.text_name}>{item.name}</Text>
        </View>
        <TouchableOpacity
          onPress={() => { this.openModalApprove(item, index) }}
          style={styles.approve_btn}
          underlayColor='transparent'>
          <Text style={styles.approve_btn_text}>DUYỆT</Text>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }

}

module.exports = DuplicateFolow;