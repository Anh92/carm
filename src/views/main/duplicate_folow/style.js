import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors'

module.exports = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: "#F6F6F6"
  },
  text_name: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Bold"
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 80,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
    marginRight: 20,
    marginLeft: 20
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  fix_dup_staff_box: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    backgroundColor: "#F6F6F6"
  },
  approve_btn: {
    position: 'absolute',
    right: 0,
    height: 38,
    width: 70,
    backgroundColor: Colors.dark_pink,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  approve_btn_text: {
    fontSize: 12,
    color: Colors.smoke,
    fontFamily: "Roboto-Bold"
  },
  submit: {
    height: 35,
    width: 120,
    backgroundColor: Colors.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  btn_text: {
    fontSize: 15,
    color: 'white',
    fontFamily: "Roboto-Regular"
  },
  btn_box: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 10
  },
  modal_box: {
    position: 'absolute',
    left: 30,
    right: 30,
    padding: 20,
    backgroundColor: "white"
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  }
});
