import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Colors from '../../../global/colors';
import ModalDropdown from '../../../widgets/modal_dropdown';
import { connect } from 'react-redux';
import { doGetTeamSaleData } from '../../../actions/apiActions/teamSaleApiAction';
import { doTransferCustomer } from '../../../actions/mainActions/transferCustomerAction';

var Global = require('../../../global/const');
var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');

class TransferCustomer extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      hrStaffsRemoteVMs: [],
      staffId: ''
    }
    console.log('this.props.customerData');
    console.log(this.props.customerData);
  }

  renderContainer() {
    let { teamSaleData } = this.props.teamSaleApi;
    return (
      <ScrollView style={styles.container}>
        <ModalDropdown
          ref='ref_md_group'
          // options={teamSaleData}
          mStyle={1}
          options={['Nhóm 1', 'Nhóm 2', 'Nhóm 3', 'Nhóm 4', 'Nhóm 5', 'Nhóm 6', 'Nhóm 7', 'Nhóm 8', 'Nhóm 31', 'Nhóm 32', 'Nhóm 33']}
          isNormalArray={true}
          // keyItems={'teamName'}
          reloadData={this.loadDefaultData.bind(this)}
          onSelect={(item) => this.onGroupChange(item)}
          renderModal={this.renderModal.bind(this)}
          setVisibleModal={this.setVisibleModal.bind(this)}
          title={'Nhóm'} />
        <ModalDropdown
          ref='ref_md_staff'
          // options={this.state.hrStaffsRemoteVMs}
          mStyle={1}
          options={['Tu van vien 1', 'Tu van vien 2', 'Tu van vien 3', 'Tu van vien 4', 'Tu van vien 5', 'Tu van vien 6', 'Tu van vien 7', 'Tu van vien 8', 'Tu van vien 31', 'Tu van vien 32', 'Tu van vien 33']}
          isNormalArray={true}
          // keyItems={'fullname'}
          onSelect={(item) => this.setState({ staffId: item })}
          renderModal={this.renderModal.bind(this)}
          setVisibleModal={this.setVisibleModal.bind(this)}
          title={'Tư vấn bán hàng'} />
        <View style={styles.btn_box}>
          <TouchableOpacity
            onPress={() => {
              this.onSubmit();
            }}
            style={styles.submit}
            underlayColor='transparent'>
            <Text style={styles.btn_text}>Xác nhận</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
  onCancel() {
    this.setState({
      customerNeedId: -1
    }, () => {
      this.refs.ref_md_group.resetDefault();
      this.refs.ref_md_staff.resetDefault();
    });

  }
  renderRightButton() {

  }
  onGroupChange(item) {
    this.setState({ staffId: '', hrStaffsRemoteVMs: item.hrStaffsRemoteVMs }, () => {
      this.refs.ref_md_staff.resetDefault();
    })
  }
  onSubmit() {
    if (this.state.staffId == null || this.state.staffId == '') {
      this.renderPopup(null, null, 'Vui lòng chọn tư vấn bán hàng cần chuyển giao');
      return;
    }
    this.goBack()
    // this.props.doTransferCustomer({
    //   staffId: this.state.staffId,
    //   customerId: this.props.customerData.id
    // }, this);
  }

  loadDefaultData() {
    this.props.doGetTeamSaleData(this.setLoading.bind(this), this.props.navigator);
  }

  componentDidMount() {
    this.loadDefaultData();
  }
}
function mapStateToProps(state) {
  return {
    teamSaleApi: state.teamSaleApi
  };
}
export default connect(mapStateToProps, { doGetTeamSaleData, doTransferCustomer })(TransferCustomer);