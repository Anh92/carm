import {
    StyleSheet
} from 'react-native';

module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '100%',
        height: '100%'
    },
    logo_box: {
        marginTop: 80,
        width: '100%',
        paddingLeft: 100,
        paddingRight: 100
    },
    logo: {
        width: '100%'
    },
    form: {
        paddingLeft: 32,
        paddingRight: 32,
        justifyContent: 'center',
        alignItems: 'center'
    },
    submit: {
        height: 50,
        width: 200,
        backgroundColor: '#02867C',
        borderRadius: 25,
        borderWidth: 1,
        borderColor: 'transparent',
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn_text: {
        color: 'white',
        fontSize: 16
    },
    forgot_pass: {
        fontSize: 13,
        color: '#7B8182',
        marginTop: 20,
        marginBottom: 20
    }
});