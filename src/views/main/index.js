import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base/base_view'
import styles from './styles'
import Header from '../../widgets/main_header'
import DrawerControler from '../../widgets/main_drawer'
import Drawer from 'react-native-drawer-layout'
import { createStackNavigator, NavigationActions, createAppContainer, StackActions } from 'react-navigation';
import { openMenu, closeMenu, toggleMenu, setHeaderText } from '../../actions/mainAction';
import Const from '../../global/const'
import routes from './routes'

const BasicApp = createAppContainer(createStackNavigator(routes, {
    initialRouteName: 'home',
    headerMode: 'none',
    navigationOptions: {
        gesturesEnabled: false
    }
}));

class Main extends BaseView {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    // onDrawerClose={()=>{this.props.openMenu()}}
    // onDrawerOpen={()=>{this.props.closeMenu()}}
    renderBaseContent() {
        const { drawerMenuOpen, currentPage } = this.props.mainState;
        return (
            <View ref='mainView' style={styles.container}>
                <Header
                    openNewPage={this.openNewPage.bind(this)}
                    menuToggle={this.menuToggle.bind(this)}
                    renderModal={this.renderModal.bind(this)}
                    setVisibleModal={this.setVisibleModal.bind(this)} />
                <Drawer
                    drawerWidth={Const.WIDTH_SCREEN - 50}
                    drawerPosition={Drawer.positions.Left}
                    keyboardDismissMode="on-drag"
                    ref={(ref) => this._drawer = ref}
                    onDrawerClose={() => { this.props.closeMenu() }}
                    onDrawerOpen={() => { this.props.openMenu() }}
                    onDrawerStateChanged={(e) => {
                        if (e == 'Settling') {
                            this.props.toggleMenu();
                        }
                    }}
                    renderNavigationView={() => <DrawerControler
                        closeDrawer={this.closeDrawer}
                        changeChildPage={this.changeChildPage.bind(this)} />}>

                    {/* <Navigator
                        ref="navigator"
                        configureScene={() => Navigator.SceneConfigs.FloatFromRight}
                        initialRoute={MainRouter.home}
                        renderScene={this.renderUserScene.bind(this)}
                    /> */}
                    <BasicApp
                        ref={(mRef) => this.childNavigation = mRef}
                        screenProps={{
                            parentNavigator: this.props.navigation,
                            getchildNav: this.getchildNav.bind(this),
                            setLoading: this.setLoading.bind(this),
                            setVisibleModal: this.setVisibleModal.bind(this),
                            renderPopup: this.renderPopup.bind(this),
                            renderModal: this.renderModal.bind(this),
                            pushPage: this.pushPage.bind(this),
                            goBack: this.goBack.bind(this),
                            resetPage: this.resetPage.bind(this)
                        }}
                    />

                </Drawer>
            </View>
        );
    }

    getchildNav() {
        if (this.childNavigation && this.childNavigation.state) {
            return this.childNavigation.state.nav;
        }
        return { index: 0, routes: [] };
    }

    // renderUserScene(route, navigator) {
    //     const { ViewComponent, props } = route;
    //     parentNavigator = this.props.navigator;
    //     return <ViewComponent
    //         {...props}
    //         {...{ route, navigator }}
    //         {...{ parentNavigator: this.props.navigator }}
    //         {...{ gaTracker: this.props.gaTracker }}
    //         {...{ setVisibleModal: this.setVisibleModal.bind(this) }}
    //         {...{ renderModal: this.renderModal.bind(this) }}
    //         {...{ setLoading: this.setLoading.bind(this) }}
    //         {...{ renderPopup: this.renderPopup.bind(this) }}
    //         onBack={() => navigator.pop()}
    //     />
    // }

    closeDrawer = () => {
        this._drawer.closeDrawer()
    };
    openDrawer = () => {
        this._drawer.openDrawer()
    };

    menuToggle() {
        if (this.props.mainState.drawerMenuOpen) {
            this._drawer.closeDrawer();
        } else {
            this._drawer.openDrawer();
        }
    };

    openNewPage(page, params) {
        this.pushPage(page, params)
        // this.props.navigator.push(page);
    }

    changeChildPage(pageView, params) {
        let { dispatch } = this.childNavigation;
        // console.log(this.childNavigation)
        console.log(pageView)
        // this.setState({ curentRoute: pageView });
        dispatch(StackActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: pageView, params: params })]
        }))
        this.props.setHeaderText(params.title);
    }
}

function mapStateToProps(state) {
    return { mainState: state.mainPage };
}

export default connect(mapStateToProps, { openMenu, closeMenu, toggleMenu, setHeaderText })(Main);