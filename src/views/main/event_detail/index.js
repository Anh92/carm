import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  FlatList
} from 'react-native';
import Colors from '../../../global/colors';
import Routes from '../../../views/routes';
import ModalDropdown from '../../../widgets/modal_dropdown';
import { connect } from 'react-redux';
import HeaderTick from '../../../widgets/header_tick';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');

class EventDetail extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      headerBackgroundColor: Colors.dark_yellow
    }
  }

  renderContainer() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.flex_row_center}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.point}></View>
            <Text style={styles.task_title}>Khách sạn thiên đường</Text>
          </View>
        </View>
        <View style={styles.flex_row_time}>
          <View style={styles.time_point}></View>
          <View style={{ marginLeft: 8 }}>
            <Text style={styles.task_title}>08:40 18/10/2017</Text>
            <Text style={[styles.task_title, { marginTop: 8 }]}>09:40 18/10/2017</Text>
          </View>
        </View>
        <View style={styles.flex_row_center}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.point}></View>
            <Text style={styles.task_title}>Khách hàng muốn giảm giá</Text>
          </View>
        </View>
        <View style={styles.flex_row_time}>
          <View style={styles.time_point}></View>
          <View style={{ marginLeft: 8 }}>
            <Text style={styles.task_title}>Nguyễn Gia Bảo</Text>
            <Text style={[styles.task_title, { marginTop: 8 }]}>Vios SSD 039</Text>
          </View>
        </View>
        <HeaderTick title={"Người tạo"} mStyle={1} marginTop={30} />
        {this.renderAtPp(dataTemp[0], 99999)}
        <HeaderTick title={"Người tham dự"} mStyle={1} marginTop={30} />
        {this.renderAtPpList()}
      </ScrollView>
    );
  }

  renderAtPpList() {
    var views = [];
    for (var i = 0; i < dataTemp.length; i++) {
      views.push(this.renderAtPp(dataTemp[i], i));
    }
    return views;
  }
  renderAtPp(item, index) {
    return (
      <View key={'AtPp' + index} style={styles.row}>
        <Image source={item.iconProfile ? { uri: item.iconProfile } : require('../../../assets//images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} />
        <View style={{ marginLeft: 10, backgroundColor: 'transparent' }}>
          <Text style={styles.text_name}>{item.fullname}</Text>
          <Text style={styles.text_phone}>{item.phone}</Text>
        </View>
        <TouchableOpacity style={styles.touch_edit}>
          <Image source={require('../../../assets//images/close_drawer.png')} resizeMode={'contain'} style={styles.icon_right} />
        </TouchableOpacity>
      </View>
    );
  }

  renderRightButton() {
    var views = [];
    views.push(
      <TouchableOpacity key={'right-btn-1'} underlayColor={'transparent'} style={styles.button} onPress={() => { this.pushPage('add_edit_event') }}>
        <Image source={require('../../../assets//images/edit.png')} resizeMode={'contain'} style={styles.icon_qure} />
      </TouchableOpacity>
    );
    views.push(
      <TouchableOpacity key={'right-btn-2'} underlayColor={'transparent'} style={styles.button} onPress={() => { }}>
        <Image source={require('../../../assets//images/trash.png')} resizeMode={'contain'} style={styles.icon_qure} />
      </TouchableOpacity>
    );

    return views;
  }
}

export default connect(null, null)(EventDetail);
const dataTemp = [
  {
    "customerSubProfileId": 67,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 1007,
    "customerSubProfileFlowType": null,
    "id": 6,
    "code": "527ce39f833d4e1bb883e4e28fef54e5",
    "passportId": null,
    "fullname": "Tuan Tu",
    "nickname": "ut det",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phone": '0126383992',
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 68,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 68,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 1009,
    "customerSubProfileFlowType": null,
    "id": 8,
    "code": "527ce39f833d4e1bb883e4e28fef54e5",
    "passportId": null,
    "fullname": "Tuan Tu1",
    "nickname": "ut det1",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phone": '092838084758',
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 69,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 69,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 469,
    "customerSubProfileFlowType": null,
    "id": 9,
    "code": "527ce39f833d4e1bb883e4e28fef54e5",
    "passportId": null,
    "fullname": "Tuan Tu Ma",
    "nickname": "ut det ma",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phone": '08394830902',
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 70,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  }
]