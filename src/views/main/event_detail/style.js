import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  container: {
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20
  },
  flex_row_center: {
    width: '100%',
    height: 45,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  flex_row_time: {
    width: '100%',
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  point: {
    height: 8,
    width: 8,
    borderRadius: 4,
    backgroundColor: Colors.dark_yellow
  },
  time_point: {
    height: 8,
    width: 8,
    borderRadius: 4,
    backgroundColor: Colors.dark_yellow,
    position: 'absolute',
    top: 13
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50
  },
  icon_qure: {
    height: 35,
    width: 35
  },
  task_title: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    marginLeft: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 80,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  text_name: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Bold"
  },
  text_phone: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 3
  }
});
