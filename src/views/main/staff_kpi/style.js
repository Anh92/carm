import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  container: {
    padding: 20
  },
  flex_row_center: {
    width: '100%',
    height: 45,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  point: {
    height: 8,
    width: 8,
    borderRadius: 4,
    backgroundColor: Colors.dark_blue
  },
  task_title: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    marginLeft: 10,
  },
  process_right_text: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    position: 'absolute',
    right: 0,
    bottom: 8
  },
  chart_text: {
    fontSize: 12,
    color: Colors.dark,
    width: 60,
    fontFamily: "Roboto-Regular"
  },
  chart_col_box: {
    height: 20,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  chart_col_text: {
    fontSize: 12,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    paddingLeft: 8
  },
  time_row_underline: {
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
    width: '100%',
    height: 0.5
  },
  user_contact_contain: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15
  },
  chart_box: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  chart: {
    width: 160,
    height: 160
  },
  chart_text_box: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: 160,
    height: 160,
    alignItems: 'center',
    justifyContent: 'center'
  },
  chart_text_big: {
    fontSize: 16,
    color: Colors.dark,
    fontFamily: "Roboto-Bold",
  },
  chart_text_small: {
    fontSize: 11,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    backgroundColor: 'transparent'
  },
  chart_detail: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 15
  },
  chart_detail_text: {
    fontSize: 14,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    marginLeft: 8
  },
  chart_detail_number_text: {
    fontSize: 14,
    color: Colors.dark,
    fontFamily: "Roboto-Bold",
    marginLeft: 8
  },
  chart_row: {
    width: '100%',
    height: 20,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 11
  }
});
