import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Platform,
  processColor
} from 'react-native';
import Colors from '../../../global/colors';
import { connect } from 'react-redux';
import Strings from '../../../global/strings';
import Const from '../../../global/const';
import PickerRange from '../../../widgets/modal_date_range';
import { PieChart } from 'react-native-chart-kit'
var listChartColors = [Colors.dark_blue, Colors.yellow, Colors.orange, Colors.blue, Colors.purple, Colors.gray, Colors.dark, Colors.ink, Colors.smoke, Colors.dark_pink];
var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');
// var colors = [processColor(Colors.yellow), processColor(Colors.dark_pink), processColor(Colors.gray)];

const chartData = [
  { name: 'Cao', population: 5, color: Colors.yellow, legendFontColor: '#7F7F7F', legendFontSize: 15 },
  { name: 'Trung Bình', population: 5, color: Colors.dark_pink, legendFontColor: '#7F7F7F', legendFontSize: 15 },
  { name: 'Thấp', population: 2, color: Colors.gray, legendFontColor: '#7F7F7F', legendFontSize: 15 },
]
const section = {
  title: 'First',
  content: {
    high: 5,
    medium: 5,
    low: 2
  },
  dateRange: {
    fromDate: '1/8/2017',
    toDate: '30/8/2017'
  },
  contractList: [{
    month: '10/2016',
    number: 4
  },
  {
    month: '11/2016',
    number: 5
  },
  {
    month: '12/2016',
    number: 3
  },
  {
    month: '1/2017',
    number: 10
  }]
};

class StaffKpi extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      carInfo: {

      },
    }
  }

  renderContainer() {
    return (
      <ScrollView>
        <View style={styles.container}>
          {this.renderUserContractContent()}
        </View>
      </ScrollView>
    );
  }

  renderUserContractContent() {
    var tottalCustomers = (section.content.high + section.content.medium + section.content.low);
    return (
      <View>
        <TouchableOpacity onPress={() => { this.goToSaleCarPage() }}>
          <View style={styles.flex_row_center}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View style={styles.point}></View>
              <Text style={styles.task_title}>Xe bán (4/6)</Text>
            </View>
            <Text style={styles.process_right_text}>66.6%</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.flex_row_center}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.point}></View>
            <Text style={styles.task_title}>Bảo hiểm (4/6)</Text>
          </View>
          <Text style={styles.process_right_text}>46.6%</Text>
        </View>
        <View style={styles.flex_row_center}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.point}></View>
            <Text style={styles.task_title}>Nội thất (1.4/6)</Text>
          </View>
          <Text style={styles.process_right_text}>6.6%</Text>
        </View>
        <View style={styles.flex_row_center}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.point}></View>
            <Text style={styles.task_title}>TFS (1/1)</Text>
          </View>
          <Text style={styles.process_right_text}>100%</Text>
        </View>
        <View style={styles.flex_row_center}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.point}></View>
            <Text style={styles.task_title}>Xe cũ (0/6)</Text>
          </View>
          <Text style={styles.process_right_text}>0%</Text>
        </View>
        <View style={styles.flex_row_center}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.point}></View>
            <Text style={styles.task_title}>Hợp đồng chờ (3)</Text>
          </View>
        </View>
        <View style={styles.flex_row_center}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.point}></View>
            <Text style={styles.task_title}>Hợp đặt cọc (5)</Text>
          </View>
        </View>
        <View style={styles.flex_row_center}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.point}></View>
            <Text style={styles.task_title}>Hợp đồng đặt mua</Text>
          </View>
        </View>
        {this.renderContractChart(section.contractList)}
        <View style={[styles.time_row_underline, { marginTop: 20 }]} />
        <View style={styles.user_contact_contain}>
          <View style={styles.point}></View>
          <Text style={[styles.task_title, { marginRight: 20 }]}>{Strings.potential_customers + " (" + tottalCustomers + ")"}</Text>
          <PickerRange
            renderModal={this.renderModal.bind(this)}
            setVisibleModal={this.setVisibleModal.bind(this)}
            onDateResult={(rangeDate) => { console.log(rangeDate) }}
          />
        </View>
        <PieChart
          data={chartData}
          width={Const.WIDTH_SCREEN - 15}
          height={160}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#fb8c00',
            backgroundGradientTo: '#ffa726',
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          }}
          accessor="population"
          backgroundColor="transparent"
          absolute
        />
        {/* <View style={styles.chart_box}>
          {this.renderChart(this.renDataSets(section.content), tottalCustomers)}
          <View>
            <View style={styles.chart_detail}>
              <View style={{ height: 15, width: 15, backgroundColor: Colors.yellow }}></View>
              <Text style={styles.chart_detail_text}>Cao</Text>
            </View>
            <View style={[styles.chart_detail, { marginTop: 20 }]}>
              <View style={{ height: 15, width: 15, backgroundColor: Colors.dark_pink }}></View>
              <Text style={styles.chart_detail_text}>Trung bình</Text>
            </View>
            <View style={[styles.chart_detail, { marginTop: 20 }]}>
              <View style={{ height: 15, width: 15, backgroundColor: Colors.gray }}></View>
              <Text style={styles.chart_detail_text}>Thấp</Text>
            </View>
          </View>
          <View>
            <View style={styles.chart_detail}>
              <Text style={styles.chart_detail_number_text}>{': ' + section.content.high}</Text>
            </View>
            <View style={[styles.chart_detail, { marginTop: 20 }]}>
              <Text style={styles.chart_detail_number_text}>{': ' + section.content.medium}</Text>
            </View>
            <View style={[styles.chart_detail, { marginTop: 20 }]}>
              <Text style={styles.chart_detail_number_text}>{': ' + section.content.low}</Text>
            </View>
          </View>
        </View> */}
        <View style={[styles.time_row_underline]} />
      </View>
    );
  }

  goToSaleCarPage() {
    var props = {
      headerText: 'Xe Ban'
    }
    this.pushPage('sale_car', props);
  }
  renderContractChart(contractList) {
    var maxNumber = 0;
    var views = [];
    for (var i = 0; i < contractList.length; i++) {
      if (contractList[i].number > maxNumber) {
        maxNumber = contractList[i].number;
      }
    }
    for (var i = 0; i < contractList.length; i++) {
      views.push(
        <View key={'chart-contact' + i} style={styles.chart_row}>
          <Text style={styles.chart_text}>{contractList[i].month}</Text>
          <View style={styles.chart_col_box}>
            <View style={[styles.chart_col_box, { backgroundColor: listChartColors[i % 10], flex: contractList[i].number }]}>
              <Text style={styles.chart_col_text}>{contractList[i].number}</Text>
            </View>
            <View style={{ flex: maxNumber - contractList[i].number }}></View>
          </View>
        </View>
      );
    }
    return views;
  }
  renDataSets(content) {
    var values = [{ value: content.high, label: '' }, { value: content.medium, label: '' }, { value: content.low, label: '' }];
    var colors = [processColor(Colors.yellow), processColor(Colors.dark_pink), processColor(Colors.gray)];
    return {
      values,
      colors
    };
  }

  renderChart(item, tottalCustomers) {

    return (
      <View
        style={{
          flexDirection: 'column',
          width: 160,
          height: 160,
          marginTop: (Platform.OS === 'ios') ? 0 : 15,
          marginBottom: (Platform.OS === 'ios') ? 0 : 15,
          marginLeft: (Platform.OS === 'ios') ? -15 : 0
        }}>
        {/* <PieChart
          style={styles.chart}
          chartDescription={pieChartConfig.description}
          data={pieChartConfig.data}
          legend={pieChartConfig.legend}
          rotationEnabled={false}
          drawSliceText={false}
          touchEnabled={false}
          usePercentValues={false}
          centerTextRadiusPercent={100}
          transparentCircleRadius={0}
        /> */}
         <PieChart
          data={chartData}
          width={Const.WIDTH_SCREEN - 15}
          height={160}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#fb8c00',
            backgroundGradientTo: '#ffa726',
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          }}
          accessor="population"
          backgroundColor="transparent"
          absolute
        />
        <View style={styles.chart_text_box}>
          <Text style={styles.chart_text_big}>{tottalCustomers}</Text>
          <Text style={styles.chart_text_small}>Khách hàng</Text>
        </View>
      </View>
    );
  }
}
// function mapStateToProps(state) {
//   return {
//     enumTypeApi: state.enumTypeApi,
//     carBrandsApi: state.carBrandsApi,
//     userContext: state.userContext
//   };
// }
export default connect(null, null)(StaffKpi);