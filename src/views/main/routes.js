import {
  Home,
  CustomersList,
  Calendar,
  UserInfo,
  ContractList,
  DeliveryPlan
} from './child_views'
import Const from '../../global/const'

export default {
  home: {
    screen: Home,
    ga_page: Const.GA_PAGE_NAMES.home,
    title: 'Trang Chủ',
    props: {}
  },
  customers_list: {
    title: 'Danh sách khách hàng',
    ga_page: Const.GA_PAGE_NAMES.customer_listing_all,
    screen: CustomersList,
    props: {
      listType: 'all'
    }
  },
  potential_customers: {
    title: 'KH chưa theo dõi',
    ga_page: Const.GA_PAGE_NAMES.customer_listing_potential,
    screen: CustomersList,
    props: {
      listType: 'potential'
    }
  },
  folow_potential_customers: {
    title: 'Theo dõi KH tiềm năng',
    ga_page: Const.GA_PAGE_NAMES.customer_listing_potential_follow,
    screen: CustomersList,
    props: {
      listType: 'potential_follow'
    }
  },
  ssi_contact: {
    title: 'Liên lạc SSI',
    ga_page: Const.GA_PAGE_NAMES.customer_listing_contact_ssi,
    screen: CustomersList,
    props: {
      listType: 'contact_ssi'
    }
  },
  mainten_contact_1k: {
    title: 'Bảo dưỡng 1K',
    ga_page: Const.GA_PAGE_NAMES.customer_listing_contact_1k,
    screen: CustomersList,
    props: {
      listType: 'contact_1k'
    }
  },
  mainten_contact_5k: {
    title: 'Bảo dưỡng 5K',
    ga_page: Const.GA_PAGE_NAMES.customer_listing_contact_5k,
    screen: CustomersList,
    props: {
      listType: 'contact_5k'
    }
  },
  contact_old_car: {
    title: 'Liên lạc xe cũ',
    ga_page: Const.GA_PAGE_NAMES.customer_listing_contact_old,
    screen: CustomersList,
    props: {
      listType: 'contact_old'
    }
  },
  customer_birthday: {
    title: 'Sinh nhật khách hàng',
    ga_page: Const.GA_PAGE_NAMES.customer_listing_birthday,
    screen: CustomersList,
    props: {
      listType: 'Birthday'
    }
  },
  lost_customer_list: {
    title: 'Danh sách KH mất',
    ga_page: Const.GA_PAGE_NAMES.customer_listing_lost_car,
    screen: CustomersList,
    props: {
      listType: 'Lost_car'
    }
  },
  calendar: {
    title: 'Lịch làm việc',
    ga_page: Const.GA_PAGE_NAMES.calendar_home,
    screen: Calendar,
    props: {
    }
  },
  user_info: {
    title: 'Trang cá nhân',
    screen: UserInfo,
    props: {
    }
  },
  contract_list: {
    title: 'Danh sách hợp đồng',
    screen: ContractList,
    props: {
    }
  },
  delivery_plan: {
    title: 'Lập kế hoạch',
    screen: DeliveryPlan,
    props: {
    }
  }
}
