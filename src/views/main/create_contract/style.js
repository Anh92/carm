import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    paddingTop: 10,
    padding: 20
  },
  car_box: {
    height: 80,
    borderBottomColor: '#DFE0E1',
    borderBottomWidth: 0.5,
    justifyContent: 'center'
  },
  car_name: {
    fontSize: 14,
    color: Colors.dark,
    fontFamily: "Roboto-Bold"
  },
  car_status: {
    fontSize: 12,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  staff_name: {
    fontSize: 12,
    color: Colors.dark,
    fontFamily: "Roboto-Italic",
    marginTop: 5
  },
  submit: {
    marginTop: 30,
    marginBottom: 50,
    height: 35,
    width: 120,
    backgroundColor: Colors.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  btn_text: {
    color: 'white',
    fontSize: 15,
    fontFamily: "Roboto-Regular",
  },
  row_image_right: {
    position: 'absolute',
    width: 20,
    height: 20,
    right: 8
  }
});
