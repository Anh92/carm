import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput
} from 'react-native';
import Colors from '../../../global/colors';
import Routes from '../../../views/routes';
import Tabbar from '../../../widgets/tabbar';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');

class CreateContract extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      contractData: listCarTemp
    }
  }

  renderContainer() {
    return (
      <ScrollView style={styles.container}>
        {this.renderContract()}
        <TouchableOpacity
          onPress={() => { this.onPressSubmit() }}
          style={styles.submit}
          underlayColor='transparent'>
          <Text style={styles.btn_text}>Tạo</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
  renderContract() {
    var views = [];
    for (var i = 0; i < this.state.contractData.length; i++) {
      views.push(this.renderContractRows(this.state.contractData[i], i));
    }
    return views;
  }
  renderContractRows(item, index) {
    return (
      <TouchableOpacity key={"contract" + index} style={styles.car_box} onPress={() => { this.selectContract(index) }}>
        <Text style={styles.car_name}>{item.carGradeName + " - " + item.carColorName}</Text>
        <Text style={styles.car_status}>{item.carColorName}</Text>
        <Text style={styles.staff_name}>{item.transactionStatusDisplayname}</Text>
        <Image source={item.chosen ? require('../../../assets//images/seleted_gr.png') : require('../../../assets//images/unselect_gr.png')} resizeMode={'contain'} style={styles.row_image_right} />
      </TouchableOpacity>
    );
  }
  onPressSubmit() {
    const dataTemp = this.state.contractData.filter((e) => {
      return e.chosen ;
    });
    console.log(dataTemp);
    this.goBack();
  }
  selectContract(index) {
    this.state.contractData[index].chosen = !this.state.contractData[index].chosen;
    this.setState({
      contractData: this.state.contractData
    });
  }
}
module.exports = CreateContract;
var listCarTemp = [
  {
    "staffId": 481,
    "staffName": "481",
    "subProfileCarInfoId": 112,
    "isClosed": false,
    "causalClosed": null,
    "isContact": false,
    "relationships": "RELATED",
    "deliveredDate": null,
    "id": 112,
    "carGradeId": 32,
    "carColorId": 15,
    "carBrandName": "Toyota",
    "carModelName": "Fortuner",
    "carGradeName": "Fortuner 2.7V 4x4",
    "carColorName": "Trắng ngọc trai",
    "vin": null,
    "engineNumber": null,
    "plateNumber": null,
    "km": null,
    "nextKmMaintenance": null,
    "nextDateMaintenance": null,
    "isNew": true,
    "transactionId": null,
    "transactionContractId": null,
    "transactionStatusId": 1,
    "transactionStatusName": "contact",
    "transactionStatusDisplayname": "Tiếp cận",
    "transactionStatusDescription": null,
    "transactionStatuses": [
      {
        "name": "contact",
        "displayName": "Tiếp cận",
        "order": 1
      },
      {
        "name": "contract",
        "displayName": "Hợp đồng",
        "order": 2
      },
      {
        "name": "deposit",
        "displayName": "Đặt cọc",
        "order": 3
      },
      {
        "name": "paymented",
        "displayName": "Thanh toán",
        "order": 4
      },
      {
        "name": "delivered",
        "displayName": "Giao xe",
        "order": 5
      },
      {
        "name": "onek",
        "displayName": "1k",
        "order": 6
      },
      {
        "name": "fivek",
        "displayName": "5k",
        "order": 7
      },
      {
        "name": "service",
        "displayName": "Bảo dưỡng định kỳ",
        "order": 8
      }
    ]
  },
  {
    "staffId": null,
    "staffName": "null",
    "subProfileCarInfoId": 113,
    "isClosed": false,
    "causalClosed": null,
    "isContact": false,
    "relationships": "RELATED",
    "deliveredDate": null,
    "id": 113,
    "carGradeId": 32,
    "carColorId": 15,
    "carBrandName": "Toyota",
    "carModelName": "Fortuner",
    "carGradeName": "Fortuner 2.7V 4x3",
    "carColorName": "Trắng ngọc trai",
    "vin": null,
    "engineNumber": null,
    "plateNumber": null,
    "km": null,
    "nextKmMaintenance": null,
    "nextDateMaintenance": null,
    "isNew": true,
    "transactionId": null,
    "transactionContractId": null,
    "transactionStatusId": 1,
    "transactionStatusName": "contact",
    "transactionStatusDisplayname": "Tiếp cận",
    "transactionStatusDescription": null,
    "transactionStatuses": [
      {
        "name": "contact",
        "displayName": "Tiếp cận",
        "order": 1
      },
      {
        "name": "contract",
        "displayName": "Hợp đồng",
        "order": 2
      },
      {
        "name": "deposit",
        "displayName": "Đặt cọc",
        "order": 3
      },
      {
        "name": "paymented",
        "displayName": "Thanh toán",
        "order": 4
      },
      {
        "name": "delivered",
        "displayName": "Giao xe",
        "order": 5
      },
      {
        "name": "onek",
        "displayName": "1k",
        "order": 6
      },
      {
        "name": "fivek",
        "displayName": "5k",
        "order": 7
      },
      {
        "name": "service",
        "displayName": "Bảo dưỡng định kỳ",
        "order": 8
      }
    ]
  }
]