import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  RefreshControl
} from 'react-native';
import Routes from '../../../../views/routes';
import { connect } from 'react-redux';

var styles = require('./style.js');
var setBackId = Object.freeze({
  CONTRACT_INFO: 'CONTRACT_INFO'
});

class ContractList extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.create_contract_btn} onPress={() => { this.openFindCustomerPage() }}>
          <Image source={require('../../../../assets/images/add_more.png')} resizeMode={'contain'} style={{ width: 25, height: 25 }} />
          <Text style={styles.create_contract_text}>Tạo hợp đồng</Text>
        </TouchableOpacity>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={false}
              tintColor="transparent"
              onRefresh={this.onRefresh.bind(this)}
            />
          }
          data={dataTemp}
          renderItem={({ item, index }) => this.renderItems(item, index)}
        />
        {/* {isLoading &&
          <View style={styles.loading_box}>
            <ActivityIndicator size="large"
              color={Colors.gray} />
          </View>} */}
      </View>
    );
  }

  onRefresh() {

  }
  openFindCustomerPage() {
    // var page = Routes.find_customer;
    this.props.screenProps.pushPage('find_customer', { headerText: 'Chọn khách hàng' });
  }
  onpenContractInfoPage(item, index) {
    var page = Routes.contract_info;
    var props = {
      ...page.props,
      customerData: item,
      setBack: this.setBack.bind(this),
      setBackId: setBackId.CON
    };
    this.props.screenProps.pushPage('contract_info', props);
  }

  showPhone(phones) {
    var activePhoneIndex = phones.findIndex((element) => {
      return element.active;
    })
    if (activePhoneIndex != -1) {
      return phones[activePhoneIndex].number;
    }
    return phones[0].number;
  }

  //     {this.props.listType != 'customers_list' && <Text style={styles.text_status}>{item.status}</Text>}
  renderItems(item, index) {
    return (
      <TouchableOpacity onPress={() => { this.onpenContractInfoPage(item, index) }}>
        <View style={styles.row}>
          <Image source={item.iconProfile ? { uri: item.iconProfile } : require('../../../../assets/images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} />
          <View style={{ marginLeft: 10, backgroundColor: 'transparent' }}>
            <Text style={styles.text_name}>{item.fullname || item.nickname}</Text>
            <Text style={styles.text_phone}>{((item.phones && item.phones.length > 0) && this.showPhone(item.phones))}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  setBack(id, data) {
    switch (id) {

    }
  }
}

// function mapStateToProps(state) {
//   return { customerPage: state.customerPage };
// }

export default connect(null, null)(ContractList);

const dataTemp = [
  {
    "customerSubProfileId": 71,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 11,
    "code": "a55b9cbd6aea4b289e12b5e1ee601196",
    "passportId": "1234567890_122",
    "fullname": "Nguyen Van 122",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 129,
        "phoneableId": 11,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_122",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 10:04:00",
        "updatedAt": "2017-10-11 10:04:00"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 72,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 88,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 22,
    "code": "1d84934127a0495793875c2718a5cb44",
    "passportId": "1234567890_128",
    "fullname": "Nguyen Van 128",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 154,
        "phoneableId": 22,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_128",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-12 10:51:18",
        "updatedAt": "2017-10-12 10:51:18"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 89,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 92,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 470,
    "customerSubProfileFlowType": null,
    "id": 30,
    "code": "0207a50b7ecb4720a5dafcdbc96b5b96",
    "passportId": null,
    "fullname": "Tuan Em",
    "nickname": "ut rang",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phones": [
      {
        "id": 163,
        "phoneableId": 30,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "0949967580",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-12 08:49:31",
        "updatedAt": "2017-10-12 08:49:31"
      }
    ],
    "email": null,
    "job": "Mua xe",
    "province": "TP.Hồ Chí Minh",
    "district": "Quận 1",
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 93,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 67,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 1007,
    "customerSubProfileFlowType": null,
    "id": 6,
    "code": "527ce39f833d4e1bb883e4e28fef54e5",
    "passportId": null,
    "fullname": "Tuan Tu",
    "nickname": "ut det",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phones": [
      {
        "id": 122,
        "phoneableId": 6,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "0949967496",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-10 01:16:00",
        "updatedAt": "2017-10-10 01:16:00"
      }
    ],
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": true,
    "customerSubProfileType": {
      "id": 68,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 79,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 16,
    "code": "cd9e8ed0e50a4b8ab8a037debc5a3dcd",
    "passportId": null,
    "fullname": "Tuan Tu Ma",
    "nickname": "ut det ma",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phones": [
      {
        "id": 140,
        "phoneableId": 16,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "0949967497",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 04:35:25",
        "updatedAt": "2017-10-11 04:35:25"
      }
    ],
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 80,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 76,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 470,
    "customerSubProfileFlowType": null,
    "id": 13,
    "code": "0f33ff324b564b778d7b43c76ce28b68",
    "passportId": null,
    "fullname": null,
    "nickname": "Gdbdjsj",
    "gender": null,
    "genderDisplayname": null,
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phones": [
      {
        "id": 134,
        "phoneableId": 13,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "089594",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 10:19:38",
        "updatedAt": "2017-10-11 10:19:38"
      }
    ],
    "email": null,
    "job": null,
    "province": null,
    "district": null,
    "address": null,
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 77,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 87,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 470,
    "customerSubProfileFlowType": null,
    "id": 21,
    "code": "0ec3836a29ad4151933f2137fc1e65f6",
    "passportId": "1234567890_127",
    "fullname": "Nguyen Van 127",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 150,
        "phoneableId": 21,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_127",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 07:15:23",
        "updatedAt": "2017-10-11 07:15:23"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 88,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 85,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 19,
    "code": "c5b7dd6c4aa141e493b20a9d12511120",
    "passportId": "1234567890_126",
    "fullname": "Nguyen Van 126",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 146,
        "phoneableId": 19,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_126",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 11:58:13",
        "updatedAt": "2017-10-11 11:58:13"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 86,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 82,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 17,
    "code": "e70d7eeea28e454f95d2950420713f9c",
    "passportId": null,
    "fullname": "Tuan Tu Ma",
    "nickname": "ut det ma",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phones": [
      {
        "id": 143,
        "phoneableId": 17,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "0949967499",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 04:42:54",
        "updatedAt": "2017-10-11 04:42:54"
      }
    ],
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 83,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 95,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 33,
    "code": "c2c40fb94f604af88daafd560dd3bfd3",
    "passportId": "1234567890_210",
    "fullname": "Nguyen Van 210-update-xxx",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 170,
        "phoneableId": 33,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_210",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-13 03:03:45",
        "updatedAt": "2017-10-13 03:31:44"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 96,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 59,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 1,
    "code": "bfdd3b9070dc41fa8904fb6db06d56e2",
    "passportId": null,
    "fullname": "Tuan anh",
    "nickname": "ut tuoi2",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phones": [
      {
        "id": 114,
        "phoneableId": 1,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "0949967491",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-10 12:54:10",
        "updatedAt": "2017-10-10 12:54:10"
      }
    ],
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 60,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 78,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 15,
    "code": "92bbcc03370943c2b0f48a1a6031be27",
    "passportId": "1234567890_125",
    "fullname": "Nguyen Van 125",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 138,
        "phoneableId": 15,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_125",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 10:30:58",
        "updatedAt": "2017-10-11 10:30:58"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 79,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 86,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 470,
    "customerSubProfileFlowType": null,
    "id": 20,
    "code": "49b0cef1f8eb43a5b3099a96b7876d30",
    "passportId": "123",
    "fullname": "Tuan Tu Ma1",
    "nickname": "ut det ma1",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phones": [
      {
        "id": 148,
        "phoneableId": 20,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "0949967410",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 07:11:47",
        "updatedAt": "2017-10-11 07:11:47"
      }
    ],
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 87,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 70,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 10,
    "code": "cff13056257d4850b70ef9d9ceb080cf",
    "passportId": "1234567890_121",
    "fullname": "Nguyen Van 121",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 127,
        "phoneableId": 10,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_121",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 09:49:45",
        "updatedAt": "2017-10-11 09:49:45"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 71,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 74,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 470,
    "customerSubProfileFlowType": null,
    "id": 12,
    "code": "e1361fb38a4c4626be5cb27fd0a737c9",
    "passportId": null,
    "fullname": null,
    "nickname": "Dsfs do",
    "gender": null,
    "genderDisplayname": null,
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phones": [
      {
        "id": 132,
        "phoneableId": 12,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "34535",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 10:12:23",
        "updatedAt": "2017-10-11 10:12:23"
      }
    ],
    "email": null,
    "job": null,
    "province": null,
    "district": null,
    "address": null,
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 75,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 77,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "id": 14,
    "code": "63be95c4cde347008e4b30267c01912a",
    "passportId": "1234567890_124",
    "fullname": "Nguyen Van 124",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 136,
        "phoneableId": 14,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_124",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 10:22:47",
        "updatedAt": "2017-10-11 10:22:47"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 78,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  }
]