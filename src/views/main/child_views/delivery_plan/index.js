import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  RefreshControl,
  ActivityIndicator
} from 'react-native';
import Strings from '../../../../global/strings';
import Routes from '../../../../views/routes';
import Colors from '../../../../global/colors'
import { connect } from 'react-redux';
import Utils from '../../../../global/utils';

var styles = require('./style.js');

class ContractList extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={false}
              tintColor="transparent"
              onRefresh={this.onRefresh.bind(this)}
            />
          }
          data={dataTemp}
          renderItem={({ item, index }) => this.renderItems(item, index)}
        />
        {/* {isLoading &&
          <View style={styles.loading_box}>
            <ActivityIndicator size="large"
              color={Colors.gray} />
          </View>} */}
      </View>
    );
  }

  onRefresh() {

  }
  onpenContractInfoPage(item, index) {
    var page = Routes.contract_info;
    var props = {
      ...page.props,
      customerData: item,
      type: 'delivery_plan'
    };
    this.props.screenProps.pushPage('contract_info', props);
  }

  //     {this.props.listType != 'customers_list' && <Text style={styles.text_status}>{item.status}</Text>}
  renderItems(item, index) {
    return (
      <TouchableOpacity onPress={() => { this.onpenContractInfoPage(item, index) }}>
        <View style={styles.row}>
          <Image source={item.iconProfile ? { uri: item.iconProfile } : require('../../../../assets/images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} />
          <View style={{ marginLeft: 10, backgroundColor: 'transparent' }}>
            <Text style={styles.text_name}>{item.carGradeName + " - " + item.carColorName}</Text>
            <Text style={styles.text_phone}>{item.fullname || item.nickname}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

// function mapStateToProps(state) {
//   return { customerPage: state.customerPage };
// }

export default connect(null, null)(ContractList);

const dataTemp = [
  {
    "customerSubProfileId": 71,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "carGradeName": "Fortuner 2.7V 4x4",
    "carColorName": "Trắng ngọc trai",
    "id": 11,
    "code": "a55b9cbd6aea4b289e12b5e1ee601196",
    "passportId": "1234567890_122",
    "fullname": "Nguyen Van 122",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 129,
        "phoneableId": 11,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_122",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-11 10:04:00",
        "updatedAt": "2017-10-11 10:04:00"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 72,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 88,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 481,
    "customerSubProfileFlowType": null,
    "carGradeName": "Fortuner 2.7V 4x4",
    "carColorName": "Trắng ngọc trai",
    "id": 22,
    "code": "1d84934127a0495793875c2718a5cb44",
    "passportId": "1234567890_128",
    "fullname": "Nguyen Van 128",
    "nickname": "i-crm",
    "gender": "MALE",
    "genderDisplayname": "Nam",
    "vocative": "Anh/Chị",
    "marital": "SINGLE",
    "maritalDisplayname": "Độc thân",
    "birthday": "1985-01-01",
    "phones": [
      {
        "id": 154,
        "phoneableId": 22,
        "phoneableType": "com.flix.crm.server.modules.customer.domain.CrmCustomerInfoForSale",
        "number": "1234567890_128",
        "type": "MOBILE",
        "active": true,
        "order": 0,
        "status": true,
        "createdAt": "2017-10-12 10:51:18",
        "updatedAt": "2017-10-12 10:51:18"
      }
    ],
    "email": "yourname@gmail.com",
    "job": "Developer",
    "province": "TP.Hồ Chí Minh",
    "district": "Phú Nhuận",
    "address": "140A Huỳnh Văn Bánh, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 89,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  }
]