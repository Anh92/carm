import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import LunarCalendar from '../../../../widgets/lunar_calendar';
import styles from './style'
import Routes from '../../../../views/routes';
import ModelAction from '../../../../widgets/modal_actions';

const phoneAction = [{
  key: 'all',
  text: 'Tất cả'
}, {
  key: 'service',
  text: 'Lịch hẹn dịch vụ'
}, {
  key: 'add_more_phone',
  text: 'Lịch hẹn truyền thông'
}];

const workSchedule = [{
  date: '2019-08-15',
  works: [{
    time: "5:30",
    end: "2:00",
    title: "Gặp khách hàng C"
  },
  {
    time: "6:00",
    end: "2:00",
    title: "Gặp khách hàng D"
  },
  {
    time: "23:30",
    end: "2:00",
    title: "Gặp khách hàng D"
  }]
},
{
  date: '2019-08-17',
  works: [{
    time: "2:00",
    end: "2:00",
    title: "Gặp khách hàng A",
    where: 'Cafe heeel',
    note: 'Khach muon giam gia'
  },
  {
    time: "2:00",
    end: "2:00",
    title: "Gặp khách hàng B",
    where: 'dai ly'
  },
  {
    time: "2:00",
    end: "2:00",
    title: "Ăn tối với khách hàng J"
  }]
},
{
  date: '2019-08-25',
  works: [{
    time: "2:00",
    end: "2:00",
    title: "Ăn tối với khách hàng J"
  },
  {
    time: "5:30",
    end: "2:00",
    title: "Gặp khách hàng C"
  }]
}];

class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: 'Tất cả',
      works: []
    }
  }

  render() {
    return (
      <View>
        <View style={styles.btn_header_box}>
          <TouchableOpacity style={styles.header_btn} onPress={this.openCreateEventPage.bind(this)}>
            <Image source={require('../../../../assets/images/add_more.png')} style={styles.btn_header_img} />
            <Text style={styles.btn_header_text}>Tạo lịch mới</Text>
          </TouchableOpacity>
          <View style={styles.btn_wall}></View>
          <View style={styles.header_btn}>
            <ModelAction
              renderHeader={() => {
                return (
                  <View style={styles.header_modal_btn}>
                    <Image source={require('../../../../assets/images/filter.png')} style={styles.btn_header_img} />
                    <Text style={styles.btn_header_text}>{this.state.filterText}</Text>
                  </View>
                )
              }}
              onSelect={(item, dataBack) => { this.setState({ filterText: item.text }) }}
              setVisibleModal={this.props.screenProps.setVisibleModal}
              renderModal={this.props.screenProps.renderModal}
              actionList={phoneAction} />

          </View>
        </View>
        <ScrollView style={styles.scl_container}>
          <LunarCalendar
            renderModal={this.props.screenProps.renderModal}
            setVisibleModal={this.props.screenProps.setVisibleModal}
            workSchedule={workSchedule}
            onDateSelect={(date) => this.onDateSelect(date)}
            onMonthSelect={(mon) => console.log(mon)} />
          <View style={[styles.underline, { marginTop: 8 }]}></View>
          {this.renderTimeTasks()}
          <View style={{ height: 100 }}></View>
        </ScrollView>
      </View>
    );
  }
  renderTimeTasks() {
    var views = [];
    for (var i = 0; i < this.state.works.length; i++) {
      views.push(
        this.renderTaskRow(i)
      );
    }
    return views;
  }

  renderTaskRow(i) {
    return (
      <TouchableOpacity key={"timetask" + i} style={styles.time_task_row} onPress={() => this.openEventDetailPage(this.state.works[i])}>
        <View style={styles.time_task_hour_box}>
          <Text style={styles.time_task_hour_text}>{this.state.works[i].time}</Text>
          <Text style={[styles.time_task_hour_text, { marginTop: 8 }]}>{this.state.works[i].end}</Text>
        </View>
        <View style={{ height: 55, width: 5, backgroundColor: 'green', marginLeft: 8, marginRight: 8 }}></View>
        <View style={styles.time_task_detail_box}>
          <Text style={styles.time_task_hour_text}>{this.state.works[i].title}</Text>
          <Text style={styles.time_task_loca_text}>{this.state.works[i].where}</Text>
          <Text style={styles.time_task_note_text}>{this.state.works[i].note}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  onDateSelect(date) {
    if (workSchedule != undefined && workSchedule != null && workSchedule.length > 0) {
      for (var i = 0; i < workSchedule.length; i++) {
        var dateTemp = new Date(workSchedule[i].date)
        if (dateTemp.getDate() == date.getDate() && dateTemp.getMonth() + 1 == date.getMonth() + 1 && dateTemp.getFullYear() == date.getFullYear()) {
          this.setState({
            works: workSchedule[i].works
          })
          return;
        }
      }
    }
    this.setState({
      works: []
    })
  }
  openCreateEventPage() {
    var page = Routes.add_edit_event;
    var props = { ...page.props };
    this.props.screenProps.pushPage('add_edit_event', props);
  }
  openEventDetailPage(item) {
    var page = Routes.event_detail;
    var props = { ...page.props, headerText: item.title };
    this.props.screenProps.pushPage('event_detail', props);
  }
}


export default connect(null, null)(Calendar);