import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../../global/colors'

module.exports = StyleSheet.create({
  btn_header_box: {
    height: 50,
    backgroundColor: Colors.light_dark,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  header_btn: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 20,
    flex: 1
  },
  header_modal_btn: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 500
  },
  btn_wall: {
    width: 0.5,
    backgroundColor: Colors.white,
    borderWidth: 0.5,
    borderColor: Colors.border,
    height: 30
  },
  btn_header_text: {
    color: Colors.white,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    marginLeft: 8
  },
  btn_header_img: {
    width: 20,
    height: 20
  },
  time_task_row: {
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  underline: {
    width: '100%',
    height: 1,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  time_task_hour_box: {
    height: '100%',
    width: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  time_task_hour_text: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
  },
  time_task_detail_box: {
    height: 80,
    flex: 1,
    justifyContent: 'center'
  },
  time_task_loca_text: {
    fontSize: 12,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  time_task_note_text: {
    fontSize: 12,
    color: Colors.gray,
    fontFamily: "Roboto-Italic",
    marginTop: 5
  },
  scl_container: {
    width: "100%",
    height: "100%",
    padding: 10
  }
});
