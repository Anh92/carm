import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import styles from './style'
import StringSource from '../../../../global/strings'
import { doLogout } from '../../../../actions/mainActions/userInfoAction';

class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Image source={{ uri: 'https://pbs.twimg.com/profile_images/831993825635745796/HnVmB0-k.jpg' }} resizeMode={'cover'} style={styles.avatar} />
          <Text style={styles.text_name}>Nguyen Van A</Text>
          <View style={styles.input_row}>
            <Text style={styles.text_title}>Tên đăng nhập</Text>
            <Text underlineColorAndroid={'transparent'} style={styles.text_info}>hoangka</Text>
          </View>
          <View style={styles.input_row}>
            <Text style={styles.text_title}>Số điện thoại</Text>
            <Text underlineColorAndroid={'transparent'} style={styles.text_info}>09284378292</Text>
          </View>
          <View style={styles.input_row}>
            <Text style={styles.text_title}>Email</Text>
            <Text underlineColorAndroid={'transparent'} style={styles.text_info}>nguyenvana@gmail.com</Text>
          </View>
          <TouchableOpacity
            onPress={() =>
              this.props.screenProps.resetPage(0, 'login', { name: 'Jane' })
              // this.props.doLogout(this)
            }
            style={styles.submit}
            underlayColor='transparent'>
            <Text style={styles.btn_text}>{StringSource.logout_btn}</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
  componentDidMount() {

  }
}
function mapStateToProps(state) {
  return {
    userContext: state.userContext
  };
}
export default connect(mapStateToProps, { doLogout })(UserInfo);