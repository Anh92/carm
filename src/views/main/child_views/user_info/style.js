import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../../global/colors'

module.exports = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 20
  },
  avatar: {
    width: 90,
    height: 90,
    borderRadius: 45,
    marginTop: 30
  },
  input_row: {
    width: "100%",
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  },
  text_info: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  submit: {
    marginTop: 50,
    height: 50,
    width: 200,
    backgroundColor: Colors.dark_pink,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn_text: {
    color: Colors.white,
    fontSize: 18,
    fontFamily: "Roboto-Regular"
  },
  text_name: {
    fontSize: 20,
    color: Colors.dark,
    fontFamily: "Roboto-Bold",
    marginTop: 20,
    marginBottom: 30
  }
});
