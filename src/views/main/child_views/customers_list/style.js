import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../../global/colors'

module.exports = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: "#F6F6F6"
  },
  underline: {
    backgroundColor: '#555B61',
    width: '100%',
    height: 0.5,
    marginTop: 8
  },
  text_phone: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 3
  },
  text_name: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Bold"
  },
  text_status: {
    color: '#02867C',
    fontSize: 12,
    marginTop: 3
  },
  touch_filter: {
    width: 50,
    height: 50,
    right: 0,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  touch_edit: {
    height: 50,
    position: 'absolute',
    right: 0,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  filter_box: {
    height: 50,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    borderBottomColor: '#DFE0E1',
    borderBottomWidth: 1,
    width: '100%'
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50
  },
  input_filter: {
    height: 50,
    flex: 1,
    marginRight: 50,
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Italic",
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 80,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
    marginRight: 20,
    marginLeft: 20
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  submit: {
    height: 35,
    width: 120,
    backgroundColor: Colors.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  btn_text: {
    fontSize: 15,
    color: 'white',
    fontFamily: "Roboto-Regular"
  },
  btn_box: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 10
  },
  modal_box: {
    position: 'absolute',
    left: 30,
    right: 30,
    padding: 20,
    backgroundColor: "white"
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  },
  loading_box: {
    position: 'absolute',
    top: 60,
    width: '100%'
  }
});
