import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  RefreshControl,
  ActivityIndicator
} from 'react-native';
import Strings from '../../../../global/strings';
import ModelAction from '../../../../widgets/modal_actions';
import Routes from '../../../../views/routes';
import Colors from '../../../../global/colors'
import { connect } from 'react-redux';
import Utils from '../../../../global/utils';
import { doGetData } from '../../../../actions/mainActions/customerListAction';

var styles = require('./style.js');
const customerAction = [{
  key: 'make_call',
  text: 'Gọi điện thoại'
}, {
  key: 'send_mes',
  text: 'Nhắn tin'
}, {
  key: 'book_meet',
  text: 'Đặt hẹn'
}, {
  key: 'tran',
  text: 'Chuyển giao'
}];
var setBackId = Object.freeze({
  TRANSFER_CUSTOMER: 'TRANSFER_CUSTOMER',
  CUSTOMER_INFO: 'CUSTOMER_INFO'
});

class CustomersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customerModalVisible: true,
      refreshing: false,
      filterKey: '',
      customerList: [{
        fullname: 'Nguyen Van A',
        nickname: 'A Van',
        phones: [{ active: true, number: '0164354332' }],
        email: 'nguyenvana@gmail.com'
      }, {
        fullname: 'Nguyen Van B',
        nickname: 'A Van',
        phones: [{ active: true, number: '01645654654' }],
        email: 'nguyenvana@gmail.com'
      }, {
        fullname: 'Nguyen Van C',
        nickname: 'A Van',
        phones: [{ active: true, number: '09043543534' }],
        email: 'nguyenvana@gmail.com',
        isDuplicateSubProfile: true
      }, {
        fullname: 'Nguyen Van D',
        nickname: 'A Van',
        phones: [{ active: true, number: '0840853495' }],
        email: 'nguyenvana@gmail.com',
        isDuplicateSubProfile: true
      }, {
        fullname: 'Nguyen Van E',
        nickname: 'A Van',
        phones: [{ active: true, number: '07839832993' }],
        email: 'nguyenvana@gmail.com'
      }, {
        fullname: 'Nguyen Van F',
        nickname: 'A Van',
        phones: [{ active: true, number: '01893923293' }],
        email: 'nguyenvana@gmail.com'
      }]
    }
  }
  componentWillMount() {
    // this.props.doGetData(this.props.listType, this);
  }

  render() {
    const { isLoading } = this.props.customerPage;
    return (
      <View style={styles.container}>
        <View style={styles.filter_box}>
          <TouchableOpacity style={styles.button}>
            <Image source={require('../../../../assets/images/search.png')} resizeMode={'contain'} style={{ width: 30, height: 30, backgroundColor: this.props.isDropdown ? '#00554E' : 'transparent', borderRadius: 3 }} />
          </TouchableOpacity>
          <TextInput
            onChangeText={(text) => this.setState({ filterKey: text })}
            value={this.state.filterKey}
            underlineColorAndroid={'transparent'}
            style={styles.input_filter}
            placeholder={Strings.filter_name_phone_hint}
            placeholderTextColor={'#989899'}
            placeholderStyle={styles.input_holder} />
          <TouchableOpacity style={styles.touch_filter} onPress={this.openFilter.bind(this)}>
            <Image source={require('../../../../assets/images/filter.png')} resizeMode={'contain'} style={{ width: 22, height: 20, backgroundColor: this.props.isDropdown ? '#00554E' : 'transparent', borderRadius: 3 }} />
          </TouchableOpacity>
        </View>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={false}
              tintColor="transparent"
              onRefresh={this.onRefresh.bind(this)}
            />
          }
          data={this.getCustomerList()}
          renderItem={({ item, index }) => this.renderItems(item, index)}
        />
        {isLoading &&
          <View style={styles.loading_box}>
            <ActivityIndicator size="large"
              color={Colors.gray} />
          </View>}

      </View>
    );
  }
  onRefresh() {
    // this.props.doGetData(this.props.listType, this);
    // fetchData().then(() => {
    //   this.setState({ refreshing: false });
    // });
  }

  getCustomerList() {
    // var { customerList } = this.props.customerPage;
    var { customerList } = this.state;

    if (this.state.filterKey == '' || this.state.filterKey == null)
      return customerList;
    return customerList.filter((e) => {
      var mFilter = Utils.convertVietNameLetter(this.state.filterKey);
      var mFullname = Utils.convertVietNameLetter(e.fullname || "");
      var mNickname = Utils.convertVietNameLetter(e.nickname || "");
      if (mFullname.indexOf(mFilter) != -1) {
        e.showName = e.fullname;
        return true;
      }
      if (mNickname.indexOf(mFilter) != -1) {
        e.showName = e.nickname;
        return true;
      }
      mFilter = "" + parseInt("1" + mFilter);
      mFilter = mFilter.substr(1);
      if (mFilter.length == this.state.filterKey.length && e.phones && e.phones.length > 0) {
        for (var i = 0; i < e.phones.length; i++) {
          if (e.phones[i].number.indexOf(mFilter) != -1) {
            customerList.showPhone = e.phones[i].number;
            return true;
          }
        }
      }
      return false;
    });

  }

  openFilter() {
    var page = Routes.customer_filter;
    var props = { ...page.props, setBack: this.setBack.bind(this) };
    this.props.screenProps.pushPage('customer_filter', props);
  }

  doAction(item) {
    console.log(item);
    this.props.setVisibleModal(false);
  }

  onActionSelect(item, dataBack) {
    switch (item.key) {
      case 'make_call':
        break;
      case 'send_mes':
        break;
      case 'book_meet':
        break;
      case 'tran':
        var page = Routes.transfer_customer;
        var props = {
          ...page.props,
          setBack: this.setBack.bind(this),
          customerData: dataBack,
          setBackId: setBackId.TRANSFER_CUSTOMER
        };
        this.props.screenProps.pushPage('transfer_customer', props);
        break;
      case 'delete':
        // var data = this.state.data;
        // data.splice(index, 1);
        break;
      default:
        break;
    }
  }
  onpenCustomerInfoPage(item, index) {
    if (item.isDuplicateSubProfile) {
      this.props.screenProps.pushPage('duplicate_folow');
    } else {
      var page = Routes.customer_info;
      var props = {
        ...page.props,
        customerData: item,
        setBack: this.setBack.bind(this),
        setBackId: setBackId.CUSTOMER_INFO
      };
      this.props.screenProps.pushPage('customer_info', props);
    }
  }

  openModalApprove(item, index) {
    this.props.screenProps.renderModal(
      <View style={styles.modal_box}>
        <Text style={styles.text_title}>{item.name + " sẽ được chọn để chăm sóc khách hàng"}</Text>
        <View style={styles.btn_box}>
          <TouchableOpacity
            onPress={() => { this.props.setVisibleModal(false) }}
            style={[styles.submit, { backgroundColor: "#E4E4E4", marginRight: 30 }]}
            underlayColor='transparent'>
            <Text style={[styles.btn_text, { color: Colors.dark }]}>Hủy</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => { this.doApprove(item) }}
            style={styles.submit}
            underlayColor='transparent'>
            <Text style={styles.btn_text}>Lưu</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
    this.props.setVisibleModal(true);
  }

  showPhone(phones) {
    var activePhoneIndex = phones.findIndex((element) => {
      return element.active;
    })
    if (activePhoneIndex != -1) {
      return phones[activePhoneIndex].number;
    }
    return phones[0].number;
  }

  //     {this.props.listType != 'customers_list' && <Text style={styles.text_status}>{item.status}</Text>}
  renderItems(item, index) {
    return (
      <TouchableOpacity style={{ backgroundColor: item.isDuplicateSubProfile ? '#FF9898' : 'transparent' }} onPress={() => { this.onpenCustomerInfoPage(item, index) }}>
        <View style={styles.row}>
          <Image source={require('../../../../assets/images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} />
          {/* <Image source={item.iconProfile ? { uri: item.iconProfile } : require('../../../../assets/images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} /> */}
          <View style={{ marginLeft: 10, backgroundColor: 'transparent' }}>
            <Text style={styles.text_name}>{item.showName || item.fullname || item.nickname}</Text>
            <Text style={styles.text_phone}>{item.showPhone || ((item.phones && item.phones.length > 0) && this.showPhone(item.phones))}</Text>
          </View>
          <View style={styles.touch_edit}>
            <ModelAction
              setVisibleModal={this.props.screenProps.setVisibleModal.bind(this)}
              renderModal={this.props.screenProps.renderModal.bind(this)}
              onSelect={this.onActionSelect.bind(this)}
              actionList={customerAction}
              dataBack={item} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  setBack(id, data) {
    switch (id) {
      case setBackId.TRANSFER_CUSTOMER:
        // this.props.doGetData(this.props.listType, this);
        break;
      case setBackId.CUSTOMER_INFO:
        // this.props.doGetData(this.props.listType, this);
        break;
    }
  }
}

function mapStateToProps(state) {
  return { customerPage: state.customerPage };
}

export default connect(mapStateToProps, { doGetData })(CustomersList);