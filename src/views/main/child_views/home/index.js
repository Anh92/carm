import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  ScrollView
} from 'react-native';
import Expand from 'react-native-collapsible';
import { connect } from 'react-redux';
import Strings from '../../../../global/strings'
import Const from '../../../../global/const';
import Colors from '../../../../global/colors'
import ModalDropdown from '../../../../widgets/modal_dropdown';
import parentRoutes from '../../../../views/routes';
import { PieChart } from 'react-native-chart-kit'

var styles = require('./style.js');
const chartData = [
  { name: 'Cao', population: 15, color: Colors.yellow, legendFontColor: '#7F7F7F', legendFontSize: 15 },
  { name: 'Trung Bình', population: 25, color: Colors.dark_pink, legendFontColor: '#7F7F7F', legendFontSize: 15 },
  { name: 'Thấp', population: 4, color: Colors.gray, legendFontColor: '#7F7F7F', legendFontSize: 15 },
]

var workScheduleTemp = [{
  time: "2:00",
  end: "2:00",
  title: "Gặp khách hàng A",
  where: 'Cafe heeel',
  note: 'Khach muon giam gia'
},
{
  time: "2:00",
  end: "2:00",
  title: "Gặp khách hàng B",
  where: 'dai ly'
},
{
  time: "2:00",
  end: "2:00",
  title: "Ăn tối với khách hàng J"
},
{
  time: "5:30",
  end: "2:00",
  title: "Gặp khách hàng C"
},
{
  time: "6:00",
  end: "2:00",
  title: "Gặp khách hàng D"
},
{
  time: "23:30",
  end: "2:00",
  title: "Gặp khách hàng D"
}];

var listChartColors = [Colors.dark_blue, Colors.yellow, Colors.orange, Colors.blue, Colors.purple, Colors.gray, Colors.dark, Colors.ink, Colors.smoke, Colors.dark_pink];

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      daylyTaskCollapsed: false,
      workScheduleCollapsed: false,
      kpiCollapsed: false,
      curTime: new Date(),
      selectDateId: 0,
      cloclIntervalId: null,
      changeDateOf: 'fromDate',
      SECTIONS: [
        {
          title: 'First',
          content: {
            high: 5,
            medium: 5,
            low: 2
          },
          dateRange: {
            fromDate: '1/8/2017',
            toDate: '30/8/2017'
          },
          contractList: [{
            month: '10/2016',
            number: 4
          },
          {
            month: '11/2016',
            number: 5
          },
          {
            month: '12/2016',
            number: 3
          },
          {
            month: '1/2017',
            number: 10
          }]
        },
        {
          title: 'Second',
          content: {
            high: 5,
            medium: 2,
            low: 6
          },
          dateRange: {
            fromDate: '1/8/2017',
            toDate: '30/8/2017'
          },
          contractList: [{
            month: '10/2016',
            number: 1
          },
          {
            month: '11/2016',
            number: 5
          },
          {
            month: '12/2016',
            number: 2
          },
          {
            month: '1/2017',
            number: 6
          }]
        }]
    };
  }
  render() {
    const { changePage } = this.props
    return (
      <ScrollView style={styles.container}>
        {this.renderExpendView(Strings.dayly_task, this.renderDaylyTask(), this.state.daylyTaskCollapsed, 'daylyTaskCollapsed')}
        {this.renderExpendView(Strings.work_schedule, this.renderWorkSchedule(), this.state.workScheduleCollapsed, 'workScheduleCollapsed')}
        {this.renderExpendView(Strings.kpi, this.renderKpi(), this.state.kpiCollapsed, 'kpiCollapsed')}
      </ScrollView>
    );
  }
  renderExpendView(title, expendView, isCollapsed, sateName) {
    return (
      <View style={styles.expand_box}>
        <TouchableOpacity onPress={() => { this.setState({ [sateName]: !isCollapsed }) }}>
          <View style={[styles.expend_title_box, { borderBottomWidth: isCollapsed ? 0 : 0.5 }]}>
            <Text style={styles.expend_title}>{title}</Text>
            <Image source={isCollapsed ? require('../../../../assets/images/arrow_right.png') : require('../../../../assets/images/arrow_down.png')} resizeMode={'contain'} style={styles.arrow_green} />
          </View>
        </TouchableOpacity>
        <Expand collapsed={isCollapsed}>
          {expendView}
        </Expand>
      </View>
    );
  }


  //////////RENDER DaylyTask ////////////////////
  renderDaylyTask() {
    return (
      <View style={styles.expend_view}>
        {this.renderDaylyProcess('#009B97', '#9BD8DB', 3, 1, Strings.meet_customer)}
        {this.renderDaylyProcess('#F26522', '#F9A369', 4, 2, Strings.contact_customer)}
        {this.renderDaylyProcess('#FFCD33', '#FCE45B', 4, 3, Strings.planning)}
      </View>
    );
  }

  renderDaylyProcess(doneColor, color, totalTask, taskDone, title) {
    return (
      <TouchableOpacity onPress={() => { this.goToTaskPage() }}>
        <View>
          <View style={styles.process_row}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View style={styles.point}></View>
              <Text style={styles.task_title}>{title}</Text>
            </View>
            <Text style={styles.process_right_text}>{taskDone + '/' + totalTask}</Text>
          </View>
          <View style={[styles.process_bar, { backgroundColor: color }]}>
            <View style={{ width: taskDone / totalTask * 100 + '%', height: '100%', backgroundColor: doneColor }}></View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  goToTaskPage() {
    var page = parentRoutes.task;
    var props = {
      headerText: 'TEXXXXXX'
    }
    this.props.screenProps.pushPage('task', props);
  }
  //////////END RENDER DaylyTask ////////////////////

  //////////RENDER WorkSchedule ////////////////////
  getTime() {
    return ('0' + this.state.curTime.getHours()).slice(-2) + ':' + ('0' + this.state.curTime.getMinutes()).slice(-2);
  }

  getWeekDay() {
    var day = this.state.curTime.getDay();
    switch (day) {
      case 7:
        day = 'Chủ nhật,';
        break
      default:
        day = "Thứ " + (day + 1) + ",";
        break
    }
    return day;
  }
  getDate() {
    return ('0' + this.state.curTime.getDate()).slice(-2) + "/" + ('0' + (this.state.curTime.getMonth() + 1)).slice(-2) + "/" + this.state.curTime.getFullYear();
  }

  renderWorkSchedule() {
    return (
      <View style={styles.expend_view}>
        <View style={styles.time_box}>
          <Text style={styles.hour_text}>{this.getTime()}</Text>
          <View style={styles.date_box}>
            <Text style={styles.weekdays}>{this.getWeekDay()}</Text>
            <Text style={styles.date}>{this.getDate()}</Text>
          </View>
          <ScrollView style={[styles.time_rows_box, workScheduleTemp.length > 3 && { height: 210 }]} showsHorizontalScrollIndicator={false}>
            {this.renderTimeTasks()}
          </ScrollView>
        </View>
      </View>
    );
  }

  renderTimeTasks() {
    var views = [];
    for (var i = 0; i < workScheduleTemp.length; i++) {
      views.push(
        <View key={'timetask' + i} style={styles.time_task_row}>
          <View style={styles.time_task_hour_box}>
            <Text style={styles.time_task_hour_text}>{workScheduleTemp[i].time}</Text>
            <Text style={[styles.time_task_hour_text, { marginTop: 8 }]}>{workScheduleTemp[i].end}</Text>
          </View>
          <View style={{ height: 55, width: 5, backgroundColor: 'green', marginLeft: 8, marginRight: 8 }}></View>
          <View style={styles.time_task_detail_box}>
            <Text style={styles.time_task_hour_text}>{workScheduleTemp[i].title}</Text>
            <Text style={styles.time_task_loca_text}>{workScheduleTemp[i].where}</Text>
            <Text style={styles.time_task_note_text}>{workScheduleTemp[i].note}</Text>
          </View>
        </View >
      );
    }
    return views;
  }
  //////////END RENDER WorkSchedule ////////////////////

  //////////RENDER KPI ////////////////////
  renderKpi() {
    return (
      <View style={styles.expend_view}>
        <View style={styles.kpi_view_option_box}>
          <Text style={styles.kpi_view_option_text}>Xem theo</Text>
          <View style={{ flex: 1, marginLeft: 8 }}>
            <ModalDropdown options={['Tất cả', 'Tổng cộng']}
              onSelect={this.onSelect}
              defaultValue={'Tất cả'}
              isNormalArray={true}
              renderModal={this.props.screenProps.renderModal.bind(this)}
              setVisibleModal={this.props.screenProps.setVisibleModal.bind(this)}
              renderSeparator={this.renderSeparator} />
          </View>
        </View>
        <View style={styles.kpi_view_option_box}>
          <PieChart
            data={chartData}
            width={Const.WIDTH_SCREEN - 15}
            height={160}
            chartConfig={{
              backgroundColor: '#e26a00',
              backgroundGradientFrom: '#fb8c00',
              backgroundGradientTo: '#ffa726',
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            }}
            accessor="population"
            backgroundColor="transparent"
            absolute
          />
        </View>
        {this.renderListUser()}
      </View>
    );
  }

  renderContractChart(contractList) {
    var maxNumber = 0;
    var views = [];
    for (var i = 0; i < contractList.length; i++) {
      if (contractList[i].number > maxNumber) {
        maxNumber = contractList[i].number;
      }
    }
    for (var i = 0; i < contractList.length; i++) {
      views.push(
        <View key={'chart-contact' + i} style={styles.chart_row}>
          <Text style={styles.chart_text}>{contractList[i].month}</Text>
          <View style={styles.chart_col_box}>
            <View style={[styles.chart_col_box, { backgroundColor: listChartColors[i % 10], flex: contractList[i].number }]}>
              <Text style={styles.chart_col_text}>{contractList[i].number}</Text>
            </View>
            <View style={{ flex: maxNumber - contractList[i].number }}></View>
          </View>
        </View>
      );
    }
    return views;
  }
  renderListUser() {
    var views = [];
    for (var i = 0; i < this.state.SECTIONS.length; i++) {
      views.push(this.renderUserContractHeader(this.state.SECTIONS[i], i))
    }
    return views;
  }
  renderUserContractHeader(section, index) {
    return (
      <TouchableOpacity key={'user_contract_header' + index} style={styles.user_contract_header} onPress={() => { this.openStaffKpiPage(section) }}>
        <Image source={{ uri: 'https://pbs.twimg.com/profile_images/831993825635745796/HnVmB0-k.jpg' }} resizeMode={'cover'} style={styles.avatar} />
        <Text style={styles.name_text}>{section.title}</Text>
        <Image source={require('../../../../assets/images/double_arrows_right.png')} resizeMode={'contain'} style={styles.arrow_dark} />
      </TouchableOpacity>
    );
  }

  openStaffKpiPage(item) {
    // var page = parentRoutes.staff_kpi;
    var props = {
      headerText: item.title
    }
    this.props.screenProps.pushPage('staff_kpi', props);
  }

  //////////END RENDER KPI ////////////////////
  componentDidMount() {
    this.intervalId = setInterval(() => {
      this.setState({
        curTime: new Date()
      })
    }, 15000);

  }
  componentWillUnmount() {
    clearInterval(this.intervalId);
  }
}
export default connect(null, null)(Home);