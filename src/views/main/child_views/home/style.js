import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../../global/colors'

module.exports = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: Colors.smoke
  },
  expend_title: {
    fontSize: 16,
    color: Colors.dark_blue,
    fontFamily: "Roboto-Bold"
  },
  expend_title_box: {
    paddingTop: 25,
    height: 58,
    borderBottomColor: Colors.underline
  },
  time_row_underline: {
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
    width: '100%',
    height: 0.5
  },
  arrow_green: {
    position: 'absolute',
    width: 15,
    height: 15,
    right: 0,
    bottom: 16,
    tintColor: Colors.dark_blue
  },
  arrow_dark: {
    position: 'absolute',
    width: 15,
    height: 15,
    right: 0,
    tintColor: Colors.dark
  },
  calendar: {
    position: 'absolute',
    width: 20,
    height: 20,
    right: 0,
    bottom: 8,
    tintColor: Colors.dark
  },
  expand_box: {
    borderColor: Colors.underline,
    borderWidth: 0.5,
    overflow: 'hidden',
    backgroundColor: 'white',
    paddingLeft: 16,
    paddingRight: 16,
    marginBottom: 20
  },
  expend_view: {
    paddingBottom: 25
  },
  process_row: {
    height: 52,
    paddingTop: 25,
    width: '100%'
  },
  point: {
    height: 8,
    width: 8,
    borderRadius: 4,
    backgroundColor: Colors.dark_blue
  },
  task_title: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    marginLeft: 10,
  },
  process_right_text: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    position: 'absolute',
    right: 0,
    bottom: 8
  },
  process_bar: {
    height: 4,
    width: '100%'
  },
  time_box: {
    width: '100%',
    alignItems: 'center',
    marginTop: 20
  },
  hour_text: {
    fontSize: 50,
    color: Colors.dark_blue,
    fontFamily: "Roboto-Regular",
  },
  date_box: {
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
    paddingBottom: 25
  },
  weekdays: {
    fontSize: 16,
    color: Colors.dark,
    fontFamily: "Roboto-Bold"
  },
  date: {
    fontSize: 16,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    marginLeft: 3
  },
  time_rows_box: {
    width: '100%',
    height: 405
  },
  flex_row_center: {
    width: '100%',
    height: 45,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  chart_row: {
    width: '100%',
    height: 20,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 11
  },
  kpi_view_option_text: {
    fontSize: 14,
    color: Colors.dark,
    fontFamily: "Roboto-Italic"
  },
  kpi_view_option_box: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    paddingBottom: 20,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  name_text: {
    fontSize: 14,
    color: Colors.dark,
    fontFamily: "Roboto-Bold",
    marginLeft: 20
  },
  chart_text: {
    fontSize: 12,
    color: Colors.dark,
    width: 60,
    fontFamily: "Roboto-Regular"
  },
  chart_col_box: {
    height: 20,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  chart_col_text: {
    fontSize: 12,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    paddingLeft: 8
  },
  user_contract_header: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5,
  },
  user_contact_contain: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15
  },
  chart: {
    width: 160,
    height: 160
  },
  chart_text_box: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: 160,
    height: 160,
    alignItems: 'center',
    justifyContent: 'center'
  },
  chart_text_big: {
    fontSize: 16,
    color: Colors.dark,
    fontFamily: "Roboto-Bold",
  },
  chart_text_small: {
    fontSize: 11,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    backgroundColor: 'transparent'
  },
  chart_box: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  chart_detail: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 15
  },
  chart_detail_text: {
    fontSize: 14,
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    marginLeft: 8
  },
  chart_detail_number_text: {
    fontSize: 14,
    color: Colors.dark,
    fontFamily: "Roboto-Bold",
    marginLeft: 8
  },
  time_task_row: {
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  time_task_hour_box: {
    height: '100%',
    width: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  time_task_hour_text: {
    fontSize: 14,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
  },
  time_task_detail_box: {
    height: 80,
    flex: 1,
    justifyContent: 'center'
  },
  time_task_loca_text: {
    fontSize: 12,
    color: Colors.gray,
    fontFamily: "Roboto-Regular",
    marginTop: 5
  },
  time_task_note_text: {
    fontSize: 12,
    color: Colors.gray,
    fontFamily: "Roboto-Italic",
    marginTop: 5
  },
  button_date_range: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    backgroundColor: '#F1F2F2',
    paddingLeft: 8,
    flex: 1,
    marginLeft: 20
  },
  button_date_text: {
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    fontSize: 14
  }
});
