import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';
import {
  doGetSalesSupport,
  doUpdateSalesSupport,
  doGetCompany,
  doUpdateCompany,
  doGetBroker,
  doUpdateBroker,
  doUpdateCarMaintenance,
  doGetPersonContact,
  doUpdatePersonContact
} from '../../../actions/mainActions/carInfoAction';
import Colors from '../../../global/colors';
import Routes from '../../../views/routes';
import Tabbar from '../../../widgets/tabbar';
import ModalDatePicker from '../../../widgets/modal_date_picker';
import SickyScrollView from '../../../widgets/sticky_scrollview'
import { connect } from 'react-redux';
import Const from '../../../global/const';
import { doGetEnumTypeData } from '../../../actions/apiActions/enumTypeApiAction';
import ModalDropdown from '../../../widgets/modal_dropdown';
import InputRow from '../../../widgets/input_row';

var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');
const serviceTabbarData = [{
  key: 'mantain_info',
  text: 'THÔNG TIN BẢO DƯỠNG'
},
{
  key: 'cont_person',
  text: 'NGƯỜI LIÊN HỆ'
}];
const tabbarData = [{
  key: 'mantain_info',
  text: 'THÔNG TIN BẢO DƯỠNG'
},
{
  key: 'cont_person',
  text: 'NGƯỜI LIÊN HỆ'
},
{
  key: 'support_sa_info',
  text: 'THÔNG TIN HỖ TRỢ BÁN HÀNG'
},
{
  key: 'buy_for_cp_info',
  text: 'THÔNG TIN KHI MUA XE CHO CÔNG TY'
},
{
  key: 'broker_info',
  text: 'THÔNG TIN  MÔI GIỚI'
}];
const folowTabbarData = [{
  key: 'support_sa_info',
  text: 'THÔNG TIN HỖ TRỢ BÁN HÀNG'
},
{
  key: 'buy_for_cp_info',
  text: 'THÔNG TIN KHI MUA XE CHO CÔNG TY'
},
{
  key: 'broker_info',
  text: 'THÔNG TIN  MÔI GIỚI'
}];

class CarInfo extends ChildView {
  constructor(props) {
    super(props);
    this.allRefs = [];
    this.state = {
      tabbarSelected: this.getTabbarData()[0].key,
      contactPerSelectedId: null,
      salesSupportData: {},
      updateCompanyData: {},
      updateBrokerData: {},
      carDetail: this.props.navigation.getParam('carDetail', {}),
      type: this.props.navigation.getParam('type', ''),
      carMaintenance: {
        // id: this.props.carDetail.id,
        // km: this.props.carDetail.km,
        // nextKmMaintenance: this.props.carDetail.nextKmMaintenance,
        // nextDateMaintenance: this.props.carDetail.nextDateMaintenance
        id: '123',
        km: 15000,
        nextKmMaintenance: 17000,
        nextDateMaintenance: '12/12/2019'
      }
    }
  }

  renderContainer() {
    return (
      <SickyScrollView
        maxHeight={210}
        minHeight={40}
        isLoading={this.showLoading.bind(this)}
        onRefresh={this.onRefresh.bind(this)}
        renderHeader={this.renderScrollViewHeader.bind(this)}
        renderContent={this.renderTabView.bind(this)}
      />
    );
  }

  onRefresh() {
    switch (this.state.tabbarSelected) {
      case 'mantain_info':
        return false;
      case 'cont_person':
        this.setState({
          personContactData: [{
            isContact: true,
            iconProfile: 'https://avatarfiles.alphacoders.com/968/thumb-96848.png',
            fullname: 'Nguyen Van A',
            phone: '0984783789'
          }, {
            isContact: false,
            iconProfile: 'https://avatarfiles.alphacoders.com/979/thumb-97920.png',
            fullname: 'Nguyen Van B',
            phone: '0984783789'
          }],
          loadPersonContactSuccess: true,
          companyData: {},
          loadCompanyDataSuccess: false,
          brokerData: {},
          loadBrokerDataSuccess: false,
          salesSupportData: {}
        })

        // this.props.doGetPersonContact(this.props.carDetail.id, this);
        break;
      case 'support_sa_info':
        this.setState({
          personContactData: [],
          loadPersonContactSuccess: false,
          companyData: {},
          loadCompanyDataSuccess: false,
          brokerData: {},
          loadBrokerDataSuccess: false,
          salesSupportData: {
            id: '123',
            intendedTimeGetCar: '2019/12/12',
            carModelOwned: 'Dòng 1',
            demands: 'Nhu cầu 2',
            approachStarttime: '2019/09/12',
            approachSourceType: { displayName: 'Nguồn 3' },
            carStatusBuy: 'Loại 1',
            buyMethod: 'Hình thức 3',
            purposeOfUse: 'Mục đích 3',
            paymentMethod: 'Tiền mặt',
            createdAt: '2019/09/12'
          }
        })
        // this.props.doGetSalesSupport(this.props.carDetail.subProfileCarInfoId, this);
        break;
      case 'buy_for_cp_info':
        this.setState({
          personContactData: [],
          loadPersonContactSuccess: false,
          companyData: {
            companyName: 'Tesu SJC',
            companyAddress: '22 Nguyễn Bỉnh Khiêm, Quận 1',
            representative: 'Nguyễn Văn A',
            contactPerson: 'Nguyễn Văn B',
            title: 'Giám Đốc',
            taxid: 'ACV/221321321',
            companyPhone: '19001010',
            companyFax: '0909221212'
          },
          loadCompanyDataSuccess: true,
          brokerData: {},
          loadBrokerDataSuccess: false,
          salesSupportData: {}
        })
        // this.props.doGetCompany(this.props.carDetail.subProfileCarInfoId, this);
        break;
      case 'broker_info':
        this.setState({
          personContactData: [],
          loadPersonContactSuccess: false,
          companyData: {},
          loadCompanyDataSuccess: false,
          brokerData: {
            fullname: 'Nguyễn Văn B',
            phone: '0909238231',
            taxid: 'AX2132121',
            passportId: '128390029',
            bonus: '10%',
            address: '33 Hoàng Hoa Thám, Cầu Giấy, Hà Nội'
          },
          loadBrokerDataSuccess: true,
          salesSupportData: {}
        })
        // this.props.doGetBroker(this.props.carDetail.subProfileCarInfoId, this);
        break;
    }
  }

  showLoading() {
    switch (this.state.tabbarSelected) {
      case 'mantain_info':
        return false;
      case 'cont_person':
        return this.props.carInfoPage.isPersonContactLoading;
      case 'support_sa_info':
        return this.props.carInfoPage.isSalesSupportLoading;
      case 'buy_for_cp_info':
        return this.props.carInfoPage.isGetCompanyLoading;
      case 'broker_info':
        return false;
    }
  }

  getTabbarData() {
    let type = this.props.navigation.getParam('type', '');
    switch (type) {
      case 'car_info':
        return tabbarData;
      case 'service_car_info':
        return serviceTabbarData;
      case 'folow_car_info':
        return folowTabbarData;
      case 'lost_car_info':
        return folowTabbarData;
      default:
        return folowTabbarData;
    }
  }

  renderScrollViewHeader() {
    let { carDetail } = this.state;
    return (
      <View style={{ width: '100%', height: '100%' }}>
        <View style={styles.header_box}>
          {(this.state.type == 'folow_car_info' || this.state.type == 'lost_car_info') && <Text style={styles.staff_info}>{"TVBH: " + carDetail.staffName}</Text>}
          <TouchableOpacity style={styles.car_tittle_box} onPress={() => { this.onpenEditCarPage() }}>
            <Text style={styles.car_name}>{carDetail.carGradeName}</Text>
            <Text style={styles.car_color}>{carDetail.carColorName}</Text>
            <Text style={styles.brand_text}>{carDetail.carBrandName}</Text>

          </TouchableOpacity>
          {(this.state.type == 'car_info' || this.state.type == 'service_car_info') && <View style={styles.car_header_detail_box}>
            <View style={styles.detail_item_box}>
              <Text style={styles.detail_title_text}>Biển số</Text>
              <Text style={styles.detail_text}>{carDetail.plateNumber}</Text>
            </View>
            <View style={styles.detail_item_box}>
              <Text style={styles.detail_title_text}>Số khung</Text>
              <Text style={styles.detail_text}>{carDetail.vin}</Text>
            </View>
            <View style={[styles.detail_item_box, { borderRightWidth: 0 }]}>
              <Text style={styles.detail_title_text}>Số máy</Text>
              <Text style={styles.detail_text}>{carDetail.engineNumber}</Text>
            </View>
          </View>}
          {this.state.type == 'folow_car_info' && this.renderProcessPoints(carDetail.transactionStatusId, carDetail.transactionStatuses)}
          {this.state.type == 'lost_car_info' && <Text style={[styles.car_color, { marginTop: 30 }]}>Khách hàng đã hết tiền mua xe</Text>}
        </View>
        <View style={styles.tabbar}>
          <Tabbar
            data={this.getTabbarData()}
            onChange={this.onTabbarChange.bind(this)} />
        </View>
      </View >);
  }
  sendTabGa() {
    if (this.props.gaTracker) {
      switch (this.state.type) {
        case 'folow_car_info':
          switch (this.state.tabbarSelected) {
            case 'support_sa_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_car_follow_sale_support);
              break;
            case 'buy_for_cp_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_car_follow_company);
              break;
            case 'broker_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_car_follow_agency);
              break;
          }
          break;
        case 'lost_car_info':
          switch (this.state.tabbarSelected) {
            case 'support_sa_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_sale_support_car_losing);
              break;
            case 'buy_for_cp_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_company_car_losing);
              break;
            case 'broker_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_agency_car_losing);
              break;
          }
          break;
        default:
          switch (this.state.tabbarSelected) {
            case 'mantain_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_car_having_maitenance);
              break;
            case 'cont_person':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_car_having_contact_person);
              break;
            case 'support_sa_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_car_having_sale_support);
              break;
            case 'buy_for_cp_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_car_having_follow_company);
              break;
            case 'broker_info':
              this.props.gaTracker.trackScreenView(Const.GA_PAGE_NAMES.customer_car_having_follow_agency);
              break;
          }
          break;
      }
    }
  }
  onpenEditCarPage() {
    var page = Routes.edit_car
    var props = { ...page.props, carDetail: this.state.carDetail, setBack: this.setBack.bind(this) }
    this.pushPage('edit_car', props);
  }
  onTabbarChange(item) {
    this.setState({
      tabbarSelected: item.key
    }, () => {
      this.onRefresh();
      // this.sendTabGa();
    });
  }
  renderProcessPoints(id, array) {
    var acticeIndex = array.findIndex((element) => {
      return id == element.order;
    })
    if (acticeIndex == -1) acticeIndex = 0;
    var views = [];
    for (var i = 0; i < array.length; i++) {
      views.push(
        <View key={"process-point" + i} style={styles.process_point}>
          <View style={[styles.yellow_point, { backgroundColor: acticeIndex >= i ? Colors.yellow : Colors.gray }]}></View>
          <Text style={[styles.point_text, { color: acticeIndex >= i ? Colors.yellow : 'white' }]}>{array[i].displayName}</Text>
        </View>
      );
    }

    return (<View style={styles.car_header_detail_box}>
      <View style={styles.process_line_box}>
        <View style={{ flex: 1 }}></View>
        <View style={{ flex: array.length * 2 - 2, height: 1, backgroundColor: Colors.dark, flexDirection: 'row', marginTop: 5 }}>
          <View style={{ flex: acticeIndex, backgroundColor: Colors.yellow }}></View>
          <View style={{ flex: array.length - acticeIndex - 1 }}></View>
        </View>
        <View style={{ flex: 1 }}></View>
      </View>
      <View style={styles.process_line_box}>
        {views}
      </View>
    </View>);
  }
  // renderRightButton(ref) {
  // }
  onCustomFocus(ref) {
    this.onFocusWithContext(this.allRefs[ref], this);
  }

  setUpSalesSupportData(data) {
    this.setState({
      salesSupportData: {
        id: data.id,
        intendedTimeGetCar: data.intendedTimeGetCar,
        carModelOwned: data.carModelOwned,
        demands: data.demands,
        approachStarttime: data.approachStarttime,
        approachSourceTypeId: data.approachSourceType && data.approachSourceType.id,
        carStatusBuy: data.carStatusBuy,
        buyMethod: data.buyMethod,
        purposeOfUse: data.purposeOfUse,
        paymentMethod: data.paymentMethod
      }
    });
  };
  setUpdateCompanyData(data) {
    this.setState({
      updateCompanyData: {
        ...data,
        customerSubProfileCarInfoId: this.props.carDetail.subProfileCarInfoId
      }
    });
  };
  setUpdateBrokerData(data) {
    this.setState({
      updateBrokerData: {
        ...data,
        customerSubProfileCarInfoId: this.props.carDetail.subProfileCarInfoId
      }
    });
  };

  renderTabView() {
    switch (this.state.tabbarSelected) {
      case 'mantain_info':
        let { carMaintenance } = this.state;
        return (
          <View style={styles.tabview_box}>
            <InputRow
              onFocus={this.onFocusWithContext.bind(this)}
              onEndEditing={this.onEndEditing.bind(this)}
              title={'Số KM đã đi'}
              keyboardType={"phone-pad"}
              isNumber={true}
              defaultValue={carMaintenance.km}
              onChangeText={(text) => { this.setState({ carMaintenance: { ...this.state.carMaintenance, km: text } }) }} />
            <InputRow
              title={'KM bảo dưỡng tiếp theo'}
              mStyle={0}
              defaultValue={carMaintenance.nextKmMaintenance} />
            <InputRow
              title={'Ngày bảo dưỡng tiếp theo'}
              mStyle={0}
              defaultValue={carMaintenance.nextDateMaintenance} />
            <TouchableOpacity style={styles.view_contact_his_box} onPress={() => { this.openContactHisPage() }}>
              <Text style={styles.view_contact_his_text}>Lịch sử liên hệ</Text>
              <Image source={require('../../../assets//images/double_arrows_right.png')} resizeMode={'contain'} style={styles.double_arrow} />
            </TouchableOpacity>
            {this.state.type == 'service_car_info' && <TouchableOpacity style={styles.view_contact_his_box} onPress={() => { this.openServiceHisPage() }}>
              <Text style={[styles.view_contact_his_text, { color: Colors.blue }]}>Lịch sử làm dịch vụ</Text>
              <Image source={require('../../../assets//images/double_arrows_right.png')} resizeMode={'contain'} style={[styles.double_arrow, { tintColor: Colors.blue }]} />
            </TouchableOpacity>}
            <TouchableOpacity
              onPress={() => { this.props.doUpdateCarMaintenance(this.state.carMaintenance, this) }}
              style={styles.submit}
              underlayColor='transparent'>
              <Text style={styles.btn_text}>Lưu</Text>
            </TouchableOpacity>
          </View>
        );
      case 'cont_person':
        // let { personContactData, getPersonContactError, loadPersonContactSuccess } = this.props.carInfoPage;
        let { personContactData, getPersonContactError, loadPersonContactSuccess } = this.state;
        if (loadPersonContactSuccess) {
          var views = [];
          for (var i = 0; i < personContactData.length; i++) {
            views.push(this.renderContactItems(personContactData[i], i));
          }
          return views;
        }
        return (
          <View style={styles.tabview_box}>
            {getPersonContactError &&
              <Text style={styles.error_loading_text}>Tải dữ liệu không thành công, vui lòng kéo xuống để tải lại.</Text>}
          </View>
        );
      case 'support_sa_info':
        // let { demands, approachSourceTypes, carTypes, buyMethods, purposeUse } = this.props.enumTypeApi.enumData;
        // let { salesSupportData, salesSupportError } = this.props.carInfoPage;
        let { salesSupportData, salesSupportError } = this.state;

        if (salesSupportData.id != null && salesSupportData.id != undefined) {
          return (<View style={styles.tabview_box}>
            <ModalDatePicker
              title={"Thời gian dự định lấy xe"}
              setVisibleModal={this.setVisibleModal.bind(this)}
              renderModal={this.renderModal.bind(this)}
              defaultDate={salesSupportData.intendedTimeGetCar ? new Date(salesSupportData.intendedTimeGetCar) : undefined}
              onPickerChange={(date) => this.setState({
                salesSupportData: { ...this.state.salesSupportData, intendedTimeGetCar: date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2) }
              })} />
            <InputRow
              onFocus={this.onFocusWithContext.bind(this)}
              onEndEditing={this.onEndEditing.bind(this)}
              title={'Dòng xe đang sở hữu'}
              defaultValue={salesSupportData.carModelOwned}
              onChangeText={(text) => { this.setState({ salesSupportData: { ...this.state.salesSupportData, carModelOwned: text } }) }} />
            <ModalDropdown
              // options={demands}
              options={['Nhu cầu 1', 'Nhu cầu 2', 'Nhu cầu 3', 'Nhu cầu 4', 'Nhu cầu 5', 'Nhu cầu 6', 'Nhu cầu 7', 'Nhu cầu 8', 'Nhu cầu 31', 'Nhu cầu 32', 'Nhu cầu 33']}
              mStyle={1}
              isNormalArray={true}
              // keyItems={'displayName'}
              renderModal={this.renderModal.bind(this)}
              reloadData={this.loadDefaultData.bind(this)}
              setVisibleModal={this.setVisibleModal.bind(this)}
              onSelect={(item) => this.setState({ salesSupportData: { ...this.state.salesSupportData, demands: item.name } })}
              defaultValue={salesSupportData.demands}
              title={'Nhu cầu khách hàng'} />
            <ModalDatePicker
              title={"Ngày tiếp cận ban đầu"}
              defaultDate={salesSupportData.approachStarttime ? new Date(salesSupportData.approachStarttime) : undefined}
              onPickerChange={(date) => this.setState({
                salesSupportData: { ...this.state.salesSupportData, approachStarttime: date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2) }
              })}
              setVisibleModal={this.setVisibleModal.bind(this)}
              renderModal={this.renderModal.bind(this)} />
            <ModalDropdown
              // options={approachSourceTypes}
              options={['Nguồn 1', 'Nguồn 2', 'Nguồn 3', 'Nguồn 4', 'Nguồn 5', 'Nguồn 6', 'Nguồn 7', 'Nguồn 8', 'Nguồn 31', 'Nguồn 32', 'Nguồn 33']}
              isNormalArray={true}
              mStyle={1}
              // keyItems={'displayName'}
              renderModal={this.renderModal.bind(this)}
              reloadData={this.loadDefaultData.bind(this)}
              setVisibleModal={this.setVisibleModal.bind(this)}
              onSelect={(item) => this.setState({ salesSupportData: { ...this.state.salesSupportData, approachSourceTypeId: item.id } })}
              defaultValue={salesSupportData.approachSourceType && salesSupportData.approachSourceType.displayName}
              title={'Nguồn tiếp cận ban đầu'} />
            <ModalDropdown
              // options={carTypes}
              options={['Loại 1', 'Loại 2', 'Loại 3', 'Loại 4', 'Loại 5', 'Loại 6', 'Loại 7', 'Loại 8', 'Loại 31', 'Loại 32', 'Loại 33']}
              mStyle={1}
              isNormalArray={true}
              // keyItems={'displayName'}
              renderModal={this.renderModal.bind(this)}
              reloadData={this.loadDefaultData.bind(this)}
              setVisibleModal={this.setVisibleModal.bind(this)}
              onSelect={(item) => this.setState({ salesSupportData: { ...this.state.salesSupportData, carStatusBuy: item.name } })}
              defaultValue={salesSupportData.carStatusBuy}
              title={'Loại xe mua'} />
            <ModalDropdown
              // options={buyMethods}
              options={['Hình thức 1', 'Hình thức 2', 'Hình thức 3', 'Hình thức 4', 'Hình thức 5', 'Hình thức 6', 'Hình thức 7', 'Hình thức 8', 'Hình thức 31', 'Hình thức 32', 'Hình thức 33']}
              mStyle={1}
              isNormalArray={true}
              // keyItems={'displayName'}
              renderModal={this.renderModal.bind(this)}
              reloadData={this.loadDefaultData.bind(this)}
              setVisibleModal={this.setVisibleModal.bind(this)}
              onSelect={(item) => this.setState({ salesSupportData: { ...this.state.salesSupportData, buyMethod: item.name } })}
              defaultValue={salesSupportData.buyMethod}
              title={'Hình thức mua'} />
            <ModalDropdown
              // options={purposeUse}
              mStyle={1}
              options={['Mục đích 1', 'Mục đích 2', 'Mục đích 3', 'Mục đích 4', 'Mục đích 5', 'Mục đích 6', 'Mục đích 7', 'Mục đích 8', 'Mục đích 31', 'Mục đích 32', 'Mục đích 33']}
              isNormalArray={true}
              // keyItems={'displayName'}
              renderModal={this.renderModal.bind(this)}
              reloadData={this.loadDefaultData.bind(this)}
              setVisibleModal={this.setVisibleModal.bind(this)}
              onSelect={(item) => this.setState({ salesSupportData: { ...this.state.salesSupportData, purposeOfUse: item.name } })}
              defaultValue={salesSupportData.purposeOfUse}
              title={'Mục đích sử dụng'} />
            <InputRow
              onFocus={this.onFocusWithContext.bind(this)}
              onEndEditing={this.onEndEditing.bind(this)}
              title={'Thanh toán'}
              defaultValue={salesSupportData.paymentMethod}
              onChangeText={(text) => { this.setState({ salesSupportData: { ...this.state.salesSupportData, paymentMethod: text } }) }} />
            <InputRow
              title={'Ngày tạo'}
              mStyle={0}
              defaultValue={salesSupportData.createdAt} />
            {(this.state.type == 'folow_car_info' || this.state.type == 'lost_car_info') && <TouchableOpacity style={styles.view_contact_his_box} onPress={() => { this.openContactHisPage() }}>
              <Text style={styles.view_contact_his_text}>Lịch sử liên hệ</Text>
              <Image source={require('../../../assets//images/double_arrows_right.png')} resizeMode={'contain'} style={styles.double_arrow} />
            </TouchableOpacity>}
            <TouchableOpacity
              onPress={() => {
                //this.props.doUpdateSalesSupport(salesSupportData, this)
              }}
              style={styles.submit}
              underlayColor='transparent'>
              <Text style={styles.btn_text}>Lưu</Text>
            </TouchableOpacity>
          </View>)
        }
        return (
          <View style={styles.tabview_box}>
            {salesSupportError &&
              <Text style={styles.error_loading_text}>Tải dữ liệu không thành công, vui lòng kéo xuống để tải lại.</Text>}
          </View>
        );
      case 'buy_for_cp_info':
        // let { companyData, getCompanyError, loadCompanyDataSuccess } = this.props.carInfoPage;
        let { companyData, loadCompanyDataSuccess, getCompanyError } = this.state
        if (loadCompanyDataSuccess) {
          return (
            <View style={styles.tabview_box}>
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Tên công ty'}
                defaultValue={companyData.companyName}
                onChangeText={(text) => { this.setState({ updateCompanyData: { ...this.state.updateCompanyData, companyName: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Địa chỉ'}
                defaultValue={companyData.companyAddress}
                onChangeText={(text) => { this.setState({ updateCompanyData: { ...this.state.updateCompanyData, companyAddress: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Tên người đại diện'}
                defaultValue={companyData.representative}
                onChangeText={(text) => { this.setState({ updateCompanyData: { ...this.state.updateCompanyData, representative: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Tên người liên hệ'}
                defaultValue={companyData.contactPerson}
                onChangeText={(text) => { this.setState({ updateCompanyData: { ...this.state.updateCompanyData, contactPerson: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Chức vụ'}
                defaultValue={companyData.title}
                onChangeText={(text) => { this.setState({ updateCompanyData: { ...this.state.updateCompanyData, title: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'MST công ty'}
                defaultValue={companyData.taxid}
                onChangeText={(text) => { this.setState({ updateCompanyData: { ...this.state.updateCompanyData, taxid: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Số điện thoại công ty'}
                isNumber={true}
                keyboardType={"phone-pad"}
                defaultValue={companyData.companyPhone}
                onChangeText={(text) => { this.setState({ updateCompanyData: { ...this.state.updateCompanyData, companyPhone: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Fax'}
                defaultValue={companyData.companyFax}
                onChangeText={(text) => { this.setState({ updateCompanyData: { ...this.state.updateCompanyData, companyFax: text } }) }} />
              <TouchableOpacity
                onPress={() => {
                  // this.props.doUpdateCompany(this.state.updateCompanyData, this)
                }}
                style={styles.submit}
                underlayColor='transparent'>
                <Text style={styles.btn_text}>Lưu</Text>
              </TouchableOpacity>
            </View>
          );
        }
        return (
          <View style={styles.tabview_box}>
            {getCompanyError &&
              <Text style={styles.error_loading_text}>Tải dữ liệu không thành công, vui lòng kéo xuống để tải lại.</Text>}
          </View>
        );
      case 'broker_info':
        // let { brokerData, getBrokerError, loadBrokerDataSuccess } = this.props.carInfoPage;
        let { brokerData, loadBrokerDataSuccess, getBrokerError } = this.state
        if (loadBrokerDataSuccess) {
          return (
            <View style={styles.tabview_box}>
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Tên người môi giới'}
                defaultValue={brokerData.fullname}
                onChangeText={(text) => { this.setState({ updateBrokerData: { ...this.state.updateBrokerData, fullname: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Số điện thoại'}
                isNumber={true}
                keyboardType={"phone-pad"}
                defaultValue={brokerData.phone}
                onChangeText={(text) => { this.setState({ updateBrokerData: { ...this.state.updateBrokerData, phone: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'MST cá nhân'}
                defaultValue={brokerData.taxid}
                onChangeText={(text) => { this.setState({ updateBrokerData: { ...this.state.updateBrokerData, taxid: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'CMND'}
                defaultValue={brokerData.passportId}
                onChangeText={(text) => { this.setState({ updateBrokerData: { ...this.state.updateBrokerData, passportId: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Hoa hồng'}
                isNumber={true}
                keyboardType={"phone-pad"}
                defaultValue={brokerData.bonus}
                onChangeText={(text) => { this.setState({ updateBrokerData: { ...this.state.updateBrokerData, bonus: text } }) }} />
              <InputRow
                onFocus={this.onFocusWithContext.bind(this)}
                onEndEditing={this.onEndEditing.bind(this)}
                title={'Địa chỉ'}
                defaultValue={brokerData.address}
                onChangeText={(text) => { this.setState({ updateBrokerData: { ...this.state.updateBrokerData, address: text } }) }} />
              <TouchableOpacity
                onPress={() => {
                  // this.props.doUpdateBroker(this.state.updateBrokerData, this)
                }}
                style={styles.submit}
                underlayColor='transparent'>
                <Text style={styles.btn_text}>Lưu</Text>
              </TouchableOpacity>
            </View>
          );
        }
        return (
          <View style={styles.tabview_box}>
            {getBrokerError &&
              <Text style={styles.error_loading_text}>Tải dữ liệu không thành công, vui lòng kéo xuống để tải lại.</Text>}
          </View>
        );
    }
  }
  //////////////////////////RENDER mantain_info TAB////////////////////
  openContactHisPage() {
    this.pushPage('contact_history', { headerText: 'Liên hệ: ' + this.state.carDetail.plateNumber });
  }
  openServiceHisPage() {
    this.pushPage('service_history', { headerText: 'Dịch vụ: ' + this.state.carDetail.plateNumber });
  }
  //////////////////////////END RENDER mantain_info TAB////////////////////

  //////////////////////////RENDER cont_person TAB////////////////////
  renderContactItems(item, index) {
    return (
      <TouchableOpacity key={'contact-person' + index} style={styles.row} onPress={() => { this.onSelectContPer(item, index) }}>
        <Image source={{ uri: item.iconProfile }} resizeMode={'cover'} style={styles.avatar} />
        <View style={{ marginLeft: 10, backgroundColor: 'transparent' }}>
          <Text style={styles.text_name}>{item.fullname}</Text>
          <Text style={styles.text_info}>{item.phone}</Text>
        </View>
        <Image source={item.isContact ? require('../../../assets//images/single_choice_ac.png') : require('../../../assets//images/single_choice_unac.png')} resizeMode={'contain'} style={styles.row_image_right} />
      </TouchableOpacity>
    );
  }

  onSelectContPer(item, index) {
    // if (!item.isContact) {
    //   var selectData = {
    //     carInfoId: this.props.carDetail.id,
    //     customerSubProfileCarInfoId: item.customerSubProfileCarInfoId
    //   };
    //   this.props.doUpdatePersonContact(selectData, this)
    // }
    let listTemp = [].concat(this.state.personContactData)
    listTemp.forEach((data) => data.isContact = false)
    listTemp[index].isContact = true
    this.setState({ personContactData: listTemp })
  }
  //////////////////////////END RENDER cont_person TAB////////////////////
  setBack(data) {
    this.setState({
      carDetail: { ...this.state.carDetail, ...data }
    });
  }
  loadDefaultData() {
    this.props.doGetEnumTypeData(this.setLoading.bind(this), this.props.navigator);
  }
  // onBack() {
  //   this.props.navigator.pop();
  //   if (this.props.setBack)
  //     this.props.setBack(this.props.setBackId, null);
  // }
  componentDidMount() {
    this.onRefresh();
    // this.loadDefaultData();
    // switch (this.state.type) {
    //   case 'car_info':
    //     break;
    //   case 'service_car_info':
    //     break;
    //   case 'folow_car_info':
    //     // this.props.doGetSalesSupport(this.props.carDetail.subProfileCarInfoId, this);
    //     break;
    //   case 'lost_car_info':
    //     // this.props.doGetSalesSupport(this.props.carDetail.subProfileCarInfoId, this);
    //     break;
    // }
  }
  componentWillUnmount() {
    if (this.props.setBack)
      this.props.setBack(this.props.setBackId, null);
  }
}

function mapStateToProps(state) {
  return {
    carInfoPage: state.carInfoPage,
    enumTypeApi: state.enumTypeApi
  };
}
export default connect(mapStateToProps,
  {
    doGetSalesSupport,
    doGetEnumTypeData,
    doUpdateSalesSupport,
    doGetCompany,
    doUpdateCompany,
    doGetBroker,
    doUpdateBroker,
    doUpdateCarMaintenance,
    doGetPersonContact,
    doUpdatePersonContact
  })(CarInfo);