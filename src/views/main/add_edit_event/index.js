import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView
} from 'react-native';
import ModalDropdown from '../../../widgets/modal_dropdown';
import ModalFilterDropdown from '../../../widgets/modal_filter_dropdown';
import ModalDatePicker from '../../../widgets/modal_date_picker';
import ModalTimePicker from '../../../widgets/modal_time_picker';
import { connect } from 'react-redux';
import HeaderTick from '../../../widgets/header_tick';
import InputRow from '../../../widgets/input_row';

var Global = require('../../../global/const');
var styles = require('./style.js');
var ChildView = require('../../../views/base/main_child_view');

class AddCustomer extends ChildView {
  constructor(props) {
    super(props);
    this.state = {
      hoursArray: ['00', '01', '02'],
      testView: null,
      testText: null
    }

  }

  renderContainer() {
    let now = new Date();
    let minutesArray = ['00', '15', '30', '45'];
    let hoursArray = ['00', '01', '02'];
    return (
      <ScrollView style={styles.container}>
        <HeaderTick title={"Thông tin sự kiện"} mStyle={1} />
        <InputRow
          onFocus={this.onFocusWithContext.bind(this)}
          onEndEditing={this.onEndEditing.bind(this)}
          title={'Tên sự kiện'}
          onChangeText={(text) => { }}
          requireChar={true} />
        <InputRow
          onFocus={this.onFocusWithContext.bind(this)}
          onEndEditing={this.onEndEditing.bind(this)}
          title={'Địa điểm'}
          onChangeText={(text) => { }}
          requireChar={true} />
        <ModalTimePicker
          ref='mtp_from_date'
          title={"Từ"}
          isRequire={true}
          onPickerChange={this.onFromDateChange.bind(this)}
          setVisibleModal={this.setVisibleModal.bind(this)}
          renderModal={this.renderModal.bind(this)} />
        <ModalTimePicker
          ref='mtp_to_date'
          title={"Đến"}
          isRequire={true}
          onPickerChange={this.onToDateChange.bind(this)}
          setVisibleModal={this.setVisibleModal.bind(this)}
          renderModal={this.renderModal.bind(this)} />
        <ModalDropdown
          options={[{ name: '1 dfds', phone: '123', color: 'red' }, { name: '4 dfsfsd', phone: '456', color: 'yellow' }]}
          mStyle={1}
          requireChar={true}
          selectColor={'color'}
          renderModal={this.renderModal.bind(this)}
          setVisibleModal={this.setVisibleModal.bind(this)}
          title={'Phân loại'} />
        <InputRow
          onFocus={this.onFocusWithContext.bind(this)}
          onEndEditing={this.onEndEditing.bind(this)}
          title={'Ghi chú'}
          onChangeText={(text) => { }} />
        <HeaderTick title={"Khách hàng gặp mặt"} mStyle={1} marginTop={30} />
        <ModalFilterDropdown
          options={[{ name: '1 dfds', phone: '123' }, { name: '4 dfsfsd', phone: '456' }, { name: '7 fsdfs', phone: '789' }, { name: '7 fsdfs', phone: '789' }, { name: '7 fsdfs', phone: '789' }, { name: '7 fsdfs', phone: '789' }, { name: '7 fsdfs', phone: '789' }, { name: '7 fsdfs', phone: '789' }]}
          keysFilter={['name', 'phone']}
          keyItems={['name', 'phone']}
          linkText={' - '}
          onSelect={(type, item) => { console.log(type); console.log(item) }}
          setWindowMargin={this.setWindowMargin.bind(this)}
          renderModal={this.renderModal.bind(this)}
          setVisibleModal={this.setVisibleModal.bind(this)}
          title={'Khách hàng'} />
        <ModalDropdown
          options={[{ name: '1 dfds', phone: '123', color: 'red' }, { name: '4 dfsfsd', phone: '456', color: 'yellow' }]}
          mStyle={1}
          requireChar={true}
          renderModal={this.renderModal.bind(this)}
          setVisibleModal={this.setVisibleModal.bind(this)}
          title={'Xe'} />
        <HeaderTick title={"Nhân viên tham dự"} mStyle={1} marginTop={30} />
        <TouchableOpacity style={styles.add_more_box} onPress={() => { }}>
          <Image source={require('../../../assets//images/add_more.png')} resizeMode={'contain'} style={{ width: 25, height: 25 }} />
          <Text style={styles.add_more_text}>Thêm người tham dự</Text>
        </TouchableOpacity>
        {this.renderAtPpList()}

        <TouchableOpacity
          onPress={() => {
            this.onPressSubmit()
          }}
          style={styles.submit}
          underlayColor='transparent'>
          <Text style={styles.btn_text}>Lưu</Text>
        </TouchableOpacity>
      </ScrollView >
    );
  }
  renderAtPpList() {
    var views = [];
    for (var i = 0; i < dataTemp.length; i++) {
      views.push(this.renderAtPp(dataTemp[i], i));
    }
    return views;
  }
  renderAtPp(item, index) {
    return (
      <View key={'AtPp' + index} style={styles.row}>
        <Image source={item.iconProfile ? { uri: item.iconProfile } : require('../../../assets//images/avatar_default.jpg')} resizeMode={'cover'} style={styles.avatar} />
        <View style={{ marginLeft: 10, backgroundColor: 'transparent' }}>
          <Text style={styles.text_name}>{item.fullname}</Text>
          <Text style={styles.text_phone}>{item.phone}</Text>
        </View>
        <TouchableOpacity style={styles.touch_edit}>
          <Image source={require('../../../assets//images/close_drawer.png')} resizeMode={'contain'} style={styles.icon_right} />
        </TouchableOpacity>
      </View>
    );
  }

  setWindowMargin(value) {
    this.setState({
      containerMarginTop: value
    });
  }

  onFromDateChange(date) {
    this.refs.mtp_to_date.setMinDate(date);
  }
  onToDateChange(date) {
    this.refs.mtp_from_date.setMaxDate(date);
  }
  renderRightButton() {

  }

  onPressSubmit() {
    this.goBack()
  }

  componentDidMount() {
  }
}
// function mapStateToProps(state) {
//   return {
//     provinceDistrictApi: state.provinceDistrictApi,
//     enumTypeApi: state.enumTypeApi
//   };
// }
export default connect(null, null)(AddCustomer);

const dataTemp = [
  {
    "customerSubProfileId": 67,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 1007,
    "customerSubProfileFlowType": null,
    "id": 6,
    "code": "527ce39f833d4e1bb883e4e28fef54e5",
    "passportId": null,
    "fullname": "Tuan Tu",
    "nickname": "ut det",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phone": '0126383992',
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 68,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 68,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 1009,
    "customerSubProfileFlowType": null,
    "id": 8,
    "code": "527ce39f833d4e1bb883e4e28fef54e5",
    "passportId": null,
    "fullname": "Tuan Tu1",
    "nickname": "ut det1",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phone": '092838084758',
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 69,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  },
  {
    "customerSubProfileId": 69,
    "customerSubProfileDealerId": 17,
    "customerSubProfileDepartmentId": null,
    "customerSubProfileTeamId": null,
    "customerSubProfileStaffId": 469,
    "customerSubProfileFlowType": null,
    "id": 9,
    "code": "527ce39f833d4e1bb883e4e28fef54e5",
    "passportId": null,
    "fullname": "Tuan Tu Ma",
    "nickname": "ut det ma",
    "gender": "FEMALE",
    "genderDisplayname": "Nữ",
    "vocative": null,
    "marital": null,
    "maritalDisplayname": null,
    "birthday": null,
    "phone": '08394830902',
    "email": null,
    "job": "Tester",
    "province": "TP.Hồ Chí Minh",
    "district": null,
    "address": "37/5 Huỳnh Bánh Đa, P12, Phú Nhuận, TP.HCM",
    "status": null,
    "createdBy": "STAFF",
    "createdAt": null,
    "updatedAt": null,
    "isDuplicateSubProfile": false,
    "customerSubProfileType": {
      "id": 70,
      "name": "INDIVIDUAL"
    },
    "iconProfile": "https://image.flaticon.com/icons/png/512/149/149452.png",
    "isCustomerInfoForSale": true,
    "masterId": null,
    "dealerId": null,
    "groupDealerId": null,
    "active": null
  }
]