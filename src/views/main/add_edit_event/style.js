import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../../global/colors';

module.exports = StyleSheet.create({
  container: {
    padding: 15
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  },
  text_input: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    height: 40,
    paddingVertical: 0,
    paddingHorizontal: 0,
    marginTop: -5
  },
  require_character: {
    color: 'red',
    marginTop: 10
  },
  input_row: {
    flex: 1,
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  add_more_box: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },
  add_more_text: {
    color: Colors.dark,
    fontFamily: "Roboto-Bold",
    fontSize: 12,
    marginLeft: 10
  },
  submit: {
    marginTop: 30,
    marginBottom: 50,
    height: 35,
    width: 120,
    backgroundColor: Colors.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  btn_text: {
    color: 'white',
    fontSize: 15,
    fontFamily: "Roboto-Regular",
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 80,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  text_name: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Bold"
  },
  text_phone: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 3
  },
  touch_edit: {
    height: 50,
    position: 'absolute',
    right: 0,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  icon_right: {
    width: 20,
    height: 20,
    marginRight: 8,
    tintColor: Colors.gray
  }
});
