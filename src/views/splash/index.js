import React from 'react';
import {
    View,
    Image,
    Animated
} from 'react-native';
import { connect } from 'react-redux';
import { refreshToken } from '../../actions/splashAction';
import BaseView from '../../views/base/base_view'
import styles from './styles'

class Splash extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            carAnim: new Animated.Value(2),
            carMargin: "2%"
        };

        this.state.carAnim.addListener(({ value }) => {
            this.setState({ carMargin: value + '%' });
            if (value == 100) {
                //Check if REFRESH_TOKEN_STORE_ID => refresh token; else check DEVICE_TOKEN_STORE_ID =>....
                // Utils.getValueByKey(Const.REFRESH_TOKEN_STORE_ID).then((result) => {
                //     if (result !== '' && result !== undefined && result !== null) {
                //         this.props.refreshToken(result, this);
                //     } else {
                //         Utils.getValueByKey(Const.DEVICE_TOKEN_STORE_ID).then((result) => {
                //             if (result !== '' && result !== undefined && result !== null) {
                //                 this.props.navigator.replace(routes.login);
                //             } else {
                //                 this.props.navigator.replace(routes.register_device);
                //             }
                //         });
                //     }
                // });
                this.resetPage(0, 'register_device', { name: 'Jane' })
            }
        });

    }

    componentDidMount() {
        Animated.timing(
            this.state.carAnim,
            {
                toValue: 100,
                duration: 2000
            },
        ).start();
    }

    renderBaseContent() {
        let { carMargin } = this.state;//center contain cover repeat stretch

        return (
            <View>
                <Image style={styles.background} resizeMode={'stretch'} source={require('../../assets/images/splash_background.png')} ></Image>
                <View style={styles.container}>
                    <View style={styles.logo_box}>
                        <Image style={styles.logo} resizeMode={'contain'} source={require('../../assets/images/logo.png')} ></Image>
                    </View>

                    <View style={styles.bottom_car_box}>
                        <Animated.Image style={[styles.bottom_car, { marginLeft: carMargin }]} resizeMode={'contain'} source={require('../../assets/images/splash_car.png')} ></Animated.Image>
                    </View>
                </View>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        nav: state.nav
    };
}
export default connect(mapStateToProps, { refreshToken })(Splash);
