import React, {
    StyleSheet
} from 'react-native';

module.exports = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%'
    },
    logo_box: {
        width: '100%',
        alignItems: 'center'
    },
    logo: {
        width: 159,
        height: 80,
        marginTop: 184,
        justifyContent: 'center',
        alignItems: 'center'
    },
    background: {
        width: '100%',
        height: '100%'
    },
    bottom_car: {
        width: 75,
        height: 32
    },
    bottom_car_box: {
        width: '100%',
        height: 32,
        position: 'absolute',
        bottom: 5
    }
});