import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, StatusBar, Image, Animated } from 'react-native';
import { connect } from 'react-redux';
import { doLogin } from '../../actions/loginAction';
import TextField from '../../widgets/text_input';
import BaseView from '../../views/base/base_view'
import styles from './styles'
import StringSource from '../../global/strings'
import Utils from '../../global/utils'
import Const from "../../global/const"
import Route from "../../views/routes"

class Login extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            logoAnim: new Animated.Value(184),
            fadeAnim: new Animated.Value(0)
        };

    }

    componentDidMount() {
        Animated.parallel([
            Animated.timing(
                this.state.logoAnim,
                {
                    toValue: 90,
                    duration: 1000,
                },
            ),

            Animated.timing(
                this.state.fadeAnim,
                {
                    toValue: 1,
                    duration: 2000,
                }
            )
        ]).start();
        // Utils.getValueByKey(Const.LOGIN_INPUT_STORE_ID).then((result) => {
        //     console.log(result);
        //     if (result !== '' && result !== undefined && result !== null) {
        //         this.refs.tf_username.setValue(result.username);
        //         this.refs.tf_password.setValue(result.password);
        //     }
        // });
    }

    renderBaseContent() {
        let { logoAnim, fadeAnim } = this.state;
        return (
            <ScrollView style={styles.container}>
                <Animated.View style={[styles.logo_box, { marginTop: logoAnim }]}>
                    <Image style={styles.logo} resizeMode={'contain'} source={require('../../assets/images/logo.png')} ></Image>
                </Animated.View>

                <Animated.View style={[styles.form, { opacity: fadeAnim }]}>
                    <TextField
                        ref="tf_username"
                        label={StringSource.username_hint}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        height={40}
                    />

                    <TextField
                        ref="tf_password"
                        label={StringSource.password_hint}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        secureTextEntry={true}
                        height={40}
                    />

                    <TouchableOpacity
                        onPress={this.doLogin.bind(this)}
                        style={styles.submit}
                        underlayColor='transparent'>
                        <Text style={styles.btn_text}>{StringSource.login_btn}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        this.pushPage('forgot_password', { name: 'Jane' })
                    }}>
                        <Text style={styles.forgot_pass}>{StringSource.forgot_pass}</Text>
                    </TouchableOpacity>
                </Animated.View>
            </ScrollView>
        );
    }
    doLogin() {
        var userName = this.refs.tf_username.getValue();
        var password = this.refs.tf_password.getValue();
        // this.props.doLogin(userName, password, this);
        this.resetPage(0, 'main', { name: 'Jane' })
    }

}


export default connect(null, { doLogin })(Login);
