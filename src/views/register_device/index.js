import React from 'react'
import { Text, TouchableOpacity, ScrollView, Image, Animated } from 'react-native'
import { connect } from 'react-redux'
import { doRegister } from '../../actions/registerDeviceAction'
import TextField from '../../widgets/text_input'
import BaseView from '../../views/base/base_view'
import styles from './styles'
import StringSource from '../../global/strings'

class RegisterDevice extends BaseView {
    constructor(props) {
        super(props)
        this.state = {
            logoAnim: new Animated.Value(184),
            fadeAnim: new Animated.Value(0),
            isLoading: false
        }
    }

    componentDidMount() {
        // this.setVisibleModal(true)
        Animated.parallel([
            Animated.timing(
                this.state.logoAnim,
                {
                    toValue: 90,
                    duration: 2000,
                },
            ),

            Animated.timing(
                this.state.fadeAnim,
                {
                    toValue: 1,
                    duration: 3000,
                }
            )
        ]).start()
        // Utils.getValueByKey(Const.LOGIN_INPUT_STORE_ID).then((result) => {
        //     if (result !== '' && result !== undefined && result !== null) {
        //         this.refs.tf_username.setValue(result.username)
        //     }
        // })
    }

    renderBaseContent() {
        let { logoAnim, fadeAnim } = this.state
        return (
            <ScrollView style={styles.container}>
                <Animated.View style={[styles.logo_box, { marginTop: logoAnim }]}>
                    <Image style={styles.logo} resizeMode={'contain'} source={require('../../assets/images/logo.png')} ></Image>
                </Animated.View>

                <Animated.View style={[styles.form, { opacity: fadeAnim }]}>
                    <TextField
                        ref="tf_username"
                        label={StringSource.username_hint}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        height={40}
                    />

                    <TouchableOpacity
                        onPress={this.doRegister.bind(this)}
                        style={styles.submit}
                        underlayColor='transparent'>
                        <Text style={styles.btn_text}>{StringSource.register_device_btn}</Text>
                    </TouchableOpacity>
                </Animated.View>
            </ScrollView>
        )
    }

    onUserNameTextChane() {
        // this.setState({
        //     userName: text
        // })
    }
    doRegister() {
        // console.log(userName)
        // this.props.doRegister(userName, deviceToken, this)
        this.resetPage(0, 'login', { name: 'Jane' })
    }
}

// function mapStateToProps(state) {
//     return { reducerState: state.registerDevicePage }
// }

// class WrapperComponent extends BaseView {
//     render() {
//         return (
//             <View style={{ marginTop: 15 }}>
//                 {this.props.children}
//             </View>)
//     }
// }
export default connect(null, { doRegister })(RegisterDevice)
