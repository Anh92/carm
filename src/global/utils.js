import React, {
    Platform,
    AsyncStorage
} from 'react-native';
import Const from './const'

var RCTUIManager = require('NativeModules').UIManager;
function convertUnicode(string) {
    for (var key in map) {
        string = string.replace(new RegExp(key, 'g'), map[key])
    }
    return string;
}
export default Object.freeze({
    updateUIWhenShowKeyboard: function (refStr, component) {
        if (Platform.OS === 'ios') {
            var inputView = component.refs[refStr];
            var handle = React.findNodeHandle(inputView);
            var spaceScreen = Const.HEIGHT_SCREEN - Const.HEIGHT_KEYBOARD_IOS;
            RCTUIManager.measure(handle, (x, y, width, height, pageX, pageY) => {
                if (spaceScreen < pageY + height) {
                    component.setState({ containerMarginTop: (pageY - spaceScreen + height) * (-1) });
                }
            });
        }
    },
    updateUIWhenShowKeyboardWithContext: function (refStr, component) {
        if (!refStr || !component) return;
        if (Platform.OS === 'ios') {
            var spaceScreen = Const.HEIGHT_SCREEN - Const.HEIGHT_KEYBOARD_IOS;
            refStr.measure((x, y, width, height, pageX, pageY) => {
                if (spaceScreen < pageY + height) {
                    component.setState({ containerMarginTop: (pageY - spaceScreen + height) * (-1) });
                }
            });
        }
    },
    updateUIWhenHideKeyboard: function (component) {
        if (Platform.OS === 'ios') {
            component.setState({ containerMarginTop: 0 });
        }
    },
    getValueByKey: function (key) {
        return new Promise(function (resolve, reject) {
            // AsyncStorage.getItem(key).then((value) => {
            //     resolve(JSON.parse(value));
            // }).done();
            resolve({});
        });
    },
    setValueByKey: function (key, value) {
        AsyncStorage.setItem(key, value);
    },
    checkNumberInput: function (text) {
        let newText = '';
        let numbers = '0123456789';

        for (var i = 0; i < text.length; i++) {
            if (numbers.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
        }
        return newText;
    },
    b64DecodeUnicode: function (str) {
        var Base64 = require('base-64');
        // return Base64.decode(str);
        return decodeURIComponent(Base64.decode(str).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    },
    convertVietNameLetter: function (alias) {
        var str = convertUnicode(alias);
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        return str;
    },
    convertSolar2Lunar: function (dd, mm, yy) {
        var k, dayNumber, monthStart, a11, b11, lunarDay, lunarMonth, lunarYear, lunarLeap;
        dayNumber = jdFromDate(dd, mm, yy);
        k = Math.floor((dayNumber - 2415021.076998695) / 29.530588853);
        monthStart = getNewMoonDay(k + 1, 7);
        if (monthStart > dayNumber) {
            monthStart = getNewMoonDay(k, 7);
        }
        a11 = getLunarMonth11(yy, 7);
        b11 = a11;
        if (a11 >= monthStart) {
            lunarYear = yy;
            a11 = getLunarMonth11(yy - 1, 7);
        } else {
            lunarYear = yy + 1;
            b11 = getLunarMonth11(yy + 1, 7);
        }
        lunarDay = dayNumber - monthStart + 1;
        diff = Math.floor((monthStart - a11) / 29);
        lunarLeap = 0;
        lunarMonth = diff + 11;
        if (b11 - a11 > 365) {
            leapMonthDiff = getLeapMonthOffset(a11, 7);
            if (diff >= leapMonthDiff) {
                lunarMonth = diff + 10;
                if (diff == leapMonthDiff) {
                    lunarLeap = 1;
                }
            }
        }
        if (lunarMonth > 12) {
            lunarMonth = lunarMonth - 12;
        }
        if (lunarMonth >= 11 && diff < 4) {
            lunarYear -= 1;
        }
        return {
            year: lunarYear,
            month: lunarMonth,
            day: lunarDay,
            yearLunarTitle: tgString[(lunarYear - 4) % 10] + " " + sx[(lunarYear - 4) % 12]
        };
    }
});

var tgString = ['Giáp', 'Ất', 'Bính', 'Đinh', 'Mậu', 'Kỷ', 'Canh', 'Tân', 'Nhâm', 'Quý'];
var sx = ['Tý', 'Sửu', 'Dần', 'Mão', 'Thìn', 'Tỵ', 'Ngọ', 'Mùi', 'Thân', 'Dậu', 'Tuất', 'Hợi'];
var map = {
    '\u0065\u0309': '\u1EBB', //ẻ
    '\u0065\u0301': '\u00E9', //é
    '\u0065\u0300': '\u00E8', //è
    '\u0065\u0323': '\u1EB9', //ẹ
    '\u0065\u0303': '\u1EBD', //ẽ
    '\u00EA\u0309': '\u1EC3', //ể
    '\u00EA\u0301': '\u1EBF', //ế
    '\u00EA\u0300': '\u1EC1', //ề
    '\u00EA\u0323': '\u1EC7', //ệ
    '\u00EA\u0303': '\u1EC5', //ễ
    '\u0079\u0309': '\u1EF7', //ỷ
    '\u0079\u0301': '\u00FD', //ý
    '\u0079\u0300': '\u1EF3', //ỳ
    '\u0079\u0323': '\u1EF5', //ỵ
    '\u0079\u0303': '\u1EF9', //ỹ
    '\u0075\u0309': '\u1EE7', //ủ
    '\u0075\u0301': '\u00FA', //ú
    '\u0075\u0300': '\u00F9', //ù
    '\u0075\u0323': '\u1EE5', //ụ
    '\u0075\u0303': '\u0169', //ũ
    '\u01B0\u0309': '\u1EED', //ử
    '\u01B0\u0301': '\u1EE9', //ứ
    '\u01B0\u0300': '\u1EEB', //ừ
    '\u01B0\u0323': '\u1EF1', //ự
    '\u01B0\u0303': '\u1EEF', //ữ
    '\u0069\u0309': '\u1EC9', //ỉ
    '\u0069\u0301': '\u00ED', //í
    '\u0069\u0300': '\u00EC', //ì
    '\u0069\u0323': '\u1ECB', //ị
    '\u0069\u0303': '\u0129', //ĩ
    '\u006F\u0309': '\u1ECF', //ỏ
    '\u006F\u0301': '\u00F3', //ó
    '\u006F\u0300': '\u00F2', //ò
    '\u006F\u0323': '\u1ECD', //ọ
    '\u006F\u0303': '\u00F5', //õ
    '\u01A1\u0309': '\u1EDF', //ở
    '\u01A1\u0301': '\u1EDB', //ớ
    '\u01A1\u0300': '\u1EDD', //ờ
    '\u01A1\u0323': '\u1EE3', //ợ
    '\u01A1\u0303': '\u1EE1', //ỡ
    '\u00F4\u0309': '\u1ED5', //ổ
    '\u00F4\u0301': '\u1ED1', //ố
    '\u00F4\u0300': '\u1ED3', //ồ
    '\u00F4\u0323': '\u1ED9', //ộ
    '\u00F4\u0303': '\u1ED7', //ỗ
    '\u0061\u0309': '\u1EA3', //ả
    '\u0061\u0301': '\u00E1', //á
    '\u0061\u0300': '\u00E0', //à
    '\u0061\u0323': '\u1EA1', //ạ
    '\u0061\u0303': '\u00E3', //ã
    '\u0103\u0309': '\u1EB3', //ẳ
    '\u0103\u0301': '\u1EAF', //ắ
    '\u0103\u0300': '\u1EB1', //ằ
    '\u0103\u0323': '\u1EB7', //ặ
    '\u0103\u0303': '\u1EB5', //ẵ
    '\u00E2\u0309': '\u1EA9', //ẩ
    '\u00E2\u0301': '\u1EA5', //ấ
    '\u00E2\u0300': '\u1EA7', //ầ
    '\u00E2\u0323': '\u1EAD', //ậ
    '\u00E2\u0303': '\u1EAB', //ẫ
    '\u0045\u0309': '\u1EBA', //Ẻ
    '\u0045\u0301': '\u00C9', //É
    '\u0045\u0300': '\u00C8', //È
    '\u0045\u0323': '\u1EB8', //Ẹ
    '\u0045\u0303': '\u1EBC', //Ẽ
    '\u00CA\u0309': '\u1EC2', //Ể
    '\u00CA\u0301': '\u1EBE', //Ế
    '\u00CA\u0300': '\u1EC0', //Ề
    '\u00CA\u0323': '\u1EC6', //Ệ
    '\u00CA\u0303': '\u1EC4', //Ễ
    '\u0059\u0309': '\u1EF6', //Ỷ
    '\u0059\u0301': '\u00DD', //Ý
    '\u0059\u0300': '\u1EF2', //Ỳ
    '\u0059\u0323': '\u1EF4', //Ỵ
    '\u0059\u0303': '\u1EF8', //Ỹ
    '\u0055\u0309': '\u1EE6', //Ủ
    '\u0055\u0301': '\u00DA', //Ú
    '\u0055\u0300': '\u00D9', //Ù
    '\u0055\u0323': '\u1EE4', //Ụ
    '\u0055\u0303': '\u0168', //Ũ
    '\u01AF\u0309': '\u1EEC', //Ử
    '\u01AF\u0301': '\u1EE8', //Ứ
    '\u01AF\u0300': '\u1EEA', //Ừ
    '\u01AF\u0323': '\u1EF0', //Ự
    '\u01AF\u0303': '\u1EEE', //Ữ
    '\u0049\u0309': '\u1EC8', //Ỉ
    '\u0049\u0301': '\u00CD', //Í
    '\u0049\u0300': '\u00CC', //Ì
    '\u0049\u0323': '\u1ECA', //Ị
    '\u0049\u0303': '\u0128', //Ĩ
    '\u004F\u0309': '\u1ECE', //Ỏ
    '\u004F\u0301': '\u00D3', //Ó
    '\u004F\u0300': '\u00D2', //Ò
    '\u004F\u0323': '\u1ECC', //Ọ
    '\u004F\u0303': '\u00D5', //Õ
    '\u01A0\u0309': '\u1EDE', //Ở
    '\u01A0\u0301': '\u1EDA', //Ớ
    '\u01A0\u0300': '\u1EDC', //Ờ
    '\u01A0\u0323': '\u1EE2', //Ợ
    '\u01A0\u0303': '\u1EE0', //Ỡ
    '\u00D4\u0309': '\u1ED4', //Ổ
    '\u00D4\u0301': '\u1ED0', //Ố
    '\u00D4\u0300': '\u1ED2', //Ồ
    '\u00D4\u0323': '\u1ED8', //Ộ
    '\u00D4\u0303': '\u1ED6', //Ỗ
    '\u0041\u0309': '\u1EA2', //Ả
    '\u0041\u0301': '\u00C1', //Á
    '\u0041\u0300': '\u00C0', //À
    '\u0041\u0323': '\u1EA0', //Ạ
    '\u0041\u0303': '\u00C3', //Ã
    '\u0102\u0309': '\u1EB2', //Ẳ
    '\u0102\u0301': '\u1EAE', //Ắ
    '\u0102\u0300': '\u1EB0', //Ằ
    '\u0102\u0323': '\u1EB6', //Ặ
    '\u0102\u0303': '\u1EB4', //Ẵ
    '\u00C2\u0309': '\u1EA8', //Ẩ
    '\u00C2\u0301': '\u1EA4', //Ấ
    '\u00C2\u0300': '\u1EA6', //Ầ
    '\u00C2\u0323': '\u1EAC', //Ậ
    '\u00C2\u0303': '\u1EAA' //Ẫ
}


function jdFromDate(dd, mm, yy) {
    var a, y, m, jd;
    a = Math.floor((14 - mm) / 12);
    y = yy + 4800 - a;
    m = mm + 12 * a - 3;
    jd = dd + Math.floor((153 * m + 2) / 5) + 365 * y + Math.floor(y / 4) - Math.floor(y / 100) + Math.floor(y / 400) - 32045;
    if (jd < 2299161) {
        jd = dd + Math.floor((153 * m + 2) / 5) + 365 * y + Math.floor(y / 4) - 32083;
    }
    return jd;
}
function jdToDate(jd) {
    var a, b, c, d, e, m, day, month, year;
    if (jd > 2299160) { // After 5/10/1582, Gregorian calendar
        a = jd + 32044;
        b = Math.floor((4 * a + 3) / 146097);
        c = a - Math.floor((b * 146097) / 4);
    } else {
        b = 0;
        c = jd + 32082;
    }
    d = Math.floor((4 * c + 3) / 1461);
    e = c - Math.floor((1461 * d) / 4);
    m = Math.floor((5 * e + 2) / 153);
    day = e - Math.floor((153 * m + 2) / 5) + 1;
    month = m + 3 - 12 * Math.floor(m / 10);
    year = b * 100 + d - 4800 + Math.floor(m / 10);
    return new Array(day, month, year);
}
function getNewMoonDay(k, timeZone) {
    var T, T2, T3, dr, Jd1, M, Mpr, F, C1, deltat, JdNew;
    T = k / 1236.85; // Time in Julian centuries from 1900 January 0.5
    T2 = T * T;
    T3 = T2 * T;
    dr = Math.PI / 180;
    Jd1 = 2415020.75933 + 29.53058868 * k + 0.0001178 * T2 - 0.000000155 * T3;
    Jd1 = Jd1 + 0.00033 * Math.sin((166.56 + 132.87 * T - 0.009173 * T2) * dr); // Mean new moon
    M = 359.2242 + 29.10535608 * k - 0.0000333 * T2 - 0.00000347 * T3; // Sun's mean anomaly
    Mpr = 306.0253 + 385.81691806 * k + 0.0107306 * T2 + 0.00001236 * T3; // Moon's mean anomaly
    F = 21.2964 + 390.67050646 * k - 0.0016528 * T2 - 0.00000239 * T3; // Moon's argument of latitude
    C1 = (0.1734 - 0.000393 * T) * Math.sin(M * dr) + 0.0021 * Math.sin(2 * dr * M);
    C1 = C1 - 0.4068 * Math.sin(Mpr * dr) + 0.0161 * Math.sin(dr * 2 * Mpr);
    C1 = C1 - 0.0004 * Math.sin(dr * 3 * Mpr);
    C1 = C1 + 0.0104 * Math.sin(dr * 2 * F) - 0.0051 * Math.sin(dr * (M + Mpr));
    C1 = C1 - 0.0074 * Math.sin(dr * (M - Mpr)) + 0.0004 * Math.sin(dr * (2 * F + M));
    C1 = C1 - 0.0004 * Math.sin(dr * (2 * F - M)) - 0.0006 * Math.sin(dr * (2 * F + Mpr));
    C1 = C1 + 0.0010 * Math.sin(dr * (2 * F - Mpr)) + 0.0005 * Math.sin(dr * (2 * Mpr + M));
    if (T < -11) {
        deltat = 0.001 + 0.000839 * T + 0.0002261 * T2 - 0.00000845 * T3 - 0.000000081 * T * T3;
    } else {
        deltat = -0.000278 + 0.000265 * T + 0.000262 * T2;
    };
    JdNew = Jd1 + C1 - deltat;
    return Math.floor(JdNew + 0.5 + timeZone / 24)
}
function getSunLongitude(jdn, timeZone) {
    var T, T2, dr, M, L0, DL, L;
    T = (jdn - 2451545.5 - timeZone / 24) / 36525; // Time in Julian centuries from 2000-01-01 12:00:00 GMT
    T2 = T * T;
    dr = Math.PI / 180; // degree to radian
    M = 357.52910 + 35999.05030 * T - 0.0001559 * T2 - 0.00000048 * T * T2; // mean anomaly, degree
    L0 = 280.46645 + 36000.76983 * T + 0.0003032 * T2; // mean longitude, degree
    DL = (1.914600 - 0.004817 * T - 0.000014 * T2) * Math.sin(dr * M);
    DL = DL + (0.019993 - 0.000101 * T) * Math.sin(dr * 2 * M) + 0.000290 * Math.sin(dr * 3 * M);
    L = L0 + DL; // true longitude, degree
    L = L * dr;
    L = L - Math.PI * 2 * (Math.floor(L / (Math.PI * 2))); // Normalize to (0, 2*Math.PI)
    return Math.floor(L / Math.PI * 6)
}
function getLunarMonth11(yy, timeZone) {
    var k, off, nm, sunLong;
    off = jdFromDate(31, 12, yy) - 2415021;
    k = Math.floor(off / 29.530588853);
    nm = getNewMoonDay(k, timeZone);
    sunLong = getSunLongitude(nm, timeZone); // sun longitude at local midnight
    if (sunLong >= 9) {
        nm = getNewMoonDay(k - 1, timeZone);
    }
    return nm;
}
function getLeapMonthOffset(a11, timeZone) {
    var k, last, arc, i;
    k = Math.floor((a11 - 2415021.076998695) / 29.530588853 + 0.5);
    last = 0;
    i = 1; // We start with the month following lunar month 11
    arc = getSunLongitude(getNewMoonDay(k + i, timeZone), timeZone);
    do {
        last = arc;
        i++;
        arc = getSunLongitude(getNewMoonDay(k + i, timeZone), timeZone);
    } while (arc != last && i < 14);
    return i - 1;
}
