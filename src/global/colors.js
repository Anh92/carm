export default Object.freeze({
    dark_blue: '#02867C',
    green: '#7BC353',
    yellow: '#FFCD33',
    purple: '#C73667',
    orange: '#F26522',
    blue: '#0094B8',
    gray: '#676766',
    dark: '#1E2728',
    ink: '#1B2936',
    dark_pink: '#E35B5A',
    smoke: '#F6F6F6',
    light_dark: '#30404F',
    underline: '#BEBEBE',
    border: '#BFCAD1',
    white: '#ffffff',
    dark_yellow: "#FFB300",
    light_gray: "#A5A8AA"
});
