import {SplashActionType} from '../action_type'

const splashReducer = (state = SplashActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default splashReducer;