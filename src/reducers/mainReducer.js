import { MainActionType } from '../action_type'

const splashReducer = (state = MainActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case MainActionType.TOGGLE_MENU:
            return { ...state, drawerMenuOpen: !state.drawerMenuOpen, isDrawerChanging: true };
        case MainActionType.CLOSE_MENU:
            return { ...state, drawerMenuOpen: false, isDrawerChanging: false };
        case MainActionType.OPEN_MENU:
            return { ...state, drawerMenuOpen: true, isDrawerChanging: false };
        case MainActionType.SET_HEADER_TEXT:
            return { ...state, headerText: action.value };
        case MainActionType.SET_DRAWER_SELECTED_ID:
            return { ...state, drawerSelected: action.id };
        default:
            return state;
    }
};
export default splashReducer;