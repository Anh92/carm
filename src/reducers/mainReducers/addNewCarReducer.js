import {AddNewCarActionType} from '../../action_type'

const addCarReducer = (state = AddNewCarActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case AddNewCarActionType.ADD_CAR_SUCCESS:
            return {
                error: false
            };
        case AddNewCarActionType.ADD_CAR_FAIL:
            return {
                error: true
            };
        default:
            return state;
    }
};

export default addCarReducer;