import { CustomerInfoActionType } from '../../action_type'

const customerInfoReducer = (state = CustomerInfoActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case CustomerInfoActionType.START_GET_CAR_LIST:
            return {
                isCarListLoading: true,
                loadCarListError: false,
                carList: []
            };
        case CustomerInfoActionType.GET_CAR_LIST_SUCCESS:
            return {
                isCarListLoading: false,
                loadCarListError: false,
                carList: action.data
            };
        case CustomerInfoActionType.GET_CAR_LIST_FAIL:
            return {
                isCarListLoading: false,
                loadCarListError: true,
                carList: []
            };
        default:
            return state;
    }
};

export default customerInfoReducer;