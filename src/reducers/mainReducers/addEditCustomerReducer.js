import {AddEditCustomerActionType} from '../../action_type'

const addEditCustomerReducer = (state = AddEditCustomerActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case AddEditCustomerActionType.ADD_EDIT_SUCCESS:
            return {
                error: false
            };
        case AddEditCustomerActionType.ADD_EDIT_FAIL:
            return {
                error: true
            };
        default:
            return state;
    }
};

export default addEditCustomerReducer;