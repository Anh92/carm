import {EditCatActionType} from '../../action_type'

const editCarReducer = (state = EditCatActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case EditCatActionType.EDIT_CAR_SUCCESS:
            return {
                error: false
            };
        case EditCatActionType.EDIT_CAR_FAIL:
            return {
                error: true
            };
        default:
            return state;
    }
};

export default editCarReducer;