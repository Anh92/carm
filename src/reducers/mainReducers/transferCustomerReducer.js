import {TransferCustomerActionType} from '../../action_type'

const transferCustomerReducer = (state = TransferCustomerActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case TransferCustomerActionType.TRANSFER_CUSTOMER_SUCCESS:
            return {
                error: false
            };
        case TransferCustomerActionType.TRANSFER_CUSTOMER_FAIL:
            return {
                error: true
            };
        default:
            return state;
    }
};

export default transferCustomerReducer;