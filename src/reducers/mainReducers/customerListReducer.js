import { CustomerListActionType } from '../../action_type'

const customerListReducer = (state = CustomerListActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case CustomerListActionType.START_GET_CUSTOMER_DATA:
            return {
                isLoading: true,
                error: false,
                customerList: []
            };
        case CustomerListActionType.GET_CUSTOMER_SUCCESS:
            return {
                isLoading: false,
                error: false,
                customerList: action.data
            };
        case CustomerListActionType.GET_CUSTOMER_FAIL:
            return {
                isLoading: false,
                error: true,
                customerList: []
            };
        default:
            return state;
    }
};

export default customerListReducer;