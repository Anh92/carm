import { CarInfoActionType } from '../../action_type'

const carInfoReducer = (state = CarInfoActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case CarInfoActionType.START_GET_SALES_SUPPORT:
            return {
                ...state,
                salesSupportError: false,
                isSalesSupportLoading: true,
                salesSupportData: {}
            };
        case CarInfoActionType.GET_SALES_SUPPORT_SUCCESS:
            return {
                ...state,
                salesSupportError: false,
                isSalesSupportLoading: false,
                salesSupportData: action.data
            };
        case CarInfoActionType.GET_SALES_SUPPORT_FAIL:
            return {
                ...state,
                salesSupportError: true,
                isSalesSupportLoading: false,
                salesSupportData: {}
            };
        case CarInfoActionType.START_GET_COMPANY:
            return {
                ...state,
                getCompanyError: false,
                isGetCompanyLoading: true,
                loadCompanyDataSuccess: false,
                companyData: {}
            };
        case CarInfoActionType.GET_COMPANY_SUCCESS:
            return {
                ...state,
                getCompanyError: false,
                isGetCompanyLoading: false,
                loadCompanyDataSuccess: true,
                companyData: action.data
            };
        case CarInfoActionType.GET_COMPANY_FAIL:
            return {
                ...state,
                getCompanyError: true,
                isGetCompanyLoading: false,
                loadCompanyDataSuccess: false,
                companyData: {}
            };
        case CarInfoActionType.START_GET_BROKER:
            return {
                ...state,
                getBrokerError: false,
                isBrokerLoading: true,
                loadBrokerDataSuccess: false,
                brokerData: {}
            };
        case CarInfoActionType.GET_BROKER_SUCCESS:
            return {
                ...state,
                getBrokerError: false,
                isBrokerLoading: false,
                loadBrokerDataSuccess: true,
                brokerData: action.data
            };
        case CarInfoActionType.GET_BROKER_FAIL:
            return {
                ...state,
                getBrokerError: true,
                isBrokerLoading: false,
                loadBrokerDataSuccess: false,
                brokerData: {}
            };
        case CarInfoActionType.START_GET_PERSON_CONTACT:
            return {
                ...state,
                getPersonContactError: false,
                isPersonContactLoading: true,
                loadPersonContactSuccess: false,
                personContactData: []
            };
        case CarInfoActionType.GET_PERSON_CONTACT_SUCCESS:
            return {
                ...state,
                getPersonContactError: false,
                isPersonContactLoading: false,
                loadPersonContactSuccess: true,
                personContactData: action.data
            };
        case CarInfoActionType.GET_PERSON_CONTACT_FAIL:
            return {
                ...state,
                getPersonContactError: true,
                isPersonContactLoading: false,
                loadPersonContactSuccess: false,
                personContactData: []
            };
        default:
            return state;
    }
};

export default carInfoReducer;