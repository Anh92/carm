import { HomeActionType } from '../../action_type'

const homeReducer = (state = HomeActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default homeReducer;