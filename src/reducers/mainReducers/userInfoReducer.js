import { UserInfoActionType } from '../../action_type'

const userInfoReducer = (state = UserInfoActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case UserInfoActionType.LOGOUT_SUCCESS:
            return {
                error: false
            };
        case UserInfoActionType.LOGOUT_FAIL:
            return {
                error: true
            };
        default:
            return state;
    }
};

export default userInfoReducer;