import { EnumTypeApiActionType } from '../../action_type'

const enumTypeReducer = (state = EnumTypeApiActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case EnumTypeApiActionType.START_GET_ENUM_TYPE:
            return {
                isLoading: true,
                error: false,
                enumData: {}
            };
        case EnumTypeApiActionType.GET_ENUM_TYPE_SUCCESS:
            return {
                isLoading: false,
                error: false,
                enumData: action.data
            };
        case EnumTypeApiActionType.GET_ENUM_TYPE_FAIL:
            return {
                isLoading: false,
                error: true,
                enumData: {}
            };
        default:
            return state;
    }
};

export default enumTypeReducer;