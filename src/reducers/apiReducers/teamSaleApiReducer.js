import { TeamSaleApiActionType } from '../../action_type'

const teamSaleReducer = (state = TeamSaleApiActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case TeamSaleApiActionType.START_GET_TEAM_SALE:
            return {
                isLoading: true,
                error: false,
                teamSaleData: []
            };
        case TeamSaleApiActionType.GET_TEAM_SALE_SUCCESS:
            return {
                isLoading: false,
                error: false,
                teamSaleData: action.data
            };
        case TeamSaleApiActionType.GET_TEAM_SALE_FAIL:
            return {
                isLoading: false,
                error: true,
                teamSaleData: []
            };
        default:
            return state;
    }
};

export default teamSaleReducer;