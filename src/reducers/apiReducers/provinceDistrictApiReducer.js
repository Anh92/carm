import { ProvinceDistrictApiActionType } from '../../action_type'

const provinceDistrictReducer = (state = ProvinceDistrictApiActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case ProvinceDistrictApiActionType.START_GET_PROVICE:
            return {
                isLoading: true,
                error: false,
                listProvinceDistrict: []
            };
        case ProvinceDistrictApiActionType.GET_PROVICE_SUCCESS:
            return {
                isLoading: false,
                error: false,
                listProvinceDistrict: action.data
            };
        case ProvinceDistrictApiActionType.GET_PROVICE_FAIL:
            return {
                isLoading: false,
                error: true,
                listProvinceDistrict: []
            };
        default:
            return state;
    }
};

export default provinceDistrictReducer;