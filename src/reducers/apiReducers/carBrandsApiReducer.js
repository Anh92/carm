import { CarBrandsApiActionType } from '../../action_type'

const carBrandsReducer = (state = CarBrandsApiActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case CarBrandsApiActionType.START_GET_CAR_BRANDS:
            return {
                isLoading: true,
                error: false,
                carBrandsData: []
            };
        case CarBrandsApiActionType.GET_CAR_BRANDS_SUCCESS:
            return {
                isLoading: false,
                error: false,
                carBrandsData: action.data
            };
        case CarBrandsApiActionType.GET_CAR_BRANDS_FAIL:
            return {
                isLoading: false,
                error: true,
                carBrandsData: []
            };
        default:
            return state;
    }
};

export default carBrandsReducer;