import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import registerDeviceReducer from './registerDeviceReducer';
import splashReducer from './splashReducer';
import mainReducer from './mainReducer';
import homeReducer from './mainReducers/homeReducer';
import customerListReducer from './mainReducers/customerListReducer';
import addEditCustomerReducer from './mainReducers/addEditCustomerReducer';
import customerInfoReducer from './mainReducers/customerInfoReducer';
import provinceDistrictApiReducer from './apiReducers/provinceDistrictApiReducer';
import enumTypeApiReducer from './apiReducers/enumTypeApiReducer';
import addNewCarReducer from './mainReducers/addNewCarReducer';
import carInfoReducer from './mainReducers/carInfoReducer';
import carBrandsApiReducer from './apiReducers/carBrandsApiReducer';
import teamSaleApiReducer from './apiReducers/teamSaleApiReducer';
import transferCustomerReducer from './mainReducers/transferCustomerReducer';
import userInfoReducer from './mainReducers/userInfoReducer';
import userContextReducer from './userContextReducer';

export default function getRootReducer(navReducer) {
    return combineReducers({
        nav: navReducer,
        loginPage: loginReducer,
        splashPage: splashReducer,
        mainPage: mainReducer,
        homePage: homeReducer,
        registerDevicePage: registerDeviceReducer,
        customerPage: customerListReducer,
        addEditCustomerPage: addEditCustomerReducer,
        customerInfoPage: customerInfoReducer,
        provinceDistrictApi: provinceDistrictApiReducer,
        enumTypeApi: enumTypeApiReducer,
        addNewCarPage: addNewCarReducer,
        carInfoPage: carInfoReducer,
        carBrandsApi: carBrandsApiReducer,
        teamSaleApi: teamSaleApiReducer,
        transferCustomerPage: transferCustomerReducer,
        userInfoPage: userInfoReducer,
        userContext: userContextReducer
    });
}

// const reducer = combineReducers({
//     loginPage: loginReducer,
//     splashPage: splashReducer,
//     mainPage: mainReducer,
//     homePage: homeReducer,
//     registerDevicePage: registerDeviceReducer,
//     customerPage: customerListReducer,
//     addEditCustomerPage: addEditCustomerReducer,
//     customerInfoPage: customerInfoReducer,
//     provinceDistrictApi: provinceDistrictApiReducer,
//     enumTypeApi: enumTypeApiReducer,
//     addNewCarPage: addNewCarReducer,
//     carInfoPage: carInfoReducer,
//     carBrandsApi: carBrandsApiReducer,
//     teamSaleApi: teamSaleApiReducer,
//     transferCustomerPage: transferCustomerReducer,
//     userInfoPage: userInfoReducer,
//     userContext: userContextReducer
// });

// export default reducer;
