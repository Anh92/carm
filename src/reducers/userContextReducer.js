import { UserContextActionType } from '../action_type'

const userContextReducer = (state = UserContextActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case UserContextActionType.UPDATE_USER_CONTEXT:
            return action.data;
        default:
            return state;
    }
};

export default userContextReducer;