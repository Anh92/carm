import { LoginActionType } from '../action_type'

const loginReducer = (state = LoginActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case LoginActionType.LOGIN_SUCCESS:
            return {
                ...state,
                error: false
            };
        case LoginActionType.LOGIN_FAIL:
            return {
                ...state,
                error: true
            };
        default:
            return state;
    }
};

export default loginReducer;