import {RegisterDeviceActionType} from '../action_type'

const registerDeviceReducer = (state = RegisterDeviceActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case RegisterDeviceActionType.REGISTER_SUCCESS:
            return {
                error: false
            };
        case RegisterDeviceActionType.REGISTER_FAIL:
            return {
                error: true
            };
        default:
            return state;
    }
};

export default registerDeviceReducer;