const DomainAlpha = 'http://112.213.94.195:8011';

const Domain = DomainAlpha;
export default Object.freeze({
  register_device: Domain + "/api/account/register-device",
  login_api: Domain + "/api/authenticate",
  customer_list_api: Domain + "/api/customer/list",
  add_customer_api: Domain + "/api/customer/create",
  edit_customer_api: Domain + "/api/customer/update",
  customer_car_list_api: Domain + '/api/customer/car/list',
  province_district_api: Domain + '/api/customer/province-district',
  enum_type_api: Domain + '/api/customer/enum-type',
  add_new_car_api: Domain + '/api/customer/car/create',
  edit_car_api: Domain + '/api/customer/car/update',
  sales_support_api: Domain + '/api/customer/sales-support',
  car_brands_api: Domain + '/api/dealer/carbrands',
  car_company_api: Domain + '/api/customer/company',
  car_broker_api: Domain + '/api/customer/broker',
  car_person_contact_api: Domain + '/api/customer/person-contact',
  close_car_api: Domain + '/api/customer/car/closed',
  team_sale_api: Domain + '/api/customer/team-sales',
  transference_staff_api: Domain + '/api/customer/transference-staff',
  logout_api: Domain + '/api/authenticate',
  find_phone_exists_api: Domain + '/api/customer/find-exists'
});