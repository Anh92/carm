import CONST from "../../global/const"

export default Object.freeze({
  get: function (header, url, body, callback) {
    if (body != null) {
      var esc = encodeURIComponent;
      var query = Object.keys(body)
        .map(k => esc(k) + '=' + esc(body[k]))
        .join('&');
      url += "?" + query;
    }
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        ...header
      },
      body: null
    })
      .then((response) => response.json())
      .then((responseJson) => {
        callback(CONST.SUCCESS, responseJson);
      })
      .catch((error) => {
        callback(CONST.FAILURE, error);
      });
  },
  post: function (header, url, body, callback) {
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        ...header
      },
      body: JSON.stringify(body)
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson.status);
        callback(CONST.SUCCESS, responseJson);
      })
      .catch((error) => {
        callback(CONST.FAILURE, error);
      });
  },
  put: function (header, url, body, callback) {
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        ...header
      },
      body: JSON.stringify(body)
    })
      .then((response) => response.json())
      .then((responseJson) => {
        callback(CONST.SUCCESS, responseJson);
      })
      .catch((error) => {
        callback(CONST.FAILURE, error);
      });
  },
  delete: function (header, url, body, callback) {
    fetch(url, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        ...header
      },
      body: JSON.stringify(body)
    })
      .then((response) => response.json())
      .then((responseJson) => {
        callback(CONST.SUCCESS, responseJson);
      })
      .catch((error) => {
        callback(CONST.FAILURE, error);
      });
  }

});
