import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  doLogout: function (body, callback) {
    NetworkTokenUtils.delete(null, ServerPath.logout_api, body, callback);
  }
});