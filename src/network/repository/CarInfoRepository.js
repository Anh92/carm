import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  getSalesSupport: function (body, callback) {
    NetworkTokenUtils.get(null, ServerPath.sales_support_api, body, callback);
  },
  updateSalesSupport: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.sales_support_api, body, callback);
  },
  getCompany: function (body, callback) {
    NetworkTokenUtils.get(null, ServerPath.car_company_api, body, callback);
  },
  updateCompany: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.car_company_api, body, callback);
  },
  getBoker: function (body, callback) {
    NetworkTokenUtils.get(null, ServerPath.car_broker_api, body, callback);
  },
  updateBroker: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.car_broker_api, body, callback);
  },
  updateCarMaintenance: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.edit_car_api, body, callback);
  },
  getPersonContact: function (body, callback) {
    NetworkTokenUtils.get(null, ServerPath.car_person_contact_api, body, callback);
  },
  updatePersonContact: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.car_person_contact_api, body, callback);
  },
});