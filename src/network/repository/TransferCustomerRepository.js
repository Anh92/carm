import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  doTransferCustomer: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.transference_staff_api, body, callback);
  }
});
