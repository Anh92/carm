import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  editCar: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.edit_car_api, body, callback);
  }
});