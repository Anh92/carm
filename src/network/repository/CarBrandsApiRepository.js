import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  getCarBrands: function (body, callback) {
    NetworkTokenUtils.get(null, ServerPath.car_brands_api, body, callback);
  }
});