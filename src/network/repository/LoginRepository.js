import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'
import Const from '../../global/const'
import Utils from '../../global/utils'

export default Object.freeze({
  doLogin: function (body, callback) {
    Utils.getValueByKey(Const.DEVICE_TOKEN_STORE_ID).then((result) => {
        NetworkUtils.post({
          "Device-Token": result
        }, ServerPath.login_api, body, callback);
    });
  }
});