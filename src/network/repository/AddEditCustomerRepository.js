import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  addCustomer: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.add_customer_api, body, callback);
  },
  editCustomer: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.edit_customer_api, body, callback);
  },
  checkPhoneExist: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.find_phone_exists_api, body, callback);
  }
});