import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  doGetEnumType: function (body, callback) {
    NetworkTokenUtils.get(null, ServerPath.enum_type_api, body, callback);
  }
});