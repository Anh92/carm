import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  doGetTeamSale: function (body, callback) {
    console.log(ServerPath.team_sale_api)
    NetworkTokenUtils.get(null, 'http://192.168.1.173:8080/api/customer/team-sales', body, callback);
  }
});