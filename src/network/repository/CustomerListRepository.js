import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  doGeCustomerList: function (body, callback) {
    NetworkTokenUtils.get(null, ServerPath.customer_list_api, body, callback);
  }
});