import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  addNewCar: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.add_new_car_api, body, callback);
  }
});