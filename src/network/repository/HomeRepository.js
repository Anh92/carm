import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'

export default Object.freeze({
  doRegisterDevice : function(body,callback){
    NetworkUtils.post(null,ServerPath.register_device,body,callback);
  }
});