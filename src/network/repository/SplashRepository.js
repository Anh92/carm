import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'

export default Object.freeze({
  doRefresh: function (header, callback) {
    NetworkUtils.put(header, ServerPath.login_api, null, callback);
  }
});