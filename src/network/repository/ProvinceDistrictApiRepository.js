import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  doGetProvince: function (body, callback) {
    NetworkTokenUtils.get(null, ServerPath.province_district_api, body, callback);
  }
});