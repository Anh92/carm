import ServerPath from '../net/ServerPath'
import NetworkTokenUtils from '../net/NetworkTokenUtils'

export default Object.freeze({
  doGetCarList: function (body, callback) {
    NetworkTokenUtils.get(null, ServerPath.customer_car_list_api, body, callback);
  },
  doCloseCar: function (body, callback) {
    NetworkTokenUtils.post(null, ServerPath.close_car_api, body, callback);
  }
});