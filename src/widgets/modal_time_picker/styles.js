import React, {
    StyleSheet
} from 'react-native';
import Colors from '../../global/colors'

module.exports = StyleSheet.create({
    picker_text_style: {
        fontSize: 12,
        color: Colors.gray,
        fontFamily: "Roboto-Regular"
    },
    input_row: {
        flex: 1,
        height: 60,
        borderBottomColor: Colors.underline,
        borderBottomWidth: 0.5
    },
    text_title: {
        color: Colors.dark,
        fontSize: 14,
        fontFamily: "Roboto-Regular",
        marginTop: 10
    },
    text_select: {
        color: Colors.gray,
        fontSize: 12,
        fontFamily: "Roboto-Regular",
        marginTop: 8
    },
    right_button: {
        position: 'absolute',
        height: 60,
        width: 30,
        right: 0,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    icon_right: {
        width: 25,
        height: 25,
        marginRight: 8,
        tintColor: Colors.gray
    },
    picker_box: {
        backgroundColor: Colors.smoke,
        borderWidth: 1,
        borderColor: Colors.border,
        position: 'absolute',
        borderRadius: 3
    },
    require_character: {
        color: 'red',
        marginTop: 10
    },
    time_picker_box: {
        flexDirection: 'row'
    },
    time_picker: {
        flex: 1,
        height: 200
    },
    time_picker_title: {
        color: Colors.dark,
        fontSize: 14,
        fontFamily: "Roboto-Regular",
        alignSelf: 'center',
        marginTop: 5
    },
});
