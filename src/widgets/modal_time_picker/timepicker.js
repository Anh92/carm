
import React, { Component } from 'react';
import {
    Platform,
    Picker,
    View,
    Text
} from 'react-native';

import Colors from '../../global/colors'

var styles = require('./styles');
const PropTypes = require('prop-types');
const fullMinutesArray = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"];
const fullHoursArray = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"];
export default class MTimePicker extends Component {
    static propTypes = {
        onTimeChange: PropTypes.any,
        selectedValue: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            hour: fullHoursArray[0],
            minute: fullMinutesArray[0]
        };
    }

    render() {
        let { minTime, maxTime } = this.props;
        if (Platform.OS == 'ios') {
            return (
                <View style={styles.time_picker_box}>
                    <View style={styles.time_picker}>
                        <Text style={styles.time_picker_title}>Giờ</Text>
                        <Picker
                            selectedValue={this.state.hour}
                            onValueChange={(itemValue, itemIndex) => this.onHourChange(itemValue)}>
                            {this.renderPickerHours()}
                        </Picker>
                    </View>
                    <View style={styles.time_picker}>
                        <Text style={styles.time_picker_title}>Phút</Text>
                        <Picker
                            style={{ height: 170 }}
                            selectedValue={this.state.minute}
                            onValueChange={(itemValue, itemIndex) => this.onMuniteChange(itemValue)}>
                            {this.renderPickerMinutes()}
                        </Picker>
                    </View>
                </View>
            );
        }
        var { WheelPicker } = require('react-native-wheel-picker-android')
        return (
            <View style={styles.time_picker_box}>
                <View style={styles.time_picker}>
                    <Text style={styles.time_picker_title}>Giờ</Text>
                    <WheelPicker
                        onItemSelected={(event) => this.onHourChange(event.data)}
                        isCurved
                        isCyclic
                        itemTextColor={Colors.gray}
                        selectedItemPosition={this.props.selectedValue ? (parseInt(this.props.selectedValue.split(":")[0])) : 0}
                        data={fullHoursArray}
                        style={{ width: 85, height: 200 }} />
                </View>
                <View style={styles.time_picker}>
                    <Text style={styles.time_picker_title}>Phút</Text>
                    <WheelPicker
                        onItemSelected={(event) => this.onMuniteChange(event.data)}
                        isCurved
                        isCyclic
                        itemTextColor={Colors.gray}
                        selectedItemPosition={this.props.selectedValue ? (parseInt(this.props.selectedValue.split(":")[1])) : 0}
                        data={fullMinutesArray}
                        style={{ width: 85, height: 200 }} />
                </View>
            </View>
        );
    }

    onHourChange(itemValue) {
        this.props.onTimeChange(itemValue + ":" + this.state.minute);
        this.setState({ hour: itemValue })
    }
    onMuniteChange(itemValue) {
        this.props.onTimeChange(this.state.hour + ":" + itemValue);
        this.setState({ minute: itemValue })
    }

    renderPickerHours() {
        var views = [];
        for (var i = 0; i < fullHoursArray.length; i++) {
            views.push(<Picker.Item key={'hours' + i} label={fullHoursArray[i]} value={fullHoursArray[i]} />);
        }
        return views;
    }
    renderPickerMinutes() {
        var views = [];
        for (var i = 0; i < fullMinutesArray.length; i++) {
            views.push(<Picker.Item key={'Minutes' + i} label={fullMinutesArray[i]} value={fullMinutesArray[i]} />);
        }
        return views;
    }

    onDateTimeChange(date) {
        this.setState({
            date: date,
        });
        if (this.props.onDateChange) {
            this.props.onDateChange(date);
        }
    }

    setMinTime(date) {
        this.setState(
            { minTime: date }
        );
    }
    setMaxTime(date) {
        this.setState(
            { maxTime: date }
        );
    }
    componentDidMount() {
        var hourTimeTemp = ['00', '00'];
        if (this.props.selectedValue) {
            hourTimeTemp = this.props.selectedValue.split(":");
        }
        this.setState({
            hour: hourTimeTemp[0],
            minute: hourTimeTemp[1]
        });
    }
}
