
import React, { Component } from 'react';
import {
    TouchableWithoutFeedback,
    View,
    Text,
    Image,
    Dimensions
} from 'react-native';
import ModalDatePicker from '../../widgets/modal_date_picker';
import MTimePicker from './timepicker'
import moment from 'moment';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class ModalDateTimePicker extends Component {
    static propTypes = {
        renderHeader: PropTypes.any,
        onPickerChange: PropTypes.any,
        pickerWidthDefault: PropTypes.number,
        title: PropTypes.string,
        setVisibleModal: PropTypes.any,
        renderModal: PropTypes.any,
        isFullMode: PropTypes.bool,
        isRequire: PropTypes.bool,
        // defaultDate: PropTypes.any => still need
    };

    constructor(props) {
        super(props);
        this._header = null;
        this._picker = null;
        this.timePicker = null;
        var dateValue = this.props.defaultDate ? ('0' + this.props.defaultDate.getDate()).slice(-2) + "/" + ('0' + (this.props.defaultDate.getMonth() + 1)).slice(-2) + "/" + this.props.defaultDate.getFullYear() : '';
        this.state = {
            modalVisible: false,
            dateValue: dateValue,
            pickerTop: 0,
            pickerWidth: 200,
            pickerLeft: 0,
            date: new Date(),
            selectedTime: null,
            selectedDateTime: null,
            minDate: props.minDate ? new Date(props.minDate.getFullYear(), props.minDate.getMonth(), props.minDate.getDate()) : undefined,
            maxDate: props.maxDate ? new Date(props.maxDate.getFullYear(), props.maxDate.getMonth(), props.maxDate.getDate()) : undefined
        };
    }

    render() {
        return (
            <View>
                <TouchableWithoutFeedback>
                    <View ref={header => this._header = header}>
                        {this.props.renderHeader ? this.props.renderHeader() : this.renderHeader()}
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    renderTimePickerModal(pTop, pLeft) {
        return (
            <TouchableWithoutFeedback>
                <View style={[styles.picker_box, { top: pTop, left: pLeft, width: 170, height: 220 }]}>
                    <MTimePicker
                        selectedValue={this.state.selectedTime}
                        onTimeChange={this.onTimeChange.bind(this)} />
                </View>
            </TouchableWithoutFeedback>
        );
    }

    onDateChange(date) {
        this.setState({
            date: date
        });
    }
    onTimeChange(time) {
        var dateTemp = null;
        if (this.state.selectedDateTime) {
            var dateTempString = this.state.selectedDateTime.getFullYear() + "-" + ('0' + (this.state.selectedDateTime.getMonth() + 1)).slice(-2) + "-" + ('0' + this.state.selectedDateTime.getDate()).slice(-2) + "T" + time + ":00";
            dateTemp = moment(dateTempString, 'YYYY-MM-DD hh:mm:ss').toDate();
        }
        this.setState({
            selectedTime: time,
            selectedDateTime: dateTemp
        });
        if (this.props.onPickerChange && dateTemp) {
            this.props.onPickerChange(dateTemp);
        }
    }

    setMinDate(date) {
        if (this.state.selectedDateTime && date.getTime() > this.state.selectedDateTime.getTime()) {
            this.setState(
                {
                    selectedTime: ('0' + date.getHours()).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2),
                    selectedDateTime: date
                }
            );
            if (this.props.onPickerChange) {
                this.props.onPickerChange(date);
            }
        }
        this.setState(
            {
                minDate: new Date(date.getFullYear(), date.getMonth(), date.getDate())
            }
        );
    }
    setMaxDate(date) {
        if (this.state.selectedDateTime && date.getTime() < this.state.selectedDateTime.getTime()) {
            this.setState(
                {
                    selectedTime: ('0' + date.getHours()).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2),
                    selectedDateTime: date
                }
            );
            if (this.props.onPickerChange) {
                this.props.onPickerChange(date);
            }
        }
        this.setState(
            {
                maxDate: new Date(date.getFullYear(), date.getMonth(), date.getDate())
            }
        );
    }

    openTimePicker() {
        let dimensions = Dimensions.get('window');
        let windowHeight = dimensions.height;

        this._header.measure((a, b, width, height, px, py) => {
            var pickerTop = py + 10;
            if (pickerTop + 220 > windowHeight && 220 <= py) {
                pickerTop = py - 220;
            }

            this.props.renderModal(this.renderTimePickerModal(pickerTop, px));
            this.props.setVisibleModal(true, true);
        });
    }

    onSelectDatePicker(date) {
        var selectTimeTemp = "00:00";
        if (this.state.selectedTime) {
            selectTimeTemp = this.state.selectedTime;
        }
        var dateTempString = date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2) + "T" + selectTimeTemp + ":00";
        var dateTemp = moment(dateTempString, 'YYYY-MM-DD hh:mm:ss').toDate();
        var dateValue = ('0' + dateTemp.getDate()).slice(-2) + "/" + ('0' + (dateTemp.getMonth() + 1)).slice(-2) + "/" + dateTemp.getFullYear();

        this.setState({
            dateValue: dateValue,
            selectedTime: selectTimeTemp,
            selectedDateTime: dateTemp
        });
        if (this.props.onPickerChange) {
            this.props.onPickerChange(dateTemp);
        }
    }

    renderHeader() {
        return (
            <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1 }} ref={(mRef) => this.timePicker = mRef}>
                    <TouchableWithoutFeedback onPress={() => { this.openTimePicker() }}>
                        <View style={styles.input_row}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.text_title}>{this.props.title ? this.props.title : 'Chọn ngày'}</Text>
                                {this.props.isRequire && <Text style={styles.require_character}>*</Text>}
                            </View>
                            <Text style={styles.text_select}>{this.state.selectedTime}</Text>
                            <View style={styles.right_button}>
                                <Image source={require('../../assets/images/clock.png')} resizeMode={'contain'} style={styles.icon_right} />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={{ flex: 1, marginLeft: 20 }}>
                    <ModalDatePicker
                        title={" "}
                        defaultDate={this.state.selectedDateTime}
                        setVisibleModal={this.props.setVisibleModal}
                        renderModal={this.props.renderModal}
                        minDate={this.state.minDate ? new Date(this.state.minDate) : new Date('1/1/1900')}
                        maxDate={this.state.maxDate ? new Date(this.state.maxDate) : new Date('1/1/3000')}
                        onPickerChange={this.onSelectDatePicker.bind(this)} />
                </View>
            </View>
        );
    }
}
