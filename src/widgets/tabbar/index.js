import React, { Component } from 'react';
import {
    FlatList,
    Text,
    View,
    TouchableOpacity,
    RefreshControl
} from 'react-native';
import Colors from '../../global/colors'

var styles = require('./styles');
const PropTypes = require('prop-types');
var widthSources = [];
var tabbarWidth = 0;

export default class Tabbar extends Component {
    static propTypes = {
        data: PropTypes.array,
        onChange: PropTypes.any,
        fixFlex: PropTypes.bool,
        defaultKey: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.list = null;
        this.state = {
            tabSelected: props.defaultKey ? props.defaultKey : props.data[0].key,
            refreshing: false,
            data: this.props.data,

        };
    }

    render() {
        if (this.props.fixFlex) {
            return (
                <View style={styles.tabbar}>
                    {this.renderFixFlexRows()}
                </View>
            );
        } else {
            return (
                <View style={styles.tabbar} onLayout={(event) => { tabbarWidth = event.nativeEvent.layout.width; }}>
                    <FlatList
                        refreshControl={
                            <RefreshControl
                                tintColor="transparent"
                                refreshing={false} />}
                        data={this.state.data}
                        horizontal={true}
                        ref={(ref) => { this.list = ref; }}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item, index }) => this.renderRows(item, index)}
                    /></View>
            );
        }
    }
    _onRefresh() {
        // this.setState({ refreshing: true });
        // fetchData().then(() => {
        //     this.setState({ refreshing: false });
        // });
    }
    renderRows(item, index) {
        return (
            <TouchableOpacity onLayout={(event) => { widthSources[index] = event.nativeEvent.layout.width; }} onPress={() => { this.onTabSelect(item, index) }} style={[styles.tabbar_btn, { borderBottomColor: this.state.tabSelected == item.key ? Colors.dark_blue : 'transparent' }]}>
                <Text style={[styles.tabbar_text, { color: this.state.tabSelected == item.key ? Colors.dark_pink : Colors.smoke }]}>{item.text}</Text>
            </TouchableOpacity>
        );
    }
    renderFixFlexRow(item, index) {
        return (
            <TouchableOpacity key={'flex-row' + index} onPress={() => { this.onTabSelect(item, index) }} style={[styles.tabbar_btn_fix, { borderBottomColor: this.state.tabSelected == item.key ? Colors.dark_blue : 'transparent' }]}>
                <Text style={[styles.tabbar_text, { color: this.state.tabSelected == item.key ? Colors.dark_pink : Colors.smoke }]}>{item.text}</Text>
            </TouchableOpacity>
        );
    }
    renderFixFlexRows() {
        var views = [];
        for (var i = 0; i < this.props.data.length; i++) {
            views.push(this.renderFixFlexRow(this.props.data[i], i));
        }
        return views;
    }
    onTabSelect(item, index) {
        if (this.state.tabSelected != item.key && item.text != '') {
            this.setState({
                tabSelected: item.key
            });
            if (this.props.onChange)
                this.props.onChange(item);

        }
        if (!this.props.fixFlex) {
            if (index < widthSources.length) {
                var restWidth = 0;

                for (var i = index; i < widthSources.length; i++) {
                    restWidth = restWidth + widthSources[i];
                }
                if (restWidth > tabbarWidth) {
                    this.list.scrollToIndex({ index: index });
                } else {
                    var frontWidth = 0;
                    for (var i = 0; i <= index; i++) {
                        frontWidth = frontWidth + widthSources[i];
                    }
                    if (frontWidth > tabbarWidth) {
                        this.list.scrollToEnd();
                    }
                }
            }
            // this.list.scrollToIndex({ index: index });//viewPosition: index, viewOffset: index
        }
    }
    componentDidMount() {
        if (!this.props.fixFlex) {
            var dataTemp = this.state.data;
            // if (dataTemp[dataTemp.length - 1].key == 1000) {
            //     dataTemp.pop();
            // } else {
            //     dataTemp.push({ key: 1000, text: '' });
            // }
            this.setState({
                data: []
            }, () => {
                this.setState({
                    data: dataTemp
                });
            });
        }
    }
}
