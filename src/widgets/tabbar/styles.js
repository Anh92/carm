import React, {
    StyleSheet
} from 'react-native';
import Colors from '../../global/colors'

module.exports = StyleSheet.create({
    tabbar: {
        width: '100%',
        height: 40,
        backgroundColor: Colors.ink,
        flexDirection: 'row'
    },
    tabbar_btn: {
        height: 40,
        borderBottomWidth: 4,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 8,
        paddingRight: 8
    },
    tabbar_btn_fix:{
        height: 40,
        borderBottomWidth: 4,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 8,
        paddingRight: 8,
        flex:1
    },
    tabbar_text: {
        fontSize: 12,
        color: Colors.smoke,
        fontFamily: "Roboto-Bold",
        marginTop: 5
    }
});
