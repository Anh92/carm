import React, { Component } from "react";
import {
  View,
  Text
} from "react-native";

import Color from '../../global/colors'

const WeekDay = {
  week: ["T.2", "T.3", "T.4", "T.5", "T.6", "T.7", "CN"]
};
export default class CalendarHead extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let nodes = WeekDay.week.map((text, index) => {
      return (
        <View key={index} style={styles.day}>
          <Text style={styles.weekday}>{text}</Text>
        </View>
      );
    });
    return (
      <View>
        <View style={[{flexDirection: "row"}, this.props.style]}>{nodes}</View>
      </View>
    );
  }
}

const styles = {
  day: {
    flex: 1,
    height: 30,
    borderBottomWidth: 0.5,
    borderBottomColor: Color.border,
    justifyContent: "center",
    alignItems: "center"
  },
  weekday: {
    color: Color.dark,
    fontSize: 20,
    fontFamily: "Roboto-Regular"
  }
};
