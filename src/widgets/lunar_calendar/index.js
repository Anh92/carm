import React, { Component } from "react";
import {
  View
} from "react-native";

import CalendarHeader from "./calendarHeader";
import CalendarHead from "./calendarHead";
import CalendarBody from "./calendarBody";
import Color from '../../global/colors';

const PropTypes = require('prop-types');

export default class Calendar extends Component {
  static propTypes = {
    workSchedule: PropTypes.array,
    minDate: PropTypes.object,
    maxDate: PropTypes.object,
    onLayout: PropTypes.any,
    backgroundColor: PropTypes.string,
    defaultDate: PropTypes.any
  };

  constructor(props) {
    super(props);
    let cur = props.defaultDate ? props.defaultDate : new Date();
    this.state = {
      date: cur,
      current: props.defaultDate,
      date_box_size: 0
    };
  }

  onNavChange = (date) => {
    this.setState({
      date: date
    });
    this.props.onMonthSelect(date)
  };

  onSelectedChange = (date) => {
    this.setState({
      current: date
    });
    this.props.onDateSelect(date);
  };

  render() {
    let date = this.state.date;
    let current = this.state.current;
    return (
      <View style={[{ backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : Color.white }, this.props.style]} onLayout={this.onLayout.bind(this)}>
        <CalendarHeader
          style={this.props.headerStyle}
          date={date}
          onNavChange={this.onNavChange} />
        <CalendarHead style={this.props.weekHeadStyle} />
        <CalendarBody
          ref="body"
          dateStyle={this.props.dateStyle}
          selectDateStyle={this.props.selectDateStyle}
          weekendStyle={this.props.weekendStyle}
          current={current}
          date={date}
          minDate={this.props.minDate}
          maxDate={this.props.maxDate}
          workSchedule={this.props.workSchedule}
          onSelectedChange={this.onSelectedChange} />
      </View>
    );
  }
  onLayout(event) {
    this.refs.body.setState({ date_box_size: event.nativeEvent.layout.width / 7 });
  }
}
