import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Image
} from "react-native";

import Color from '../../global/colors'
import MonthPicker from '../../widgets/month_picker'

export default class CalendarHeader extends Component {
  constructor(props) {
    super(props);
    this.date = this.props.date;
    this.picker_month = null;
    this.state = {
      year: this.date.getFullYear(),
      month: this.date.getMonth() + 1,
      day: this.date.getDay()
    };
  }

  onClickLeft() {
    let _year = this.state.year;
    let _month = this.state.month;
    _month--;
    // this.setState({ month: _month });
    if (_month === 0) {
      _month = 12;
      _year--;
    }
    if (_year > 1908) {
      this.setState({
        month: _month,
        year: _year,
      });
      this.picker_month.setDefault({ year: _year, month: _month });
      this.props.onNavChange(new Date(_year, _month - 1));
    }
  };

  onClickRight() {
    let _year = this.state.year;
    let _month = this.state.month;
    _month++;
    // this.setState({ month: _month });
    if (_month === 13) {
      _month = 1;
      _year++;
    }
    if (_year < 3001) {
      this.setState({
        month: _month,
        year: _year,
      });
      this.picker_month.setDefault({ year: _year, month: _month });
      this.props.onNavChange(new Date(_year, _month - 1));
    }
  };

  onPickerMonthSelect(item) {
    this.setState(item);
    this.props.onNavChange(new Date(item.year, item.month - 1));
  }

  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.title}>
          <TouchableOpacity onPress={this.onClickLeft.bind(this)} style={styles.center}>
            <Image source={require('../../assets/images/arrow_left.png')} style={styles.arrow_icon} />
          </TouchableOpacity>
          <View style={styles.date}>
            <MonthPicker
              ref={(mRef) => this.picker_month = mRef}
              renderModal={this.props.renderModal}
              onSelect={this.onPickerMonthSelect.bind(this)}
              defaultMonth={{ month: this.state.month, year: this.state.year }} />
            {/* <Text style={{ fontSize: 20, }}>{('0' + this.state.month).slice(-2) + "/" + this.state.year}</Text> */}
          </View>
          <TouchableOpacity onPress={this.onClickRight.bind(this)} style={styles.center}>
            <Image source={require('../../assets/images/arrow_right.png')} style={styles.arrow_icon} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  center: {
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  date: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10
  },
  arrow_icon: {
    height: 40,
    width: 40,
    tintColor: Color.dark_blue
  }
});
