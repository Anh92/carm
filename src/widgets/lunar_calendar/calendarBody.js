import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from "react-native";

import Utils from '../../global/utils';
import Color from '../../global/colors';
const PropTypes = require('prop-types');

export default class CalendarBody extends Component {
  static propTypes = {
    workSchedule: PropTypes.array,
    minDate: PropTypes.object,
    maxDate: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      date_box_size: 0
    }
  }

  getFirstDay = (year, month) => {
    let firstDay = new Date(year, month - 1, 1);
    return firstDay.getDay() > 0 ? firstDay.getDay() - 1 : 6;
  };

  getMonthLen = (year, month) => {
    let nextMonth = new Date(year, month, 1);
    nextMonth.setHours(nextMonth.getHours() - 3);
    return nextMonth.getDate();
  };

  getCalendarTable = (year, month) => {
    //let self = this;
    let monthLen = this.getMonthLen(year, month);
    let firstDay = this.getFirstDay(year, month);
    let list = [[]];
    let i, cur, row, col;
    for (i = firstDay; i--;) {
      list[0].push('');
    }
    for (i = 1; i <= monthLen; i++) {
      cur = i + firstDay - 1;
      row = Math.floor(cur / 7);
      col = cur % 7;
      list[row] = list[row] || [];
      list[row].push(i);
    }
    let lastRow = list[row];
    let remain = 7 - list[row].length;
    for (i = 7 - lastRow.length; i--;) {
      lastRow.push('');
    }
    return list;
  };

  onClickCallback = (year, month, day, selectAction) => {
    this.props.onSelectedChange(new Date(year, month - 1, day), selectAction);
  };

  render() {
    //let self = this;
    let today = new Date();
    let minDate = this.props.minDate;
    let maxDate = this.props.maxDate;
    let date = this.props.date;
    let year = date.getFullYear();
    let month = date.getMonth() + 1;

    let curDate = this.props.current;
    let curYear = curDate ? curDate.getFullYear() : null;
    let curMonth = curDate ? curDate.getMonth() + 1 : null;
    let curDay = curDate ? curDate.getDate() : null;
    let todayYear = today.getFullYear();
    let todayMonth = today.getMonth() + 1;
    let todayDay = today.getDate();


    let table = this.getCalendarTable(year, month);
    let rows = table.map((row, rowId) => {
      let days = row.map((day, index) => {
        let isCur = (year === curYear) && (month === curMonth) && (day === curDay);
        let isToday = (year === todayYear) && (month === todayMonth) && (day === todayDay);
        let haveTask = this.checkWorkSchedule(day, month, year);
        let canChoose = true;
        let mDayWithoutTime = new Date(year, month - 1, day);
        if (minDate && minDate.getTime() > mDayWithoutTime.getTime()) {
          canChoose = false;
        }
        if (canChoose && maxDate && maxDate.getTime() < mDayWithoutTime.getTime()) {
          canChoose = false;
        }
        let lunarDate;
        let lunarDateView;
        let pressCb = ((isCur || !canChoose) ? () => {
        } : () => {
          if (day != null && day != '')
            this.onClickCallback(year, month, day);
        });
        let className = [styles.center, { width: this.state.date_box_size, height: this.state.date_box_size }];

        if (isCur) className.push({
          backgroundColor: Color.dark_blue,
          borderRadius: this.state.date_box_size / 2
        });

        if (day) {
          lunarDate = Utils.convertSolar2Lunar(day, month, year);
          lunarDateView = (
            <View>
              <Text style={[styles.lunar, isToday && { color: Color.dark_pink }, isCur && { color: Color.white }, !canChoose && { color: Color.light_gray }]}>
                {(lunarDate.day == 1 || day == 1) ? lunarDate.day + "/" + lunarDate.month : lunarDate.day + ""}
              </Text>
            </View>
          );
        }
        return (
          <TouchableOpacity
            key={index}
            style={[className]}
            onPress={pressCb}>
            <Text style={[styles.day_text, isToday && { color: Color.dark_pink }, isCur && { color: Color.white }, !canChoose && { color: Color.light_gray }]}>{day + ""}</Text>
            {lunarDateView}
            {haveTask && <View style={[styles.have_task, isCur && { backgroundColor: Color.dark_pink }]}></View>}
          </TouchableOpacity>
        );
      });
      return (
        <View
          key={rowId}
          style={[styles.row]}>
          <View style={styles.row}>{days}</View>
        </View>
      );
    });
    return (
      <View style={[styles.rows]}>
        <View>{rows}</View>
      </View>
    );
  }

  checkWorkSchedule(day, month, year) {
    let workSchedule = this.props.workSchedule;
    if (workSchedule != undefined && workSchedule != null && workSchedule.length > 0) {
      for (var i = 0; i < workSchedule.length; i++) {
        var dateTemp = new Date(workSchedule[i].date)
        if (dateTemp.getDate() == day && dateTemp.getMonth() + 1 == month && dateTemp.getFullYear() == year) {
          return true;
        }
      }
    }
    return false;
  }
}

const styles = StyleSheet.create({
  text_center: {
    justifyContent: "center",
    alignItems: "center"
  },
  row: {
    flexDirection: "row"
  },
  have_task: {
    backgroundColor: Color.dark_blue,
    position: 'absolute',
    top: "20%",
    right: "20%",
    height: 8,
    width: 8,
    borderRadius: 4
  },
  center: {
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center"
  },
  lunar: {
    color: Color.gray,
    fontSize: 10,
    fontFamily: "Roboto-Regular"
  },
  day_text: {
    color: Color.dark,
    fontSize: 18,
    fontFamily: "Roboto-Regular"
  },
});
