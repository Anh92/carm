
import React, { Component } from 'react';
import {
    Animated,
    ScrollView,
    StyleSheet,
    View,
    RefreshControl,
    ActivityIndicator
} from 'react-native';
import Const from '../../global/const'
const PropTypes = require('prop-types');
import Colors from '../../global/colors';

export default class ScrollableHeader extends Component {
    static propTypes = {
        maxHeight: PropTypes.number,
        minHeight: PropTypes.number,
        renderHeader: PropTypes.any,
        renderContent: PropTypes.any,
        isLoading: PropTypes.any
    };
    constructor(props) {
        super(props);
        this.stickyScroll = null;
        this.state = {
            scrollY: new Animated.Value(0)
        };
    }
    render() {
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, this.props.maxHeight - this.props.minHeight],
            outputRange: [this.props.maxHeight, this.props.minHeight],
            extrapolate: 'clamp',
        });
        return (
            <View style={styles.fill}>
                <ScrollView
                    ref={(mRef) => this.stickyScroll = mRef}
                    style={styles.fill}
                    scrollEventThrottle={16}
                    refreshControl={
                        <RefreshControl
                            refreshing={false}
                            tintColor="transparent"
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
                    )}>

                    <View style={{ marginTop: this.props.maxHeight, minHeight: Const.HEIGHT_SCREEN }}>
                        {this.props.renderContent()}
                    </View>
                </ScrollView>
                <Animated.View style={[styles.header, { height: headerHeight }]}>
                    {this.props.renderHeader()}
                </Animated.View>
                {(this.props.isLoading ? this.props.isLoading() : false) &&
                    <View style={{ position: 'absolute', top: this.props.maxHeight + 20, width: '100%' }}>
                        <ActivityIndicator size="large"
                            color={Colors.gray} />
                    </View>}
            </View>
        );
    }
    // { height: headerHeight }
    onRefresh() {
        if (this.props.onRefresh) {
            this.props.onRefresh();
        }
    }
    scrollTo(data) {
        this.stickyScroll.scrollTo({ x: data.x, y: data.y, animated: data.animated })
    }
}


const styles = StyleSheet.create({
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        overflow: 'hidden',
    },
    fill: {
        flex: 1,
    }
});