import React, {
    StyleSheet
} from 'react-native';
import Colors from '../../global/colors'

module.exports = StyleSheet.create({
    input_row: {
        flex: 1,
        height: 60,
        borderBottomColor: Colors.underline,
        borderBottomWidth: 0.5
    },
    text_title: {
        color: Colors.dark,
        fontSize: 14,
        fontFamily: "Roboto-Regular",
        marginTop: 10
    },
    require_character: {
        color: 'red',
        marginTop: 10
    },
    text_input: {
        color: Colors.gray,
        fontSize: 12,
        fontFamily: "Roboto-Regular",
        height: 40,
        paddingVertical: 0,
        paddingHorizontal: 0,
        marginTop: -5
    },
    text_select: {
        fontSize: 12,
        color: Colors.gray,
        fontFamily: "Roboto-Regular",
        marginTop: 5
      }
});
