import React, { Component } from 'react';
import {
    Text,
    View,
    TextInput
} from 'react-native';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class InputRow extends Component {
    static propTypes = {
        mStyle: PropTypes.number,
        title: PropTypes.string,
        onChangeText: PropTypes.any,
        marginTop: PropTypes.number,
        defaultValue: PropTypes.any,
        onFocus: PropTypes.any,
        onEndEditing: PropTypes.any,
        requireChar: PropTypes.bool,
        isNumber: PropTypes.bool,
        dataBack: PropTypes.any,
        keyboardType: PropTypes.string
    };

    constructor(props) {
        super(props);
        this._ref_input = null;
        this.state = {
            value: (props.defaultValue != null && props.defaultValue != undefined) ? (props.defaultValue + "") : ""
        };
    }

    render() {
        var input = null;
        switch (this.props.mStyle) {
            case 0:
                input = <Text style={styles.text_select}>{this.state.value}</Text>
                break;
            default:
                input = <TextInput
                    ref={(mRef) => this._ref_input = mRef}
                    underlineColorAndroid={'transparent'}
                    style={styles.text_input}
                    defaultValue={this.state.value}
                    value={this.state.value}
                    keyboardType={this.props.keyboardType}
                    onFocus={() => { this.props.onFocus(this._ref_input) }}
                    onChangeText={(text) => this.onChangeText(text)}
                    onEndEditing={() => { this.props.onEndEditing(this.props.dataBack) }} />
                break;
        }
        return (
            <View style={styles.input_row}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text_title}>{this.props.title}</Text>
                    {this.props.requireChar && <Text style={styles.require_character}>*</Text>}
                </View>
                {input}
            </View>
        );
    }
    onChangeText(text) {
        let newText = '';
        if (this.props.isNumber) {
            let numbers = '0123456789';
            for (var i = 0; i < text.length; i++) {
                if (numbers.indexOf(text[i]) > -1) {
                    newText = newText + text[i];
                }
            }
        } else {
            newText = text;
        }
        this.setState({
            value: newText
        });
        if (this.props.onChangeText) {
            this.props.onChangeText(newText, this.props.dataBack);
        }
    }

}
