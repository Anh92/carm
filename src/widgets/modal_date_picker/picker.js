import React, { Component } from 'react';
import {
    TouchableWithoutFeedback,
    Modal,
    View,
    Text,
    Image,
    Dimensions
} from 'react-native';
import Consts from '../../global/const'
import Colors from '../../global/colors'
import LunarCalendar from '../lunar_calendar';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class ModalDatePicker extends Component {
    static propTypes = {
        renderHeader: PropTypes.any,
        onPickerChange: PropTypes.any,
        minDate: PropTypes.any,
        maxDate: PropTypes.any,
        setVisibleModal: PropTypes.any,
        isFullMode: PropTypes.bool,
        defaultDate: PropTypes.any,
        pickerHeight: PropTypes.any,
        onSelectDatePicker: PropTypes.any
    };

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => this.props.setVisibleModal(false)}>
                <View style={{ position: "absolute", right: 0, top: 0, left: 0, bottom: 0, alignContent: 'center', justifyContent: 'center' }}>
                    <TouchableWithoutFeedback>
                        <View style={[styles.picker_box, { height: this.props.pickerHeight }]}>
                            <LunarCalendar
                                minDate={this.props.minDate ? this.props.minDate : new Date('1/1/1900')}
                                maxDate={this.props.maxDate ? this.props.maxDate : new Date('1/1/3001')}
                                onDateSelect={(date) => this.props.onSelectDatePicker(date)}
                                backgroundColor={Colors.smoke}
                                defaultDate={this.props.defaultDate}
                                onMonthSelect={(mon) => console.log(mon)} />
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}
