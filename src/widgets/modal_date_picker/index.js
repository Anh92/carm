import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    TouchableOpacity,
    Modal,
    TouchableWithoutFeedback
} from 'react-native';
import Picker from './picker';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class ModalDatePicker extends Component {
    static propTypes = {
        renderHeader: PropTypes.any,
        onPickerChange: PropTypes.any,
        pickerWidthDefault: PropTypes.number,
        title: PropTypes.string,
        minDate: PropTypes.any,
        maxDate: PropTypes.any,
        isFullMode: PropTypes.bool,
        defaultDate: PropTypes.any,
        requireChar: PropTypes.bool
    };

    constructor(props) {
        super(props);
        this._header = null;
        this._picker = null;
        var dateValue = this.props.defaultDate ? ('0' + this.props.defaultDate.getDate()).slice(-2) + "/" + ('0' + (this.props.defaultDate.getMonth() + 1)).slice(-2) + "/" + this.props.defaultDate.getFullYear() : '';
        this.state = {
            modalVisible: false,
            dateValue: dateValue,
            pickerTop: 0,
            pickerWidth: 200,
            pickerLeft: 0,
            isModal: false,
            defaultDate: props.defaultDate,
            showModal: false,
            pickerHeight: 0
        };
    }

    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.openPicker() }}>
                    <View ref={header => this._header = header}>
                        {this.props.renderHeader ? this.props.renderHeader() : this.renderHeader()}
                    </View>
                </TouchableOpacity>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.showModal}>
                    <TouchableWithoutFeedback onPress={() => this.setState({ showModal: false })}>
                        <View ref="modalContent" style={{ backgroundColor: this.state.modalTransparent ? 'transparent' : 'rgba(0, 0, 0, 0.6)', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                            <Picker
                                minDate={this.props.minDate}
                                maxDate={this.props.maxDate}
                                setVisibleModal={this.setVisibleModal.bind(this)}
                                pickerHeight={this.state.pickerHeight}
                                defaultDate={this.props.defaultDate}
                                onSelectDatePicker={this.onSelectDatePicker.bind(this)} />
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        );
    }
    renderModal(gen_view) {
        this.setState({
            modalView: gen_view
        });
    }
    setVisibleModal(value) {
        this.setState({
            showModal: value
        });
    }
    renderPickerModal(pickerHeight) {
        return (
            <Picker
                minDate={this.props.minDate}
                maxDate={this.props.maxDate}
                setVisibleModal={this.setVisibleModal.bind(this)}
                pickerHeight={pickerHeight}
                defaultDate={this.props.defaultDate}
                onSelectDatePicker={this.onSelectDatePicker.bind(this)} />
        );
    }
    onSelectDatePicker(date) {
        this.setVisibleModal(false);
        var dateValue = ('0' + date.getDate()).slice(-2) + "/" + ('0' + (date.getMonth() + 1)).slice(-2) + "/" + date.getFullYear();
        this.setState({
            dateValue: dateValue,
            defaultDate: date
        });
        if (this.props.onPickerChange) {
            this.props.onPickerChange(date);
        }
    }
    openPicker() {
        let dimensions = Dimensions.get('window');
        let windowWidth = dimensions.width;
        var pickerHeight = 6 * ((windowWidth - 60) / 7) + 90;
        // this.renderModal(this.renderPickerModal(pickerHeight));
        // this.setVisibleModal(true);
        this.setState({
            showModal: true,
            pickerHeight
        });
    }

    resetDefault(defaultDate) {
        var dateValue = this.props.defaultDate ? ('0' + this.props.defaultDate.getDate()).slice(-2) + "/" + ('0' + (this.props.defaultDate.getMonth() + 1)).slice(-2) + "/" + this.props.defaultDate.getFullYear() : '';
        this.setState({
            dateValue: dateValue
        });
    }

    renderHeader() {
        return (
            <View style={styles.input_row}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text_title}>{this.props.title ? this.props.title : 'Chọn ngày'}</Text>
                    {this.props.requireChar && <Text style={styles.require_character}>*</Text>}
                </View>
                <Text style={styles.text_select}>{this.state.dateValue}</Text>
                <View style={styles.right_button}>
                    <Image source={require('../../assets/images/calendar.png')} resizeMode={'contain'} style={styles.icon_right} />
                </View>
            </View>
        );
    }
}
