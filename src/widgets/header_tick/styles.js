import React, {
    StyleSheet
} from 'react-native';
import Colors from '../../global/colors'

module.exports = StyleSheet.create({
    title_box: {
        height: 40,
        borderBottomColor: Colors.underline,
        borderBottomWidth: 0.5,
        flex: 1
    },
    title_text: {
        position: 'absolute',
        bottom: 10,
        color: Colors.dark,
        fontSize: 18,
        fontFamily: "Roboto-Light"
    },
    row_image_right: {
        width: 20,
        height: 20,
        right: 8
    },
    tick_right: {
        position: 'absolute',
        right: 0,
        bottom: 10
    }
});
