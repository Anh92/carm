import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Image
} from 'react-native';
import Switch from '../../widgets/switch';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class HeaderTick extends Component {
    static propTypes = {
        mStyle: PropTypes.number,
        title: PropTypes.string,
        onChange: PropTypes.any,
        imageActive: PropTypes.any,
        imageUnActive: PropTypes.any,
        marginTop: PropTypes.number,
        defaultValue: PropTypes.bool
    };

    constructor(props) {
        super(props);
        this.state = {
            chosen: props.defaultValue,
            imageActive: props.imageActive ? props.imageActive : require('../../assets/images/single_choice_ac.png'),
            imageUnActive: props.imageUnActive ? props.imageUnActive : require('../../assets/images/single_choice_unac.png'),
        };
    }

    render() {
        var tickRight = null;
        switch (this.props.mStyle) {
            case 0:
                tickRight = <Switch ref={'swtich_style_0'} onChangeState={(state) => { this.onChangeState(state) }} active={this.state.chosen} />
                break;
            case 1:
                break;
            default:
                tickRight = <Image source={this.state.chosen ? this.state.imageActive : this.state.imageUnActive} resizeMode={'contain'} style={styles.row_image_right} />
                break;
        }
        return (
            <TouchableOpacity style={[styles.title_box, this.props.marginTop && { marginTop: this.props.marginTop }]} onPress={this.onClick.bind(this)}>
                <Text style={styles.title_text}>{this.props.title}</Text>
                <View style={styles.tick_right}>
                    {tickRight}
                </View>
            </TouchableOpacity>);

    }
    onChangeState(state) {
        // alert("state " + state + " chosen " + this.state.chosen);
        if (this.state.chosen != state) {
            this.setState({
                chosen: state
            }, () => {
                if (this.props.onChange) {
                    this.props.onChange(this.state.chosen)
                }

            });
        }
    }
    setValue(value) {
        this.setState({
            chosen: value
        });
    }
    onClick() {
        this.setState({
            chosen: !this.state.chosen
        }, () => {
            if (this.props.mStyle == 0) {
                this.refs.swtich_style_0.toggle();
            }
            if (this.props.onChange) {
                this.props.onChange(this.state.chosen)
            }

        });
    }
}
