import React, { Component } from "react";
import { View, TextInput, StyleSheet } from "react-native";
import Underline from './Underline';
import FloatingLabel from './FloatingLabel';
const PropTypes = require('prop-types');

export default class TextField extends Component {
  static propTypes = {
    duration: PropTypes.number,
    label: PropTypes.string,
    highlightColor: PropTypes.string,
    labelColor: PropTypes.string,
    borderColor: PropTypes.string,
    textColor: PropTypes.string,
    textFocusColor: PropTypes.string,
    textBlurColor: PropTypes.string,
    onFocus: PropTypes.any,
    onBlur: PropTypes.any,
    onChangeText: PropTypes.any,
    onChange: PropTypes.any,
    value: PropTypes.string,
    inputStyle: PropTypes.object,
    wrapperStyle: PropTypes.object,
    labelStyle: PropTypes.object,
    multiline: PropTypes.bool,
    autoGrow: PropTypes.bool,
    height: PropTypes.any
  };
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      text: props.value,
      height: props.height
    };
  }
  focus() {
    this.refs.input.focus();
  }
  blur() {
    this.refs.input.blur();
  }
  isFocused() {
    return this.state.isFocused;
  }

  render() {
    let {
      label,
      highlightColor,
      duration,
      labelColor,
      borderColor,
      textColor,
      textFocusColor,
      textBlurColor,
      onFocus,
      onBlur,
      onChangeText,
      onChange,
      value,
      inputStyle,
      wrapperStyle,
      labelStyle,
      height,
      autoGrow,
      multiline,
      secureTextEntry,
      ...props
    } = this.props;
    return (
      <View style={[styles.wrapper, this.state.height ? { height: undefined } : {}, wrapperStyle, { width: '100%' }]} ref="wrapper">
        <TextInput
          secureTextEntry={secureTextEntry ? true : false}
          style={[styles.textInput, {
            color: textColor
          }, (this.state.isFocused && textFocusColor) ? {
            color: textFocusColor
          } : {}, (!this.state.isFocused && textBlurColor) ? {
            color: textBlurColor
          } : {}, inputStyle, this.state.height ? { height: this.state.height } : {}]}
          multiline={multiline}
          onFocus={() => {
            this.setState({ isFocused: true });
            this.refs.floatingLabel.floatLabel();
            this.refs.underline.expandLine();
            onFocus && onFocus();
          }}
          onBlur={() => {
            this.setState({ isFocused: false });
            !this.state.text.length && this.refs.floatingLabel.sinkLabel();
            this.refs.underline.shrinkLine();
            onBlur && onBlur();
          }}
          onChangeText={(text) => {
            this.setState({ text });
            onChangeText && onChangeText(text);
          }}
          onChange={(event) => {
            if (autoGrow) {
              this.setState({ height: event.nativeEvent.contentSize.height });
            }
            onChange && onChange(event);
          }}
          ref="input"
          value={this.state.text}
          {...props}
        />
        <Underline
          ref="underline"
          highlightColor={highlightColor}
          duration={duration}
          borderColor={borderColor}
        />
        <FloatingLabel
          isFocused={this.state.isFocused}
          ref="floatingLabel"
          focusHandler={this.focus.bind(this)}
          label={label}
          labelColor={labelColor}
          highlightColor={highlightColor}
          duration={duration}
          hasValue={(this.state.text.length) ? true : false}
          style={labelStyle}
        />
      </View>
    );
  }
  getValue() {
    return this.state.text;
  }
  setValue(value) {
    if (value != null && value != undefined && value != '') {
      this.setState({
        text: value
      });
      this.refs.floatingLabel.floatLabel();
    }

  }
}

TextField.defaultProps = {
  duration: 200,
  labelColor: '#9E9E9E',
  borderColor: '#E0E0E0',
  textColor: '#000',
  value: '',
  underlineColorAndroid: 'rgba(0,0,0,0)',
  multiline: false,
  autoGrow: false,
  height: undefined
};

const styles = StyleSheet.create({
  wrapper: {
    height: 60,
    paddingTop: 30
  },
  textInput: {
    fontSize: 14,
    height: 34,
    lineHeight: 20,
    color: '#A5A8AA',
    textAlignVertical: 'center',
    paddingVertical: 0,
    paddingHorizontal: 0
  }
});
