import React, { Component } from 'react';
import {
  TouchableWithoutFeedback,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList
} from 'react-native';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class ModalDropdownPicker extends Component {
  static propTypes = {
    options: PropTypes.array,
    setVisibleModal: PropTypes.any,
    renderModal: PropTypes.any,
    mStyle: PropTypes.number,
    title: PropTypes.string,
    requireChar: PropTypes.bool,
    onSelect: PropTypes.any,
    defaultValue: PropTypes.string,
    keyItems: PropTypes.string,
    reloadData: PropTypes.any,
    isNormalArray: PropTypes.bool,
    positionStyle: PropTypes.object,
    renderButton: PropTypes.any,
    marginTop: PropTypes.number,
    setWindowMargin: PropTypes.any,
    keysFilter: PropTypes.array,
    selectColor: PropTypes.string
  };

  constructor(props) {
    super(props);
    this._header = null;
    this._picker = null;
    this.state = {
      modalVisible: false,
      pickerTop: 0,
      pickerLeft: 0,
      buttonText: props.defaultValue,
      selectedIndex: -1,
      isModelOpen: false,
      filterKey: null
    };
  }

  render() {
    var button;
    let selectColor = this.props.selectColor;
    let selectIndex = this.state.selectedIndex;
    switch (this.props.mStyle) {
      case 1:
        button =
          (
            <View style={styles.input_row}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.text_title}>{this.props.title}</Text>
                {this.props.requireChar && <Text style={styles.require_character}>*</Text>}
              </View>
              <Text style={styles.text_select} numberOfLines={1}>{this.state.buttonText}</Text>
              <View style={styles.right_button}>
                <Image source={require('../../assets/images/arrow_down.png')} resizeMode={'contain'} style={styles.icon_right} />
              </View>
              {selectColor && <View style={[styles.selected_color, (selectIndex != -1) && { backgroundColor: this.props.options[selectIndex][selectColor] }]} />}
            </View >
          )
        break;
      case 2:
        button = this.props.renderButton(this.state.isModelOpen);
        break;
      default:
        button =
          (
            <View style={styles.button}>
              <Text style={[styles.buttonText, this.props.textStyle]}
                numberOfLines={1}>
                {this.state.buttonText}
              </Text>
              <Image source={require('../../assets/images/arrow_down.png')} resizeMode={'contain'} style={styles.arrow_green} />

            </View>
          )
        break;
    }

    //TextInput => onEndEditing={this.onEndEditing.bind(this)}
    return (
      <TouchableOpacity
        ref={button => this._button = button}
        disabled={this.props.disabled}
        accessible={this.props.accessible}
        onPress={this.openPicker.bind(this)}>
        {
          button
        }
      </TouchableOpacity>
    );
  }

  openPicker() {
    if (this.props.mStyle == 3) return;
    if (this.props.options && this.props.options.length > 0) {
      let dimensions = Dimensions.get('window');
      let windowHeight = dimensions.height;
      let dropdownHeight = 0;
      if (this.props.options.length < 5) {
        dropdownHeight = this.props.options.length * 45 + 20;
      } else {
        dropdownHeight = 5 * 45 + 20;
      }
      this._button.measure((a, b, width, height, px, py) => {
        var pickerTop = py + (this.props.marginTop || 0);

        // var pickerTop = py + (this.props.marginTop ? this.props.marginTop : (height + 5));
        if (pickerTop + dropdownHeight > windowHeight && dropdownHeight <= py) {
          pickerTop = py - dropdownHeight;
        }
        this.props.renderModal(this.renderPickerModal(pickerTop, px, width, dropdownHeight));
        this.props.setVisibleModal(true, true);
        this.setState({ isModelOpen: true });
      });
    }
    else {
      if (this.props.reloadData) {
        this.props.reloadData();
      }
    }
  }

  renderPickerModal(pTop, pLeft, pWidth, pHeight) {
    if (this.state.selectedIndex == -1) {
      this.checkDefaultSelected();
    }
    return (
      <TouchableWithoutFeedback onPress={() => { this.closeModel() }}>
        <View style={{ width: '100%', height: '100%' }}>
          <TouchableWithoutFeedback onPress={() => { }}>
            <View style={[styles.picker_box, { top: pTop, width: pWidth, height: pHeight }, this.props.positionStyle ? this.props.positionStyle : { left: pLeft }]}>
              <FlatList
                data={this.props.options}
                renderItem={({ item, index }) => this.renderPickerItems(item, index)}
                automaticallyAdjustContentInsets={false}
                keyExtractor={this.keyExtractor}
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  closeModel() {
    if (this.props.mStyle != 3 || !this.refs.ref_input_search.isFocused()) {
      this.props.setVisibleModal(false);
      this.setState({ isModelOpen: false });
      if (this.props.mStyle == 3) {
        var text = "";
        if (this.state.selectedIndex != -1) {
          var item = this.props.options[this.state.selectedIndex];
          text = this.props.isNormalArray ? item : (this.props.keyItems ? item[this.props.keyItems] : item.name);
        }
        this.setState({
          filterKey: text
        });
        if (this.props.setWindowMargin) {
          this.props.setWindowMargin(0);
        }
      }
    } else {
      if (this.refs.ref_input_search)
        this.refs.ref_input_search.blur();
    }
  }

  keyExtractor(item, index) {
    return index;
  }

  renderPickerItems(item, index) {
    let selectColor = this.props.selectColor;
    return (
      <TouchableOpacity onPress={() => {
        this.setState({ isModelOpen: false });
        this.returnAction(item, index);
      }}>
        <View style={[styles.action_row, index == this.state.selectedIndex ? { backgroundColor: '#D9D9D9' } : {}]}>
          <Text style={styles.action_text}>{this.props.isNormalArray ? item : (this.props.keyItems ? item[this.props.keyItems] : item.name)}</Text>
          {selectColor && <View style={[styles.row_point_color, { backgroundColor: item[selectColor] }]} />}
        </View>
      </TouchableOpacity>
    );
  }
  returnAction(item, index) {
    this.props.setVisibleModal(false);
    this.setState({
      buttonText: this.props.isNormalArray ? item : (this.props.keyItems ? item[this.props.keyItems] : item.name),
      selectedIndex: index,
      filterKey: this.props.isNormalArray ? item : (this.props.keyItems ? item[this.props.keyItems] : item.name)
    });
    if (this.props.onSelect) {
      this.props.onSelect(item);
    }
    // if (this.props.setWindowMargin) {
    //   this.props.setWindowMargin(0);
    // }
  }

  resetDefault(defaultText) {
    this.setState({
      buttonText: defaultText ? defaultText : "",
      selectedIndex: -1,
      filterKey: null
    });
  }
  checkDefaultSelected() {
    if (this.props.defaultValue != null && this.props.defaultValue != "" && this.props.options && this.props.options.length > 0) {
      var activeIndex = -1;
      if (this.props.isNormalArray) {
        activeIndex = this.props.options.findIndex((element) => {
          return element == this.props.defaultValue;
        })
      } else if (this.props.keyItems) {
        activeIndex = this.props.options.findIndex((element) => {
          return element[this.props.keyItems] == this.props.defaultValue;
        })
      } else {
        activeIndex = this.props.options.findIndex((element) => {
          return element.name == this.props.defaultValue;
        })
      }
      this.setState({
        selectedIndex: activeIndex
      });
    }
  }
}
