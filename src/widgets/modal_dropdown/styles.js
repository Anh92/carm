import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../global/colors'
module.exports = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    backgroundColor: '#F1F2F2',
    paddingLeft: 8
  },
  buttonText: {
    color: Colors.dark,
    fontFamily: "Roboto-Regular",
    fontSize: 14
  },
  arrow_green: {
    position: 'absolute',
    width: 15,
    height: 15,
    right: 8,
    tintColor: Colors.dark
  },
  input_row: {
    flex: 1,
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  },
  text_select: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    marginTop: 8
  },
  right_button: {
    position: 'absolute',
    height: 60,
    width: 30,
    right: 0,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  require_character: {
    color: 'red',
    marginTop: 10
  },
  icon_right: {
    width: 20,
    height: 20,
    marginRight: 8,
    tintColor: Colors.gray
  },
  picker_box: {
    backgroundColor: Colors.smoke,
    borderWidth: 1,
    borderColor: Colors.border,
    position: 'absolute',
    borderRadius: 3,
    paddingTop: 10
  },
  action_row: {
    height: 45,
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20
  },
  action_text: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Regular"
  },
  selected_color: {
    position: 'absolute',
    height: 14,
    width: 14,
    right: 40,
    borderRadius: 7,
    marginTop: 23
  },
  row_point_color: {
    position: 'absolute',
    height: 14,
    width: 14,
    right: 16,
    borderRadius: 7,
    marginTop: 23
  }
});
