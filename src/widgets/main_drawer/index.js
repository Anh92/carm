import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Image
} from 'react-native';
import Expand from 'react-native-collapsible';
import MenuData from './menu_items';
import { connect } from 'react-redux';
import Const from "../../global/const";
import { setDrawerSelectedId } from '../../actions/mainAction';
import { updateUserContext } from '../../actions/splashAction';
import Utils from '../../global/utils';
import MainRoutes from '../../views/main/routes';
const PropTypes = require('prop-types');

var styles = require('./styles');

class ControlPanel extends Component {
  static propTypes = {
    closeDrawer: PropTypes.any,
    changeChildPage: PropTypes.any
  };
  constructor(props) {
    super(props);
    this.state = {
      openids: []
    }
  }

  checkOpen(check_id) {
    if (this.state.openids.indexOf(check_id) != -1) {
      return false;
    }
    return true;
  }

  selectItem(item) {
    if (this.props.mainState.drawerSelected != item.id) {
      this.props.setDrawerSelectedId(item.id);
      if (item.parent != null && item.parent != undefined) {
        this.setState({ openids: item.parent });
      } else {
        this.setState({ openids: [] });
      }
      this.props.changeChildPage(item.route, { title: item.text });
      this.props.closeDrawer();
    }

  }
  openUserInfoPage() {
    this.props.setDrawerSelectedId(null);
    // this.setState({ openids: [] });
    this.props.changeChildPage('user_info', { title: 'Trang cá nhân' });
    this.props.closeDrawer();
  }

  setOpenId(item) {
    if (this.state.openids.indexOf(item.id) != -1) {
      if (item.parent != null && item.parent != undefined) {
        this.setState({ openids: item.parent });
      } else {
        this.setState({ openids: [] });
      }
    } else {
      var listid = [item.id];
      if (item.parent != null && item.parent != undefined) {
        listid = listid.concat(item.parent);
      }
      this.setState({ openids: listid });
    }
  }


  renTouch(item, level) {
    return (
      <TouchableOpacity key={'touch' + level + item.id} onPress={() => this.setOpenId(item)}>
        <View style={[styles.row, { marginLeft: level * 45, backgroundColor: !this.checkOpen(item.id) ? '#101E2A' : 'transparent' }]}>
          <View style={styles.left_row}>
            {item.icon != null && <View style={styles.button}>
              <Image source={item.icon} resizeMode={'contain'} style={{ width: 20, height: 20 }} />
            </View>}
            <Text style={styles.row_text}>{item.text}</Text>
          </View>
          <View style={styles.button}>
            <Image source={this.checkOpen(item.id) ? require('../../assets/images/arrow_right.png') : require('../../assets/images/arrow_down.png')} resizeMode={'contain'} style={{ width: 10, height: 10 }} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  renRowView(item, level) {
    const { drawerMenuOpen, drawerSelected } = this.props.mainState;
    return (
      <TouchableOpacity key={"row" + level + item.id} onPress={() => this.selectItem(item)}>
        <View style={[styles.row, { marginLeft: level * 45, backgroundColor: (drawerSelected == item.id && level == 0) ? '#02867C' : 'transparent' }]} >
          <View style={styles.left_row}>
            {item.icon != null && <View style={styles.button}>
              <Image source={item.icon} resizeMode={'contain'} style={{ width: 20, height: 20, tintColor: (drawerSelected == item.id) ? 'white' : '#939598' }} />
            </View>}
            <Text style={[styles.row_text, { color: (drawerSelected == item.id && level != 0) ? '#02867C' : 'white' }]}>{item.text}</Text>
          </View>
          {item.right_icon && <View style={styles.button}>
            <Image source={require('../../assets/images/arrow_right.png')} resizeMode={'contain'} style={{ width: 10, height: 10 }} />
          </View>}
        </View>
      </TouchableOpacity>
    );
  }

  renMenu = (level, menu_data, parent_id) => {
    var rows = [];
    for (var i in menu_data) {
      if (menu_data[i].child) {
        rows.push(this.renTouch(menu_data[i], level));
        rows.push(this.renMenu(level + 1, menu_data[i].child, menu_data[i].id));
      } else {
        rows.push(this.renRowView(menu_data[i], level));
      }
    }
    if (parent_id) {
      return (
        <Expand key={"expand" + level + parent_id} collapsed={this.checkOpen(parent_id)}>
          {rows}
        </Expand>
      );
    } else {
      return rows;
    }

  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.user_info} onPress={() => { this.openUserInfoPage() }}>
          <View style={styles.avatar_box}>
            <Image source={{ uri: 'https://pbs.twimg.com/profile_images/831993825635745796/HnVmB0-k.jpg' }} resizeMode={'cover'} style={{ width: 90, height: 90, borderRadius: 45 }} />
          </View>
          <View style={{ justifyContent: 'center' }}>
            <Text style={styles.text_name}>Nguyễn Văn A</Text>
            <Text style={styles.text_pos}>nguyenvana@gmail.com</Text>
          </View>
        </TouchableOpacity>

        <ScrollView style={{ marginTop: 20 }}>
          {this.renMenu(0, MenuData.getMenuData(this.props.userContext.permissions), null)}
        </ScrollView>
      </View>

    )
  }
  componentDidMount() {
    if (this.props.userContext.staffId == null || this.props.userContext.staffId == undefined) {
      Utils.getValueByKey(Const.ACCESS_TOKEN_STORE_ID).then((result) => {
        if (result !== '' && result !== undefined && result !== null) {
          this.props.updateUserContext(result);
        }
      });
    }
    console.log(this.props.userContext);
  }
}
function mapStateToProps(state) {
  return {
    mainState: state.mainPage,
    userContext: state.userContext
  };
}
export default connect(mapStateToProps, { setDrawerSelectedId, updateUserContext })(ControlPanel);