import React, {
  StyleSheet
} from 'react-native';

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#22313F',
  },
  user_info: {
    height: 130,
    width: '100%',
    backgroundColor: '#101E2A',
    flexDirection: 'row'
  },
  avatar_box: {
    height: 130,
    width: 130,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    width: 50
  },
  left_row: {
    height: 45,
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row'
  },
  row_text: {
    fontSize: 14,
    color: 'white',
    fontFamily: "Roboto-Bold"
  },
  text_name: {
    fontSize: 14,
    color: 'white',
    fontFamily: "Roboto-Bold"
  },
  text_pos: {
    fontSize: 12,
    color: 'white',
    fontFamily: "Roboto-Bold"
  },
  row: {
    height: 45,
    borderBottomColor: '#555B61',
    borderBottomWidth: 0.5,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
});
