import MainRoutes from '../../views/main/routes';
import Strings from '../../global/strings'
import Const from '../../global/const'
export default Object.freeze({
    getMenuData: function (permissions) {
        var menuData = [{
            id: 'home',
            text: Strings.home_page,
            icon: require('../../assets/images/home.png'),
            child: null,
            route: 'home',
            parent: null,
            
            right_icon: false
        }, {
            id: 'cus',
            text: Strings.customer_management,
            icon: require('../../assets/images/group.png'),
            right_icon: true,
            parent: null,
            child: getlistingItems(permissions)
        }, {
            id: 'mata',
            text: 'Quản lý công việc',
            icon: require('../../assets/images/folder.png'),
            right_icon: true,
            child: [{
                id: 'list_contract',
                text: "Danh sách hợp đồng",
                icon: null,
                right_icon: true,
                route: 'contract_list',
                child: null,
                parent: ['mata']
            }, {
                id: 'delivery_plan',
                text: "Lập kế hoạch giao xe",
                icon: null,
                right_icon: true,
                route: 'delivery_plan',
                child: null,
                parent: ['mata']
            }]
        }, {
            id: 'cawo',
            text: 'Lịch làm việc',
            icon: require('../../assets/images/calendar.png'),
            child: null,
            parent: null,
            route: 'calendar',
            right_icon: false
        }, {
            id: 'inap',
            text: 'Cài đặt ứng dụng',
            icon: require('../../assets/images/setting.png'),
            child: null,
            parent: null,
            route: 'home',
            right_icon: false
        }];
        // menuData.push({
        //     id: 'home',
        //     text: Strings.home_page,
        //     icon: require('../../assets/images/home.png'),
        //     child: null,
        //     route: MainRoutes.home,
        //     parent: null,
        //     right_icon: false
        // });
        // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_ACCESS) != -1) {
        //     menuData.push({
        //         id: 'cus',
        //         text: Strings.customer_management,
        //         icon: require('../../assets/images/group.png'),
        //         right_icon: true,
        //         parent: null,
        //         child: getlistingItems(permissions)
        //     });
        // }
        // menuData.push({
        //     id: 'mata',
        //     text: 'Quản lý công việc',
        //     icon: require('../../assets/images/folder.png'),
        //     right_icon: true,
        //     child: [{
        //         id: 'list_contract',
        //         text: "Danh sách hợp đồng",
        //         icon: null,
        //         right_icon: true,
        //         route: MainRoutes.contract_list,
        //         child: null,
        //         parent: ['mata']
        //     }, {
        //         id: 'delivery_plan',
        //         text: "Lập kế hoạch giao xe",
        //         icon: null,
        //         right_icon: true,
        //         route: MainRoutes.delivery_plan,
        //         child: null,
        //         parent: ['mata']
        //     }]
        // });
        // menuData.push({
        //     id: 'cawo',
        //     text: 'Lịch làm việc',
        //     icon: require('../../assets/images/calendar.png'),
        //     child: null,
        //     parent: null,
        //     route: MainRoutes.calendar,
        //     right_icon: false
        // });

        // menuData.push({
        //     id: 'inap',
        //     text: 'Cài đặt ứng dụng',
        //     icon: require('../../assets/images/setting.png'),
        //     child: null,
        //     parent: null,
        //     route: MainRoutes.home,
        //     right_icon: false
        // });
        return menuData;
    }
});

function getlistingItems(permissions) {
    var listItems = [{
        id: 'lscus',
        text: Strings.customer_list,
        icon: null,
        right_icon: true,
        route: 'customers_list',
        child: null,
        parent: ['cus'],
    }, {
        id: 'lscusfu',
        text: Strings.potential_customer_list,
        icon: null,
        right_icon: true,
        route: 'potential_customers',
        child: null,
        parent: ['cus'],
    }, {
        id: 'lscusfl',
        text: Strings.folow_potential_customer,
        icon: null,
        right_icon: true,
        route: 'folow_potential_customers',
        parent: ['cus'],
        child: null
    }, {
        id: 'ssicontact',
        text: Strings.ssi_contact,
        icon: null,
        right_icon: true,
        route: 'ssi_contact',
        parent: ['cus'],
        child: null
    }, {
        id: 'contact1k',
        text: Strings.mainten_contact_1k,
        icon: null,
        right_icon: true,
        route: 'mainten_contact_1k',
        parent: ['cus'],
        child: null
    }, {
        id: 'contact5k',
        text: Strings.mainten_contact_5k,
        icon: null,
        right_icon: true,
        route: 'mainten_contact_5k',
        parent: ['cus'],
        child: null
    }, {
        id: 'contactoldcar',
        text: Strings.contact_old_car,
        icon: null,
        right_icon: true,
        route: 'contact_old_car',
        parent: ['cus'],
        child: null
    }, {
        id: 'custombirth',
        text: Strings.customer_birthday,
        icon: null,
        right_icon: true,
        route: 'customer_birthday',
        parent: ['cus'],
        child: null
    }, {
        id: 'lostcuslist',
        text: Strings.lost_customer_list,
        icon: null,
        right_icon: true,
        route: 'lost_customer_list',
        parent: ['cus'],
        child: null
    }];
    // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_LISTINGALL_ACCESS) != -1) {
    //     listItems.push({
    //         id: 'lscus',
    //         text: Strings.customer_list,
    //         icon: null,
    //         right_icon: true,
    //         route: MainRoutes.customers_list,
    //         child: null,
    //         parent: ['cus'],
    //     });
    // }
    // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_NOFOLLOW_ACCESS) != -1) {
    //     listItems.push({
    //         id: 'lscusfu',
    //         text: Strings.potential_customer_list,
    //         icon: null,
    //         right_icon: true,
    //         route: MainRoutes.potential_customers,
    //         child: null,
    //         parent: ['cus'],
    //     });
    // }
    // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_LISTINGPOTENTIALFOLLOW_ACCESS) != -1) {
    //     listItems.push({
    //         id: 'lscusfl',
    //         text: Strings.folow_potential_customer,
    //         icon: null,
    //         right_icon: true,
    //         route: MainRoutes.folow_potential_customers,
    //         parent: ['cus'],
    //         child: null
    //     });
    // }
    // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_LISTINGSSI_ACCESS) != -1) {
    //     listItems.push({
    //         id: 'ssicontact',
    //         text: Strings.ssi_contact,
    //         icon: null,
    //         right_icon: true,
    //         route: MainRoutes.ssi_contact,
    //         parent: ['cus'],
    //         child: null
    //     });
    // }
    // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_LISTING1K_ACCESS) != -1) {
    //     listItems.push({
    //         id: 'contact1k',
    //         text: Strings.mainten_contact_1k,
    //         icon: null,
    //         right_icon: true,
    //         route: MainRoutes.mainten_contact_1k,
    //         parent: ['cus'],
    //         child: null
    //     });
    // }
    // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_LISTING5K_ACCESS) != -1) {
    //     listItems.push({
    //         id: 'contact5k',
    //         text: Strings.mainten_contact_5k,
    //         icon: null,
    //         right_icon: true,
    //         route: MainRoutes.mainten_contact_5k,
    //         parent: ['cus'],
    //         child: null
    //     });
    // }
    // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_LISTINGOLDCAR_ACCESS) != -1) {
    //     listItems.push({
    //         id: 'contactoldcar',
    //         text: Strings.contact_old_car,
    //         icon: null,
    //         right_icon: true,
    //         route: MainRoutes.contact_old_car,
    //         parent: ['cus'],
    //         child: null
    //     });
    // }
    // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_LISTINGBIRTHDAY_ACCESS) != -1) {
    //     listItems.push({
    //         id: 'custombirth',
    //         text: Strings.customer_birthday,
    //         icon: null,
    //         right_icon: true,
    //         route: MainRoutes.customer_birthday,
    //         parent: ['cus'],
    //         child: null
    //     });
    // }
    // if (permissions.indexOf(Const.permissions.PERM_CUSTOMERPROFILE_LISTINGALL_ACCESS) != -1) {
    //     listItems.push({
    //         id: 'lostcuslist',
    //         text: Strings.lost_customer_list,
    //         icon: null,
    //         right_icon: true,
    //         route: MainRoutes.lost_customer_list,
    //         parent: ['cus'],
    //         child: null
    //     });
    // }
    return listItems;
}