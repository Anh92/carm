import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
} from 'react-native';
import Color from '../../global/colors';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class Month extends Component {
    static propTypes = {
        renderHeader: PropTypes.any,
        setVisibleModal: PropTypes.any,
        onSelect: PropTypes.any,
        defaultMonth: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {
            defaultYear: props.defaultMonth ? props.defaultMonth.year : new Date().getFullYear(),
            year: props.defaultMonth ? props.defaultMonth.year : new Date().getFullYear(),
            month: props.defaultMonth ? props.defaultMonth.month : new Date().getMonth() + 1,
            isSelectMonth: true
        }
    }

    render() {
        if (this.state.isSelectMonth)
            return (
                <View>
                    <View style={styles.year_box}>
                        <TouchableOpacity style={styles.year_box} onPress={() => this.setState({ isSelectMonth: false })}>
                            <Text style={styles.title_text}>{this.state.defaultYear + ""}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.onPressPreYear()} style={styles.left_icon}>
                            <Image source={require('../../assets/images/arrow_left.png')} style={styles.arrow_icon} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.onPressNextYear()} style={styles.right_icon}>
                            <Image source={require('../../assets/images/arrow_right.png')} style={styles.arrow_icon} />
                        </TouchableOpacity>
                    </View>
                    {this.renderMonths()}
                </View>
            );
        let startYear = this.state.defaultYear - this.state.defaultYear % 10;
        if (startYear < 1910) {
            startYear = 1910;
        }
        if (startYear >= 3000) {
            startYear = 2990;
        }
        let endYear = startYear + 9;
        return (
            <View>
                <View style={styles.year_box}>
                    <Text style={styles.title_text}>{startYear + " - " + endYear}</Text>
                    <TouchableOpacity onPress={() => this.onPressPreTenYear()} style={styles.left_icon}>
                        <Image source={require('../../assets/images/arrow_left.png')} style={styles.arrow_icon} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onPressNextTenYear()} style={styles.right_icon}>
                        <Image source={require('../../assets/images/arrow_right.png')} style={styles.arrow_icon} />
                    </TouchableOpacity>
                </View>
                {this.renderYears()}
            </View>
        );
    }
    onPressPreYear() {
        if (this.state.defaultYear > 1909) {
            this.setState({ defaultYear: this.state.defaultYear - 1 })
        }
    }
    onPressNextYear() {
        if (this.state.defaultYear < 3000) {
            this.setState({ defaultYear: this.state.defaultYear + 1 })
        }
    }
    onPressPreTenYear() {
        if (this.state.defaultYear > 1919) {
            this.setState({ defaultYear: this.state.defaultYear - 10 })
        }
    }
    onPressNextTenYear() {
        if (this.state.defaultYear < 2990) {
            this.setState({ defaultYear: this.state.defaultYear + 10 })
        }
    }
    renderMonths() {
        var views = [];
        for (var i = 1; i < 5; i++) {
            var months = [];
            var startMonth = i * 3 - 2;
            for (var j = startMonth; j < startMonth + 3; j++) {
                months.push(
                    this.renderMonth(j)
                );
            }
            views.push(
                <View key={'month-row' + i} style={styles.month_row}>
                    {months}
                </View>);
        }
        return views;
    }
    renderMonth(j) {
        var today = new Date();
        var isToday = (today.getFullYear() == this.state.defaultYear) && (today.getMonth() + 1 == j);
        var isSeleted = (this.state.defaultYear == this.state.year) && (j == this.state.month);
        return (
            <TouchableOpacity onPress={() => this.onSelectMonth(j)} key={'month-' + j} style={[styles.month_box, isSeleted && { backgroundColor: Color.dark_blue, borderRadius: 5 }]}>
                <Text style={[styles.month_text, isToday && { color: Color.dark_pink }, isSeleted && { color: Color.white }]}>
                    {'Tháng ' + j}
                </Text>
            </TouchableOpacity>
        );
    }
    onSelectMonth(month) {
        this.props.onSelect({
            month: month,
            year: this.state.defaultYear
        });
        this.setState({
            month: month,
            year: this.state.defaultYear
        });
    }
    renderYears() {
        var views = [];
        let startYear = this.state.defaultYear - this.state.defaultYear % 10 - 1;
        if (startYear < 1909) {
            startYear = 1909;
        }
        if (startYear > 2989) {
            startYear = 2989;
        }
        let startIndex = 1;
        for (var i = 1; i < 5; i++) {
            var years = [];
            for (var j = 0; j < 3; j++) {
                years.push(
                    this.renderYear(startYear, startIndex)
                );
                startYear++;
                startIndex++;
            }
            views.push(
                <View key={'year-row' + i} style={styles.month_row}>
                    {years}
                </View>);
        }
        return views;
    }
    renderYear(year, startIndex) {
        var today = new Date();
        var isTodayYear = today.getFullYear() == year;
        var isSeleted = year == this.state.year;
        return (
            <TouchableOpacity onPress={() => this.onSelectYear(year)} key={'year-' + year} style={[styles.month_box, isSeleted && { backgroundColor: Color.dark_blue, borderRadius: 5 }]}>
                <Text style={[styles.month_text, isTodayYear && { color: Color.dark_pink }, (startIndex == 1 || startIndex == 12) && { color: Color.light_gray }, isSeleted && { color: Color.white }]}>
                    {year + ""}
                </Text>
            </TouchableOpacity>
        );
    }
    onSelectYear(year) {
        this.setState({
            // year: year,
            isSelectMonth: true,
            defaultYear: year
        });
    }
}
