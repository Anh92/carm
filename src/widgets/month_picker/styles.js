import React, {
    StyleSheet
} from 'react-native';
import Colors from '../../global/colors'

module.exports = StyleSheet.create({
    pop_up: {
        backgroundColor: 'white',
        position: 'absolute',
        right: 30,
        left: 30,
        paddingTop: 8,
        height: 220,
        paddingBottom: 8,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: 3
    },
    date: {
        justifyContent: "center",
        alignItems: "center",
        height: 40,
        width: 100
    },
    title_text: {
        color: Colors.dark,
        fontSize: 20,
        fontFamily: "Roboto-Regular"
    },
    left_icon: {
        height: 40,
        width: 100,
        justifyContent: "center",
        alignItems: "center",
        position: 'absolute',
        left: 0,
    },
    right_icon: {
        height: 40,
        width: 100,
        justifyContent: "center",
        alignItems: "center",
        position: 'absolute',
        right: 0,
    },
    arrow_icon: {
        height: 30,
        width: 30,
        tintColor: Colors.dark_blue
    },
    year_box: {
        height: 40,
        justifyContent: "center",
        alignItems: "center"
    },
    month_row: {
        height: 40,
        flexDirection: 'row',
        paddingLeft: 20,
        paddingRight: 20
    },
    month_text: {
        color: Colors.dark,
        fontSize: 14,
        fontFamily: "Roboto-Regular"
    },
    month_box: {
        height: 40,
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
});
