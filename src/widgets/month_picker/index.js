import React, { Component } from 'react';
import {
    TouchableWithoutFeedback,
    View,
    Text,
    Dimensions,
    Modal
} from 'react-native';
import Month from './month'

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class MonthPicker extends Component {
    static propTypes = {
        renderHeader: PropTypes.any,
        onSelect: PropTypes.any,
        defaultMonth: PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {
            year: props.defaultMonth ? props.defaultMonth.year : new Date().getFullYear(),
            month: props.defaultMonth ? props.defaultMonth.month : new Date().getMonth() + 1,
            title: props.defaultMonth ? (('0' + props.defaultMonth.month).slice(-2) + "/" + props.defaultMonth.year) : (('0' + (new Date().getMonth() + 1)).slice(-2) + "/" + new Date().getFullYear()),
            pickerTop: 0,
            showModal: false
        }
        this._header = null;
    }

    render() {
        return (
            <View>
                <TouchableWithoutFeedback onPress={() => { this.openActionModal() }}>
                    <View ref={header => this._header = header}>
                        {this.props.renderHeader ? this.props.renderHeader() : this.renderHeader()}
                    </View>
                </TouchableWithoutFeedback>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.showModal}>
                    <TouchableWithoutFeedback onPress={() => this.setState({ showModal: false })}>
                        <View ref="modalContent" style={{ backgroundColor: this.state.modalTransparent ? 'transparent' : 'rgba(0, 0, 0, 0.6)', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableWithoutFeedback>
                                <View style={[styles.pop_up, { top: this.state.pickerTop }]}>
                                    <Month
                                        onSelect={this.returnPickedMonth.bind(this)}
                                        defaultMonth={{
                                            month: this.state.month,
                                            year: this.state.year
                                        }} />
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        );
    }

    renderHeader() {
        return (
            <View style={styles.date}>
                <Text style={styles.title_text}>{this.state.title}</Text>
            </View>
        );
    }

    openActionModal() {
        let dimensions = Dimensions.get('window');
        let windowHeight = dimensions.height;
        this._header.measure((a, b, width, height, px, py) => {
            var pickerTop = py + 35;
            if (pickerTop + 220 > windowHeight && 220 <= py) {
                pickerTop = py - 220;
            }
            this.setState({ pickerTop, showModal: true })
        });

    }

    renderMonthPicker(pickerTop) {
        return (
            <TouchableWithoutFeedback>
                <View style={[styles.pop_up, { top: pickerTop }]}>
                    <Month
                        onSelect={this.returnPickedMonth.bind(this)}
                        defaultMonth={{
                            month: this.state.month,
                            year: this.state.year
                        }} />
                </View>
            </TouchableWithoutFeedback>);
    }

    returnPickedMonth(item) {
        this.setState({ showModal: false })
        this.setState({
            title: ('0' + item.month).slice(-2) + "/" + item.year,
            month: item.month,
            year: item.year
        });
        if (this.props.onSelect) {
            this.props.onSelect(item);
        }
    }
    setDefault(item) {
        this.setState({
            year: item.year,
            month: item.month,
            title: ('0' + item.month).slice(-2) + "/" + item.year
        });
    }
}
