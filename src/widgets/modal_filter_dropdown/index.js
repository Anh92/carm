import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';

import FilterFlatList from './filter_flatlist'

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class ModalDropdownPicker extends Component {
  static propTypes = {
    options: PropTypes.array,
    setVisibleModal: PropTypes.any,
    renderModal: PropTypes.any,
    mStyle: PropTypes.number,
    title: PropTypes.string,
    requireChar: PropTypes.bool,
    onSelect: PropTypes.any,
    defaultValue: PropTypes.string,
    keyItems: PropTypes.array,
    reloadData: PropTypes.any,
    isNormalArray: PropTypes.bool,
    linkText: PropTypes.string,
    positionStyle: PropTypes.object,
    renderButton: PropTypes.any,
    marginTop: PropTypes.number,
    setWindowMargin: PropTypes.any,
    keysFilter: PropTypes.array,
  };

  constructor(props) {
    super(props);
    this._header = null;
    this._picker = null;
    this.filterFlatList = null;
    this.state = {
      modalVisible: false,
      pickerTop: 0,
      pickerLeft: 0,
      buttonText: props.defaultValue,
      selectedIndex: -1,
      isModal: false
    };
  }

  render() {
    return (
      <TouchableOpacity
        ref={button => this._button = button}
        disabled={this.props.disabled}
        accessible={this.props.accessible}
        onPress={this.openPicker.bind(this)}>
        <View style={styles.input_row}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.text_title}>{this.props.title}</Text>
            {this.props.requireChar && <Text style={styles.require_character}>*</Text>}
          </View>
          <Text style={[styles.text_select, { marginLeft: 30 }]} numberOfLines={1}>{this.state.buttonText}</Text>
          <Image source={require('../../assets/images/search.png')} resizeMode={'contain'} style={styles.icon} />
          <TouchableOpacity style={styles.right_button} onPress={() => { this.resetDefault() }}>
            <Image source={require('../../assets/images/close_drawer.png')} resizeMode={'contain'} style={styles.icon_right} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  }

  renderPickerModal() {
    return (<FilterFlatList
      ref={(mRef) => this.filterFlatList = mRef}
      options={this.props.options}
      filterKey={this.state.buttonText}
      defaultValue={this.props.defaultValue}
      keyItems={this.props.keyItems}
      title={this.props.title}
      closeModal={this.closeModal.bind(this)}
      keysFilter={this.props.keysFilter}
      isNormalArray={this.props.isNormalArray}
      linkText={this.props.linkText}
      selectedIndex={this.state.selectedIndex}
      returnAction={this.returnAction.bind(this)} />)
  }


  openPicker() {
    if (this.props.options && this.props.options.length > 0) {
      this.props.renderModal(this.renderPickerModal());
      this.props.setVisibleModal(true);
    }
    else {
      if (this.props.reloadData) {
        this.props.reloadData();
      }
    }
  }

  closeModal() {
    this.props.setVisibleModal(false);
  }

  returnAction(item, index) {
    this.props.setVisibleModal(false);
    this.setState({
      buttonText: this.getActionText(item),
      selectedIndex: index
    });
    if (this.props.onSelect) {
      this.props.onSelect(1, item);
    }
  }
  getActionText(item) {
    if (this.props.isNormalArray)
      return item;
    if (this.props.keyItems) {
      var linkText = this.props.linkText ? this.props.linkText : " ";
      var actionText = "";
      for (var i = 0; i < this.props.keyItems.length; i++) {
        actionText += item[this.props.keyItems[i]];
        if (i < this.props.keyItems.length - 1) {
          actionText += linkText;
        }
      }
      return actionText;
    }
    return item.name;
  }
  resetDefault(defaultText) {
    this.setState({
      buttonText: defaultText ? defaultText : "",
      selectedIndex: -1,
    });
    if (this.props.onSelect) {
      this.props.onSelect(0, null);
    }
  }
}
