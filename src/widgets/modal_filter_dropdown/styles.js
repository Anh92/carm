import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../global/colors'
module.exports = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    backgroundColor: '#F1F2F2',
    paddingLeft: 8
  },
  input_row: {
    width: "100%",
    height: 60,
    borderBottomColor: Colors.underline,
    borderBottomWidth: 0.5
  },
  text_title: {
    color: Colors.dark,
    fontSize: 14,
    fontFamily: "Roboto-Regular",
    marginTop: 10
  },
  text_select: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    marginTop: 8
  },
  right_button: {
    position: 'absolute',
    height: 60,
    width: 30,
    right: 0,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  require_character: {
    color: 'red',
    marginTop: 10
  },
  icon_right: {
    width: 20,
    height: 20,
    marginRight: 8,
    tintColor: Colors.gray
  },
  icon: {
    position: 'absolute',
    width: 25,
    height: 25,
    left: 0,
    bottom: 5,
    tintColor: Colors.gray
  },
  action_row: {
    height: 45,
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20
  },
  action_text: {
    color: Colors.black,
    fontSize: 14,
    fontFamily: "Roboto-Regular"
  },
  text_input: {
    color: Colors.gray,
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    height: 40,
    paddingVertical: 0,
    paddingHorizontal: 0,
    marginTop: -5,
    marginLeft: 30
  },
  filter_box: {
    marginLeft: 10,
    marginTop: 10,
    marginRight: 10,
    backgroundColor: Colors.smoke,
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 5,
    paddingBottom: 5,
    borderWidth: 1,
    borderColor: Colors.border,
    borderRadius: 3
  },
});
