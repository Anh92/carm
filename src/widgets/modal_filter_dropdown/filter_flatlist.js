import React, { Component } from 'react';
import {
  TouchableWithoutFeedback,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput
} from 'react-native';
import Utils from '../../global/utils';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class ModalDropdownPicker extends Component {
  static propTypes = {
    options: PropTypes.array,
    filterKey: PropTypes.string,
    defaultValue: PropTypes.string,
    keyItems: PropTypes.array,
    isNormalArray: PropTypes.bool,
    selectedIndex: PropTypes.number,
    returnAction: PropTypes.any,
    keysFilter: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.state = {
      filterKey: props.filterKey
    };
  }


  render() {
    return (<View style={styles.container}>
      <TouchableWithoutFeedback onPress={() => { this.closeCustomModal() }}>
        <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, paddingBottom: 200 }}>
          <TouchableWithoutFeedback>
            <View style={styles.filter_box}>
              <View style={styles.input_row}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.text_title}>{this.props.title}</Text>
                  {this.props.requireChar && <Text style={styles.require_character}>*</Text>}
                </View>
                <TextInput
                  ref="ref_input_search"
                  underlineColorAndroid={'transparent'}
                  style={styles.text_input}
                  placeholderTextColor={'#989899'}
                  value={this.state.filterKey}
                  disableFullscreenUI={false}
                  onChangeText={(text) => { this.setState({ filterKey: text }); }} />
                <Image source={require('../../assets/images/search.png')} resizeMode={'contain'} style={styles.icon} />
              </View>
              <FlatList
                data={this.filterData()}
                renderItem={({ item, index }) => this.renderPickerItems(item, index)}
                keyExtractor={this.keyExtractor}
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>

    </View>)
  }
  keyExtractor(item, index) {
    return index;
  }
  filterData() {
    if (this.props.options && this.props.options.length > 0) {
      const data = this.props.options;
      if (this.state.filterKey == '' || this.state.filterKey == null)
        return data;
      var mKey = Utils.convertVietNameLetter(this.state.filterKey);
      return data.filter((e) => {
        if (this.props.isNormalArray) {
          var mE = Utils.convertVietNameLetter(e);
          return mE.indexOf(mKey) != -1;
        } else if (this.props.keysFilter && this.props.keysFilter.length > 0) {
          for (var i = 0; i < this.props.keysFilter.length; i++) {
            var mE = Utils.convertVietNameLetter(e[this.props.keysFilter[i]]);
            if (mE.indexOf(mKey) != -1) {
              return true
            }
          }
          return false;
        } else {
          return true;
        }
      });
    }
  }

  setFilterKey(text) {
    this.setState({
      filterKey: text
    });
  }

  closeCustomModal() {
    if (this.refs.ref_input_search.isFocused()) {
      this.refs.ref_input_search.blur();
    } else {
      this.props.closeModal()
    }
  }

  renderPickerItems(item, index) {
    return (
      <TouchableOpacity onPress={() => {
        this.props.returnAction(item, index)
      }}>
        <View style={styles.action_row}>
          <Text style={styles.action_text}>{this.getActionText(item)}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  getActionText(item) {
    if (this.props.isNormalArray)
      return item;
    if (this.props.keyItems) {
      var linkText = this.props.linkText ? this.props.linkText : " ";
      var actionText = "";
      for (var i = 0; i < this.props.keyItems.length; i++) {
        actionText += item[this.props.keyItems[i]];
        if (i < this.props.keyItems.length - 1) {
          actionText += linkText;
        }
      }
      return actionText;
    }
    return item.name;
  }

  componentDidMount() {
    // console.log(this.refs.ref_input_search);
    this.refs.ref_input_search.focus();
  }
}
