/**
*
* Header Component
*
* @author  Minh Huy
* @version 1.0
* @since   18-05-2017
*/

import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableHighlight,
  Text
} from 'react-native';
import { connect } from 'react-redux';
import ModalDropdown from '../../widgets/modal_dropdown';
import Routes from '../../views/routes';
import { changeChildPage, setDrawerSelectedId } from '../../actions/mainAction';
const PropTypes = require('prop-types');

var styles = require('./styles');

class Header extends Component {

  static propTypes = {
    menuToggle: PropTypes.any,
    closeDrawer: PropTypes.any,
    openNewPage: PropTypes.any,
    renderModal: PropTypes.any,
    setVisibleModal: PropTypes.any
  };

  constructor(props) {
    super(props);
    this.state = {
      topRightMenuItems: ['Gọi điện thoại', 'Nhắn tin']
    }
  }

  render() {
    const { drawerMenuOpen, currentPage, notifyNumber, headerText, isDrawerChanging } = this.props.mainState;
    return (
      <View style={styles.container}>
        <TouchableHighlight underlayColor={'transparent'} style={styles.button} onPress={() => { if (!isDrawerChanging) this.props.menuToggle() }}>
          <Image source={!drawerMenuOpen ? require('../../assets/images/hamburger.png') : require('../../assets/images/close_drawer.png')} resizeMode={'contain'} style={styles.icon} />
        </TouchableHighlight>

        <View style={styles.content_center}>
          <Text style={styles.title} numberOfLines={1}>{headerText}</Text>
        </View>

        <View style={styles.container_right}>
          <TouchableHighlight underlayColor={'transparent'} style={styles.button} onPress={() => {
            this.props.openNewPage('add_customer', { headerText: 'Thêm khách hàng' })
          }}>
            <Image source={require('../../assets/images/plus_white.png')} resizeMode={'contain'} style={styles.icon_qure} />
          </TouchableHighlight>
          <TouchableHighlight underlayColor={'transparent'} style={styles.button} onPress={() => { this.props.openNewPage('notify', { headerText: 'Thông báo' }) }
          }>
            <View style={styles.notify_box}>
              <Image source={require('../../assets/images/notify.png')} resizeMode={'contain'} style={styles.icon_qure} />
              <View style={styles.notify_number_box}>
                <Text style={styles.notify_number} numberOfLines={1}>{notifyNumber}</Text>
              </View>
            </View>
          </TouchableHighlight>
          <View underlayColor={'transparent'} style={styles.button} >
            <View style={styles.icon_box}>
              <ModalDropdown
                options={this.state.topRightMenuItems}
                renderButton={this.renderMenuDropDownBtn}
                isNormalArray={true}
                mStyle={2}
                renderModal={this.props.renderModal.bind(this)}
                setVisibleModal={this.props.setVisibleModal.bind(this)}
                positionStyle={{ width: 200, right: 8 }} />
            </View>
          </View>
        </View>
      </View >
    );
  }
  renderMenuDropDownBtn(showDropdown) {
    return (
      <View style={[{ backgroundColor: showDropdown ? '#00554E' : 'transparent' }, styles.icon_box]}>
        <Image source={require('../../assets/images/three_dots_white.png')} resizeMode={'contain'} style={styles.icon} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return { mainState: state.mainPage };
}

export default connect(mapStateToProps, { changeChildPage, setDrawerSelectedId })(Header);
