import React, {
  StyleSheet
} from 'react-native';
import Colors from '../../global/colors';

module.exports = StyleSheet.create({
  container: {
    height: 50,
    width: '100%',
    backgroundColor: Colors.dark_blue,
    flexDirection: 'row'
  },
  icon: {
    height: 20
  },
  title: {
    fontSize: 16,
    color: 'white',
    fontFamily: "Roboto-Regular"
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50
  },
  content_center: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50
  },
  container_right: {
    position:'absolute',
    height: 50,
    right: 0,
    backgroundColor: Colors.dark_blue,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  notify_number_box: {
    position: 'absolute',
    backgroundColor: '#E35B5A',
    width: 18, height: 18,
    top: 8,
    right: 10,
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center'
  },
  notify_box: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50
  },
  notify_number: {
    color: 'white',
    fontSize: 10,
    backgroundColor: 'transparent',
    fontFamily: "Roboto-Regular"
  },
  icon_box: {
    borderRadius: 3,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon_qure: {
    height: 30,
    width: 30
  }
});
