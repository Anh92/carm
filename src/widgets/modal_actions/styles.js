import React, {
    StyleSheet
} from 'react-native';
import Colors from '../../global/colors'

module.exports = StyleSheet.create({
    right_button: {
        height: 60,
        width: 30,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    icon_right: {
        width: 20,
        height: 20,
        marginRight: 8,
        tintColor: Colors.gray
    },
    pop_up: {
        backgroundColor: 'white',
        position: 'absolute',
        right: 30,
        left: 30,
        paddingTop: 8,
        paddingBottom: 8,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: 3
    },
    action_row: {
        height: 45,
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 20,
        marginRight: 20
    },
    action_text: {
        color: Colors.black,
        fontSize: 14,
        fontFamily: "Roboto-Regular"
    }
});
