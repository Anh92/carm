import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    FlatList,
    TouchableOpacity
} from 'react-native';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class ModalActions extends Component {
    static propTypes = {
        renderHeader: PropTypes.any,
        setVisibleModal: PropTypes.any,
        renderModal: PropTypes.any,
        onSelect: PropTypes.any,
        actionList: PropTypes.array,
        index: PropTypes.number,
        dataBack: PropTypes.any
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.openActionModal() }}>
                    <View>
                        {this.props.renderHeader ? this.props.renderHeader() : this.renderHeader()}
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    openActionModal() {
        this.props.renderModal(this.renderActionList());
        this.props.setVisibleModal(true);
    }

    renderActionList() {
        return (
            <View style={styles.pop_up}>
                <FlatList
                    data={this.props.actionList}
                    renderItem={({ item }) => this.renderActionItems(item)}
                />
            </View>);
    }

    renderActionItems(item) {
        return (
            <TouchableOpacity onPress={() => {
                this.returnAction(item)
            }}>
                <View style={styles.action_row}>
                    <Text style={styles.action_text}>{item.text}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    returnAction(item) {
        this.props.setVisibleModal(false);
        if (this.props.onSelect) {
            this.props.onSelect(item, this.props.index || this.props.dataBack);
        }
    }

    renderHeader() {
        return (
            <View style={styles.right_button}>
                <Image source={require('../../assets/images/three_dots_white.png')} resizeMode={'contain'} style={styles.icon_right} />
            </View>
        );
    }
}
