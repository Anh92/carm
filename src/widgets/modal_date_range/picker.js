import React, { Component } from 'react';
import {
    TouchableWithoutFeedback,
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import Colors from '../../global/colors'
import ModalDatePicker from '../../widgets/modal_date_picker';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class ModalDateRangePicker extends Component {
    static propTypes = {
        onDateResult: PropTypes.any,
        renderModal: PropTypes.any,
        setVisibleModal: PropTypes.any,
        fromDateValue: PropTypes.string,
        toDateValue: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this._header = null;
        this._picker = null;
        this.state = {
            fromDate: props.fromDateValue,
            toDate: props.toDateValue,
            isModal: false,
            pickerWidth: 200,
            pickerTop: 0,
            pickerLeft: 0,
            minDate: new Date('1/1/1900'),
            maxDate: new Date('1/1/3000'),
            onChange: 'fromDate',
            modalView: null
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback>
                    <View style={styles.select_date_box}>
                        <ModalDatePicker
                            title={"Đến ngày"}
                            onPickerChange={(date) => {
                                this.setState({
                                    fromDate: date
                                }); console.log(date)
                            }}
                            defaultDate={this.state.fromDate}
                            maxDate={this.state.toDate}
                            setVisibleModal={this.setDatePickerVisible.bind(this)}
                            renderModal={this.renderPickerModal.bind(this)} />
                        <ModalDatePicker
                            title={"Đến ngày"}
                            onPickerChange={(date) => {
                                this.setState({
                                    toDate: date
                                }); console.log(date)
                            }}
                            minDate={this.state.fromDate}
                            defaultDate={this.state.toDate}
                            setVisibleModal={this.setDatePickerVisible.bind(this)}
                            renderModal={this.renderPickerModal.bind(this)} />
                        <View style={styles.select_date_btn_box}>
                            <TouchableOpacity underlayColor='transparent' style={[styles.select_date_btn, { backgroundColor: Colors.smoke }]} onPress={() => { this.closePicker() }}>
                                <Text style={[styles.select_date_btn_text, { color: Colors.dark_blue }]}>Hủy</Text>
                            </TouchableOpacity>
                            <TouchableOpacity underlayColor='transparent' style={[styles.select_date_btn, { backgroundColor: Colors.dark_blue, marginLeft: 20 }]} onPress={() => { this.onSetBackDate() }}>
                                <Text style={[styles.select_date_btn_text, { color: Colors.smoke }]}>Xác nhận</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                {this.state.isModal &&
                    <TouchableWithoutFeedback onPress={() => { this.closeModal() }}>
                        <View ref="modalContent" style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                            {this.state.modalView}
                        </View>
                    </TouchableWithoutFeedback>}
            </View>
        );
    }

    setDatePickerVisible(value) {
        this.setState({
            isModal: value
        });
    }

    renderPickerModal(view) {
        this.setState({
            modalView: view
        });
    }

    closeModal() {
        this.setState({
            isModal: false
        });
    }

    onSetBackDate() {
        if (this.props.setVisibleModal) {
            this.props.setVisibleModal(false, false)
        }
        if (this.props.onDateResult) {
            this.props.onDateResult({
                fromDate: this.state.fromDate,
                toDate: this.state.toDate
            });
        }
    }

    closePicker() {
        if (this.props.setVisibleModal) {
            this.props.setVisibleModal(false, false)
        }
    }
}
