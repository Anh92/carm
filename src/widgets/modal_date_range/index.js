import React, { Component } from 'react';
import {
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import Picker from './picker'

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class ModalDateRangePicker extends Component {
    static propTypes = {
        onDateResult: PropTypes.any,
        renderModal: PropTypes.any,
        setVisibleModal: PropTypes.any,
        fromDateValue: PropTypes.string,
        toDateValue: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this._header = null;
        this._picker = null;
        this.state = {
            fromDate: props.fromDate,
            toDate: props.toDate
        };
    }

    render() {
        return (
            <TouchableOpacity style={styles.button_date_range} onPress={() => { this.openDateRangePicker() }}>
                <Text style={styles.button_date_text} numberOfLines={1}>
                    {this.formatDisplayDate()}
                </Text>
                <Image source={require('../../assets/images/arrow_down.png')} resizeMode={'contain'} style={[styles.arrow_dark, { right: 8 }]} />
            </TouchableOpacity>
        );
    }
    formatDisplayDate() {
        let fromDate = this.state.fromDate;
        let toDate = this.state.toDate;
        if (fromDate && toDate) {
            return ('0' + fromDate.getDate()).slice(-2) + "/" + ('0' + (fromDate.getMonth() + 1)).slice(-2) + " - " + ('0' + toDate.getDate()).slice(-2) + "/" + ('0' + (toDate.getMonth() + 1)).slice(-2);
        }
        return "";
    }
    openDateRangePicker() {
        this.props.renderModal(
            <Picker
                onDateResult={this.onDateResult.bind(this)}
                setVisibleModal={this.props.setVisibleModal}
                renderModal={this.props.renderModal}
                fromDateValue={this.state.fromDate}
                toDateValue={this.state.toDate}
            />
        );
        this.props.setVisibleModal(true, false);
    }
    onDateResult(rangeDate) {
        this.setState({
            fromDate: rangeDate.fromDate,
            toDate: rangeDate.toDate
        });
        if (this.props.onDateResult)
            this.props.onDateResult(rangeDate)
    }
}
