import React, {
    StyleSheet
} from 'react-native';
import Colors from '../../global/colors'

module.exports = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    },
    select_date_box: {
        position: 'absolute',
        left: 25,
        right: 25,
        backgroundColor: 'white',
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 25,
        paddingBottom: 25,
        borderWidth: 1,
        borderColor: Colors.border,
        borderRadius: 3
    },
    button_date_text: {
        color: Colors.dark,
        fontFamily: "Roboto-Regular",
        fontSize: 14
    },
    arrow_dark: {
        position: 'absolute',
        width: 15,
        height: 15,
        right: 0,
        tintColor: Colors.dark
      },
    button_date_range: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
        backgroundColor: '#F1F2F2',
        paddingLeft: 8,
        flex: 1
    },
    select_date_text: {
        fontSize: 14,
        color: Colors.gray,
        fontFamily: "Roboto-Regular",
        marginTop: 10
    },
    select_date_btn_box: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 25
    },
    select_date_btn_text: {
        fontSize: 14,
        color: Colors.gray,
        fontFamily: "Roboto-Regular"
    },
    select_date_btn: {
        height: 35,
        width: 120,
        backgroundColor: Colors.dark_blue,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    calendar: {
        position: 'absolute',
        width: 20,
        height: 20,
        right: 0,
        bottom: 8,
        tintColor: Colors.dark
    },
    btn_select_date: {
        width: "100%",
        height: 30,
        borderBottomColor: Colors.underline,
        borderBottomWidth: 0.5
    }
});
