export default Object.freeze({
    START_GET_PROVICE: 'START_GET_PROVICE',
    GET_PROVICE_SUCCESS: 'GET_PROVICE_SUCCESS',
    GET_PROVICE_FAIL: 'GET_PROVICE_FAIL',
    DEFAULT_STATE: {
        isLoading: false,
        error: false,
        listProvinceDistrict: []
    }
});