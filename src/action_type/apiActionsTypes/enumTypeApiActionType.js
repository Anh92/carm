export default Object.freeze({
    START_GET_ENUM_TYPE: 'START_GET_ENUM_TYPE',
    GET_ENUM_TYPE_SUCCESS: 'GET_ENUM_TYPE_SUCCESS',
    GET_ENUM_TYPE_FAIL: 'GET_ENUM_TYPE_FAIL',
    DEFAULT_STATE: {
        isLoading: false,
        error: false,
        enumData: {}
    }
});