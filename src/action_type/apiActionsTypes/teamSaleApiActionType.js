export default Object.freeze({
    START_GET_TEAM_SALE: 'START_GET_TEAM_SALE',
    GET_TEAM_SALE_SUCCESS: 'GET_TEAM_SALE_SUCCESS',
    GET_TEAM_SALE_FAIL: 'GET_TEAM_SALE_FAIL',
    DEFAULT_STATE: {
        isLoading: false,
        error: false,
        teamSaleData: []
    }
});