export default Object.freeze({
    START_GET_CAR_BRANDS: 'START_GET_CAR_BRANDS',
    GET_CAR_BRANDS_SUCCESS: 'GET_CAR_BRANDS_SUCCESS',
    GET_CAR_BRANDS_FAIL: 'GET_CAR_BRANDS_FAIL',
    DEFAULT_STATE: {
        isLoading: false,
        error: false,
        carBrandsData: []
    }
});