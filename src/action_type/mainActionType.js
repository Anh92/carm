import Strings from '../global/strings'
import MainRouter from '../views/main/routes'

export default Object.freeze({
    TOGGLE_MENU: 'TOGGLE_MENU',
    CLOSE_MENU: 'CLOSE_MENU',
    OPEN_MENU: 'OPEN_MENU',
    SET_DRAWER_SELECTED_ID: 'SET_DRAWER_SELECTED_ID',
    SET_HEADER_TEXT: 'SET_HEADER_TEXT',
    DEFAULT_STATE: {
        isLoading: false,
        error: false,
        headerText: Strings.home_page,
        notifyNumber: 10,
        currentPage: MainRouter.customers_list,
        drawerSelected: 'home',
        drawerMenuOpen: false,
        isDrawerChanging: false
    }
});