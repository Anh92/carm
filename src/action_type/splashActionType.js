export default  Object.freeze({
    REFRESH_TOKEN_SUCCESS: 'REFRESH_TOKEN_SUCCESS',
    REFRESH_TOKEN_FAIL: 'REFRESH_TOKEN_FAIL',
    DEFAULT_STATE : {
        error: false
    }
});