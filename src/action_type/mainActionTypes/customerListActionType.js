export default Object.freeze({
    START_GET_CUSTOMER_DATA: 'START_GET_CUSTOMER_DATA',
    GET_CUSTOMER_SUCCESS: 'GET_CUSTOMER_SUCCESS',
    GET_CUSTOMER_FAIL: 'GET_CUSTOMER_FAIL',
    DEFAULT_STATE: {
        isLoading: false,
        error: false,
        customerList: []
    }
});