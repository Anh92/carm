export default Object.freeze({
    ADD_CAR_SUCCESS: 'ADD_CAR_SUCCESS',
    ADD_CAR_FAIL: 'ADD_CAR_FAIL',
    DEFAULT_STATE: {
        error: false
    }
});