export default Object.freeze({
    EDIT_CAR_SUCCESS: 'EDIT_CAR_SUCCESS',
    EDIT_CAR_FAIL: 'EDIT_CAR_FAIL',
    DEFAULT_STATE: {
        error: false
    }
});