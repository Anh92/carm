export default Object.freeze({
    START_GET_CAR_LIST: 'START_GET_CAR_LIST',
    GET_CAR_LIST_SUCCESS: 'GET_CAR_LIST_SUCCESS',
    GET_CAR_LIST_FAIL: 'GET_CAR_LIST_FAIL',
    DEFAULT_STATE: {
        isCarListLoading: false,
        loadCarListError: false,
        carList: []
    }
});