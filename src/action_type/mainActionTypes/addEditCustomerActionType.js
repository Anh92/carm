export default Object.freeze({
    ADD_EDIT_SUCCESS: 'ADD_EDIT_SUCCESS',
    ADD_EDIT_FAIL: 'ADD_EDIT_FAIL',
    DEFAULT_STATE: {
        error: false
    }
});