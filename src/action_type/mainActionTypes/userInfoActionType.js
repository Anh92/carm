export default  Object.freeze({
    LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
    LOGOUT_FAIL: 'LOGOUT_FAIL',
    DEFAULT_STATE : {
        error: false
    }
});