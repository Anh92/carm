export default Object.freeze({
    UPDATE_USER_CONTEXT: 'UPDATE_USER_CONTEXT',
    DEFAULT_STATE: {
        roles: [],
        permissions: []
    }
});
