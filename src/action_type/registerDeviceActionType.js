export default  Object.freeze({
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    REGISTER_FAIL: 'REGISTER_FAIL',
    DEFAULT_STATE : {
        error: false
    }
});