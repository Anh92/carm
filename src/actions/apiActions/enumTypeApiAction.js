import EnumTypeApiRepository from '../../network/repository/EnumTypeApiRepository';
import Const from '../../global/const'
import { EnumTypeApiActionType } from '../../action_type'
import Routes from '../../views/routes'

export function startGetData() {
    return {
        type: EnumTypeApiActionType.START_GET_ENUM_TYPE
    };
}

export function getDataSuccess(data) {
    return {
        type: EnumTypeApiActionType.GET_ENUM_TYPE_SUCCESS,
        data: data
    };
}

export function getDataFail() {
    return { type: EnumTypeApiActionType.GET_ENUM_TYPE_FAIL };
}

export function doGetEnumTypeData(setLoading, navigator) {
    return dispatch => {
        dispatch(startGetData());
        if (setLoading) setLoading(true);
        EnumTypeApiRepository.doGetEnumType(null, function (status, responseJson) {
            if (setLoading) setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getDataSuccess(responseJson.data));
                } else {
                    dispatch(getDataFail());
                }
            } else if (status == Const.TOKEN_ERROR) {
                navigator.resetTo(Routes.login);
            } else {
                dispatch(getDataFail());
            }
        });
    };
}
