import TeamSaleApiRepository from '../../network/repository/TeamSaleApiRepository';
import Const from '../../global/const'
import { TeamSaleApiActionType } from '../../action_type'
import Routes from '../../views/routes'

export function startGetData() {
    return {
        type: TeamSaleApiActionType.START_GET_TEAM_SALE
    };
}

export function getDataSuccess(data) {
    return {
        type: TeamSaleApiActionType.GET_TEAM_SALE_SUCCESS,
        data: data
    };
}

export function getDataFail() {
    return { type: TeamSaleApiActionType.GET_TEAM_SALE_FAIL };
}

export function doGetTeamSaleData(setLoading, navigator) {
    return dispatch => {
        console.log("start get team sale data");
        dispatch(startGetData());
        if (setLoading) setLoading(true);
        TeamSaleApiRepository.doGetTeamSale(null, function (status, responseJson) {
            if (setLoading) setLoading(false);
            console.log(responseJson);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getDataSuccess(responseJson.data));
                } else {
                    dispatch(getDataFail());
                }
            } else if (status == Const.TOKEN_ERROR) {
                navigator.resetTo(Routes.login);
            } else {
                dispatch(getDataFail());
            }
        });
    };
}
