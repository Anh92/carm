import CarBrandsApiRepository from '../../network/repository/CarBrandsApiRepository';
import Const from '../../global/const'
import { CarBrandsApiActionType } from '../../action_type'
import Routes from '../../views/routes'

export function startGetCarBrandsData() {
    return {
        type: CarBrandsApiActionType.START_GET_CAR_BRANDS
    };
}

export function getCarBrandsDataSuccess(data) {
    return {
        type: CarBrandsApiActionType.GET_CAR_BRANDS_SUCCESS,
        data: data
    };
}

export function getCarBrandsDataFail() {
    return { type: CarBrandsApiActionType.GET_CAR_BRANDS_FAIL };
}

export function doGetCarBrandsData(setLoading, navigator) {
    return dispatch => {
        dispatch(startGetCarBrandsData());
        if (setLoading) setLoading(true);
        CarBrandsApiRepository.getCarBrands(null, function (status, responseJson) {
            if (setLoading) setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getCarBrandsDataSuccess(responseJson.data));
                } else {
                    dispatch(getCarBrandsDataFail());
                }
            } else if (status == Const.TOKEN_ERROR) {
                navigator.resetTo(Routes.login);
            } else {
                dispatch(getCarBrandsDataFail());
            }
        });
    };
}
