import ProvinceDistrictApiRepository from '../../network/repository/ProvinceDistrictApiRepository';
import Const from '../../global/const'
import { ProvinceDistrictApiActionType } from '../../action_type'
import Routes from '../../views/routes'

export function startGetData() {
    return {
        type: ProvinceDistrictApiActionType.START_GET_PROVICE
    };
}

export function getDataSuccess(data) {
    return {
        type: ProvinceDistrictApiActionType.GET_PROVICE_SUCCESS,
        data: data
    };
}

export function getDataFail() {
    return { type: ProvinceDistrictApiActionType.GET_PROVICE_FAIL };
}

export function doGetProvinceData(setLoading, navigator) {
    return dispatch => {
        dispatch(startGetData());
        if (setLoading) setLoading(true);
        ProvinceDistrictApiRepository.doGetProvince(null, function (status, responseJson) {
            console.log(responseJson);
            if (setLoading) setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getDataSuccess(responseJson.data));
                } else {
                    dispatch(getDataFail());
                }
            } else if (status == Const.TOKEN_ERROR) {
                navigator.resetTo(Routes.login);
            } else {
                dispatch(getDataFail());
            }
        });
    };
}
