import AddEditCustomerRepository from '../../network/repository/AddEditCustomerRepository';
import Const from '../../global/const'
import { AddEditCustomerActionType } from '../../action_type'
import Routes from '../../views/routes'

export function addEditSuccess() {
    return {
        type: AddEditCustomerActionType.ADD_EDIT_SUCCESS
    };
}

export function addEditFail() {
    return { type: AddEditCustomerActionType.ADD_EDIT_FAIL };
}

export function doEdit(data, context) {
    return dispatch => {
        context.setLoading(true);
        console.log(data)
        AddEditCustomerRepository.editCustomer(data, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    context.props.setBack(context.props.setBackId, responseJson.data);
                    context.props.navigator.pop();
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}
export function doUpdateForContract(data, context) {
    return dispatch => {
        context.setLoading(true);
        console.log(data)
        AddEditCustomerRepository.editCustomer(data, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    var page = Routes.create_contract;
                    // page.props = {
                    //   ...page.props,
                    //   customerSubProfileId: this.props.customerData.customerSubProfileId,
                    //   setBackId: setBackId.CUSTOMER_HIS,
                    //   setBack: this.setBack.bind(this)
                    // };
                    context.props.navigator.replace(page);
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}
export function doAdd(data, context) {
    return dispatch => {
        context.setLoading(true);
        console.log(data);
        AddEditCustomerRepository.addCustomer(data, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    context.props.navigator.pop();
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}

export function checkPhoneExist(data, context) {
    return dispatch => {
        context.setLoading(true);
        console.log(data);
        AddEditCustomerRepository.checkPhoneExist(data, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    context.autoFillExistData(responseJson.data);
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}