import TransferCustomerRepository from '../../network/repository/TransferCustomerRepository';
import Const from '../../global/const'
import { TransferCustomerActionType } from '../../action_type'
import Routes from '../../views/routes'

export function addSuccess() {
    return {
        type: TransferCustomerActionType.TRANSFER_CUSTOMER_SUCCESS
    };
}

export function addFail() {
    return { type: TransferCustomerActionType.TRANSFER_CUSTOMER_FAIL };
}

export function doTransferCustomer(data, context) {
    return dispatch => {
        console.log(data);
        context.setLoading(true);
        TransferCustomerRepository.doTransferCustomer(data, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    context.props.setBack(context.props.setBackId, null);
                    context.props.navigator.pop();
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}