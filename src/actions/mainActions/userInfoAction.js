import UserInfoRepository from '../../network/repository/UserInfoRepository';
import Const from '../../global/const'
import { UserInfoActionType } from '../../action_type'
import routes from '../../views/routes'
import Utils from '../../global/utils'

export function loginSuccess() {
    return {
        type: UserInfoActionType.LOGOUT_SUCCESS
    };
}

export function loginFail() {
    return { type: UserInfoActionType.LOGOUT_FAIL };
}

export function doLogout(context) {
    return dispatch => {
        context.props.setLoading(true);
        UserInfoRepository.doLogout(null, function (status, responseJson) {
            context.props.setLoading(false);
            Utils.setValueByKey(Const.ACCESS_TOKEN_STORE_ID, '');
            Utils.setValueByKey(Const.REFRESH_TOKEN_STORE_ID, '');
            Utils.getValueByKey(Const.LOGIN_INPUT_STORE_ID).then((result) => {
                if (result !== '' && result !== undefined && result !== null) {
                    Utils.setValueByKey(Const.LOGIN_INPUT_STORE_ID, JSON.stringify({ username: result.username, password: '' }));
                }
                context.props.parentNavigator.replace(routes.login);
            });

        });
    };
}
