import CarInfoRepository from '../../network/repository/CarInfoRepository';
import Const from '../../global/const'
import { CarInfoActionType } from '../../action_type'
import Routes from '../../views/routes'

export function startGetCarInfo(type) {
    return {
        type: type
    };
}

export function getCarInfouccess(type, data) {
    return {
        type: type,
        data: data
    };
}

export function getCarInfoFail(type) {
    return {
        type: type
    };
}

export function doGetSalesSupport(id, context) {
    return dispatch => {
        dispatch(startGetCarInfo(CarInfoActionType.START_GET_SALES_SUPPORT));
        CarInfoRepository.getSalesSupport({
            customerSubProfileCarInfoId: id
        }, function (status, responseJson) {
            console.log(responseJson);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getCarInfouccess(CarInfoActionType.GET_SALES_SUPPORT_SUCCESS, responseJson.data));
                    context.setUpSalesSupportData(responseJson.data);
                } else {
                    dispatch(getCarInfoFail(CarInfoActionType.GET_SALES_SUPPORT_FAIL));
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                dispatch(getCarInfoFail(CarInfoActionType.GET_SALES_SUPPORT_FAIL));
            }
        });
    };
}

export function doUpdateSalesSupport(data, context) {
    return dispatch => {
        context.setLoading(true);
        CarInfoRepository.updateSalesSupport(data, function (status, responseJson) {
            context.setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    // dispatch(getSalesSupportSuccess(responseJson.data));
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}

export function doGetCompany(id, context) {
    return dispatch => {
        dispatch(startGetCarInfo(CarInfoActionType.START_GET_COMPANY));
        CarInfoRepository.getCompany({
            customerSubProfileCarInfoId: id
        }, function (status, responseJson) {
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getCarInfouccess(CarInfoActionType.GET_COMPANY_SUCCESS, responseJson.data ? responseJson.data : {}));
                    context.setUpdateCompanyData(responseJson.data ? responseJson.data : {});
                } else {
                    dispatch(getCarInfoFail(CarInfoActionType.GET_COMPANY_FAIL));
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                dispatch(getCarInfoFail(CarInfoActionType.GET_COMPANY_FAIL));
            }
        });
    };
}

export function doUpdateCompany(data, context) {
    return dispatch => {
        context.setLoading(true);
        CarInfoRepository.updateCompany(data, function (status, responseJson) {
            context.setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    context.setUpdateCompanyData(responseJson.data ? responseJson.data : {});
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}

export function doGetBroker(id, context) {
    return dispatch => {
        dispatch(startGetCarInfo(CarInfoActionType.START_GET_BROKER));
        CarInfoRepository.getBoker({
            customerSubProfileCarInfoId: id
        }, function (status, responseJson) {
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getCarInfouccess(CarInfoActionType.GET_BROKER_SUCCESS, responseJson.data ? responseJson.data : {}));
                    context.setUpdateBrokerData(responseJson.data ? responseJson.data : {});
                } else {
                    dispatch(getCarInfoFail(CarInfoActionType.GET_BROKER_FAIL));
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                dispatch(getCarInfoFail(CarInfoActionType.GET_BROKER_FAIL));
            }
        });
    };
}

export function doUpdateBroker(data, context) {
    return dispatch => {
        context.setLoading(true);
        CarInfoRepository.updateBroker(data, function (status, responseJson) {
            context.setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    context.setUpdateBrokerData(responseJson.data ? responseJson.data : {});
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}

export function doUpdateCarMaintenance(data, context) {
    return dispatch => {
        context.setLoading(true);
        CarInfoRepository.updateCarMaintenance(data, function (status, responseJson) {
            context.setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    // context.setUpdateBrokerData(responseJson.data ? responseJson.data : {});
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}

export function doGetPersonContact(id, context) {
    return dispatch => {
        dispatch(startGetCarInfo(CarInfoActionType.START_GET_PERSON_CONTACT));
        CarInfoRepository.getPersonContact({
            carInfoId: id
        }, function (status, responseJson) {
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getCarInfouccess(CarInfoActionType.GET_PERSON_CONTACT_SUCCESS, responseJson.data ? responseJson.data : []));
                } else {
                    dispatch(getCarInfoFail(CarInfoActionType.GET_PERSON_CONTACT_FAIL));
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                dispatch(getCarInfoFail(CarInfoActionType.GET_PERSON_CONTACT_FAIL));
            }
        });
    };
}

export function doUpdatePersonContact(data, context) {
    return dispatch => {
        context.setLoading(true);
        CarInfoRepository.updatePersonContact(data, function (status, responseJson) {
            context.setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getCarInfouccess(CarInfoActionType.GET_PERSON_CONTACT_SUCCESS, responseJson.data ? responseJson.data : []));
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}