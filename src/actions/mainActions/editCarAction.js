import EditCarRepository from '../../network/repository/EditCarRepository';
import Const from '../../global/const'
import { EditCarActionType } from '../../action_type'
import Routes from '../../views/routes'

export function editSuccess() {
    return {
        type: EditCarActionType.EDIT_CAR_SUCCESS
    };
}

export function editFail() {
    return { type: EditCarActionType.EDIT_CAR_FAIL };
}

export function doEditCar(data, context) {
    return dispatch => {
        console.log(data);
        context.setLoading(true);
        EditCarRepository.editCar(data, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    context.props.setBack(responseJson.data);
                    context.props.navigator.pop();
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}