import CustomerInfoRepository from '../../network/repository/CustomerInfoRepository';
import Const from '../../global/const'
import { CustomerInfoActionType } from '../../action_type'
import Routes from '../../views/routes'

export function startGetCarList() {
    return {
        type: CustomerInfoActionType.START_GET_CAR_LIST
    };
}

export function getCarListSuccess(data) {
    return {
        type: CustomerInfoActionType.GET_CAR_LIST_SUCCESS,
        data: data
    };
}

export function getCarListFail() {
    return { type: CustomerInfoActionType.GET_CAR_LIST_FAIL };
}

export function doGetCarList(customerId, context) {
    return dispatch => {
        dispatch(startGetCarList());
        CustomerInfoRepository.doGetCarList({
            customerCode: customerId
        }, function (status, responseJson) {
            console.log(responseJson)
            // context.setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getCarListSuccess(responseJson.data));
                } else {
                    dispatch(getCarListFail());
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.props.navigator.resetTo(Routes.login);
            } else {
                dispatch(getCarListFail());
            }
        });
    };
}

export function doCloseCar(data, context) {
    return dispatch => {
        context.setLoading(true);
        console.log(data);
        CustomerInfoRepository.doCloseCar(data, function (status, responseJson) {
            console.log(responseJson);
            context.setLoading(false);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(doGetCarList(context.props.customerData.code, context));
                } else {
                    context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.props.navigator.resetTo(Routes.login);
            } else {
                context.renderPopup(null, 'Không thành công', 'Có lỗi xảy ra! Vui lòng thử lại');
            }
        });
    };
}
