import CustomerListRepository from '../../network/repository/CustomerListRepository';
import Const from '../../global/const'
import { CustomerListActionType } from '../../action_type'
import Routes from '../../views/routes'

export function startData() {
    return {
        type: CustomerListActionType.START_GET_CUSTOMER_DATA
    };
}

export function getDataSuccess(data) {
    return {
        type: CustomerListActionType.GET_CUSTOMER_SUCCESS,
        data: data
    };
}

export function getDataFail() {
    return { type: CustomerListActionType.GET_CUSTOMER_FAIL };
}

export function doGetData(type, context) {
    return dispatch => {
        dispatch(startData());
        // context.props.setLoading(true);
        CustomerListRepository.doGeCustomerList({
            type: type
        }, function (status, responseJson) {
            console.log(responseJson);
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    dispatch(getDataSuccess(responseJson.data));
                } else {
                    dispatch(getDataFail());
                }
            } else if (status == Const.TOKEN_ERROR) {
                context.props.parentNavigator.resetTo(Routes.login);
            } else {
                dispatch(getDataFail());
            }
        });
    };
}
