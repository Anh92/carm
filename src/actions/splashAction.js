import { UserContextActionType } from '../action_type'
import SplashRepository from '../network/repository/SplashRepository';
import routes from '../views/routes'
import Const from '../global/const'
import Utils from '../global/utils'

export function updateUserContext(token) {
    try {
        var userContext = JSON.parse(Utils.b64DecodeUnicode(token.split(".")[1])).userContext;
        return {
            type: UserContextActionType.UPDATE_USER_CONTEXT,
            data: userContext
        };
    } catch (err) {
    }
}

export function refreshToken(refreshToken, context) {
    return dispatch => {
        context.setLoading(true);
        SplashRepository.doRefresh({
            Authorization: 'Bearer ' + refreshToken,
        }, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    Utils.setValueByKey(Const.ACCESS_TOKEN_STORE_ID, JSON.stringify(responseJson.data.access_token));
                    Utils.setValueByKey(Const.REFRESH_TOKEN_STORE_ID, JSON.stringify(responseJson.data.refresh_token));
                    dispatch(updateUserContext(responseJson.data.access_token));
                    context.props.navigator.replace(routes.main);
                } else {
                    context.props.navigator.replace(routes.login);
                }
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}
