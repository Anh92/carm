import LoginRepository from '../network/repository/LoginRepository';
import { UserContextActionType } from '../action_type'
import Const from "../global/const"
import { LoginActionType } from '../action_type'
import routes from '../views/routes'
import Utils from '../global/utils'

export function loginSuccess() {
    return {
        type: LoginActionType.LOGIN_SUCCESS
    };
}

export function loginFail() {
    return { type: LoginActionType.LOGIN_FAIL };
}
export function updateUserContext(token) {
    try {
        var userContext = JSON.parse(Utils.b64DecodeUnicode(token.split(".")[1])).userContext;
        return {
            type: UserContextActionType.UPDATE_USER_CONTEXT,
            data: userContext
        };
    } catch (err) {
    }
}

export function doLogin(username, password, context) {
    return dispatch => {
        context.setLoading(true);
        LoginRepository.doLogin({
            username: username,
            password: password,
            rememberMe: true
        }, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.code == Const.API_CODE_OK) {
                    Utils.setValueByKey(Const.ACCESS_TOKEN_STORE_ID, JSON.stringify(responseJson.data.access_token));
                    Utils.setValueByKey(Const.REFRESH_TOKEN_STORE_ID, JSON.stringify(responseJson.data.refresh_token));
                    Utils.setValueByKey(Const.LOGIN_INPUT_STORE_ID, JSON.stringify({ username, password }));
                    dispatch(updateUserContext(responseJson.data.access_token));
                    context.props.navigator.replace(routes.main);
                } else {
                    context.renderPopup(null, 'Đăng nhập không thành công', 'Tên đăng nhập hoặc mật khẩu không chính xác! Vui lòng thử lại');
                }
            } else {
                context.renderPopup(null, Const.NETOWRK_ERROR_TITLE, Const.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}
