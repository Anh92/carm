import RegisterDeviceRepository from '../network/repository/RegisterDeviceRepository';
import CONST from '../global/const'
import { Platform } from 'react-native';
import { RegisterDeviceActionType } from '../action_type'
import routes from '../views/routes'
import Utils from '../global/utils'

export function registerSuccess() {
    return {
        type: RegisterDeviceActionType.REGISTER_SUCCESS
    };
}

export function registerFail() {
    return { type: RegisterDeviceActionType.REGISTER_FAIL };
}

export function doRegister(username, deviceToken, context) {
    return dispatch => {
        context.setLoading(true);
        RegisterDeviceRepository.doRegisterDevice({
            username: username,
            devicePlatform: (Platform.OS == 'ios') ? "IOS" : "ANDROID",
            deviceToken: deviceToken
        }, function (status, responseJson) {
            console.log(responseJson)
            context.setLoading(false);
            if (status == CONST.SUCCESS) {
                if (responseJson.code == CONST.API_CODE_OK) {
                    Utils.setValueByKey(CONST.DEVICE_TOKEN_STORE_ID, JSON.stringify(responseJson.data.deviceToken));
                    Utils.setValueByKey(CONST.LOGIN_INPUT_STORE_ID, JSON.stringify({username}));
                    context.props.navigator.replace(routes.login);
                } else {
                    context.renderPopup(null, CONST.NETOWRK_ERROR_TITLE, CONST.NETOWRK_ERROR_MESSAGE);
                }
            } else {
                context.renderPopup(null, CONST.NETOWRK_ERROR_TITLE, CONST.NETOWRK_ERROR_MESSAGE);
            }
        });
    };
}
