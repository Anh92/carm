import { MainActionType } from '../action_type'

export function toggleMenu() {
    return { type: MainActionType.TOGGLE_MENU };
}

export function openMenu() {
    return { type: MainActionType.OPEN_MENU };
}

export function closeMenu() {
    return { type: MainActionType.CLOSE_MENU };
}

export function setHeaderText(value) {
    return { type: MainActionType.SET_HEADER_TEXT, value };
}

export function setDrawerSelectedId(id) {
    return { type: MainActionType.SET_DRAWER_SELECTED_ID, id };
}